if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"Polish Forces W", // Name of the faction
		west, // The side of the faction
		"psz_common", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		[], // Scouts and unarmed light vehicles
		[], // Armed light vehicles
		["PSZ_PL_Star_944"], // Dedicated troop trucks
		["PSZ_PL_Zbik_M96_W","PSZ_PL_Zbik_M97_W"], // Armed + Armored Troop Transports
		[], // Unarmed helicopters
		[], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["PSZ_PL_WDL10_Soldier_SL","PSZ_PL_WDL10_Soldier_MG","PSZ_PL_WDL10_Soldier_Ammo_PKM","PSZ_PL_WDL10_Soldier_Rifleman","PSZ_PL_WDL10_Soldier_TL","PSZ_PL_WDL10_Soldier_MG","PSZ_PL_WDL10_Soldier_Ammo_PKM","PSZ_PL_WDL10_Soldier_Rifleman"],
			["PSZ_PL_WDL10_Soldier_SL","PSZ_PL_WDL10_Soldier_MG","PSZ_PL_WDL10_Soldier_Ammo_PKM","PSZ_PL_WDL10_Soldier_Rifleman","PSZ_PL_WDL10_Soldier_TL","PSZ_PL_WDL10_Soldier_Grenadier_RPG7","PSZ_PL_WDL10_Soldier_Ammo_RPG7","PSZ_PL_WDL10_Soldier_Rifleman"]
		]
	];
