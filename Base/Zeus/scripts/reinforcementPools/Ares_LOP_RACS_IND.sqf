if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"RACS LOP IND", // Name of the faction
		independent, // The side of the faction
		"lop_faction_racs", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["LOP_RACS_Landrover"], // Scouts and unarmed light vehicles
		["LOP_RACS_Landrover_M2"], // Armed light vehicles
		["LOP_RACS_Truck"], // Dedicated troop trucks
		["LOP_RACS_M113_W"], // Armed + Armored Troop Transports
		["LOP_RACS_MH9"], // Unarmed helicopters
		["LOP_RACS_UH60M"], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["LOP_RACS_Infantry_SL", "LOP_RACS_Infantry_MG","LOP_RACS_Infantry_MG_Asst", "LOP_RACS_Infantry_Rifleman_2", "LOP_RACS_Infantry_TL", "LOP_RACS_Infantry_MG","LOP_RACS_Infantry_MG_Asst", "LOP_RACS_Infantry_Rifleman_2"],
			["LOP_RACS_Infantry_SL", "LOP_RACS_Infantry_MG","LOP_RACS_Infantry_MG_Asst", "LOP_RACS_Infantry_Rifleman_2", "LOP_RACS_Infantry_TL", "LOP_RACS_Infantry_AT","LOP_RACS_Infantry_Rifleman_2", "LOP_RACS_Infantry_Rifleman_2"]
		]
	];