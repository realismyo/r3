if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"BAF D", // Name of the faction
		west, // The side of the faction
		"CUP_Creatures_Military_BAF", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["CUP_B_LR_Transport_GB_D"], // Scouts and unarmed light vehicles
		["CUP_B_BAF_Coyote_L2A1_D","CUP_B_Jackal2_L2A1_GB_D","CUP_B_LR_Special_M2_GB_D","CUP_B_Mastiff_HMG_GB_D","CUP_B_Ridgback_HMG_GB_D"], // Armed light vehicles
		[], // Dedicated troop trucks
		["CUP_B_FV432_Bulldog_GB_D_RWS","CUP_B_FV432_Bulldog_GB_D"], // Armed + Armored Troop Transports
		["CUP_B_CH47F_GB","CUP_B_AW159_Unarmed_GB"], // Unarmed helicopters
		[], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["CUP_B_BAF_Soldier_SL_DDPM","CUP_B_BAF_Soldier_AR_DDPM","CUP_B_BAF_Soldier_AAR_DDPM","CUP_B_BAF_Soldier_DDPM","CUP_B_BAF_Soldier_TL_DDPM","CUP_B_BAF_Soldier_AR_DDPM","CUP_B_BAF_Soldier_AAR_DDPM","CUP_B_BAF_Soldier_DDPM"],
			["CUP_B_BAF_Soldier_SL_DDPM","CUP_B_BAF_Soldier_AR_DDPM","CUP_B_BAF_Soldier_AAR_DDPM","CUP_B_BAF_Soldier_DDPM","CUP_B_BAF_Soldier_TL_DDPM","CUP_B_BAF_Soldier_AT_DDPM","CUP_B_BAF_Soldier_AAT_DDPM","CUP_B_BAF_Soldier_DDPM"]
		]
	];
