if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"USMC W", // Name of the faction
		west, // The side of the faction
		"rhsusf_main", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["rhsusf_m998_w_s_4dr_fulltop","rhsusf_m998_w_s_4dr","rhsusf_m998_w_s_4dr_halftop","rhsusf_m998_w_s_2dr_fulltop","rhsusf_m998_w_s_2dr","rhsusf_m998_w_s_2dr_halftop","rhsusf_m1025_w_s"], // Scouts and unarmed light vehicles
		["rhsusf_m1025_w_s_m2","rhsusf_rg33_m2_usmc_wd"], // Armed light vehicles
		["rhsusf_M1083A1P2_B_wd_open_fmtv_usarmy","rhsusf_M1083A1P2_B_wd_fmtv_usarmy"], // Dedicated troop trucks
		[], // Armed + Armored Troop Transports
		["RHS_UH1Y_UNARMED","rhsusf_CH53E_USMC"], // Unarmed helicopters
		[], // Armed helicopters
		[], // Unarmed boats
		["rhsusf_mkvsoc"], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["rhsusf_usmc_marpat_wd_squadleader","rhsusf_usmc_marpat_wd_autorifleman_m249","rhsusf_usmc_marpat_wd_autorifleman_m249_ass","rhsusf_usmc_marpat_wd_grenadier","rhsusf_usmc_marpat_wd_teamleader","rhsusf_usmc_marpat_wd_autorifleman_m249","rhsusf_usmc_marpat_wd_autorifleman_m249_ass","rhsusf_usmc_marpat_wd_rifleman"],
			["rhsusf_usmc_marpat_wd_squadleader","rhsusf_usmc_marpat_wd_autorifleman_m249","rhsusf_usmc_marpat_wd_autorifleman_m249_ass","rhsusf_usmc_marpat_wd_grenadier","rhsusf_usmc_marpat_wd_teamleader","rhsusf_usmc_marpat_wd_riflemanat","rhsusf_usmc_marpat_wd_rifleman","rhsusf_usmc_marpat_wd_rifleman"]
		]
	];
