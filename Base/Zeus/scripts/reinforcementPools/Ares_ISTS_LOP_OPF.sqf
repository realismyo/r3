if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"Islamic State LOP OPF", // Name of the faction
		east, // The side of the faction
		"lop_faction_ists", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["LOP_ISTS_Landrover","LOP_ISTS_M1025_D"], // Scouts and unarmed light vehicles
		["LOP_ISTS_Landrover_M2","LOP_ISTS_M1025_W_M2"], // Armed light vehicles
		["LOP_ISTS_Truck"], // Dedicated troop trucks
		["LOP_ISTS_BTR60","LOP_ISTS_M113_W"], // Armed + Armored Troop Transports
		[], // Unarmed helicopters
		[], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["LOP_ISTS_Infantry_SL","LOP_ISTS_Infantry_AR","LOP_ISTS_Infantry_AR_Ass","LOP_ISTS_Infantry_Rifleman","LOP_ISTS_Infantry_SL","LOP_ISTS_Infantry_AR","LOP_ISTS_Infantry_AR_Ass","LOP_ISTS_Infantry_Rifleman_2"],
			["LOP_ISTS_Infantry_SL","LOP_ISTS_Infantry_AR","LOP_ISTS_Infantry_AR_Ass","LOP_ISTS_Infantry_Rifleman","LOP_ISTS_Infantry_SL","LOP_ISTS_Infantry_AT","LOP_ISTS_Infantry_Rifleman","LOP_ISTS_Infantry_Rifleman"]
		]
	];
