if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"USA Army D", // Name of the faction
		west, // The side of the faction
		"rhsusf_main", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["rhsusf_m998_d_4dr","rhsusf_m998_d_4dr_halftop","rhsusf_m998_d_4dr_fulltop","rhsusf_m998_d_2dr","rhsusf_m998_d_2dr_halftop","rhsusf_m998_d_2dr_fulltop","rhsusf_m1025_d"], // Scouts and unarmed light vehicles
		["rhsusf_m1025_d_m2","rhsusf_M1078A1P2_B_M2_d_fmtv_usarmy","rhsusf_M1083A1P2_B_M2_d_fmtv_usarmy"], // Armed light vehicles
		["rhsusf_M1078A1P2_d_fmtv_usarmy","rhsusf_M1078A1P2_d_open_fmtv_usarmy","rhsusf_M1083A1P2_d_fmtv_usarmy","rhsusf_M1083A1P2_d_open_fmtv_usarmy","rhsusf_M1083A1P2_B_d_fmtv_usarmy","rhsusf_M1083A1P2_B_d_open_fmtv_usarmy"], // Dedicated troop trucks
		["rhsusf_m113d_usarmy","rhsusf_m113d_usarmy_M240","rhsusf_M1117_D"], // Armed + Armored Troop Transports
		["RHS_MELB_MH6M","RHS_CH_47F_10","RHS_UH60M_MEV2_d"], // Unarmed helicopters
		["RHS_UH60M_d"], // Armed helicopters
		[], // Unarmed boats
		["rhsusf_mkvsoc"], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["rhsusf_army_ucp_squadleader","rhsusf_army_ucp_autorifleman","rhsusf_army_ucp_autoriflemana","rhsusf_army_ucp_grenadier","rhsusf_army_ucp_teamleader","rhsusf_army_ucp_autorifleman","rhsusf_army_ucp_autoriflemana","rhsusf_army_ucp_rifleman"],
			["rhsusf_army_ucp_squadleader","rhsusf_army_ucp_autorifleman","rhsusf_army_ucp_autoriflemana","rhsusf_army_ucp_grenadier","rhsusf_army_ucp_teamleader","rhsusf_army_ucp_riflemanat","rhsusf_army_ucp_rifleman","rhsusf_army_ucp_rifleman"]
		]
	];
