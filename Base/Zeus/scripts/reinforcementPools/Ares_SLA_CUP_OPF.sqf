if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"Sahrani Liberation Forces CUP OPF", // Name of the faction
		east, // The side of the faction
		"CUP_Creatures_Military_SLA", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["CUP_O_UAZ_Unarmed_SLA","CUP_O_UAZ_Open_SLA"], // Scouts and unarmed light vehicles
		["CUP_O_UAZ_MG_SLA","CUP_O_BRDM2_HQ_SLA"], // Armed light vehicles
		["CUP_O_Ural_Open_SLA","CUP_O_Ural_SLA"], // Dedicated troop trucks
		["CUP_O_BTR60_SLA","CUP_O_BRDM2_SLA"], // Armed + Armored Troop Transports
		["CUP_O_Mi8_SLA_1","CUP_O_UH1H_SLA"], // Unarmed helicopters
		["CUP_O_Mi8_SLA_2"], // Armed helicopters
		["CUP_O_PBX_SLA"], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["CUP_O_sla_Soldier_SL","CUP_O_sla_Soldier_AR","CUP_O_SLA_Soldier_Backpack","CUP_O_sla_Soldier","CUP_O_sla_Soldier_SL","CUP_O_sla_Soldier_MG","CUP_O_sla_Soldier_AMG","CUP_O_sla_Soldier_LAT"],
			["CUP_O_sla_Soldier_SL","CUP_O_sla_Soldier_AR","CUP_O_SLA_Soldier_Backpack","CUP_O_sla_Soldier","CUP_O_sla_Soldier_SL","CUP_O_sla_Soldier_AT","CUP_O_sla_Soldier","CUP_O_SLA_Soldier_Backpack"]
		]
	];
