if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"CUP RACS IND", // Name of the faction
		independent, // The side of the faction
		"CUP_Creatures_Military_RACS", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["CUP_I_LR_Transport_RACS"], // Scouts and unarmed light vehicles
		["CUP_I_LR_MG_RACS"], // Armed light vehicles
		[], // Dedicated troop trucks
		["CUP_I_M113_RACS"], // Armed + Armored Troop Transports
		["CUP_I_MH6J_RACS","CUP_I_SA330_Puma_HC2_RACS","CUP_I_UH60L_Unarmed_RACS"], // Unarmed helicopters
		["CUP_I_UH60L_RACS"], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["CUP_I_RACS_SL", "CUP_I_RACS_MMG","CUP_I_RACS_Soldier", "CUP_I_RACS_Soldier", "CUP_I_RACS_SL", "CUP_I_RACS_MMG","CUP_I_RACS_Soldier","CUP_I_RACS_Medic"],
			["CUP_I_RACS_SL", "CUP_I_RACS_MMG","CUP_I_RACS_Soldier", "CUP_I_RACS_Soldier", "CUP_I_RACS_SL", "CUP_I_RACS_Soldier_MAT","CUP_I_RACS_Medic","CUP_I_RACS_Soldier"]
		]
	];
