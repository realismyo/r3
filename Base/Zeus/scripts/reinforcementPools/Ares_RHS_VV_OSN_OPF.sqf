if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"Russian VV-OSN RHS OPF", // Name of the faction
		east, // The side of the faction
		"rhs_main", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["rhs_tigr_vv","rhs_tigr_m_vv","rhs_uaz_vv","rhs_uaz_open_vv"], // Scouts and unarmed light vehicles
		["rhs_tigr_sts_vv"], // Armed light vehicles
		["RHS_Ural_VV_01","RHS_Ural_Open_VV_01"], // Dedicated troop trucks
		["rhs_btr60_vv"], // Armed + Armored Troop Transports
		["RHS_Mi8mt_vdv","RHS_Mi8mt_vdv"], // Unarmed helicopters
		["RHS_Mi8MTV3_FAB_vdv","RHS_Mi8MTV3_vdv"], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["rhs_mvd_izlom_sergeant","rhs_mvd_izlom_rifleman","rhs_mvd_izlom_machinegunner","rhs_mvd_izlom_marksman","rhs_mvd_izlom_sergeant","rhs_mvd_izlom_grenadier_rpg","rhs_mvd_izlom_machinegunner","rhs_mvd_izlom_rifleman"],
			["rhs_mvd_izlom_sergeant","rhs_mvd_izlom_rifleman","rhs_mvd_izlom_machinegunner","rhs_mvd_izlom_marksman","rhs_mvd_izlom_sergeant","rhs_mvd_izlom_grenadier_rpg","rhs_mvd_izlom_rifleman_LAT","rhs_mvd_izlom_rifleman"]
		]
	];
