if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"Russian VDV-EMR Desert RHS OPF", // Name of the faction
		east, // The side of the faction
		"rhs_main", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["rhs_tigr_3camo_vdv","rhs_tigr_m_3camo_vdv","rhsgref_BRDM2UM_vdv"], // Scouts and unarmed light vehicles
		["rhsgref_BRDM2_HQ_vdv","rhs_tigr_sts_3camo_vdv"], // Armed light vehicles
		["rhs_gaz66_vdv","rhs_gaz66o_vdv","RHS_Ural_vdv_01","RHS_Ural_Open_vdv_01"], // Dedicated troop trucks
		["rhs_btr70_vdv","rhs_btr80_vdv"], // Armed + Armored Troop Transports
		["RHS_Mi8mt_vdv","RHS_Mi8mt_vdv"], // Unarmed helicopters
		["RHS_Mi8MTV3_FAB_vdv","RHS_Mi8MTV3_vdv"], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["rhs_vdv_des_sergeant","rhs_vdv_des_rifleman","rhs_vdv_des_machinegunner","rhs_vdv_des_machinegunner_assistant","rhs_vdv_des_sergeant","rhs_vdv_des_RShG2","rhs_vdv_des_machinegunner","rhs_vdv_des_machinegunner_assistant"],
			["rhs_vdv_des_sergeant","rhs_vdv_des_rifleman","rhs_vdv_des_arifleman","rhs_vdv_des_machinegunner_assistant","rhs_vdv_des_sergeant","rhs_vdv_des_RShG2","rhs_vdv_des_at","rhs_vdv_des_rifleman"]
		]
	];
