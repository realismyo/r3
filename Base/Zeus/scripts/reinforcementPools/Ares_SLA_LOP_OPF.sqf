if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"Sahrani Liberation Forces LOP OPF", // Name of the faction
		east, // The side of the faction
		"lop_faction_sla", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["LOP_SLA_UAZ","LOP_SLA_UAZ_Open"], // Scouts and unarmed light vehicles
		["LOP_SLA_UAZ_DshKM"], // Armed light vehicles
		["LOP_SLA_Ural","LOP_SLA_Ural_open"], // Dedicated troop trucks
		["LOP_SLA_BTR60","LOP_SLA_BTR70"], // Armed + Armored Troop Transports
		["LOP_SLA_Mi8MT_Cargo"], // Unarmed helicopters
		["LOP_SLA_Mi8MTV3_FAB"], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["LOP_SLA_Infantry_SL","LOP_SLA_Infantry_MG","LOP_SLA_Infantry_MG_Asst","LOP_SLA_Infantry_Rifleman","LOP_SLA_Infantry_SL","LOP_SLA_Infantry_MG","LOP_SLA_Infantry_MG_Asst","LOP_SLA_Infantry_GL"],
			["LOP_SLA_Infantry_SL","LOP_SLA_Infantry_MG","LOP_SLA_Infantry_MG_Asst","LOP_SLA_Infantry_Rifleman","LOP_SLA_Infantry_SL","LOP_SLA_Infantry_AT","LOP_SLA_Infantry_Rifleman","LOP_SLA_Infantry_Rifleman"]
		]
	];
