if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"African Insurgents LOP OPF", // Name of the faction
		east, // The side of the faction
		"lop_faction_afr", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["sub_ins_af_offroad", "sub_ins_af_offroad"], // Scouts and unarmed light vehicles
		["sub_ins_af_offroad_armed","sub_ins_af_offroad_armed"], // Armed light vehicles
		["sub_ins_af_van"], // Dedicated troop trucks
		["sub_ins_af_btr60", "sub_ins_af_btr60"], // Armed + Armored Troop Transports
		[], // Unarmed helicopters
		[], // Armed helicopters
		["sub_ins_af_boat"], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["sub_ins_af_sl", "sub_ins_af_rifleman","sub_ins_af_mg", "sub_ins_af_tl", "sub_ins_af_rifleman", "sub_ins_af_gl"],
			["sub_ins_af_sl", "sub_ins_af_rifleman","sub_ins_af_lat", "sub_ins_af_tl", "sub_ins_af_rifleman", "sub_ins_af_gl"]
		]
	];