if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"Russian MSV-EMR RHS OPF", // Name of the faction
		east, // The side of the faction
		"rhs_main", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["rhs_tigr_msv","rhs_tigr_m_msv","rhsgref_BRDM2UM_msv"], // Scouts and unarmed light vehicles
		["rhsgref_BRDM2_HQ_msv","rhs_tigr_sts_msv"], // Armed light vehicles
		["rhs_gaz66_msv","rhs_gaz66o_msv","RHS_Ural_MSV_01","RHS_Ural_Open_MSV_01"], // Dedicated troop trucks
		["rhs_btr70_msv","rhs_btr80_ms"], // Armed + Armored Troop Transports
		["RHS_Mi8mt_vdv","RHS_Mi8mt_vdv"], // Unarmed helicopters
		["RHS_Mi8MTV3_FAB_vdv","RHS_Mi8MTV3_vdv"], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["rhs_msv_emr_sergeant","rhs_msv_emr_rifleman","rhs_msv_emr_machinegunner","rhs_msv_emr_machinegunner_assistant","rhs_msv_emr_sergeant","rhs_msv_emr_RShG2","rhs_msv_emr_machinegunner","rhs_msv_emr_machinegunner_assistant"],
			["rhs_msv_emr_sergeant","rhs_msv_emr_rifleman","rhs_msv_emr_machinegunner","rhs_msv_emr_machinegunner_assistant","rhs_msv_emr_sergeant","rhs_msv_emr_RShG2","rhs_msv_emr_at","rhs_msv_emr_rifleman"]
		]
	];