if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"National Troops RHS IND", // Name of the faction
		independent, // The side of the faction
		"rhsgref_main", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["rhsgref_nat_uaz","rhsgref_nat_uaz_open"], // Scouts and unarmed light vehicles
		["rhsgref_nat_uaz_dshkm"], // Armed light vehicles
		["rhsgref_nat_ural","rhsgref_nat_ural_open","rhsgref_nat_ural_work","rhsgref_nat_ural_work_open"], // Dedicated troop trucks
		["rhsgref_nat_btr70"], // Armed + Armored Troop Transports
		[], // Unarmed helicopters
		[], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["rhsgref_nat_rifleman_m92","rhsgref_nat_machinegunner","rhsgref_nat_militiaman_kar98k","rhsgref_nat_rifleman_akms","rhsgref_nat_rifleman_m92","rhsgref_nat_machinegunner","rhsgref_nat_rifleman_akms","rhsgref_nat_grenadier"],
			["rhsgref_nat_rifleman_m92","rhsgref_nat_machinegunner","rhsgref_nat_militiaman_kar98k","rhsgref_nat_rifleman_akms","rhsgref_nat_rifleman_m92","rhsgref_nat_grenadier_rpg","rhsgref_nat_grenadier_rpg","rhsgref_nat_rifleman_akm"]
		]
	];
