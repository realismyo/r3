if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"Takistan Armed Forces LOP OPF", // Name of the faction
		east, // The side of the faction
		"lop_faction_tka", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["LOP_TKA_UAZ","LOP_TKA_UAZ_Open"], // Scouts and unarmed light vehicles
		["LOP_TKA_UAZ_DshKM"], // Armed light vehicles
		["LOP_TKA_Ural","LOP_TKA_Ural_open"], // Dedicated troop trucks
		["LOP_TKA_BTR60","LOP_TKA_BTR70"], // Armed + Armored Troop Transports
		["LOP_TKA_Mi8MT_Cargo"], // Unarmed helicopters
		["LOP_TKA_Mi8MTV3_FAB"], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["LOP_TKA_Infantry_SL","LOP_TKA_Infantry_MG","LOP_TKA_Infantry_MG_Ass","LOP_TKA_Infantry_Rifleman_2","LOP_TKA_Infantry_TL","LOP_TKA_Infantry_MG","LOP_TKA_Infantry_MG_Ass","LOP_TKA_Infantry_Rifleman_3"],
			["LOP_TKA_Infantry_SL","LOP_TKA_Infantry_MG","LOP_TKA_Infantry_MG_Ass","LOP_TKA_Infantry_Rifleman_2","LOP_TKA_Infantry_TL","LOP_TKA_Infantry_AT","LOP_TKA_Infantry_Rifleman_2","LOP_TKA_Infantry_Rifleman_2"]
		]
	];
