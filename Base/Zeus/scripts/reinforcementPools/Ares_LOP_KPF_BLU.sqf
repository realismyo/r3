if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"LOP Peshmerga", // Name of the faction
		west, // The side of the faction
		"lop_faction_pesh", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["LOP_PESH_M998_D_4DR","LOP_PESH_M1025_D"], // Scouts and unarmed light vehicles
		["LOP_PESH_M1025_W_M2"], // Armed light vehicles
		["LOP_PESH_HEMTT_Transport_D","LOP_PESH_HEMTT_Covered_D"], // Dedicated troop trucks
		[], // Armed + Armored Troop Transports
		[], // Unarmed helicopters
		[], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["LOP_PESH_Infantry_TL","LOP_PESH_Infantry_MG","LOP_PESH_Infantry_Rifleman","LOP_PESH_Infantry_Rifleman_3","LOP_PESH_Infantry_TL","LOP_PESH_Infantry_MG","LOP_PESH_Infantry_Rifleman","LOP_PESH_Infantry_Rifleman_3"],
			["LOP_PESH_Infantry_TL","LOP_PESH_Infantry_MG","LOP_PESH_Infantry_Rifleman","LOP_PESH_Infantry_Rifleman_3","LOP_PESH_Infantry_TL","LOP_PESH_Infantry_AT","LOP_PESH_Infantry_AT","LOP_PESH_Infantry_Rifleman_3"]
		]
	];
