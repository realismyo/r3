if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"LOP CDF", // Name of the faction
		west, // The side of the faction
		"lop_faction_cdf", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["LOP_CDF_UAZ","LOP_CDF_UAZ_Open"], // Scouts and unarmed light vehicles
		["LOP_CDF_UAZ_DshKM"], // Armed light vehicles
		["LOP_CDF_Ural","LOP_CDF_Ural_open"], // Dedicated troop trucks
		["LOP_CDF_BTR60","LOP_CDF_BTR70"], // Armed + Armored Troop Transports
		["LOP_CDF_Mi8MT_Cargo"], // Unarmed helicopters
		[], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["LOP_CDF_Infantry_SL","LOP_CDF_Infantry_MG","LOP_CDF_Infantry_MG_Asst","LOP_CDF_Infantry_Rifleman","LOP_CDF_Infantry_TL","LOP_CDF_Infantry_MG","LOP_CDF_Infantry_MG_Asst","LOP_CDF_Infantry_Rifleman"],
			["LOP_CDF_Infantry_SL","LOP_CDF_Infantry_MG","LOP_CDF_Infantry_MG_Asst","LOP_CDF_Infantry_Rifleman","LOP_CDF_Infantry_TL","LOP_CDF_Infantry_AT","LOP_CDF_Infantry_Rifleman","LOP_CDF_Infantry_Rifleman"]
		]
	];
