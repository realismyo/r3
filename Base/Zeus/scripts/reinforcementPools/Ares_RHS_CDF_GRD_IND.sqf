if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"CDF Ground RHS IND", // Name of the faction
		independent, // The side of the faction
		"rhsgref_main", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["rhsgref_cdf_reg_uaz", "rhsgref_cdf_reg_uaz_open"], // Scouts and unarmed light vehicles
		["rhsgref_cdf_reg_uaz_dshkm","rhsgref_BRDM2_HQ"], // Armed light vehicles
		["rhsgref_cdf_ural", "rhsgref_cdf_ural_open"], // Dedicated troop trucks
		["rhsgref_cdf_btr60"], // Armed + Armored Troop Transports
		["rhsgref_cdf_reg_Mi8amt"], // Unarmed helicopters
		["rhsgref_cdf_reg_Mi17Sh"], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["rhsgref_cdf_reg_squadleader","rhsgref_cdf_reg_machinegunner","rhsgref_cdf_reg_rifleman","rhsgref_cdf_reg_rifleman","rhsgref_cdf_reg_squadleader","rhsgref_cdf_reg_machinegunner","rhsgref_cdf_reg_rifleman","rhsgref_cdf_reg_grenadier"],
			["rhsgref_cdf_reg_squadleader","rhsgref_cdf_reg_machinegunner","rhsgref_cdf_reg_rifleman","rhsgref_cdf_reg_rifleman","rhsgref_cdf_reg_squadleader","rhsgref_cdf_reg_grenadier_rpg","rhsgref_cdf_reg_rifleman","rhsgref_cdf_reg_rifleman"]
		]
	];
