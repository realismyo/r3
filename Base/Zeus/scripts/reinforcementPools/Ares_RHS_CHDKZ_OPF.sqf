if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"CHDKZ RHS OPF", // Name of the faction
		east, // The side of the faction
		"rhsgref_main", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["rhsgref_ins_uaz", "rhsgref_ins_uaz_open","rhsgref_BRDM2UM_ins"], // Scouts and unarmed light vehicles
		["rhsgref_ins_uaz_dshkm","rhsgref_BRDM2_HQ_ins","rhsgref_BRDM2_ins"], // Armed light vehicles
		["rhsgref_ins_ural", "rhsgref_ins_ural_open", "rhsgref_ins_gaz66", "rhsgref_ins_gaz66o"], // Dedicated troop trucks
		["rhsgref_ins_btr60", "rhsgref_ins_btr70"], // Armed + Armored Troop Transports
		["rhsgref_ins_Mi8amt"], // Unarmed helicopters
		[], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["rhsgref_ins_squadleader", "rhsgref_ins_rifleman_akm","rhsgref_ins_machinegunner", "rhsgref_ins_rifleman", "rhsgref_ins_g_machinegunner", "rhsgref_ins_engineer"],
			["rhsgref_ins_squadleader", "rhsgref_ins_rifleman_akm","rhsgref_ins_rifleman", "rhsgref_ins_rifleman", "rhsgref_ins_rifleman_akm", "rhsgref_ins_grenadier_rpg"]
		]
	];

