if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"CHDKZ RHS IND", // Name of the faction
		independent, // The side of the faction
		"rhsgref_main", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["rhsgref_ins_g_uaz_open", "rhsgref_ins_g_uaz"], // Scouts and unarmed light vehicles
		["rhsgref_ins_g_uaz_dshkm_chdkz"], // Armed light vehicles
		["rhsgref_ins_g_ural_open", "rhsgref_ins_g_gaz66", "rhsgref_ins_g_gaz66o", "rhsgref_ins_g_ural"], // Dedicated troop trucks
		["rhsgref_ins_g_btr60", "rhsgref_ins_g_btr60"], // Armed + Armored Troop Transports
		["rhsgref_ins_g_Mi8amt"], // Unarmed helicopters
		["rhsgref_ins_g_Mi8amt"], // Armed helicopters
		["B_Boat_Transport_01_F"], // Unarmed boats
		["CUP_B_RHIB_USMC"], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["rhsgref_ins_g_rifleman_aksu", "rhsgref_ins_g_rifleman_akm","rhsgref_ins_g_rifleman_aksu", "rhsgref_ins_g_rifleman", "rhsgref_ins_g_machinegunner", "rhsgref_ins_g_engineer"],
			["rhsgref_ins_g_rifleman_aksu", "rhsgref_ins_g_rifleman_akm","rhsgref_ins_g_rifleman_aksu", "rhsgref_ins_g_rifleman_akm", "rhsgref_ins_g_grenadier_rpg", "rhsgref_ins_g_grenadier"]
		]
	];


