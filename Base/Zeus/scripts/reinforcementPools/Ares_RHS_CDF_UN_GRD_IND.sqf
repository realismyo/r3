if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"CDF UN RHS IND", // Name of the faction
		independent, // The side of the faction
		"rhsgref_main", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["rhsgref_un_uaz"], // Scouts and unarmed light vehicles
		["rhsgref_un_m1117"], // Armed light vehicles
		["rhsgref_un_ural"], // Dedicated troop trucks
		["rhsgref_un_btr70"], // Armed + Armored Troop Transports
		["rhsgref_un_Mi8amt"], // Unarmed helicopters
		[], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["rhsgref_cdf_un_squadleader","rhsgref_cdf_un_machinegunner","rhsgref_cdf_un_rifleman","rhsgref_cdf_un_rifleman","rhsgref_cdf_un_squadleader","rhsgref_cdf_un_machinegunner","rhsgref_cdf_un_rifleman","rhsgref_cdf_un_grenadier"],
			["rhsgref_cdf_un_squadleader","rhsgref_cdf_un_machinegunner","rhsgref_cdf_un_rifleman","rhsgref_cdf_un_rifleman","rhsgref_cdf_un_squadleader","rhsgref_cdf_un_grenadier_rpg","rhsgref_cdf_un_rifleman","rhsgref_cdf_un_rifleman"]
		]
	];
