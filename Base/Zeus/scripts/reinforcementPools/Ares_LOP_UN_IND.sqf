if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"UN LOP IND", // Name of the faction
		independent, // The side of the faction
		"lop_faction_un", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["LOP_UN_UAZ","LOP_UN_UAZ_Open"], // Scouts and unarmed light vehicles
		["LOP_UN_UAZ_DshKM"], // Armed light vehicles
		["LOP_UN_Ural","LOP_UN_Ural_open"], // Dedicated troop trucks
		["LOP_UN_BTR70"], // Armed + Armored Troop Transports
		["LOP_UN_Mi8MT_Cargo"], // Unarmed helicopters
		["LOP_UN_Mi8MTV3_UPK23"], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["LOP_UN_Infantry_SL", "LOP_UN_Infantry_MG","LOP_UN_Infantry_MG_Asst", "LOP_UN_Infantry_Rifleman", "LOP_UN_Infantry_TL", "LOP_UN_Infantry_MG","LOP_UN_Infantry_MG_Asst", "LOP_UN_Infantry_Rifleman"],
			["LOP_UN_Infantry_SL", "LOP_UN_Infantry_MG","LOP_UN_Infantry_MG_Asst", "LOP_UN_Infantry_Rifleman", "LOP_UN_Infantry_TL", "LOP_UN_Infantry_AT","LOP_UN_Infantry_Rifleman", "LOP_UN_Infantry_Rifleman"]
		]
	];
