if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"Takistan Armed Forces CUP OPF", // Name of the faction
		east, // The side of the faction
		"CUP_Creatures_Military_Taki", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["CUP_O_LR_Transport_TKA","CUP_O_UAZ_Unarmed_TKA","CUP_O_UAZ_Open_TKA","CUP_O_BTR40_TKA"], // Scouts and unarmed light vehicles
		["CUP_O_BTR40_MG_TKA","CUP_O_BRDM2_HQ_TKA","CUP_O_LR_MG_TKA","CUP_O_UAZ_MG_TKA"], // Armed light vehicles
		["CUP_O_Ural_TKA","CUP_O_Ural_Open_TKA"], // Dedicated troop trucks
		["CUP_O_BRDM2_TKA","CUP_O_M113_TKA","CUP_O_BTR60_TK"], // Armed + Armored Troop Transports
		["CUP_O_Mi17_TK","CUP_O_UH1H_TKA"], // Unarmed helicopters
		[], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["CUP_O_TK_Soldier_SL","CUP_O_TK_Soldier_MG","CUP_O_TK_Soldier_AMG","CUP_O_TK_Soldier","CUP_O_TK_Soldier_SL","CUP_O_TK_Soldier_AR","CUP_O_TK_Soldier","CUP_O_TK_Soldier_LAT"],
			["CUP_O_TK_Soldier_SL","CUP_O_TK_Soldier_AT","CUP_O_TK_Soldier","CUP_O_TK_Soldier","CUP_O_TK_Soldier_SL","CUP_O_TK_Soldier_AR","CUP_O_TK_Soldier","CUP_O_TK_Soldier_LAT"]
		]
	];
