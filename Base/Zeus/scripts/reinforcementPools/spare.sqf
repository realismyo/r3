if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"", // Name of the faction
		east, // The side of the faction
		"", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["","","",""], // Scouts and unarmed light vehicles
		["","","",""], // Armed light vehicles
		["","","",""], // Dedicated troop trucks
		["","","",""], // Armed + Armored Troop Transports
		["","","",""], // Unarmed helicopters
		["","","",""], // Armed helicopters
		["","","",""], // Unarmed boats
		["","","",""], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["","","","","","","",""],
			["","","","","","","",""]
		]
	];