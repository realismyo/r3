if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"CHDKZ CUP OPF", // Name of the faction
		east, // The side of the faction
		"CUP_Creatures_Military_Chedaki", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["CUP_O_UAZ_Unarmed_CHDKZ", "CUP_O_UAZ_Open_CHDKZ"], // Scouts and unarmed light vehicles
		["CUP_O_UAZ_MG_CHDKZ","CUP_O_UAZ_MG_CHDKZ","CUP_O_Datsun_PK_Random","CUP_O_Datsun_PK"], // Armed light vehicles
		["CUP_O_Ural_Open_CHDKZ","CUP_O_Ural_Open_CHDKZ"], // Dedicated troop trucks
		["CUP_O_BRDM2_CHDKZ", "CUP_O_BRDM2_HQ_CHDKZ"], // Armed + Armored Troop Transports
		["CUP_O_Mi8_CHDKZ","CUP_O_Mi8_medevac_CHDKZ"], // Unarmed helicopters
		[], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["CUP_O_INS_Officer", "CUP_O_INS_Soldier","CUP_O_INS_Soldier_MG", "CUP_O_INS_Soldier", "CUP_O_INS_Soldier_AK74", "CUP_O_INS_Soldier_AR"],
			["CUP_O_INS_Officer", "CUP_O_INS_Soldier","CUP_O_INS_Soldier_AT", "CUP_O_INS_Soldier", "CUP_O_INS_Soldier_AK74", "CUP_O_INS_Soldier_Ammo"]
		]
	];
