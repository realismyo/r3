if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"Chernarus Forces", // Name of the faction
		west, // The side of the faction
		"rhsgref_main", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["rhsgref_cdf_b_reg_uaz","rhsgref_cdf_b_reg_uaz_open"], // Scouts and unarmed light vehicles
		["rhsgref_cdf_b_reg_uaz_dshkm","rhsgref_BRDM2_HQ_b"], // Armed light vehicles
		["rhsgref_cdf_b_gaz66","rhsgref_cdf_b_gaz66o","rhsgref_cdf_b_ural","rhsgref_cdf_b_ural_open"], // Dedicated troop trucks
		["rhsgref_cdf_b_btr60","rhsgref_cdf_b_btr70","rhsgref_BRDM2_b"], // Armed + Armored Troop Transports
		["rhsgref_cdf_b_reg_Mi8amt"], // Unarmed helicopters
		["rhsgref_cdf_b_reg_Mi17Sh"], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["rhsgref_cdf_b_reg_squadleader","rhsgref_cdf_b_reg_machinegunner","rhsgref_cdf_b_reg_rifleman","rhsgref_cdf_b_reg_rifleman","rhsgref_cdf_b_reg_squadleader","rhsgref_cdf_b_reg_machinegunner","rhsgref_cdf_b_reg_rifleman","rhsgref_cdf_b_reg_rifleman_m70"],
			["rhsgref_cdf_b_reg_squadleader","rhsgref_cdf_b_reg_machinegunner","rhsgref_cdf_b_reg_rifleman","rhsgref_cdf_b_reg_rifleman","rhsgref_cdf_b_reg_squadleader","rhsgref_cdf_b_reg_grenadier_rpg","rhsgref_cdf_b_reg_rifleman","rhsgref_cdf_b_reg_rifleman_m70"]
		]
	];
