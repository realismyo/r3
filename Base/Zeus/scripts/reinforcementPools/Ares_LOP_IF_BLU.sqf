if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"LOP Iraq Army", // Name of the faction
		west, // The side of the faction
		"lop_faction_ia", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["LOP_IA_M998_D_4DR","LOP_IA_M1025_D"], // Scouts and unarmed light vehicles
		["LOP_IA_M1025_W_M2"], // Armed light vehicles
		["LOP_IA_HEMTT_Transport_D","LOP_IA_HEMTT_Covered_D"], // Dedicated troop trucks
		["LOP_IA_BTR80"], // Armed + Armored Troop Transports
		["LOP_IA_Mi8MT_Cargo"], // Unarmed helicopters
		[], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["LOP_IA_Infantry_SL","LOP_IA_Infantry_MG","LOP_IA_Infantry_MG_Asst","LOP_IA_Infantry_Rifleman","LOP_IA_Infantry_TL","LOP_IA_Infantry_MG","LOP_IA_Infantry_MG_Asst","LOP_IA_Infantry_Rifleman"],
			["LOP_IA_Infantry_SL","LOP_IA_Infantry_MG","LOP_IA_Infantry_MG_Asst","LOP_IA_Infantry_Rifleman","LOP_IA_Infantry_TL","LOP_IA_Infantry_AT","LOP_IA_Infantry_AT_Asst","LOP_IA_Infantry_Rifleman"]
		]
	];
