if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"Russian VMF RHS OPF", // Name of the faction
		east, // The side of the faction
		"rhs_main", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["rhs_tigr_vmf","rhs_tigr_m_vmf"], // Scouts and unarmed light vehicles
		["rhs_tigr_sts_vmf"], // Armed light vehicles
		["rhs_gaz66_vmf","rhs_gaz66o_vmf"], // Dedicated troop trucks
		["rhs_btr80_vmf"], // Armed + Armored Troop Transports
		["rhs_ka60_c"], // Unarmed helicopters
		["RHS_Mi8MTV3_FAB_vdv","RHS_Mi8MTV3_vdv"], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["rhs_vmf_flora_sergeant","rhs_vmf_flora_rifleman","rhs_vmf_flora_machinegunner","rhs_vmf_flora_machinegunner_assistant","rhs_vmf_flora_sergeant","rhs_vmf_flora_RShG2","rhs_vmf_flora_machinegunner","rhs_vmf_flora_machinegunner_assistant"],
			["rhs_vmf_flora_sergeant","rhs_vmf_flora_rifleman","rhs_vmf_flora_machinegunner","rhs_vmf_flora_machinegunner_assistant","rhs_vmf_flora_sergeant","rhs_vmf_flora_RShG2","rhs_vmf_flora_at","rhs_vmf_flora_rifleman"]
		]
	];
