if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"CUP Taki Locals IND", // Name of the faction
		independent, // The side of the faction
		"CUP_Creatures_Military_TakiInsurgents", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["CUP_I_BTR40_TKG"], // Scouts and unarmed light vehicles
		["CUP_I_Datsun_PK_TK","CUP_I_Datsun_PK_TK","CUP_I_Datsun_PK_TK_Random"], // Armed light vehicles
		[], // Dedicated troop trucks
		["CUP_I_BRDM2_TK_Gue","CUP_I_BRDM2_HQ_TK_Gue"], // Armed + Armored Troop Transports
		[], // Unarmed helicopters
		[], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["CUP_I_TK_GUE_Soldier_TL", "CUP_I_TK_GUE_Soldier_AR","CUP_I_TK_GUE_Guerilla_Enfield", "CUP_I_TK_GUE_Soldier", "CUP_I_TK_GUE_Soldier_TL", "CUP_I_TK_GUE_Soldier_MG","CUP_I_TK_GUE_Soldier","CUP_I_TK_GUE_Guerilla_Enfield"],
			["CUP_I_TK_GUE_Soldier_TL", "CUP_I_TK_GUE_Soldier_AR","CUP_I_TK_GUE_Guerilla_Enfield", "CUP_I_TK_GUE_Soldier", "CUP_I_TK_GUE_Soldier_TL", "CUP_I_TK_GUE_Soldier_AT","CUP_I_TK_GUE_Soldier_AT","CUP_I_TK_GUE_Soldier_AAT"]
		]
	];
