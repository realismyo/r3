if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"Novorossiya Forces LOP OPF", // Name of the faction
		east, // The side of the faction
		"lop_faction_us", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["LOP_US_UAZ","LOP_US_UAZ_Open"], // Scouts and unarmed light vehicles
		["LOP_US_UAZ_DshKM"], // Armed light vehicles
		["LOP_US_Ural","LOP_US_Ural_open"], // Dedicated troop trucks
		["LOP_US_BTR60"], // Armed + Armored Troop Transports
		[], // Unarmed helicopters
		[], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["LOP_US_Infantry_SL","LOP_US_Infantry_MG","LOP_US_Infantry_MG_Ass","LOP_US_Infantry_Rifleman","LOP_US_Infantry_TL","LOP_US_Infantry_MG","LOP_US_Infantry_MG_Ass","LOP_US_Infantry_Rifleman_3"],
			["LOP_US_Infantry_SL","LOP_US_Infantry_MG","LOP_US_Infantry_MG_Ass","LOP_US_Infantry_Rifleman","LOP_US_Infantry_TL","LOP_US_Infantry_AT","LOP_US_Infantry_Rifleman","LOP_US_Infantry_Rifleman_4"]
		]
	];

