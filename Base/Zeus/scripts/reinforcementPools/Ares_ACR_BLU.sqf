if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"ACR", // Name of the faction
		west, // The side of the faction
		"acr_a3_main", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["ACR_A3_LandRover"], // Scouts and unarmed light vehicles
		["acr_a3_dingo_wdl","ACR_A3_LandRover_MG"], // Armed light vehicles
		[], // Dedicated troop trucks
		["ACR_A3_BRDM2","ACR_A3_BRDM2_HQ"], // Armed + Armored Troop Transports
		["ACR_A3_Mi17_base_CZ_EP1"], // Unarmed helicopters
		["ACR_A3_Mi171Sh_rockets_CZ_EP1"], // Armed helicopters
		["B_ACR_A3_Boat_Transport_01"], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["B_ACR_A3_Soldier_SL","B_ACR_A3_Soldier_AR","B_ACR_A3_Soldier_01","B_ACR_A3_Soldier_01","B_ACR_A3_Soldier_TL","B_ACR_A3_Soldier_AR","B_ACR_A3_Soldier_01","B_ACR_A3_Soldier_medic"],
			["B_ACR_A3_Soldier_SL","B_ACR_A3_Soldier_AR","B_ACR_A3_Soldier_01","B_ACR_A3_Soldier_01","B_ACR_A3_Soldier_TL","B_ACR_A3_Soldier_MAT","B_ACR_A3_Soldier_01","B_ACR_A3_Soldier_medic"]
		]
	];
