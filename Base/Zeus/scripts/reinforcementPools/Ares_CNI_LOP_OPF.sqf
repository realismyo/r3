if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"Chernarussion National Ins LOP OPF", // Name of the faction
		east, // The side of the faction
		"lop_faction_napa", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["LOP_NAPA_Landrover","LOP_NAPA_Offroad"], // Scouts and unarmed light vehicles
		["LOP_NAPA_Landrover_M2","LOP_NAPA_Offroad_M2"], // Armed light vehicles
		["LOP_NAPA_Truck"], // Dedicated troop trucks
		[], // Armed + Armored Troop Transports
		[], // Unarmed helicopters
		[], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["LOP_NAPA_Infantry_TL","LOP_NAPA_Infantry_MG","LOP_NAPA_Infantry_MG_Ass","LOP_NAPA_Infantry_Rifleman_3","LOP_NAPA_Infantry_TL","LOP_NAPA_Infantry_MG","LOP_NAPA_Infantry_MG_Ass","LOP_NAPA_Infantry_Rifleman_2"],
			["LOP_NAPA_Infantry_TL","LOP_NAPA_Infantry_MG","LOP_NAPA_Infantry_MG_Ass","LOP_NAPA_Infantry_Rifleman_3","LOP_NAPA_Infantry_TL","LOP_NAPA_Infantry_AT","LOP_NAPA_Infantry_Rifleman_3","LOP_NAPA_Infantry_Rifleman_3"]
		]
	];
