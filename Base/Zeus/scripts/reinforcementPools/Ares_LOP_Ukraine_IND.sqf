if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"Ukraine Forces LOP IND", // Name of the faction
		independent, // The side of the faction
		"lop_faction_ukr", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["LOP_UKR_UAZ","LOP_UKR_UAZ_Open"], // Scouts and unarmed light vehicles
		["LOP_UKR_UAZ_DshKM"], // Armed light vehicles
		["LOP_UKR_KAMAZ_Transport","LOP_UKR_KAMAZ_Covered"], // Dedicated troop trucks
		["LOP_UKR_BTR80","LOP_UKR_BTR70"], // Armed + Armored Troop Transports
		["LOP_UKR_Mi8MT_Cargo"], // Unarmed helicopters
		["LOP_UKR_Mi8MTV3_FAB","LOP_UKR_Mi8MTV3_UPK23"], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["LOP_UKR_Infantry_sergeant", "LOP_UKR_Infantry_AR","LOP_UKR_Infantry_AR_Asst", "LOP_UKR_Infantry_Rifleman2", "LOP_UKR_Infantry_sergeant", "LOP_UKR_Infantry_AR","LOP_UKR_Infantry_AR_Asst", "LOP_UKR_Infantry_LAT"],
			["LOP_UKR_Infantry_sergeant", "LOP_UKR_Infantry_AR","LOP_UKR_Infantry_AR_Asst", "LOP_UKR_Infantry_Rifleman", "LOP_UKR_Infantry_sergeant", "LOP_UKR_Infantry_RPG","LOP_UKR_Infantry_Rifleman", "LOP_UKR_Infantry_Rifleman"]
		]
	];
