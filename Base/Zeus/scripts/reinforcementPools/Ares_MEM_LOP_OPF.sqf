if (isNil "Ares_Reinforcement_Mission_Unit_Pools") then
{
	Ares_Reinforcement_Mission_Unit_Pools = [];
};

Ares_Reinforcement_Mission_Unit_Pools pushBack
	[
		"Middle Eastern Malitia LOP OPF", // Name of the faction
		east, // The side of the faction
		"lop_faction_am", // The name of the root class for the addon that defines these units (must exist or the side will not show up). Use "" to always show.
		["LOP_AM_Landrover","LOP_AM_Offroad","LOP_AM_UAZ","LOP_AM_UAZ_Open"], // Scouts and unarmed light vehicles
		["LOP_AM_Landrover_M2","LOP_AM_Offroad_M2","LOP_AM_UAZ_DshKM"], // Armed light vehicles
		["LOP_AM_Truck"], // Dedicated troop trucks
		["LOP_AM_BTR60"], // Armed + Armored Troop Transports
		[], // Unarmed helicopters
		[], // Armed helicopters
		[], // Unarmed boats
		[], // Armed boats
		[
			// Squad setups to load into vehicles. Note - these may get truncated to fit into empty spaces or small vehicles.
			["LOP_AM_Infantry_SL","LOP_AM_Infantry_AR","LOP_AM_Infantry_AR_Ass","LOP_AM_Infantry_Rifleman","LOP_AM_Infantry_SL","LOP_AM_Infantry_AR","LOP_AM_Infantry_AR_Ass","LOP_AM_Infantry_Marksman"],
			["LOP_AM_Infantry_SL","LOP_AM_Infantry_AR","LOP_AM_Infantry_AR_Ass","LOP_AM_Infantry_Rifleman","LOP_AM_Infantry_SL","LOP_AM_Infantry_AT","LOP_AM_Infantry_Rifleman","LOP_AM_Infantry_Rifleman"]
		]
	];
