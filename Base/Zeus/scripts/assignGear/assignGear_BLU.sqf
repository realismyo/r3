/* 
Gear Assign Script for Arma 3
by Mr. Agnet

- Covers standard RIFM platoon roles for players and crate/vehicle loadouts for crates and/or vehicles.
- If you want to add more just ask me or do so by observation of how the others work.
- Report any and all issues to Mr. Agnet via the forums, steam, slack, ts etc.

- Current Player Loadouts: 
"coyld", "pltld", "pltmed", "pltfac", "pltuavop", 
"secco", "secmedic", "sectl", "ar", "aar", "rm", "rmat", "rmsc", "dmr", "gren", "mmg", "mmgass", 
"rotarypilot", "rotarycrew", "fixedpilot", "crewmander", "crewman", 
"hmggun", "hmgass", "gmggun", "gmgass", "matgun", "matammo", "hvymatgun", "hvymatammo", "hatgun", "hatammo", "aagun", "aaammo", 
"spectl", "specrm", "divertl", "diver", "sniper", "amsniper", "spotter", "engi", "heavygren", "carbineer", "smgunner", "shotgunner", "ladderman"

- Current Crate/Vehicle Loadouts:
"hqammo", "sectionammo", "rifleammo", "glammo", "arammo", 
"mmgammo", "latammo", "matammo", "hatammo", "aaammo", "hqmeds", "secmeds", "atmines", "apmines", "demosmall", 
"demobig", "mix", "marskmancrate", "snipercrate", "latcrate", 
"matcrate", "hatteam", "diverpair", "hmgtripod", "hmgweapon", "gmgtripod", "gmgweapon", "hatammo", "hqtools", "sectiontools", 
"engitools", "nightvisions", "nightgear", "uavgear", "ctabgear", "attachments", "cableties", "flashabangs", "wetsuits", "steerchutes", 
"nonsteerchutes", "bananas", "blank"

===== Using this Script =====
- In their current state, the scripts will not work for AI. In fact a whole different script will be required.

- If called from unit init field, needs follow the following format, with the desired loadout as a lower case string value (in "").
- nul = [this,"loadout"] execVM "scripts\assignGear.sqf";
e.g. - nul = [this,"pltld"] execVM "scripts\assignGear.sqf";
- This would equip a platoon leader with the gear defined below.
- All available loadout options are annotated above.


- Additional cases can be added below to support additional roles.
- The variables at the beginning of the defines script influence what gear players will receive, they should be fairly straightforward.
=============================
*/ 

private [
"_customGear",
"_sidearms","_nightGear","_scopes","_suppressors","_assignCTab","_assignCTabCameras",
"_gls","_3GLs","_variant","_camoPattern","_headgear","_parachutes","_flashbangs","_cableTies","_radioSelection","_underwaterWeapons",
"_delay","_unit","_loadout","_team"
];

// =================================================== Gear Script Variables ==================================================
// Dictates whether lobby parameters are used for the gear script variables.
// If false, uses values as set below 'Lobby Parameters not used', otherwise, uses values from description.ext.

_customGear = ["Gear_GearController",0] call BIS_fnc_GetParamValue;
if (_customGear == 0) then { _customGear = false; } else { _customGear = true; }; 

// ============================================== Parameter Compatible Variables ==============================================

if (_customGear) then {

	// ========= Lobby Parameters Used ==========
	// Dictates value based on parameter settings .
	
	// Sidearms
	_sidearms = ["Gear_Sidearmcontroller",0] call BIS_fnc_GetParamValue;
	if (_sidearms == 0) then { _sidearms = false; } else { _sidearms = true; }; 
	
	// Night vision goggles and IR strobes equipment.
	_nightGear = ["Gear_NightGearController",0] call BIS_fnc_GetParamValue;
	if (_nightGear == 0) then { _nightGear = false; } else { _nightGear = true; }; 
	
	// Scopes or regular attachments.
	_scopes = ["Gear_ScopeController",1] call BIS_fnc_GetParamValue;
	
	// Suppressors where applicable.		
	_suppressors = ["Gear_SuppressorController",0] call BIS_fnc_GetParamValue;
	if (_suppressors == 0) then { _suppressors = false; } else { _suppressors = true; }; 
	
	// Assign CTab Devices.	
	_assignCTab = ["Gear_CTabController",0] call BIS_fnc_GetParamValue;
	if (_assignCTab == 0) then { _assignCTab = false; } else { _assignCTab = true; };

	// Assign CTab Helmet cameras to units - assignCTab must also be true.	
	_assignCTabCameras = ["Gear_CTabCamController",0] call BIS_fnc_GetParamValue;
	if (_assignCTabCameras == 0) then { _assignCTabCameras = false; } else { _assignCTabCameras = true; }; 
	
	// ==========================================
	
} else {

	// ========= Lobby Parameters not used ======
	// Enter desired default values here.
	
	_sidearms = false; 				// Sidearm enabler. If true, players get the defined sidearm, if false, they don't. Such bool, much ean. 
	_nightGear = false;				// Night vision goggles and IR strobes equipment.			
	_scopes = 1;					// Scopes or regular attachments - 0 for irons, 1 for reflex type, 2 for magnified type			
	_suppressors = false;			// Suppressors where applicable.	
	_assignCTab = false;			// Assign CTab Devices.			
	_assignCTabCameras = false;		// Assign CTab Helmet cameras to units - assignCTab must also be true.
	
	// ==========================================
};
// ===================================================== Non-Param values =====================================================
// Values that cannot be set by description.ext, must be defined here.
// - Check relevants scripts for applicable variables/values. 
// - "Default" values are placeholder, they can be altered as desired. 

_gls = true; 						// enable/disable underslug GLs for common roles. 
_3GLs = false;
_variant = "default"; 				// Weapon Category. Check weapon script comments for available cases.  
_camoPattern = "default";			// Uniform Camo Pattern. Check uniform script comments for available cases. 	
_headgear = "default";				// Uniform Headgear Type. Check uniform script comments for available cases. 
_parachutes = "none";				// Parachute type. Default "none". Available cases: "steerable", "nonsteerable", "none". Automatically chucks pack on front of player, assigns chute after. None assigns no chute, no front pack. 
_flashbangs = 0;					// Amount of flashbangs, integer required. Set to 0 for none. Set to something >0 for however many flashabangs you want to give people. wow. 
_cableTies = 0;						// Amount of cable ties, integer required. Set to 0 for none. Set to something >0 for however many cable ties you want to give people. 
_radioSelection = "detection";		// Radio assignment. "detection" for side detect & assignment, "defined" for script defined radio & assignment. Also ties in UAV packs.
_underwaterWeapons = true;			// Divers assigned underwater rifles, if false then same rifle as everyone else. 

// ============================================================================================================================
// variable assignment

_delay = 0.1;
_unit = _this select 0;
_loadout = toLower (_this select 1);
if ((count _this > 2)) then { if (typeName (_this select 2) == "STRING") then { _team = _this select 2; }; };

// ============================================================================================================================

// waits until mission has started, make sure unit exists, or wait until it does
waitUntil {time > 1};																			
if (_unit != _unit) then { waitUntil { !isNull _unit; }; };
if (!isDedicated && isMultiplayer) then { waitUntil { !isNull _unit; }; };
if ((!isDedicated && isMultiplayer) && (_unit isKindOf "Man")) then { waitUntil { !isNull _unit && isPlayer _unit; }; };

// disable AI stuffs
if ((_unit isKindOf "Man") && !(isPlayer _unit)) then { {_unit disableAI _x} forEach ["TARGET","AUTOTARGET","MOVE","FSM","ANIM"]; };

// script needs to run on the unit/player's computer - if the unit is a player, then server exits. If not, then player exits. 
if (!(local _unit)) exitWith {};						
if ((isMultiplayer && isServer) && isPlayer _unit) exitWith {};
if ((isMultiplayer && !isServer) && !isPlayer _unit) exitWith {};

// ============================================================================================================================
// Include files for weapons, uniforms, functions and tools. 
// Alter the lines below to include different classes, or edit those files.

// Variable Includes, Alter these for different stuff
#include "weapons\weapons_MX.sqf";			// weapons classes.
#include "uniforms\uniforms_NATO.sqf";		// uniform classes.

// General Includes
#include "assignGear_Functions.sqf";		// functions used by the gear script.
#include "assignGear_GenericItems.sqf";		// tools and miscellaneous items assigned to players.

// ============================================================================================================================
// Gear Roles/Cargo Contents
// - Uses a Switch Statement to define roles.
// - Includes roles both for players and things like ammo crates and vehicles. 
// - Peforms a check to see if the unit is a man or not. 
// - NOTE: roles intended for players ('man' entities) may not work correctly on crates or vehicles, and vice versa. 
 
// Gear Removal
if (_plebUniform != "") then { removeUniform _unit; };
if (_goggles != "") then { removeGoggles _unit; };
removeAllItems _unit;
removeAllWeapons _unit; 
removeAllAssignedItems _unit;
removeVest _unit;
removeHeadgear _unit;
removeBackpack _unit;

// ================================================================================================================================================================
// If unit is a man then use this switch

if (_unit isKindOf "Man") then {
	
	// Switch Statement Begins
	switch (_loadout) do {
		// ========================================
		// =========== Company HQ Roles ===========
		// ========================================
		// COYCO/COY2iC
		case "coyld" : {
			["leader"] call _addClothes;
			["rangefinder","tablet"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;  
			if (_gls) then {
				if (_3GLs) then {
					_unit addMagazines [_glExplody,2];
					_unit addMagazines [_glSmokeOne,1];
					_unit addMagazines [_glSmokeTwo,1];
				} else {
					_unit addMagazines [_glExplody,4];
					_unit addMagazines [_glSmokeOne,1];
					_unit addMagazines [_glSmokeTwo,1];
				};
				_unit addMagazines [_rifleGLMag,5];
				_unit addMagazines [_rifleTracerMag,4];
				_unit addWeapon _rifleGL;
				_unit addMagazines [_glExplody,1];
			} else {
				_unit addMagazines [_rifleMag,5];
				_unit addMagazines [_rifleTracerMag,4];
				_unit addWeapon _rifle;
			};
			["coy"] call _addRuck;
			["general",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};
		
		// ========================================
		// =========== Platoon HQ Roles ===========
		// ========================================
		// PltCO/PltSGT
		case "pltld" : {
			["leader"] call _addClothes;
			["rangefinder","tablet"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;
			if (_gls) then {
				if (_3GLs) then {
					_unit addMagazines [_glExplody,2];
					_unit addMagazines [_glSmokeOne,1];
					_unit addMagazines [_glSmokeTwo,1];
				} else {
					_unit addMagazines [_glExplody,4];
					_unit addMagazines [_glSmokeOne,1];
					_unit addMagazines [_glSmokeTwo,1];
				};
				_unit addMagazines [_rifleGLMag,5];
				_unit addMagazines [_rifleTracerMag,4];
				_unit addWeapon _rifleGL;
				_unit addMagazines [_glExplody,1];
			} else {
				_unit addMagazines [_rifleMag,5];
				_unit addMagazines [_rifleTracerMag,4];
				_unit addWeapon _rifle;
			};
			["plt"] call _addRuck;
			["general",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};
		// Plt Medic - doubles for COY Medic
		case "pltmed" : {
			["medic"] call _addClothes;
			["binocular","android"] call _addBasics;
			_unit addItem _radioTwo;
			for "_i" from 1 to 4 do {_unit addItemToUniform _smoke};
			for "_i" from 1 to 9 do {_unit addItemToVest _rifleMag};
			for "_i" from 1 to 10 do {_unit addItemToVest _bandageOne};
			for "_i" from 1 to 10 do {_unit addItemToUniform _bandageOne};
			for "_i" from 1 to 4 do {_unit addItemToUniform _injectorOne};
			for "_i" from 1 to 4 do {_unit addItemToUniform _injectorTwo};
			_unit addWeapon _rifle;
			call _addSidearm;
			["medic"] call _addRuck;
			["general",false,false] call _addAttachments;
		};
		// Plt FAC/FO - doubles for COY FAC/FO
		case "pltfac" : {
			["pleb"] call _addClothes;
			["designator","tablet"] call _addBasics;
			{ _unit addItemToUniform _x; _unit addItemToUniform _x; } foreach _facSmokes;
			_unit addItemToVest _dagr;
			_unit addItemToVest _rangeFinder;
			for "_i" from 1 to 2 do {_unit addItemToUniform _designatorBat};
			if (_gls) then {
				if (_3GLs) then {
					for "_i" from 1 to 2 do {_unit addItemToVest _glSmokeOne};
					for "_i" from 1 to 2 do {_unit addItemToVest _glSmokeTwo};
				} else {
					for "_i" from 1 to 5 do {_unit addItemToVest _glSmokeOne};
					for "_i" from 1 to 5 do {_unit addItemToVest _glSmokeTwo};
				};
				for "_i" from 1 to 9 do {_unit addItemToVest _rifleGLMag};
				_unit addWeapon _rifleGL;
				_unit addItemToVest _glSmokeOne;
			} else {
				for "_i" from 1 to 9 do {_unit addItemToVest _rifleMag};
				_unit addWeapon _rifle;
			};
			["fac"] call _addRuck;
			["general",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};
		// Plt UAV Operator
		case "pltuavop" : {
			["pleb"] call _addClothes;
			["designator","tablet"] call _addBasics;
			_unit addItemToUniform _radioTwo;
			_unit addItemToUniform _uavBat;
			_unit addItemToUniform _designatorBat;
			_unit addItemToVest _uavTool;
			_unit addItemToVest _dagr;
			_unit addItemToVest _rangeFinder;
			{ _unit addItemToVest _x; _unit addItemToVest _x; } foreach _facSmokes;
			for "_i" from 1 to 8 do {_unit addItemToVest _rifleMag};
			_unit addWeapon _rifle;
			call _addSidearm;
			["uavop"] call _addRuck;
			["general",false,false] call _addAttachments;
			["vest"] call _IFAK;
		};
		// Rifleman
		case "pltrm" : {
			["pleb"] call _addClothes;
			["binocular","android"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;  
			_unit addMagazines [_rifleMag,9];
			_unit addWeapon _rifle;
			["rm"] call _addRuck;
			["general",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};	
		
		// ========================================
		// ============ Section Roles =============
		// ========================================
		// Section Commander
		case "secco" : {
			["leader"] call _addClothes;
			["rangefinder","tablet"] call _addBasics;
			_unit addItem _radioTwo;
			{ _unit addItemToUniform _x; } foreach _throwG;  
			if (_gls) then {
				if (_3GLs) then {
					_unit addMagazines [_glExplody,2];
					_unit addMagazines [_glSmokeOne,1];
					_unit addMagazines [_glSmokeTwo,1];
				} else {
					_unit addMagazines [_glExplody,4];
					_unit addMagazines [_glSmokeOne,1];
					_unit addMagazines [_glSmokeTwo,1];
				};
				_unit addMagazines [_rifleGLMag,5];
				_unit addMagazines [_rifleTracerMag,4];
				_unit addWeapon _rifleGL;
				_unit addMagazines [_glExplody,1];
			} else {
				_unit addMagazines [_rifleMag,5];
				_unit addMagazines [_rifleTracerMag,4];
				_unit addWeapon _rifle;
			};
			["secco"] call _addRuck;
			["general",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};
		// Section Medic
		case "secmedic" : {
			["medic"] call _addClothes;
			["binocular","android"] call _addBasics; 
			for "_i" from 1 to 4 do {_unit addItemToUniform _smoke};
			for "_i" from 1 to 9 do {_unit addItemToVest _rifleMag};
			for "_i" from 1 to 10 do {_unit addItemToVest _bandageOne};
			for "_i" from 1 to 4 do {_unit addItemToVest _injectorOne};
			for "_i" from 1 to 4 do {_unit addItemToVest _injectorTwo};
			for "_i" from 1 to 10 do {_unit addItemToUniform _bandageOne};
			for "_i" from 1 to 4 do {_unit addItemToUniform _injectorOne};
			for "_i" from 1 to 4 do {_unit addItemToUniform _injectorTwo};
			_unit addWeapon _rifle;
			call _addSidearm;
			["secmedic"] call _addRuck;
			["general",false,false] call _addAttachments;
		};
		// Section Team Leader / 2iC
		case "sectl" : {
			["leader"] call _addClothes;
			["rangefinder","android"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;  
			if (_gls) then {
				if (_3GLs) then {
					_unit addMagazines [_glExplody,2];
					_unit addMagazines [_glSmokeOne,1];
					_unit addMagazines [_glSmokeTwo,1];
				} else {
					_unit addMagazines [_glExplody,4];
					_unit addMagazines [_glSmokeOne,1];
					_unit addMagazines [_glSmokeTwo,1];
				};
				_unit addMagazines [_rifleGLMag,5];
				_unit addMagazines [_rifleTracerMag,4];
				_unit addWeapon _rifleGL;
				_unit addMagazines [_glExplody,1];
			} else {
				_unit addMagazines [_rifleMag,5];
				_unit addMagazines [_rifleTracerMag,4];
				_unit addWeapon _rifle;
			};
			["tl"] call _addRuck;
			["general",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};
		// Automatic Rifleman
		case "ar" : {
			["gunner"] call _addClothes;
			["none","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;
			_unit addMagazines [_autoRifleMag,_arMagCount];
			_unit addWeapon _autoRifle;
			_unit addMagazine _autoTracerMag;
			call _addSidearm;
			["ar"] call _addRuck;
			["ar",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};
		// Assistant Automatic Rifleman
		case "aar" : {
			["pleb"] call _addClothes;
			["binocular","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;  
			_unit addMagazines [_rifleMag,9];
			_unit addWeapon _rifle;
			["aar"] call _addRuck;
			["general",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};
		// Rifleman
		case "rm" : {
			["pleb"] call _addClothes;
			["none","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;  
			_unit addMagazines [_rifleMag,9];
			_unit addWeapon _rifle;
			["rm"] call _addRuck;
			["general",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};	
		// Rifleman
		case "rmammo" : {
			["pleb"] call _addClothes;
			["none","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;  
			_unit addMagazines [_rifleMag,9];
			_unit addWeapon _rifle;
			["rmammo"] call _addRuck;
			["general",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};	
		// Rifleman (disposable light AT)
		case "rmat" : {
			["pleb"] call _addClothes;
			["none","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;  
			_unit addMagazines [_rifleMag,9];
			_unit addWeapon _rifle;
			["rmat"] call _addRuck;
			["general",false,false] call _addAttachments;
			_unit addWeapon _lat;
			["pack"] call _IFAK;
		};
		// Scoped Rifleman
		case "rmsc" : {
			["pleb"] call _addClothes;
			["none","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;  
			_unit addMagazines [_rifleScopedMag,9];
			_unit addWeapon _rifleScoped;
			["rmsc"] call _addRuck;
			["dmr",true,false] call _addAttachments;
			["pack"] call _IFAK;
		};
		// Designated Marksman
		case "dmr" : {
			["pleb"] call _addClothes;
			["none","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;  
			_unit addMagazines [_dmrMag,9];
			_unit addWeapon _dmr;
			call _addSidearm;
			["dmr"] call _addRuck;
			["dmr",true,false] call _addAttachments;
			["pack"] call _IFAK;
		};
		// Grenadier
		case "gren" : {
			["gren"] call _addClothes;
			["none","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;  
			if (_gls) then {
				if (_3GLs) then {
					_unit addMagazines [_glExplody,2];
					_unit addMagazines [_glSmokeOne,1];
					_unit addMagazines [_glSmokeTwo,1];
				} else {
					_unit addMagazines [_glExplody,4];
					_unit addMagazines [_glSmokeOne,1];
					_unit addMagazines [_glSmokeTwo,1];
				};
				_unit addMagazines [_rifleGLMag,9];
				_unit addWeapon _rifleGL;
				_unit addMagazines [_glExplody,1];
			} else {
				_unit addMagazines [_rifleMag,9];
				_unit addWeapon _rifle;
			};
			["gren"] call _addRuck;
			["general",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};
		// Machinegunner
		case "mmg" : {
			["gunner"] call _addClothes;
			["none","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG; 
			_unit addMagazines [_mmgMag,2];
			_unit addWeapon _mmg;
			_unit addMagazine _mmgMag;
			call _addSidearm;
			["mmg"] call _addRuck;
			["mmg",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};
		// Assistant Machinegunner
		case "mmgass" : {
			["pleb"] call _addClothes;
			["binocular","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;  
			_unit addMagazines [_rifleMag,9];
			_unit addWeapon _rifle;
			["mmgass"] call _addRuck;
			["general",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};
		
		// ========================================
		// ============ Vehicle Crews =============
		// ========================================
		// Rotary Wing Pilot
		case "rotarypilot" : {
			["rpilot"] call _addClothes;
			["none","android"] call _addBasics;
			_unit addItem _radioTwo;
			{ _unit addItemToUniform _x; _unit addItemToUniform _x; } foreach _facSmokes;
			for "_i" from 1 to 6 do {_unit addItemToVest _smgMag};
			_unit addWeapon _smg;
			call _addSidearm;
			["pilot"] call _addRuck;
			["pack"] call _IFAK;
		};
		case "rotarycrew" : {
			["rcrew"] call _addClothes;
			["binocular","android"] call _addBasics;
			_unit addItem _radioTwo;
			{ _unit addItemToUniform _x; _unit addItemToUniform _x; } foreach _facSmokes;
			for "_i" from 1 to 6 do {_unit addItemToVest _carbineMag};
			_unit addWeapon _carbine;
			call _addSidearm;
			["aircrew"] call _addRuck;
			["pack"] call _IFAK;
		};
		// Fixed Wing Pilot
		case "fixedpilot" : {
			["fpilot"] call _addClothes;
			["none","android"] call _addBasics;
			_unit addItem _radioTwo;
			{ _unit addItemToUniform _x; _unit addItemToUniform _x; } foreach _facSmokes;
			for "_i" from 1 to 6 do {_unit addItemToVest _smgMag};
			_unit addWeapon _smg;
			call _addSidearm;
			["pilot"] call _addRuck;
			["pack"] call _IFAK;
		};
		// Crew Commander
		case "crewmander" : {
			["crew"] call _addClothes;
			["rangefinder","android"] call _addBasics;
			_unit addItem _radioTwo;
			for "_i" from 1 to 2 do {_unit addItemToUniform _smoke};
			for "_i" from 1 to 6 do {_unit addItemToVest _carbineMag};
			_unit addWeapon _carbine;
			call _addSidearm;
			["crew"] call _addRuck;
			["general",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};
		// Crewman
		case "crewman" : {
			["crew"] call _addClothes;
			["none","none"] call _addBasics;
			_unit addItem _radioTwo;
			for "_i" from 1 to 2 do {_unit addItemToUniform _smoke};
			{ _unit linkItem _x } foreach _ftlItems;
			for "_i" from 1 to 6 do {_unit addItemToVest _carbineMag};
			_unit addWeapon _carbine;
			call _addSidearm;
			["crewman"] call _addRuck;
			["general",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};
		
		// ========================================
		// ========= Static Weapons Teams =========
		// ========================================
		// HMG Gunner
		case "hmggun" : {
			["gunner"] call _addClothes;
			["none","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;  
			for "_i" from 1 to 8 do {_unit addItemToVest _rifleMag};
			_unit addWeapon _rifle;
			call _addSidearm;
			["hmggun"] call _addRuck;
			["general",false,false] call _addAttachments;
			["vest"] call _IFAK;
		};
		// HMG Assistant
		case "hmgass" : {
			["pleb"] call _addClothes;
			["rangefinder","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;  
			for "_i" from 1 to 8 do {_unit addItemToVest _rifleMag};
			_unit addWeapon _rifle;
			call _addSidearm;
			["hmgass"] call _addRuck;
			["general",false,false] call _addAttachments;
			["vest"] call _IFAK;
		};
		// GMG Gunner
		case "gmggun" : {
			["gunner"] call _addClothes;
			["none","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;  
			for "_i" from 1 to 8 do {_unit addItemToVest _rifleMag};
			_unit addWeapon _rifle;
			call _addSidearm;
			["gmggun"] call _addRuck;
			["general",false,false] call _addAttachments;
			["vest"] call _IFAK;
		};
		// GMG Assistant
		case "gmgass" : {
			["pleb"] call _addClothes;
			["rangefinder","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;  
			for "_i" from 1 to 8 do {_unit addItemToVest _rifleMag};
			_unit addWeapon _rifle;
			call _addSidearm;
			["gmgass"] call _addRuck;
			["general",false,false] call _addAttachments;
			["vest"] call _IFAK;
		};
		
		// ========================================
		// ============ Launcher Teams ============
		// ========================================
		// MAT Gunner
		case "matgun" : {
			["pleb"] call _addClothes;
			["none","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;  
			for "_i" from 1 to 9 do {_unit addItemToVest _rifleMag};
			_unit addWeapon _rifle;
			call _addSidearm;
			["matgun"] call _addRuck;
			_unit addWeapon _matLaunch;
			["carlito",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};
		// MAT Assisstant
		case "matammo" : {
			["pleb"] call _addClothes;
			["binocular","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;  
			for "_i" from 1 to 9 do {_unit addItemToVest _rifleMag};
			_unit addWeapon _rifle;
			call _addSidearm;
			["matass"] call _addRuck;
			["general",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};
		// MAT Gunner
		case "hvymatgun" : {
			["pleb"] call _addClothes;
			["none","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;  
			for "_i" from 1 to 9 do {_unit addItemToVest _rifleMag};
			_unit addWeapon _rifle;
			call _addSidearm;
			["hvymatgun"] call _addRuck;
			_unit addWeapon _matLaunch;
			["carlito",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};
		// MAT Assisstant
		case "hvymatammo" : {
			["pleb"] call _addClothes;
			["rangefinder","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;  
			for "_i" from 1 to 9 do {_unit addItemToVest _rifleMag};
			_unit addWeapon _rifle;
			call _addSidearm;
			["hvymatass"] call _addRuck;
			["general",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};
		// HAT Gunner
		case "hatgun" : {
			["pleb"] call _addClothes;
			["none","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;  
			for "_i" from 1 to 9 do {_unit addItemToVest _rifleMag};
			_unit addWeapon _rifle;
			call _addSidearm;
			["hat"] call _addRuck;
			["general",false,false] call _addAttachments;
			_unit addWeapon _hatLaunch;
			["pack"] call _IFAK;
		};
		// HAT Assistant
		case "hatammo" : {
			["pleb"] call _addClothes;
			["rangefinder","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;  
			for "_i" from 1 to 9 do {_unit addItemToVest _rifleMag};
			_unit addWeapon _rifle;
			call _addSidearm;
			["hatass"] call _addRuck;
			["general",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};
		// AA Gunner
		case "aagun" : {
			["pleb"] call _addClothes;
			["none","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;  
			for "_i" from 1 to 9 do {_unit addItemToVest _rifleMag};
			_unit addWeapon _rifle;
			call _addSidearm;
			["aa"] call _addRuck;
			["general",false,false] call _addAttachments;
			_unit addWeapon _aaLaunch;
			["pack"] call _IFAK;
		};
		// AA Assistant
		case "aaammo" : {
			["pleb"] call _addClothes;
			["rangefinder","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;  
			for "_i" from 1 to 9 do {_unit addItemToVest _rifleMag};
			_unit addWeapon _rifle;
			call _addSidearm;
			["aaass"] call _addRuck;
			["general",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};
		
		// ========================================
		// =========== Specialist Roles ===========
		// ========================================
		// Sniper
		case "sniper" : {
			["sniper"] call _addClothes;
			["none","none"] call _addBasics;
			{ _unit linkItem _x } foreach _secItems;
			{ _unit addItemToUniform _x } foreach _sniperItems;
			{ _unit addItemToUniform _x; } foreach _throwG;
			for "_i" from 1 to 9 do {_unit addItemToVest _boltRifleMag};
			_unit addWeapon _boltRifle;
			call _addSidearm;
			["sniper"] call _addRuck;
			["sniper",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};
		// Anti-Material Sniper
		case "amsniper" : {
			["sniper"] call _addClothes;
			["none","none"] call _addBasics;
			{ _unit linkItem _x } foreach _secItems;
			{ _unit addItemToUniform _x } foreach _sniperItems;
			{ _unit addItemToUniform _x; } foreach _throwG;
			for "_i" from 1 to 9 do {_unit addItemToVest _amRifleMag};
			_unit addWeapon _amRifle;
			call _addSidearm;
			["amsniper"] call _addRuck;
			["amsniper",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};
		// Spotter
		case "spotter" : {
			["sniper"] call _addClothes;
			["designator","android"] call _addBasics;
			{ _unit linkItem _x } foreach _secItems;
			{ _unit addItemToUniform _x } foreach _sniperItems;
			{ _unit addItemToUniform _x; } foreach _throwG;
			for "_i" from 1 to 9 do {_unit addItemToVest _rifleGLMag};
			_unit addMagazines [_glExplody,5];
			_unit addMagazines [_glSmokeOne,2];
			_unit addMagazines [_glSmokeTwo,2];
			_unit addWeapon _rifleGL;
			["spotter"] call _addRuck;
			["general",false,true] call _addAttachments;
		};
		// SpecOps Team Leader
		case "spectl" : {
			["spec"] call _addClothes;
			["rangefinder","android"] call _addBasics;
			_unit addItem _radioTwo;
			_unit linkItem _nightVision;
			{ _unit addItemToUniform _x; } foreach _throwG;  
			_unit addMagazines [_rifleGLMag_spec,7];
			_unit addMagazines [_rifleGLTracerMag_spec,2];
			_unit addMagazines [_glExplody,5];
			_unit addMagazines [_glSmokeOne,1];
			_unit addMagazines [_glSmokeTwo,1];
			_unit addWeapon _rifleGL_spec;
			for "_i" from 1 to 4 do {_unit addItemToVest _flashbang};
			for "_i" from 1 to 4 do {_unit addItemToUniform _cableTieItem};
			["spectl"] call _addRuck;
			["specops",true,true] call _addAttachments;
			["pack"] call _IFAK;
		};
		// SpecOps Team Member
		case "specrm" : {
			["spec"] call _addClothes;
			["binocular","none"] call _addBasics;
			_unit linkItem _nightVision;
			{ _unit addItemToUniform _x; } foreach _throwG;
			_unit addMagazines [_rifleMag_spec,7];
			_unit addMagazines [_rifleTracerMag_spec,2];
			for "_i" from 1 to 4 do {_unit addItemToVest _flashbang};
			for "_i" from 1 to 4 do {_unit addItemToVest _cableTieItem};
			_unit addWeapon _rifle_spec;
			call _addSidearm;
			["specrm"] call _addRuck;
			["specops",true,true] call _addAttachments;
			["pack"] call _IFAK;
		};	
		// Diver Team Leader
		case "divertl" : {
			["diver"] call _addClothes;
			["rangefinder","android"] call _addBasics;
			_unit addItem _radioTwo;
			{ _unit addItem _x } foreach _secItems;
			_unit addMagazines [_rifleDiverMagOne,3];
			_unit addMagazines [_rifleDiverMagTwo,3];
			{ _unit addMagazines [_x,2]; } foreach _throwG;
			_unit addWeapon _rifleDiver;
			["divertl"] call _addRuck;
			["diver",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};
		// Diver General
		case "diver" : {
			["diver"] call _addClothes;
			["none","none"] call _addBasics;
			_unit addMagazines [_rifleDiverMagOne,3];
			_unit addMagazines [_rifleDiverMagTwo,3];
			{ _unit addMagazines [_x,2]; } foreach _throwG;
			_unit addWeapon _rifleDiver;
			["diver"] call _addRuck;
			["diver",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};
		// Engineer
		case "engi" : {
			["pleb"] call _addClothes;
			["none","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;
			for "_i" from 1 to 9 do {_unit addItemToVest _rifleMag};
			_unit addWeapon _rifle;
			["engi"] call _addRuck;
			["general",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};	
		// Demo Man
		case "demoman" : {
			["pleb"] call _addClothes;
			["none","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;
			for "_i" from 1 to 9 do {_unit addItemToVest _rifleMag};
			_unit addWeapon _rifle;
			["demoman"] call _addRuck; // largeruck
			["general",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};	
		// Heavy Grenadier - MGL system
		case "heavygren" : {
			["pleb"] call _addClothes;
			["none","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;
			_unit addItemToVest _glHeavyExplody;
			_unit addWeapon _glHeavy;
			_unit addItemToVest _glHSmokesOne;
			_unit addItemToVest _glHSmokesTwo;
			_unit addItemToVest _glHeavyExplody;
			call _addSidearm;
			["heavygren"] call _addRuck; 
			["heavygren",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};	
		
		// ========================================
		// ============== Extra Roles =============
		// ========================================
		// Carbineer
		case "carbineer" : {
			["pleb"] call _addClothes;
			["none","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;
			for "_i" from 1 to 9 do {_unit addItemToVest _carbineMag};
			_unit addWeapon _carbine;
			call _addSidearm;
			["carb"] call _addRuck;
			["general",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};	
		// SMG Gunner
		case "smgunner" : {
			["pleb"] call _addClothes;
			["none","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;
			for "_i" from 1 to 9 do {_unit addItemToVest _smgMag};
			_unit addWeapon _smg;
			call _addSidearm;
			["smg"] call _addRuck;
			["none",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};	
		// Shotgunner
		case "shotgunner" : {
			["pleb"] call _addClothes;
			["none","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;
			for "_i" from 1 to 4 do {_unit addItemToVest _shottySlug};
			for "_i" from 1 to 4 do {_unit addItemToVest _shottyBuck};
			_unit addWeapon _shotty;
			_unit addItemToVest _shottySlug;
			call _addSidearm;
			["shotty"] call _addRuck;
			["none",false,false] call _addAttachments;
			["pack"] call _IFAK;
		};	
		// Ladderman
		case "ladderman" : {
			["pleb"] call _addClothes;
			["none","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;
			for "_i" from 1 to 9 do {_unit addItemToVest _rifleMag};
			_unit addWeapon _rifle;
			["ladder"] call _addRuck;
			["general",false,false] call _addAttachments;
		};	
		// ========================================
		// if undefined or incorrectly defined, give hint and assign standard rifleman gear
		default {
			_unit groupChat format ["No or incorrectly defined loadout for unit: %1",_unit];
			["pleb"] call _addClothes;
			["none","none"] call _addBasics;
			{ _unit addItemToUniform _x; } foreach _throwG;
			for "_i" from 1 to 9 do {_unit addItemToVest _rifleMag};
			_unit addWeapon _rifle;
			["rm"] call _addRuck;
			["general",false,false] call _addAttachments;
		};
		// ========================================
	};
	// ========================================
	// Post Gear Assign Function, handles misc stuff.
	call _postGearAssign; 

	// ========================================
	// AssignGear Finisher

	_unit setVariable ["Ace_medical_medicClass", 1, true];										// sets unit as medic
	[_unit,_insignia] call bis_fnc_setUnitInsignia;												// sets unit uniform insignia
	_unit selectWeapon (primaryWeapon _unit);													// selects unit's primary weapon
	_unit switchMove "AmovPercMstpSlowWrflDnon";												// sets stance with weapon lowered
	sleep _delay;																				// wait a sec
	_unit groupChat format ["%1 completed gear assign.", name _unit];	// hint that unit has finished gear assign
	if (!isNil "_team") then { _unit assignTeam _team; };

	// ========================================
};

// ================================================================================================================================================================
// If unit isn't a man, then fill it with this stuff.

if (!(_unit isKindOf "Man")) then {   
	switch (_loadout) do {
	
		// clear crate cargo
		clearMagazineCargoGlobal _unit;
		clearWeaponCargoGlobal _unit;
		clearBackpackCargoGlobal _unit;
		clearItemCargoGlobal _unit;

		// ========================================
		// ============= Ammo Only Crates =========
		// ========================================
		case "hqammo" : {
			_unit addMagazineCargoGlobal [_rifleMag,40];
			_unit addMagazineCargoGlobal [_grenade,10];
			_unit addMagazineCargoGlobal [_smoke,10];
			_unit addMagazineCargoGlobal [_glExplody,20];
			_unit addMagazineCargoGlobal [_glSmokeOne,10];
			_unit addMagazineCargoGlobal [_glSmokeTwo,10];
			_unit addMagazineCargoGlobal [_designatorBat,2];
			_unit addItemCargoGlobal [_bandageOne,20];
			_unit addItemCargoGlobal [_injectorOne,10];
		};
		case "sectionammo" : {
			_unit addMagazineCargoGlobal [_rifleMag,50];
			_unit addMagazineCargoGlobal [_autoRifleMag,10];
			_unit addMagazineCargoGlobal [_grenade,10];
			_unit addMagazineCargoGlobal [_smoke,10];
			_unit addMagazineCargoGlobal [_glExplody,20];
			_unit addMagazineCargoGlobal [_glSmokeOne,10];
			_unit addMagazineCargoGlobal [_glSmokeTwo,10];
			_unit addItemCargoGlobal [_bandageOne,20];
			_unit addItemCargoGlobal [_injectorOne,10];
			_unit addItemCargoGlobal [_spareBarrel,2];
		};
		case "rifleammo" : {
			_unit addMagazineCargoGlobal [_rifleMag,80];
			_unit addMagazineCargoGlobal [_grenade,10];
			_unit addMagazineCargoGlobal [_smoke,10];
		};
		case "glammo" : {
			_unit addMagazineCargoGlobal [_rifleGLMag,40];
			_unit addMagazineCargoGlobal [_glExplody,40];
			_unit addMagazineCargoGlobal [_glSmokeOne,20];
			_unit addMagazineCargoGlobal [_glSmokeTwo,20];
		};
		case "arammo" : {
			_unit addMagazineCargoGlobal [_rifleMag,20];
			_unit addMagazineCargoGlobal [_autoRifleMag,20];
			_unit addMagazineCargoGlobal [_autoTracerMag,20];
			_unit addMagazineCargoGlobal [_grenade,10];
			_unit addMagazineCargoGlobal [_smoke,10];
			_unit addItemCargoGlobal [_spareBarrel,2];
		};
		case "mmgammo" : {
			_unit addMagazineCargoGlobal [_rifleMag,20];
			_unit addMagazineCargoGlobal [_mmgMag,20];
			_unit addMagazineCargoGlobal [_grenade,10];
			_unit addMagazineCargoGlobal [_smoke,10];
			_unit addItemCargoGlobal [_spareBarrel,2];
		};
		case "dmrammo" : {
			_unit addMagazineCargoGlobal [_dmrMag,20];
			_unit addMagazineCargoGlobal [_grenade,10];
			_unit addMagazineCargoGlobal [_smoke,10];
		};
		case "latammo" : {
			_unit addMagazineCargoGlobal [_rifleMag,20];
			_unit addWeaponCargoGlobal [_lat,10];
			_unit addWeaponCargoGlobal [_latMag,10];
		};
		case "matammo" : {
			_unit addMagazineCargoGlobal [_rifleMag,20];
			_unit addMagazineCargoGlobal [_matMag,10];
		};
		case "hatammo" : {
			_unit addMagazineCargoGlobal [_rifleMag,20];
			_unit addMagazineCargoGlobal [_hatMag,10];
		};
		case "aaammo" : {
			_unit addMagazineCargoGlobal [_rifleMag,20];
			_unit addMagazineCargoGlobal [_aaMag,10];
		};
			
		// ========================================
		// =========== Medical Supplies ===========
		// ========================================
		
		case "hqmeds" : {
			_unit addItemCargoGlobal [_injectorOne,30];
			_unit addItemCargoGlobal [_injectorTwo,10];
			_unit addItemCargoGlobal [_bandageOne,60];
			_unit addItemCargoGlobal [_bloodOne,10];
			_unit addItemCargoGlobal [_bloodTwo,5];
			_unit addItemCargoGlobal [_bloodThree,2];
		};
		case "secmeds" : {
			_unit addItemCargoGlobal [_injectorOne,16];
			_unit addItemCargoGlobal [_bandageOne,48];
			_unit addItemCargoGlobal [_bloodOne,5];
		};
		case "bodybags" : {
			_unit addItemCargoGlobal [_bodybag,20];
		};
		
		// ========================================
		// ============== Explosives ==============
		// ========================================
		
		case "atmines" : {
			_unit addItemCargoGlobal [_atMine,10];
			_unit addItemCargoGlobal [_slam,10];
			_unit addItemCargoGlobal [_clackOne,8];
			_unit addItemCargoGlobal [_defuseKit,8];
		};
		case "apmines" : {
			_unit addItemCargoGlobal [_apersMine,10];
			_unit addItemCargoGlobal [_apersBound,10];
			_unit addItemCargoGlobal [_apersTrip,10];
			_unit addItemCargoGlobal [_claymore,10];
			_unit addItemCargoGlobal [_clackOne,8];
			_unit addItemCargoGlobal [_defuseKit,8];
		};
		case "demosmall" : {
			_unit addItemCargoGlobal [_demoCharge,10];
			_unit addItemCargoGlobal [_clackOne,10];
			_unit addItemCargoGlobal [_defuseKit,10];
		};
		case "demobig" : {
			_unit addItemCargoGlobal [_satchelCharge,10];
			_unit addItemCargoGlobal [_clackOne,8];
			_unit addItemCargoGlobal [_defuseKit,8];
		};
		case "mix" : {
			_unit addItemCargoGlobal [_atMine,4];
			_unit addItemCargoGlobal [_slam,8];
			_unit addItemCargoGlobal [_apersTrip,8];
			_unit addItemCargoGlobal [_claymore,8];
			_unit addItemCargoGlobal [_demoCharge,8];
			_unit addItemCargoGlobal [_satchelCharge,4];
			_unit addItemCargoGlobal [_clackOne,8];
			_unit addItemCargoGlobal [_defuseKit,8];
		};
		
		// ========================================
		// ============== Weapons Teams ===========
		// ========================================
		
		case "marskmancrate" : {
			_unit addWeaponCargoGlobal [_rifleScoped,1];
			_unit addWeaponCargoGlobal [_rangeFinder,1];
			_unit addMagazineCargoGlobal [_rifleScopedMag,20];
			{ _unit addItemCargoGlobal [_x ,1]; } forEach _sniperItems;
			{ _unit addItemCargoGlobal [_x ,1]; } forEach _scoped_rifleAttachments;
		};
		case "snipercrate" : {
			_unit addWeaponCargoGlobal [_boltRifle,1];
			_unit addWeaponCargoGlobal [_designator,1];
			_unit addMagazineCargoGlobal [_designatorBat,2];
			_unit addMagazineCargoGlobal [_boltRifleMag,10];
			{ _unit addItemCargoGlobal [_x,1]; } forEach _sniperItems;
			{ _unit addItemCargoGlobal [_x,1]; } forEach _sniperAttachments;
			{ _unit addItemCargoGlobal [_x,2]; } forEach _secTools;
			{ _unit addItemCargoGlobal [_x,2]; } forEach _secItems;
		};
		case "latcrate" : {
			_unit addWeaponCargoGlobal [_lat,1];
			_unit addWeaponCargoGlobal [_designator,1];
			_unit addMagazineCargoGlobal [_designatorBat,2];
			_unit addMagazineCargoGlobal [_latMag,4];
		};
		case "matcrate" : {
			_unit addWeaponCargoGlobal [_matLaunch,1];
			_unit addItemCargoGlobal [_matScope,1];
			_unit addWeaponCargoGlobal [_designator,1];
			_unit addMagazineCargoGlobal [_designatorBat,2];
			_unit addMagazineCargoGlobal [_matMag,4];
		};
		case "hatteam" : {
			_unit addWeaponCargoGlobal [_hatLaunch,1];
			_unit addWeaponCargoGlobal [_designator,1];
			_unit addMagazineCargoGlobal [_designatorBat,2];
			_unit addMagazineCargoGlobal [_hatMag,2];
		};
		case "diverpair" : {
			_unit addWeaponCargoGlobal [_rifleDiver,2];
			_unit addMagazineCargoGlobal [_rifleDiverMagOne,20];
			_unit addMagazineCargoGlobal [_rifleDiverMagTwo,20];
			_unit addMagazineCargoGlobal [_grenade,8];
			_unit addMagazineCargoGlobal [_smoke,8];
			_unit addItemCargoGlobal [_diverUniform,2];
			_unit addItemCargoGlobal [_diverVest,2];
			_unit addBackpackCargoGlobal [_diverRuck ,2];
			{ _unit addItemCargoGlobal [_x ,2]; } forEach _basicTools;
			{ _unit addItemCargoGlobal [_x ,2]; } forEach _basicItems;
			{ _unit addItemCargoGlobal [_x,2]; } forEach _secTools;
			{ _unit addItemCargoGlobal [_x,2]; } forEach _secItems;
		};
		
		// ========================================
		// =========== Heavy Weapons Teams ========
		// ========================================
		
		case "hmgweapon" : {
			_unit addBackpackCargoGlobal [_hmgBarrel,1];
			_unit addBackpackCargoGlobal [_hmgTripod,1];
		};
		case "gmgweapon" : {
			_unit addBackpackCargoGlobal [_gmgTripod,1];
			_unit addBackpackCargoGlobal [_gmgBarrel,1];
		};
		
		// ========================================
		// ================= Tools ================
		// ========================================
		
		case "hqtools" : {
			{ _unit addItemCargoGlobal [_x ,8]; } forEach _basicTools;
			{ _unit addItemCargoGlobal [_x ,8]; } forEach _basicItems;
			{ _unit addItemCargoGlobal [_x ,5]; } forEach _pltTools;
			{ _unit addItemCargoGlobal [_x ,5]; } forEach _pltItems;
			_unit addItemCargoGlobal [_backupSight,8];
			_unit addItemCargoGlobal [_strobe,8];
		};
		case "sectiontools" : {
			{ _unit addItemCargoGlobal [_x,8]; } forEach _basicTools;
			{ _unit addItemCargoGlobal [_x,8]; } forEach _basicItems;
			_unit addItemCargoGlobal [_binos,2];
			_unit addItemCargoGlobal [_backupSight,8];
			_unit addItemCargoGlobal [_strobe,8];
			_unit addItemCargoGlobal [_spareBarrel,2];
		};
		case "engitools" : {
			_unit addItemCargoGlobal [_toolbox,2];
			_unit addItemCargoGlobal [_wireCutters,2];
			_unit addItemCargoGlobal [_defuseKit,2];
			_unit addBackpackCargoGlobal [_medRuck,2];
			_unit addItemCargoGlobal [_binos,2];
			_unit addItemCargoGlobal [_clackOne,2];
		};
		case "nightvisions" : {
			_unit addItemCargoGlobal [_nightVision,8];
		};
		case "nightgear" : {
			_unit addMagazineCargoGlobal [_glFlareOne,30];
			_unit addMagazineCargoGlobal [_glFlareTwo,30];
			_unit addMagazineCargoGlobal [_chemlightOne,30];
			_unit addMagazineCargoGlobal [_chemlightTwo,30];
			_unit addMagazineCargoGlobal [_handFlareOne,30];
			_unit addMagazineCargoGlobal [_handFlareTwo,30];
		};
		case "radiogear" : {
			_unit addItemCargoGlobal [_radioOne,20];
			_unit addItemCargoGlobal [_radioTwo,20];
			_unit addItemCargoGlobal [_radioFive,20];
		};
		case "uavgear" : {
			_unit addItemCargoGlobal [_uavTool,4];
			_unit addBackpackCargoGlobal [_uavRuck,4];
		};
		case "ctabgear" : {
			_unit addItemCargoGlobal [_tabletDevice,5];
			_unit addItemCargoGlobal [_androidDevice,5];
			_unit addItemCargoGlobal [_cameraDevice,10];
		};
		case "attachments" : {
			{ _unit addItemCargoGlobal [_x ,10]; } forEach _generalAttachments;
			{ _unit addItemCargoGlobal [_x ,5]; } forEach _autoRifleAttachments;
			{ _unit addItemCargoGlobal [_x ,5]; } forEach _mmgAttachments;
			{ _unit addItemCargoGlobal [_x ,10]; } forEach _pistolAttachments;
		};
		case "cableties" : {
			_unit addItemCargoGlobal [_cableTieItem,40];
		};
		case "flashabangs" : {
			_unit addItemCargoGlobal [_flashbang,40];
		};
		case "wetsuits" : {
			_unit addItemCargoGlobal [_diverUniform,8];
			_unit addItemCargoGlobal [_diverVest,8];
			_unit addBackpackCargoGlobal [_diverRuck ,8];
		};
		
		// ========================================
		// =================== Misc ===============
		// ========================================
		
		case "steerchutes" : {
			_unit addBackpackCargoGlobal [_steerableChute,30];
		};
		case "nonsteerchutes" : {
			_unit addBackpackCargoGlobal [_nonsteerableChute,30];
		};
		case "bananas" : {
			_unit addItemCargoGlobal [_bananas,10];
		};
		case "blank" : {
			// empties the crate if necessary, doesn't add anything. 
		};
		
		// ========================================
		// If no or incorrect case, present message.
		
		default {
			 systemChat format ["%1 completed gear assign: %2", name _unit, toUpper (_loadout)];	// hint that unit has finished gear assign
		};
		
		// ========================================
	};
};

// ================================================================================================================================================================