// AssignGear Functions
// by Mr. Agnet
// - Defines all of the functions as used by the assignGear.sqf script.

// ============================================================
// Declares variables

private ["_addClothes","_addBasics","_clearRuck","_addRuck","_addAttachments","_IFAK","_backpackToFront","_para","_postGearAssign","_addSidearm","_setIrons"];

// ============================================================
// AddClothes
// - Adds basic clothes/vest/helmet etc. to a unit
// - Possible Cases: leader, medic, gunner, gren, pleb, rpilot, rcrew, fpilot, crew, diver, sniper
// - Example: ["leader"] call _addClothes;

_addClothes = {
	private ["_type"];
	_type = toLower (_this select 0);
	if (_plebUniform != "") then { _unit forceAddUniform _plebUniform; };
	if (_goggles != "") then { _unit addGoggles _goggles; };
	
	switch (_type) do {
		case "leader" : {
			_unit addHeadgear _plebHelmet;
			_unit addVest _glVest;
		};
		case "medic" : {
			_unit addHeadgear _plebHelmet;
			_unit addVest _medVest;
		};
		case "gunner" : {
			_unit addHeadgear _plebHelmet;
			_unit addVest _gunnerVest;
		};
		case "gren" : {
			_unit addHeadgear _plebHelmet;
			_unit addVest _glVest;
		};
		case "pleb" : {
			_unit addHeadgear _plebHelmet;
			_unit addVest _plebVest;
		};
		case "rpilot" : {
			removeUniform _unit;
			_unit forceAddUniform _rpilotUniform;
			_unit addHeadgear _rotaryPilotHelmet;
			_unit addVest _pilotVest;
		};
		case "rcrew" : {
			removeUniform _unit;
			_unit forceAddUniform _rpilotUniform;
			_unit addHeadgear _rotaryCrewHelmet;
			_unit addVest _pilotVest;
		};
		case "fpilot" : {
			removeUniform _unit;
			_unit forceAddUniform _fpilotUniform;
			_unit addHeadgear _fixedPilotHelmet;
			_unit addVest _pilotVest;
		};
		case "crew" : {
			removeUniform _unit;
			_unit forceAddUniform _crewUniform;
			_unit addHeadgear _crewmanHelmet;
			_unit addVest _crewVest;
		};
		case "spec" : {
			removeUniform _unit;
			removeGoggles _unit;
			_unit forceAddUniform _specUniform;
			_unit addHeadgear _specHelmet;
			_unit addGoggles _specGoggles;
			_unit addVest _specVest;
		};
		case "diver" : {
			removeUniform _unit;
			removeGoggles _unit;
			_unit forceAddUniform _diverUniform;
			_unit addGoggles _divingGoggles;
			_unit addVest _diverVest;
		};
		case "sniper" : {
			removeUniform _unit;
			_unit forceAddUniform _sniperUniform;
			_unit addHeadgear _plebHelmet;
			_unit addVest _sniperVest;
		};
		default {
			hint format ["Incorrect clothes call for unit: %1",_unit];
			_unit addHeadgear _plebHelmet;
			_unit addVest _plebVest;
		};
	};
};

// ============================================================
// AddBasics
// - Adds the basic items and tools of the loadouts to a unit
// - Adds binocular item and cTab device depeding on parameters given
// - Example: ["binocular","android"] call _addBasics;

_addBasics = {
	private ["_bino","_dev"];
	_bino = toLower(_this select 0);
	_dev = toLower(_this select 1);
	sleep _delay;
	if ((!isNil "_nightGear") && _nightGear) then { 
		_basicTools = _basicTools + [_nightVision]; 
	};
	if ((!isNil "_nightGear") && _nightGear) then { 
		_basicItems = _basicItems + [_strobe]; 
	};
	{ _unit linkItem _x } foreach _basicTools; 		// add and assign each of the basic tools
	{ _unit addItem _x } foreach _basicItems; 		// add each of the basic items, can't be assigned
	sleep _delay;
	switch (_bino) do {
		case "rangefinder" : {
			_unit addWeapon _rangeFinder;
		};
		case "designator" : {
			_unit addWeapon _designator;
		};
		case "binocular" : {
			 _unit addWeapon _binos; 
		};
		case "none" : {};
		default {};
	};
	switch (_dev) do {
		case "tablet" : {
			if (_assignCTab) then {
				_unit linkItem _tabletDevice; 
				{_unit addItem _x} forEach _pltItems; 
			} else {
				{_unit linkItem _x} forEach _pltItems; 
			}; 
		};
		case "android" : {
			if (_assignCTab) then {
				_unit linkItem _tabletDevice; 
				{_unit addItem _x} forEach _secItems; 
			} else {
				{_unit linkItem _x} forEach _secItems; 
			}; 
		};
		case "microdagr" : {
			if (_assignCTab) then {
				_unit linkItem _tabletDevice; 
				{_unit addItem _x} forEach _secItems; 
			} else {
				{_unit linkItem _x} forEach _secItems; 
			}; 
		};
		case "none" : {};
		default {};
		if (_assignCTabCameras) then {_unit addItem _cameraDevice; };
	};
};


// ============================================================
// CLearRuck
// - Empties a unit's ruck
// - Example: call _clearRuck;

_clearRuck = {
	clearWeaponCargoGlobal (unitBackpack _unit);
	clearMagazineCargoGlobal (unitBackpack _unit);
	clearItemCargoGlobal (unitBackpack _unit);
	sleep _delay;
};

// ============================================================
// AddRuck
// - Adds ruck to the unit.
// - Adds a backpack and cargo to that backpack.
// - Note that these cases are mostly similar to the unit's loadout case.
// - Possible Cases: plt, medic, fac, uavop, secco, tl, ar, aar, rm, rmat, dmr, gren, mmg, mmgass, hat, aa, crew, pilot, hmggun, hmgass, gmggun, gmgass, diver, sniper, spotter
// - Example: ["plt"] call _addRuck;

_addRuck = {
	private ["_pack"];
	_pack = toLower(_this select 0);
	switch (_pack) do {
		case "coy" : {
			_unit addBackpack _medRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addItemCargoGlobal [_injectorOne,2];
			(unitBackpack _unit) addItemCargoGlobal [_injectorTwo,4];
			(unitBackpack _unit) addItemCargoGlobal [_radioFive,1];
			if (_gls) then {
				if (_3GLs) then {
					(unitBackpack _unit) addMagazineCargoGlobal [_rifleGLMag,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_rifleTracerMag,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_glExplody,4];
					(unitBackpack _unit) addMagazineCargoGlobal [_glSmokeOne,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_glSmokeTwo,2];
				} else {
					(unitBackpack _unit) addMagazineCargoGlobal [_rifleGLMag,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_rifleTracerMag,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_glExplody,6];
					(unitBackpack _unit) addMagazineCargoGlobal [_glSmokeOne,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_glSmokeTwo,2];
				};
			} else {
				(unitBackpack _unit) addMagazineCargoGlobal [_rifleGLMag,4];
				(unitBackpack _unit) addMagazineCargoGlobal [_grenade,2];
				(unitBackpack _unit) addMagazineCargoGlobal [_smoke,2];
			};
		};
		case "plt" : {
			_unit addBackpack _medRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addItemCargoGlobal [_injectorOne,2];
			(unitBackpack _unit) addItemCargoGlobal [_injectorTwo,4];
			(unitBackpack _unit) addItemCargoGlobal [_radioFive,1];
			if (_gls) then {
				if (_3GLs) then {
					(unitBackpack _unit) addMagazineCargoGlobal [_rifleGLMag,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_rifleTracerMag,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_glExplody,4];
					(unitBackpack _unit) addMagazineCargoGlobal [_glSmokeOne,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_glSmokeTwo,2];
				} else {
					(unitBackpack _unit) addMagazineCargoGlobal [_rifleGLMag,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_rifleTracerMag,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_glExplody,6];
					(unitBackpack _unit) addMagazineCargoGlobal [_glSmokeOne,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_glSmokeTwo,2];
				};
			} else {
				(unitBackpack _unit) addMagazineCargoGlobal [_rifleGLMag,4];
				(unitBackpack _unit) addMagazineCargoGlobal [_grenade,2];
				(unitBackpack _unit) addMagazineCargoGlobal [_smoke,2];
			};
		};
		case "medic" : {
			_unit addBackpack _largeRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addItemCargoGlobal [_bandageOne,60];	// bandages
			(unitBackpack _unit) addItemCargoGlobal [_injectorOne,20];	// morphine
			(unitBackpack _unit) addItemCargoGlobal [_injectorTwo,20];	// epinephrine
			(unitBackpack _unit) addItemCargoGlobal [_bloodThree,2];	// bloodbag 1000
			(unitBackpack _unit) addItemCargoGlobal [_bloodTwo,4];		// bloodbag 500
			(unitBackpack _unit) addItemCargoGlobal [_bloodOne,8];		// bloodbag 250
			(unitBackpack _unit) addItemCargoGlobal [_smoke,10];
		};
		case "fac" : {
			_unit addBackpack _medRuck;
			call _clearRuck;
			(unitBackpack _unit) addItemCargoGlobal [_designatorBat,1];
			(unitBackpack _unit) addItemCargoGlobal [_injectorTwo,2];
			(unitBackpack _unit) addItemCargoGlobal [_radioFive,1];
			if (_gls) then {
				(unitBackpack _unit) addMagazineCargoGlobal [_rifleGLMag,2];
				(unitBackpack _unit) addMagazineCargoGlobal [_rifleTracerMag,2];
				(unitBackpack _unit) addMagazineCargoGlobal [_glSmokeOne,2];
				(unitBackpack _unit) addMagazineCargoGlobal [_glSmokeTwo,2];
			} else {
				(unitBackpack _unit) addMagazineCargoGlobal [_rifleMag,4];
				(unitBackpack _unit) addMagazineCargoGlobal [_grenade,2];
			};
			{ (unitBackpack _unit) addMagazineCargoGlobal [_x ,2]; } forEach _facSmokes;
		};
		case "uavop" : {
			_unit addBackpack _uavRuck;
			call _clearRuck;
		};
		case "secco" : {
			_unit addBackpack _smallRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addItemCargoGlobal [_injectorOne,2];
			(unitBackpack _unit) addItemCargoGlobal [_injectorTwo,4];
			if (_gls) then {
				if (_3GLs) then {
					(unitBackpack _unit) addMagazineCargoGlobal [_rifleGLMag,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_rifleTracerMag,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_glExplody,4];
					(unitBackpack _unit) addMagazineCargoGlobal [_glSmokeOne,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_glSmokeTwo,2];
				} else {
					(unitBackpack _unit) addMagazineCargoGlobal [_rifleGLMag,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_rifleTracerMag,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_glExplody,6];
					(unitBackpack _unit) addMagazineCargoGlobal [_glSmokeOne,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_glSmokeTwo,2];
				};
			} else {
				(unitBackpack _unit) addMagazineCargoGlobal [_rifleGLMag,4];
				(unitBackpack _unit) addMagazineCargoGlobal [_grenade,2];
				(unitBackpack _unit) addMagazineCargoGlobal [_smoke,2];
			};
		};
		case "secmedic" : {
			_unit addBackpack _medRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addItemCargoGlobal [_bandageOne,20];	// bandages
			(unitBackpack _unit) addItemCargoGlobal [_injectorOne,8];	// morphine
			(unitBackpack _unit) addItemCargoGlobal [_injectorTwo,8];	// epinephrine
			(unitBackpack _unit) addItemCargoGlobal [_bloodThree,1];	// bloodbag 1000
			(unitBackpack _unit) addItemCargoGlobal [_bloodTwo,2];		// bloodbag 500
			(unitBackpack _unit) addItemCargoGlobal [_bloodOne,4];		// bloodbag 250
			(unitBackpack _unit) addItemCargoGlobal [_smoke,8];
			(unitBackpack _unit) addMagazineCargoGlobal [_autoRifleMag,1];
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleMag,4];
		};
		case "tl" : {
			_unit addBackpack _smallRuck;
			call _clearRuck;
			sleep _delay;
			if (_gls) then {
				if (_3GLs) then {
					(unitBackpack _unit) addMagazineCargoGlobal [_rifleGLMag,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_rifleTracerMag,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_glExplody,6];
					(unitBackpack _unit) addMagazineCargoGlobal [_glSmokeOne,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_glSmokeTwo,2];
				} else {
					(unitBackpack _unit) addMagazineCargoGlobal [_rifleGLMag,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_rifleTracerMag,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_glExplody,8];
					(unitBackpack _unit) addMagazineCargoGlobal [_glSmokeOne,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_glSmokeTwo,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_autoRifleMag,1];
				};
			} else {
				(unitBackpack _unit) addMagazineCargoGlobal [_rifleGLMag,4];
				(unitBackpack _unit) addMagazineCargoGlobal [_grenade,2];
				(unitBackpack _unit) addMagazineCargoGlobal [_smoke,2];
			};
		};
		case "ar" : {
			_unit addBackpack _medRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addMagazineCargoGlobal [_autoRifleMag,_arMagCount];
			(unitBackpack _unit) addMagazineCargoGlobal [_autoTracerMag,1];
			(unitBackpack _unit) addItemCargoGlobal [_spareBarrel,1];
		};
		case "aar" : {
			_unit addBackpack _medRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addMagazineCargoGlobal [_autoRifleMag,_arMagCount];
			(unitBackpack _unit) addMagazineCargoGlobal [_autoTracerMag,1];
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleMag,6];
			(unitBackpack _unit) addMagazineCargoGlobal [_grenade,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_smoke,2];
		};
		case "rm" : {
			_unit addBackpack _smallRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleMag,8];
			(unitBackpack _unit) addMagazineCargoGlobal [_grenade,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_smoke,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_autoRifleMag,1];
		};
		case "rmammo" : {
			_unit addBackpack _medRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleMag,10];
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleTracerMag,4];
			if (_gls) then {
				if (_3GLs) then {
					(unitBackpack _unit) addMagazineCargoGlobal [_glExplody,4];
					(unitBackpack _unit) addMagazineCargoGlobal [_grenade,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_smoke,2];
				} else {
					(unitBackpack _unit) addMagazineCargoGlobal [_glExplody,8];
					(unitBackpack _unit) addMagazineCargoGlobal [_grenade,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_smoke,2];
				};
			} else {
				(unitBackpack _unit) addMagazineCargoGlobal [_rifleMag,4];
				(unitBackpack _unit) addMagazineCargoGlobal [_rifleTracerMag,4];
			};
			(unitBackpack _unit) addMagazineCargoGlobal [_autoRifleMag,2];
		};
		case "rmat" : {
			_unit addBackpack _smallRuck;
			call _clearRuck;
			sleep _delay;
			if !(_latMag isEqualTo "NLAW_F") then { (unitBackpack _unit) addMagazineCargoGlobal [_latMag,1]; };
			sleep _delay;
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleMag,6];
			(unitBackpack _unit) addMagazineCargoGlobal [_grenade,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_smoke,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_autoRifleMag,1];
		};
		case "rmsc" : {
			_unit addBackpack _smallRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleScopedMag,8];
			(unitBackpack _unit) addMagazineCargoGlobal [_grenade,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_smoke,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_autoRifleMag,1];
		};
		case "dmr" : {
			_unit addBackpack _smallRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addMagazineCargoGlobal [_dmrMag,8];
			(unitBackpack _unit) addMagazineCargoGlobal [_grenade,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_smoke,2];
		};
		case "gren" : {
			_unit addBackpack _smallRuck;
			call _clearRuck;
			sleep _delay;
			if (_gls) then {
				if (_3GLs) then {
					(unitBackpack _unit) addMagazineCargoGlobal [_rifleGLMag,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_rifleTracerMag,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_glExplody,6];
					(unitBackpack _unit) addMagazineCargoGlobal [_glSmokeOne,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_glSmokeTwo,2];
				} else {
					(unitBackpack _unit) addMagazineCargoGlobal [_rifleGLMag,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_rifleTracerMag,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_glExplody,8];
					(unitBackpack _unit) addMagazineCargoGlobal [_glSmokeOne,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_glSmokeTwo,2];
					(unitBackpack _unit) addMagazineCargoGlobal [_autoRifleMag,1];
				};
			} else {
				(unitBackpack _unit) addMagazineCargoGlobal [_rifleMag,8];
				(unitBackpack _unit) addMagazineCargoGlobal [_grenade,2];
				(unitBackpack _unit) addMagazineCargoGlobal [_smoke,2];
				(unitBackpack _unit) addMagazineCargoGlobal [_autoRifleMag,1];
			};
		};
		case "mmg" : {
			_unit addBackpack _medRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addMagazineCargoGlobal [_mmgMag,2];
			(unitBackpack _unit) addItemCargoGlobal [_spareBarrel,1];	
		};
		case "mmgass" : {
			_unit addBackpack _medRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleMag,4];
			(unitBackpack _unit) addMagazineCargoGlobal [_mmgMag,4];	
		};
		case "matgun" : {
			_unit addBackpack _medRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addMagazineCargoGlobal [_matMag,2];
			sleep _delay;
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleMag,4];
		};
		case "matass" : {
			_unit addBackpack _medRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addMagazineCargoGlobal [_matMag,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleMag,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_grenade,1];
			(unitBackpack _unit) addMagazineCargoGlobal [_smoke,1];
		};
		case "hvymatgun" : {
			_unit addBackpack _largeRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleMag,4];
			(unitBackpack _unit) addMagazineCargoGlobal [_matMag,3];
		};
		case "hvymatass" : {
			_unit addBackpack _largeRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleMag,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_matMag,4];
		};
		case "hat" : {
			_unit addBackpack _medRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleMag,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_hatMag,1];
		};
		case "hatass" : {
			_unit addBackpack _largeRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleMag,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_hatMag,2];
		};
		case "aa" : {
			_unit addBackpack _medRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleMag,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_aaMag,1];
		};
		case "aaass" : {
			_unit addBackpack _largeRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleMag,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_aaMag,2];
		};
		case "crew" : {
			_unit addBackpack _medRuck;
			call _clearRuck;
			(unitBackpack _unit) addItemCargoGlobal [_radioFive,1];
			(unitBackpack _unit) addItemCargoGlobal [_toolbox,1];
		};
		case "crewman" : {
			_unit addBackpack _medRuck;
			call _clearRuck;
			(unitBackpack _unit) addItemCargoGlobal [_toolbox,1];
		};
		case "pilot" : {
			_unit addBackpack _smallRuck;
			call _clearRuck;
			(unitBackpack _unit) addItemCargoGlobal [_radioFive,1];
			{ (unitBackpack _unit) addMagazineCargoGlobal [_x ,2]; } forEach _facSmokes;
		};
		case "aircrew" : {
			_unit addBackpack _smallRuck;
			call _clearRuck;
			(unitBackpack _unit) addMagazineCargoGlobal [_carbineMag,4];
			{ (unitBackpack _unit) addMagazineCargoGlobal [_x ,2]; } forEach _facSmokes;
			(unitBackpack _unit) addItemCargoGlobal [_bandageOne,8];
			(unitBackpack _unit) addItemCargoGlobal [_injectorOne,4];
			(unitBackpack _unit) addItemCargoGlobal [_injectorTwo,4];
			(unitBackpack _unit) addItemCargoGlobal [_bloodTwo,2];
		};
		case "hmggun" : {
			_unit addBackpack _hmgBarrel;
		};
		case "hmgass" : {
			_unit addBackpack _hmgTripod;
		};
		case "gmggun" : {
			_unit addBackpack _gmgBarrel;
		};
		case "gmgass" : {
			_unit addBackpack _gmgTripod;
		};
		case "spectl" : {
			_unit addBackpack _smallRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleGLMag_spec,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleGLTracerMag_spec,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_glExplody,4];
			(unitBackpack _unit) addMagazineCargoGlobal [_glSmokeOne,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_glSmokeTwo,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_flashbang,2];
			(unitBackpack _unit) addItemCargoGlobal [_cableTieItem,4];
			(unitBackpack _unit) addItemCargoGlobal [_backupSight,1];
			(unitBackpack _unit) addItemCargoGlobal [_strobe,1];
		};
		case "specrm" : {
			_unit addBackpack _smallRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleMag_spec,6];
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleTracerMag_spec,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_grenade,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_smoke,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_flashbang,2];
			(unitBackpack _unit) addItemCargoGlobal [_cableTieItem,4];
			(unitBackpack _unit) addItemCargoGlobal [_backupSight,1];
			(unitBackpack _unit) addItemCargoGlobal [_strobe,1];
		};
		case "divertl" : {
			_unit addBackpack _diverRuck;
			call _clearRuck;
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleDiverMagOne,4];
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleDiverMagTwo,4];
			(unitBackpack _unit) addMagazineCargoGlobal [_grenade,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_smoke,2];
		};
		case "diver" : {
			_unit addBackpack _diverRuck;
			call _clearRuck;
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleDiverMagOne,4];
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleDiverMagTwo,4];
			(unitBackpack _unit) addMagazineCargoGlobal [_grenade,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_smoke,2];
		};
		case "sniper" : {
			_unit addBackpack _sniperRuck;
			call _clearRuck;
			(unitBackpack _unit) addMagazineCargoGlobal [_boltRifleMag,4];
			(unitBackpack _unit) addMagazineCargoGlobal [_grenade,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_smoke,2];
		};
		case "amsniper" : {
			_unit addBackpack _sniperRuck;
			call _clearRuck;
			(unitBackpack _unit) addMagazineCargoGlobal [_amRifleMag,4];
			(unitBackpack _unit) addMagazineCargoGlobal [_grenade,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_smoke,2];
		};
		case "spotter" : {
			_unit addBackpack _medRuck;
			call _clearRuck;
			(unitBackpack _unit) addItemCargoGlobal [_radioFive,1];
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleGLMag,4];
			(unitBackpack _unit) addItemCargoGlobal [_bandageOne,10];	// bandages
			(unitBackpack _unit) addItemCargoGlobal [_injectorOne,5];	// morphine
			(unitBackpack _unit) addItemCargoGlobal [_injectorTwo,5];	// epinephrine
			(unitBackpack _unit) addItemCargoGlobal [_bloodTwo,2];		// bloodbag
		};
		case "engi" : {
			_unit addBackpack _medRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addItemCargoGlobal [_toolbox,1];
			(unitBackpack _unit) addItemCargoGlobal [_wireCutters,1];
			(unitBackpack _unit) addItemCargoGlobal [_defuseKit,1];
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleMag,4];
			(unitBackpack _unit) addMagazineCargoGlobal [_grenade,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_smoke,2];
		};
		case "carb" : {
			_unit addBackpack _smallRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addMagazineCargoGlobal [_carbineMag,8];
			(unitBackpack _unit) addMagazineCargoGlobal [_grenade,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_smoke,2];
		};
		case "smg" : {
			_unit addBackpack _smallRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addMagazineCargoGlobal [_smgMag,8];
			(unitBackpack _unit) addMagazineCargoGlobal [_grenade,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_smoke,2];
		};
		case "shotty" : {
			_unit addBackpack _smallRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addMagazineCargoGlobal [_shottyBuck,4];
			(unitBackpack _unit) addMagazineCargoGlobal [_shottySlug,4];
			(unitBackpack _unit) addMagazineCargoGlobal [_grenade,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_smoke,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_flashbang,2];
		};
		case "demoman" : {
			_unit addBackpack _medRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addMagazineCargoGlobal [_rifleMag,4];
			(unitBackpack _unit) addMagazineCargoGlobal [_grenade,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_smoke,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_demoCharge,4];
			(unitBackpack _unit) addMagazineCargoGlobal [_satchelCharge,1];
			(unitBackpack _unit) addItemCargoGlobal [_clackOne,1];
			(unitBackpack _unit) addItemCargoGlobal [_defuseKit,1];
		};
		case "heavygren" : {
			_unit addBackpack _medRuck;
			call _clearRuck;
			sleep _delay;
			(unitBackpack _unit) addMagazineCargoGlobal [_glHeavyExplody,4];
			(unitBackpack _unit) addMagazineCargoGlobal [_glHSmokesOne,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_glHSmokesTwo,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_grenade,2];
			(unitBackpack _unit) addMagazineCargoGlobal [_smoke,2];
		};
		case "ladder" : {
			_unit addBackpack _ladder;
		};
		default {
			_unit groupChat format ["Incorrect ruck call for unit: %1",_unit];
		};
	};
};

// ============================================================
// AddAttachments
// - Addition of Attachments to a Unit's weapon
// - Follows ["case",scopes,suppressors] call _addAttachments;
// - Example: ["general",false,true] call _addAttachments; = general attachments with no scopes or suppressors.
// - Example: ["dmr",true,true] call _addAttachments; = dmr attachments with scopes and suppressors.
// - Example: ["general",false,true] call _addAttachments; = general attachments, no scopes, with suppressors.
// - Possible Cases: general, dmr, ar, mmg, sniper, amsniper, diver, carlito
// - Note that pistol attachments are done regardless of what is called.
// - Parameters dictate attachments for all instances of that role.

_addAttachments = {
	private ["_type","_sco","_sup","_primaryAttachments","_secondaryAttachments","_handgunAttachments"];
	
	_type =  toLower (_this select 0);
	_sco = _this select 1;
	_sup = _this select 2;
	
	_primaryAttachments = primaryWeaponItems _unit;
	_secondaryAttachments = secondaryWeaponItems _unit;
	_handgunAttachments = handgunItems _unit;
	
	if ((!isNil "_nightGear") && _nightGear) then { 
		private "_test";
		_test = _generalAttachments find "acc_flashlight";
		if (_test != -1) then {_generalAttachments set [_test,"acc_pointer_IR"]};
		_test = _dmrAttachments find "acc_flashlight";
		if (_test != -1) then {_dmrAttachments set [_test,"acc_pointer_IR"]};
		_test = _autoRifleAttachments find "acc_flashlight";
		if (_test != -1) then {_autoRifleAttachments set [_test,"acc_pointer_IR"]};
		_test = _mmgAttachments find "acc_flashlight";
		if (_test != -1) then {_mmgAttachments set [_test,"acc_pointer_IR"]};
		_test = _scoped_generalAttachments find "acc_flashlight";
		if (_test != -1) then {_scoped_generalAttachments set [_test,"acc_pointer_IR"]};
		_test = _scoped_dmrAttachments find "acc_flashlight";
		if (_test != -1) then {_scoped_dmrAttachments set [_test,"acc_pointer_IR"]};
		_test = _scoped_autoRifleAttachments find "acc_flashlight";
		if (_test != -1) then {_scoped_autoRifleAttachments set [_test,"acc_pointer_IR"]};
		_test = _scoped_mmgAttachments find "acc_flashlight";
		if (_test != -1) then {_scoped_mmgAttachments set [_test,"acc_pointer_IR"]};
		_test = _generalAttachments find "CUP_acc_Flashlight";
		if (_test != -1) then {_generalAttachments set [_test,"CUP_acc_ANPEQ_2"]};
		_test = _dmrAttachments find "CUP_acc_Flashlight";
		if (_test != -1) then {_dmrAttachments set [_test,"CUP_acc_ANPEQ_2"]};
		_test = _autoRifleAttachments find "CUP_acc_Flashlight";
		if (_test != -1) then {_autoRifleAttachments set [_test,"CUP_acc_ANPEQ_2"]};
		_test = _mmgAttachments find "CUP_acc_Flashlight";
		if (_test != -1) then {_mmgAttachments set [_test,"CUP_acc_ANPEQ_2"]};
		_test = _scoped_generalAttachments find "CUP_acc_Flashlight";
		if (_test != -1) then {_scoped_generalAttachments set [_test,"CUP_acc_ANPEQ_2"]};
		_test = _scoped_dmrAttachments find "CUP_acc_Flashlight";
		if (_test != -1) then {_scoped_dmrAttachments set [_test,"CUP_acc_ANPEQ_2"]};
		_test = _scoped_autoRifleAttachments find "CUP_acc_Flashlight";
		if (_test != -1) then {_scoped_autoRifleAttachments set [_test,"CUP_acc_ANPEQ_2"]};
		_test = _scoped_mmgAttachments find "CUP_acc_Flashlight";
		if (_test != -1) then {_scoped_mmgAttachments set [_test,"CUP_acc_ANPEQ_2"]};
	};
	
	if ((!isNil "_scopes") && _scopes == 2) then {
		_backupSight = _generalAttachments select 0;
		_generalAttachments = _scoped_generalAttachments;
		_dmrAttachments = _scoped_dmrAttachments;
		_autoRifleAttachments = _scoped_autoRifleAttachments;
		_mmgAttachments = _scoped_mmgAttachments;
	};
	
	if ((!isNil "_suppressors") && _suppressors) then {
		_generalAttachments = _generalAttachments + _suppressed_generalAttachments;
		_dmrAttachments = _dmrAttachments + _suppressed_dmrAttachments;
		_autoRifleAttachments = _autoRifleAttachments + _suppressed_autoRifleAttachments;
		_mmgAttachments = _mmgAttachments + _suppressed_mmgAttachments;
		_specOpsAttachments = _specOpsAttachments + _specOpsAttachments;
		_scoped_generalAttachments = _scoped_generalAttachments + _suppressed_generalAttachments;
		_scoped_dmrAttachments = _scoped_dmrAttachments + _suppressed_dmrAttachments;
		_scoped_autoRifleAttachments = _scoped_autoRifleAttachments + _suppressed_autoRifleAttachments;
		_scoped_mmgAttachments = _scoped_mmgAttachments + _suppressed_mmgAttachments;
		_scoped_specOpsAttachments = _scoped_specOpsAttachments + _suppressed_specOpsAttachments;
		_pistolAttachments = _pistolAttachments + _suppressed_pistolAttachments;
	};
	
	if (!isNil "_primaryAttachments") then { { _unit removePrimaryWeaponItem _x } forEach _primaryAttachments; };
	if (!isNil "_secondaryAttachments") then { { _unit removeSecondaryWeaponItem _x } forEach _secondaryAttachments; };
	if (!isNil "_handgunAttachments") then { { _unit removeHandgunItem _x } forEach _handgunAttachments; };
	
	switch (_type) do {
		case "general" : {
			if !(_sco) then { if (!isNil "_generalAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _generalAttachments; }; };
			if (_sco) then { if (!isNil "_scoped_generalAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _scoped_generalAttachments; }; };
			if (_sup) then { if (!isNil "_suppressed_generalAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _suppressed_generalAttachments; }; };
			if (!isNil "_pistolAttachments") then { { _unit addHandgunItem _x } forEach _pistolAttachments; };
			if (_sup) then { if (!isNil "_suppressed_pistolAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _suppressed_pistolAttachments; }; };
		};
		case "dmr" : {
			if !(_sco) then { if (!isNil "_dmrAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _dmrAttachments; }; };
			if (_sco) then { if (!isNil "_scoped_dmrAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _scoped_dmrAttachments; }; };
			if (_sup) then { if (!isNil "_suppressed_dmrAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _suppressed_dmrAttachments; }; };
			if (!isNil "_pistolAttachments") then { { _unit addHandgunItem _x } forEach _pistolAttachments; };
			if (_sup) then { if (!isNil "_suppressed_pistolAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _suppressed_pistolAttachments; }; };
		};
		case "ar" : {
			if !(_sco) then { if (!isNil "_autoRifleAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _autoRifleAttachments; }; };
			if (_sco) then { if (!isNil "_scoped_autoRifleAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _scoped_autoRifleAttachments; }; };
			if (_sup) then { if (!isNil "_suppressed_autoRifleAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _suppressed_autoRifleAttachments; }; };
			if (!isNil "_pistolAttachments") then { { _unit addHandgunItem _x } forEach _pistolAttachments; };
			if (_sup) then { if (!isNil "_generalAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _suppressed_pistolAttachments; }; };
		};
		case "mmg" : {
			if !(_sco) then { if (!isNil "_mmgAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _mmgAttachments; }; };
			if (_sco) then { if (!isNil "_scoped_mmgAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _scoped_mmgAttachments; }; };
			if (_sup) then { if (!isNil "_suppressed_mmgAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _suppressed_mmgAttachments; }; };
			if (!isNil "_pistolAttachments") then { { _unit addHandgunItem _x } forEach _pistolAttachments; };
			if (_sup) then { if (!isNil "_suppressed_pistolAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _suppressed_pistolAttachments; }; };
		};
		case "sniper" : {
			if (!isNil "_sniperAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _sniperAttachments; };
			if (!isNil "_pistolAttachments") then { { _unit addHandgunItem _x } forEach _pistolAttachments; };
			if (_sup) then { if (!isNil "_suppressed_pistolAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _suppressed_pistolAttachments; }; };
		};
		case "amsniper" : {
			if (!isNil "_amsniperAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _amsniperAttachments; };
			if (!isNil "_pistolAttachments") then { { _unit addHandgunItem _x } forEach _pistolAttachments; };
			if (_sup) then { if (!isNil "_suppressed_pistolAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _suppressed_pistolAttachments; }; };
		};
		case "specops" : {
			if !(_sco) then { if (!isNil "_specOpsAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _specOpsAttachments; }; };
			if (_sco) then { if (!isNil "_scoped_specOpsAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _scoped_specOpsAttachments; }; };
			if (_sup) then { if (!isNil "_suppressed_specOpsAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _suppressed_specOpsAttachments; }; };
			if (!isNil "_pistolAttachments") then { { _unit addHandgunItem _x } forEach _pistolAttachments; };
			if (_sup) then { if (!isNil "_suppressed_pistolAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _suppressed_pistolAttachments; }; };
		};    
		case "diver" : {
			if (!_underwaterWeapons && !_sco) then { if (!isNil "_generalAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _generalAttachments; }; };
			if (!isNil "_pistolAttachments") then { { _unit addHandgunItem _x } forEach _pistolAttachments; };
			if (_sup) then { if (!isNil "_suppressed_pistolAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _suppressed_pistolAttachments; }; };
		};

		// ===================================================================================================
		// M3 Scope
		
		case "carlito" : {
			if !(_sco) then { if (!isNil "_generalAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _generalAttachments; }; };
			if (_sco) then { if (!isNil "_scoped_generalAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _scoped_generalAttachments; }; };
			if (_sup) then { if (!isNil "_suppressed_generalAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _suppressed_generalAttachments; }; };
			if (!isNil "_pistolAttachments") then {{ _unit addHandgunItem _x } forEach _pistolAttachments;};
			if (_sup) then { if (!isNil "_suppressed_pistolAttachments") then { { _unit addPrimaryWeaponItem _x } forEach _suppressed_pistolAttachments; }; };
			if (!isNil "_matScope") then {  _unit addSecondaryWeaponItem _matScope; };
		};
		
		// ===================================================================================================
		
		// Add no attachments
		case "none" : {};
		
		// ===================================================================================================
		
		default {
			_unit groupChat format ["Incorrect attachment call for unit: %1",_unit];
		};
	};
};

// ============================================================
// IFAK
// - Basic Meds for the unit
// - Example: ["vest"] call _IFAK;
// - Example: ["pack"] call _IFAK;
// - Array parameter dictates whether meds go into backpack or vest for all instances of that role. 
// - Use "pack"/"vest"/"uniform"
// - default is pack

_IFAK = {
	private ["_location"];
	_location = toLower(_this select 0); 
	switch (_location) do {
		case "pack" : {
			if (!isNull (unitBackpack _unit)) then {
				(unitBackpack _unit) addItemCargoGlobal [_bandageOne,6];
				(unitBackpack _unit) addItemCargoGlobal [_injectorOne,2];
			};
		};
		case "vest" : {
			for "_i" from 1 to 6 do {_unit addItemToVest _bandageOne};
			for "_i" from 1 to 2 do {_unit addItemToVest _injectorOne};
		};
		case "uniform" : {
			for "_i" from 1 to 6 do {_unit addItemToUniform _bandageOne};
			for "_i" from 1 to 2 do {_unit addItemToUniform _injectorOne};
		};
		default {
			if (!isNull (unitBackpack _unit)) then {
				(unitBackpack _unit) addItemCargoGlobal [_bandageOne,6];
				(unitBackpack _unit) addItemCargoGlobal [_injectorOne,2];
			};
		};
	};
};

// ============================================================
// BackpackToFront
// - Backpack becomes Frontpack, thanks to @backpackonchest
// - Example: call _backpackToFront;
// - Automatically calls this function before adding parachutes, if parachutes are enabled.

_backpackToFront = {
	waitUntil {!isNil "Zade_BOC_fnc_BackpackOnChest"};
	[_unit] call Zade_BOC_fnc_BackpackOnChest;
};


// ============================================================
// Para
// - If parachutes enabled, place backpack on front, then add the parachute.
// - "steerable" - adds steerable chutes.
// - "nonsteerable" - adds nonsteerable chutes.
// - "none" - adds no chutes.
// - wow

_para = {
	switch (_parachutes) do {
		case "steerable" : {
			// roles in this array will not receive parachutes.
			if !(_loadout in ["rotarypilot","fixedpilot","crewmander","crewman"]) then { 
				 call _backpackToFront;
				 sleep _delay;
				 _unit addBackpack _steerableChute;
			};
		};
		case "nonsteerable" : {
			// roles in this array will not receive parachutes.
			if !(_loadout in ["rotarypilot","fixedpilot","crewmander","crewman"]) then { 
				 call _backpackToFront;
				 sleep _delay;
				 _unit addBackpack _nonsteerableChute;
			};
		};
		case "none" : {};
		default {};
	};
};

// ============================================================
// PostGearAssign
// - Handles extra stuff, like backup sights, cable ties, flashbangs and parachutes.

_postGearAssign = {
	// Scope Stuff
	// If scopes are enabled, add a secondary reflex sight to unit's vests, except for those in this array. Deemed unnecessary for those roles by Agnet.
	if (_scopes == 2) then {
		if !(_loadout in ["rotarypilot","rotarycrew","fixedpilot","sniper","amsniper","spectl","specrm","divertl","diver"]) then { 
			_unit addItemToVest _backupSight;
		};
	};

	// Parachute Function
	// See variable @ top for more details. If false, does nothing.
	call _para;
	
	// Iron Sight Function
	// Removes weapon sights if applicable. 
	if !(_loadout in ["sniper","amsniper","spotter","spectl","specrm","dmr"]) then { call _setIrons; };

	// Flashbang Stuff
	// add/remove roles to/from this array for them to be/not be equipped with cableties, when enabled. 
	if (_loadout in ["pltld","secco","sectl","ar","aar","rm","rmat","rmsc","dmr","gren","mmg","mmgass","divertl","diver","carbineer","smgunner"]) then {
		if (!isNil "_flashbangs" && _flashbangs > 0) then { 
			for "_i" from 1 to (round _flashbangs) do { (unitBackpack _unit) addMagazineCargoGlobal [_flashbang,1]; }; 
		};
	};

	// Cable Tie Stuff
	// Add/remove roles to/from this array for them to be/not be equipped with cableties, when enabled. 
	if (_loadout in ["pltld","secco","sectl","ar","aar","rm","rmat","rmsc","dmr","gren","mmg","mmgass","divertl","diver","carbineer","smgunner"]) then {
		if (!isNil "_cableTies" && _cableTies > 0) then {
			for "_i" from 1 to (round _cableTies) do { _unit addItemToUniform _cableTieItem }; 
		};
	};
	
	// ACE Map Lights
	if ((_mapLight in items _unit) && ((date select 3 <= 6) || (date select 3 >= 18))) then {
		[_mapLight] call ace_map_fnc_switchFlashlight; [""] call ace_map_fnc_flashlightGlow; 
	};
	
	// ACE Map Tools
	ace_maptools_mapTool_Shown = 2;
	
	// Reset Stamina
	
	_unit setStamina 200;
	
};

// ============================================================
// Sidearm
// - Adds applicable sidearm if variable is true and function is called. 

_addSidearm = {
	if (_sideArms) then {
		for "_i" from 1 to 3 do {_unit addItemToVest _pistolMag};
		_unit addWeapon _pistol;
	};
};

// ============================================================
// Iron Sights
// - Removes added optic

_setIrons = {
	private ["_items","_attachment"];
	if (_scopes == 0) then {
		_items = _unit weaponAccessories (primaryWeapon _unit);
		_attachment = _items select 2;
		if (!isNil "_attachment" && {_attachment != ""}) then { _unit removePrimaryWeaponItem _attachment; };
	};
};

// ============================================================