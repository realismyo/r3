// AssignGear - Weapons
// by Mr. Agnet
// - Defines all of the weapon classes to be used by the assignGear script.
// - Current Class: MP5
// - Primary Mod: HLC
// - Variables: None.

// =======================================================================
// Declares variables

private [
"_rifle","_rifleGL","_rifleScoped","_autoRifle","_carbine","_dmr","_mmg","_smg","_pistol",
"_rifle_spec","_rifleGL_spec",
"_rifleMag","_rifleMatchMag","_rifleTracerMag","_rifleGLMag","_rifleScopedMag","_autoRifleMag","_autoTracerMag","_carbineMag","_dmrMag","_mmgMag","_mmgTracerMag","_smgMag","_pistolMag",
"_rifleMag_spec","_rifleTracerMag_spec","_rifleGLMag_spec","_rifleGLTracerMag_spec",
"_glExplody","_glSmokeOne","_glSmokeTwo","_glFlareOne","_glFlareTwo",
"_shotty","_shottyBuck","_shottySlug","_glHeavy","_glHeavyExplody","_glHSmokesOne","_glHSmokesTwo",
"_lat","_latMag","_matLaunch","_matMag","_matScope","_hatLaunch","_hatMag","_aaLaunch","_aaMag",
"_boltRifle","_boltRifleMag","_amRifle","_amRifleMag",
"_hmgBarrel","_hmgTripod","_hmgMag","_gmgBarrel","_gmgTripod","_gmgMag",
"_rifleDiver","_rifleDiverMagOne","_rifleDiverMagTwo",
"_generalAttachments","_dmrAttachments","_autoRifleAttachments","_mmgAttachments","_specOpsAttachments",
"_sniperAttachments","_amSniperAttachments","_pistolAttachments",
"_scoped_generalAttachments","_scoped_dmrAttachments","_scoped_autoRifleAttachments","_scoped_mmgAttachments","_scoped_specOpsAttachments",
"_suppressed_generalAttachments","_suppressed_dmrAttachments","_suppressed_autoRifleAttachments","_suppressed_mmgAttachments","_suppressed_specOpsAttachments",
"_suppressed_pistolAttachments","_arMagCount"
];

_arMagCount = 2;

// =======================================================================
// =========================== General Weapons ===========================
// =======================================================================

_rifle = "hlc_smg_MP5N";
_rifleGL = "hlc_smg_9mmar";
_rifleScoped = "hlc_smg_MP5N";
_autoRifle = "hlc_m249_pip4";
_carbine = "hlc_smg_MP5N";
_dmr = "rhs_weap_m14ebrri";
_mmg = "rhs_weap_m240B_CAP";
_smg = "hlc_smg_MP5N";
_pistol = "rhsusf_weap_m1911a1";

// ================= SpecOps Weapons ==============

_rifle_spec = "hlc_smg_MP5N";
_rifleGL_spec = "hlc_smg_9mmar";

// ================= General Magazines ============

_rifleMag = "hlc_30Rnd_9x19_B_MP5";
_rifleMatchMag = "hlc_30Rnd_9x19_B_MP5";
_rifleTracerMag = "hlc_30Rnd_9x19_B_MP5";
_rifleGLMag = "hlc_30Rnd_9x19_B_MP5";
_rifleScopedMag = "hlc_30Rnd_9x19_B_MP5";
_autoRifleMag = "hlc_200rnd_556x45_M_SAW";
_autoTracerMag = "hlc_200rnd_556x45_T_SAW";
_carbineMag = "hlc_30Rnd_9x19_B_MP5";
_dmrMag = "rhsusf_20Rnd_762x51_m118_special_Mag";
_mmgMag = "rhsusf_100Rnd_762x51";
_mmgTracerMag = "rhsusf_100Rnd_762x51";
_smgMag = "hlc_30Rnd_9x19_B_MP5";
_pistolMag = "rhsusf_mag_7x45acp_MHP";

// ================= SpecOps Magazines ============

_rifleMag_spec = "hlc_30Rnd_9x19_B_MP5";
_rifleTracerMag_spec = "hlc_30Rnd_9x19_B_MP5";
_rifleGLMag_spec = "hlc_30Rnd_9x19_B_MP5";
_rifleGLTracerMag_spec = "hlc_30Rnd_9x19_B_MP5";

// ===================== GL Rounds ================

_glExplody = "1Rnd_HE_Grenade_shell";
_glSmokeOne = "1Rnd_SmokeGreen_Grenade_shell";
_glSmokeTwo = "1Rnd_SmokeRed_Grenade_shell";
_glFlareOne = "UGL_FlareGreen_F";
_glFlareTwo = "UGL_FlareRed_F";

// =======================================================================
// =========================== Weapons Teams =============================
// =======================================================================

// ================== Shotgun =====================

_shotty = "rhs_weap_M590_8RD";
_shottyBuck = "rhsusf_8Rnd_00Buck";
_shottySlug = "rhsusf_8Rnd_Slug";

// ============== Heavy Grenadier =================

_glHeavy = "rhs_weap_m32";
_glHeavyExplody = "rhsusf_mag_6Rnd_M433_HEDP";
_glHSmokesOne = "rhsusf_mag_6Rnd_M715_green";
_glHSmokesTwo = "rhsusf_mag_6Rnd_M713_red";

// =================== LAT ========================

_lat = "rhs_weap_M136_hedp";
_latMag = "rhs_m136_hedp_mag";

// =================== MAT ========================

_matLaunch = "CUP_launch_MAAWS";
_matMag = "CUP_MAAWS_HEAT_M";
_matScope = "CUP_optic_MAAWS_Scope";

// =================== HAT ========================

_hatLaunch = "rhs_weap_fgm148";
_hatMag = "rhs_fgm148_magazine_AT";

// =================== Anti-Air ===================

_aaLaunch = "rhs_weap_fim92";
_aaMag = "rhs_fim92_mag";

// ==================== Snipers ===================

_boltRifle = "hlc_rifle_awmagnum_BL";
_boltRifleMag = "hlc_5rnd_300WM_FMJ_AWM";
_amRifle = "srifle_LRR_F"; 
_amRifleMag = "7Rnd_408_Mag";


// ================= HMG Team =====================

_hmgBarrel = "B_HMG_01_weapon_F";
_hmgTripod = "B_HMG_01_support_F";
_hmgMag = "";	// no magazines as of yet

// ================= GMG Team =====================

_gmgBarrel = "B_GMG_01_weapon_F";
_gmgTripod = "B_HMG_01_support_F";
_gmgMag = "";	// no magazines as of yet

// ==================== Diver =====================

if (_underwaterWeapons) then { 
	_rifleDiver = "arifle_SDAR_F"; 
} else { 
	_rifleDiver = _rifle; 
};
if (_underwaterWeapons) then { 
	_rifleDiverMagOne = "30Rnd_556x45_Stanag";	// standard mag
	_rifleDiverMagTwo = "20Rnd_556x45_UW_mag";	// underwater mag
} else { 
	_rifleDiverMagOne = _rifleMag;
	_rifleDiverMagTwo = _rifleMag;
};

// ================ Attachments ===================

_generalAttachments = ["rhsusf_acc_eotech_552","acc_flashlight"];
_dmrAttachments = ["rhsusf_acc_eotech_552","acc_flashlight","bipod_01_F_blk"];
_autoRifleAttachments = ["rhsusf_acc_eotech_552","acc_flashlight","bipod_01_F_blk"];
_mmgAttachments = ["rhsusf_acc_eotech_552","acc_flashlight"];
_specOpsAttachments = ["rhsusf_acc_eotech_552","acc_pointer_IR"]; 
_sniperAttachments = ["optic_SOS"];
_amSniperAttachments = ["optic_SOS"];
_pistolAttachments = ["optic_MRD"];

_scoped_generalAttachments = ["rhsusf_acc_ACOG3","acc_flashlight"]; 
_scoped_dmrAttachments = ["optic_MRCO","acc_flashlight","bipod_01_F_blk"];
_scoped_autoRifleAttachments = ["rhsusf_acc_ELCAN","acc_flashlight","bipod_01_F_blk"];
_scoped_mmgAttachments = ["rhsusf_acc_ELCAN","acc_flashlight"];
_scoped_specOpsAttachments = ["rhsusf_acc_ACOG3","acc_pointer_IR"];

_suppressed_generalAttachments = ["hlc_muzzle_Agendasix"];
_suppressed_dmrAttachments = [];
_suppressed_autoRifleAttachments = ["muzzle_snds_M"];
_suppressed_mmgAttachments = [];
_suppressed_specOpsAttachments = ["hlc_muzzle_Agendasix"];
_suppressed_pistolAttachments = [];

// =======================================================================