// AssignGear - Weapons
// by Mr. Agnet
// - Defines all of the weapon classes to be used by the assignGear script.
// - Current Class: AKs
// - Primary Mod: RHS
// - Variables: None.

// =======================================================================
// Declares variables

private [
"_rifle","_rifleGL","_rifleScoped","_autoRifle","_carbine","_dmr","_mmg","_smg","_pistol",
"_rifle_spec","_rifleGL_spec",
"_rifleMag","_rifleMatchMag","_rifleTracerMag","_rifleGLMag","_rifleScopedMag","_autoRifleMag","_autoTracerMag","_carbineMag","_dmrMag","_mmgMag","_mmgTracerMag","_smgMag","_pistolMag",
"_rifleMag_spec","_rifleTracerMag_spec","_rifleGLMag_spec","_rifleGLTracerMag_spec",
"_glExplody","_glSmokeOne","_glSmokeTwo","_glFlareOne","_glFlareTwo",
"_shotty","_shottyBuck","_shottySlug","_glHeavy","_glHeavyExplody","_glHSmokesOne","_glHSmokesTwo",
"_lat","_latMag","_matLaunch","_matMag","_matScope","_hatLaunch","_hatMag","_aaLaunch","_aaMag",
"_boltRifle","_boltRifleMag","_amRifle","_amRifleMag",
"_hmgBarrel","_hmgTripod","_hmgMag","_gmgBarrel","_gmgTripod","_gmgMag",
"_rifleDiver","_rifleDiverMagOne","_rifleDiverMagTwo",
"_generalAttachments","_dmrAttachments","_autoRifleAttachments","_mmgAttachments","_specOpsAttachments",
"_sniperAttachments","_amSniperAttachments","_pistolAttachments",
"_scoped_generalAttachments","_scoped_dmrAttachments","_scoped_autoRifleAttachments","_scoped_mmgAttachments","_scoped_specOpsAttachments",
"_suppressed_generalAttachments","_suppressed_dmrAttachments","_suppressed_autoRifleAttachments","_suppressed_mmgAttachments","_suppressed_specOpsAttachments",
"_suppressed_pistolAttachments","_arMagCount"
];

_arMagCount = 3;

// =======================================================================
// =========================== General Weapons ===========================
// =======================================================================

_rifle = "rhs_weap_ak74m";
_rifleGL = "rhs_weap_ak74m_gp25";
_rifleScoped = "rhs_weap_ak74m";
_carbine = "rhs_weap_ak74m";
_autoRifle = "hlc_rifle_rpk74n";
_dmr = "rhs_weap_svds_npz";
_mmg = "rhs_weap_pkp";
_smg = "hlc_rifle_aks74u";
_pistol = "rhs_weap_makarov_pmm";

// ================== SpecOps Weapons =============

_rifle_spec = "rhs_weap_ak74m";
_rifleGL_spec = "rhs_weap_ak74m_gp25";

// ================= General Magazines ============

_rifleMag = "rhs_30Rnd_545x39_AK";
_rifleMatchMag = "rhs_30Rnd_545x39_7N22_AK";
_rifleTracerMag = "rhs_30Rnd_545x39_AK_green";
_rifleGLMag = "rhs_30Rnd_545x39_AK";
_rifleScopedMag = "rhs_30Rnd_545x39_AK";
_autoRifleMag = "hlc_60Rnd_545x39_t_rpk";
_autoTracerMag = "hlc_60Rnd_545x39_t_rpk";
_carbineMag = "rhs_30Rnd_545x39_AK";
_dmrMag = "rhs_10Rnd_762x54mmR_7N1";
_mmgMag = "rhs_100Rnd_762x54mmR";
_mmgTracerMag = "rhs_100Rnd_762x54mmR_green";
_smgMag = "hlc_30Rnd_545x39_B_AK";
_pistolMag = "rhs_mag_9x18_12_57N181S";

// ================= SpecOps Magazines ============

_rifleMag_spec = "rhs_30Rnd_545x39_AK";
_rifleTracerMag_spec = "rhs_30Rnd_545x39_AK_green";
_rifleGLMag_spec = "rhs_30Rnd_545x39_AK";
_rifleGLTracerMag_spec = "rhs_30Rnd_545x39_AK_green";

// ===================== GL Rounds ================

_glExplody = "rhs_VOG25";
_glSmokeOne = "rhs_GRD40_Green";
_glSmokeTwo = "rhs_GRD40_Red";
_glFlareOne = "rhs_VG40OP_green";
_glFlareTwo = "rhs_VG40OP_red";
		
// =======================================================================
// =========================== Weapons Teams =============================
// =======================================================================

// ================== Shotgun =====================

_shotty = "rhs_weap_M590_8RD";
_shottyBuck = "rhsusf_8Rnd_00Buck";
_shottySlug = "rhsusf_8Rnd_Slug";

// ============== Heavy Grenadier =================

_glHeavy = "rhs_weap_m32";
_glHeavyExplody = "rhsusf_mag_6Rnd_M433_HEDP";
_glHSmokesOne = "rhsusf_mag_6Rnd_M715_green";
_glHSmokesTwo = "rhsusf_mag_6Rnd_M713_red";

// =================== LAT ========================

_lat = "rhs_weap_rpg26";
_latMag = "rhs_rpg26_mag";

// =================== MAT ========================

_matLaunch = "launch_RPG32_F";
_matMag = "RPG32_F";
_matScope = "CUP_optic_MAAWS_Scope";

// =================== HAT ========================

_hatLaunch = "launch_O_Titan_short_F";
_hatMag = "Titan_AT";

// =================== Anti-Air ===================

_aaLaunch = "rhs_weap_igla";
_aaMag = "rhs_mag_9k38_rocket";

// ==================== Snipers ===================

_boltRifle = "hlc_rifle_awmagnum_BL";
_boltRifleMag = "hlc_5rnd_300WM_FMJ_AWM";
_amRifle = "srifle_LRR_F"; 
_amRifleMag = "7Rnd_408_Mag";

// ================= HMG Team =====================

_hmgBarrel = "O_HMG_01_weapon_F";
_hmgTripod = "O_HMG_01_support_F";
_hmgMag = "";	// no magazines as of yet

// ================= GMG Team =====================

_gmgBarrel = "O_GMG_01_weapon_F";
_gmgTripod = "O_HMG_01_support_F";
_gmgMag = "";	// no magazines as of yet

// ==================== Diver =====================

if (_underwaterWeapons) then { 
	_rifleDiver = "arifle_SDAR_F"; 
} else { 
	_rifleDiver = _rifle; 
};
if (_underwaterWeapons) then { 
	_rifleDiverMagOne = "30Rnd_556x45_Stanag";	// standard mag
	_rifleDiverMagTwo = "20Rnd_556x45_UW_mag";	// underwater mag
} else { 
	_rifleDiverMagOne = _rifleMag;
	_rifleDiverMagTwo = _rifleMag;
};

// ================ Attachments ===================

_generalAttachments = ["rhs_acc_pkas"];
_dmrAttachments = ["optic_ACO_grn"];
_autoRifleAttachments = ["rhs_acc_pkas"];
_mmgAttachments = ["rhs_acc_pkas"];
_specOpsAttachments = ["rhs_acc_pkas","acc_pointer_IR"]; 
_sniperAttachments = ["optic_SOS"];
_amSniperAttachments = ["optic_SOS"];
_pistolAttachments = [];

_scoped_generalAttachments = ["rhs_acc_1p29"]; 
_scoped_dmrAttachments = ["optic_MRCO"];
_scoped_autoRifleAttachments = ["HLC_Optic_1p29"];
_scoped_mmgAttachments = ["rhs_acc_1p78","acc_flashlight"];
_scoped_specOpsAttachments = ["rhs_acc_1p29","acc_pointer_IR"];

_suppressed_generalAttachments = ["rhs_acc_dtk4short"];		
_suppressed_dmrAttachments = ["rhs_acc_tgpv"];
_suppressed_autoRifleAttachments = ["hlc_muzzle_762SUP_AK"];
_suppressed_mmgAttachments = [];
_suppressed_specOpsAttachments = ["rhs_acc_dtk4short"];
_suppressed_pistolAttachments = [];

// =======================================================================