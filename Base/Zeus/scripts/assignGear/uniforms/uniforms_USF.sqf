// AssignGear - Uniforms
// by Mr. Agnet
// - Defines all of the wearable items to be used by the assignGear script.
// - Current Side, Faction: BLUFOR, USF
// - Primary Mod: RHS
// - Variables: _camoPattern - "ocp", "ucp", "m81", "marpatwood", "marpatdes"
//				_headgear - "helmets", "softcover", "opshelm"

// =======================================================================
// Declares variables

private [
"_plebUniformArray","_plebRandom","_plebUniform",
"_plebHelmetArray","_plebHRandom","_plebHelmet",
"_plebVest","_gunnerVest","_glVest","_medVest",
"_smallRuck","_medRuck","_largeRuck",
"_specUniform","_specHelmet","_specGoggles","_specVest","_specRuck",
"_crewUniform","_rpilotUniform","_fpilotUniform",
"_crewmanHelmetArray","_crewmanHRandom","_crewmanHelmet","_crewmanHelmetArray","_crewmanHRandom","_rotaryPilotHelmet","_rotaryCrewHelmet","_fixedPilotHelmet",
"_crewVest","_pilotVest",
"_sniperUniform","_sniperVest","_sniperRuck",
"_diverUniform","_diverVest","_diverRuck",
"_goggles","_divingGoggles","_insignia",
"_airRadioRuck","_radioRuck","_diverRadioRuck","_uavRuck","_uavTool"
];

// =======================================================================
// =========================== Camo Specific =============================
// =======================================================================

_goggles = "";	// leave as "" for player defined
_insignia = ""; 

switch (_camoPattern) do {
	case "ocp" : {
		// ==================== Uniforms ==================

		_plebUniformArray = ["rhs_uniform_cu_ocp"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "rhs_uniform_cu_ocp";
		_rpilotUniform = "rhs_uniform_cu_ocp";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["rhsusf_ach_helmet_headset_ocp","rhsusf_ach_bare_tan_headset","rhsusf_ach_helmet_headset_ocp","rhsusf_ach_bare_tan_headset_ess","rhsusf_ach_helmet_headset_ocp","rhsusf_ach_helmet_headset_ess_ocp"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["rhs_Booniehat_ocp","H_Cap_tan","rhsusf_bowman_cap","H_Cap_oli_hs","H_Watchcap_khk","H_Bandanna_khk_hs","rhsusf_patrolcap_ocp","rhs_Booniehat_ocp"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "opshelm" : {
				_plebHelmetArray = ["rhsusf_opscore_03_ocp","rhsusf_opscore_01_tan","rhsusf_opscore_03_ocp","rhsusf_opscore_01_tan","rhsusf_opscore_03_ocp"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["rhsusf_ach_helmet_headset_ocp","rhsusf_ach_bare_tan_headset","rhsusf_ach_helmet_headset_ocp","rhsusf_ach_bare_tan_headset_ess","rhsusf_ach_helmet_headset_ocp","rhsusf_ach_helmet_headset_ess_ocp"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		_crewmanHelmetArray = ["rhsusf_cvc_green_helmet","rhsusf_cvc_green_ess","rhsusf_cvc_green_helmet","rhsusf_cvc_green_ess","rhsusf_cvc_green_helmet"];	
		_crewmanHRandom = (floor(random (count _crewmanHelmetArray))); 
		_crewmanHelmet = _crewmanHelmetArray select _crewmanHRandom; // enter single string value to remove randommess
		
		// ===================== Vests ====================
		
		_plebVest = "rhsusf_iotv_ocp_Rifleman";
		_gunnerVest = "rhsusf_iotv_ocp_SAW";
		_glVest = "rhsusf_iotv_ocp_Grenadier";
		_medVest = "rhsusf_iotv_ocp_Medic";
		_pilotVest = "V_TacVest_oli";
		_crewVest = "rhsusf_iotv_ocp_Rifleman";
		
		// ===================== Rucks ====================
		
		_smallRuck = "rhsusf_assault_eagleaiii_ocp";
		_medRuck = "B_Kitbag_cbr";
		_largeRuck = "B_Carryall_cbr";
		
		// ==================== SpecOps ===================
		
		_specUniform = "rhs_uniform_cu_ocp";
		_specHelmet = "rhsusf_opscore_03_ocp";
		_specGoggles = "rhs_scarf";
		_specVest = "rhsusf_iotv_ocp_SAW";
		_specRuck = "rhsusf_assault_eagleaiii_ocp";
		
		// ================================================
	};
	case "ucp" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["rhs_uniform_cu_ucp"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "rhs_uniform_cu_ucp";
		_rpilotUniform = "rhs_uniform_cu_ucp";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["rhsusf_ach_helmet_headset_ucp","rhsusf_ach_helmet_headset_ess_ucp","rhsusf_ach_helmet_headset_ucp","rhsusf_ach_helmet_headset_ess_ucp","rhsusf_ach_helmet_headset_ucp"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["rhs_Booniehat_ucp","H_Watchcap_blk","rhsusf_bowman_cap","H_Cap_oli_hs","rhsusf_bowman_cap","H_Watchcap_blk","rhs_Booniehat_ucp"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "opshelm" : {
				_plebHelmetArray = ["rhsusf_opscore_03_ocp","rhsusf_opscore_01_tan","rhsusf_opscore_03_ocp","rhsusf_opscore_01_tan","rhsusf_opscore_03_ocp"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["H_HelmetSpecB","H_HelmetSpecB_snakeskin","H_HelmetSpecB","H_HelmetSpecB_paint1","H_HelmetSpecB"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		_crewmanHelmetArray = ["rhsusf_cvc_helmet","rhsusf_cvc_ess","rhsusf_cvc_helmet","rhsusf_cvc_ess","rhsusf_cvc_helmet"];	
		_crewmanHRandom = (floor(random (count _crewmanHelmetArray)));
		_crewmanHelmet = _crewmanHelmetArray select _crewmanHRandom; // enter single string value to remove randommess
		
		// ===================== Vests ====================
		
		_plebVest = "rhsusf_iotv_ucp_Rifleman";
		_gunnerVest = "rhsusf_iotv_ucp_SAW";
		_glVest = "rhsusf_iotv_ucp_Grenadier";
		_medVest = "rhsusf_iotv_ucp_Medic";
		_pilotVest = "V_TacVest_blk";
		_crewVest = "rhsusf_iotv_ucp_Rifleman";
		
		// ===================== Rucks ====================
		
		_smallRuck = "rhsusf_assault_eagleaiii_ucp";
		_medRuck = "B_Kitbag_cbr";
		_largeRuck = "B_Carryall_cbr";
		
		// ===================== SpecOps ==================
		
		_specUniform = "rhs_uniform_cu_ucp";
		_specHelmet = "rhsusf_opscore_03_ocp";
		_specGoggles = "rhs_scarf";
		_specVest = "rhsusf_iotv_ucp_SAW";
		_specRuck = "rhsusf_assault_eagleaiii_ucp";
		
		// ================================================
	};
	case "m81" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["rhs_uniform_FROG01_m81"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "rhs_uniform_FROG01_m81";
		_rpilotUniform = "U_B_HeliPilotCoveralls";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["rhsusf_ach_helmet_M81","rhsusf_ach_bare","rhsusf_ach_helmet_M81","rhsusf_ach_bare_ess","rhsusf_ach_helmet_M81"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["rhs_Booniehat_m81","H_Cap_oli","rhs_Booniehat_m81","H_Cap_oli","rhs_Booniehat_m81"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "opshelm" : {
				_plebHelmetArray = ["rhsusf_mich_bare_norotos","rhsusf_mich_bare_norotos_arc","rhsusf_mich_bare_norotos","rhsusf_mich_bare_norotos_arc","rhsusf_mich_bare_norotos"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["rhsusf_ach_helmet_M81","rhsusf_ach_bare","rhsusf_ach_helmet_M81","rhsusf_ach_bare_ess","rhsusf_ach_helmet_M81"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		_crewmanHelmetArray = ["rhsusf_cvc_green_helmet","rhsusf_cvc_green_ess","rhsusf_cvc_green_helmet","rhsusf_cvc_green_ess","rhsusf_cvc_green_helmet"];	
		_crewmanHRandom = (floor(random (count _crewmanHelmetArray))); 
		_crewmanHelmet = _crewmanHelmetArray select _crewmanHRandom; // enter single string value to remove randommess
		
		// ===================== Vests ====================
		
		_plebVest = "rhsusf_spc_rifleman";
		_gunnerVest = "rhsusf_spc_iar";
		_glVest = "rhsusf_spc_teamleader";
		_medVest = "rhsusf_spc_corpsman";
		_pilotVest = "V_TacVest_blk";
		_crewVest = "rhsusf_spc_crewman";
		
		// ===================== Rucks ====================
		
		_smallRuck = "rhsusf_assault_eagleaiii_coy";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_oli";
		
		// ===================== SpecOps ==================
		
		_specUniform = "rhs_uniform_FROG01_m81";
		_specHelmet = "rhsusf_ach_bare_ess";
		_specGoggles = "G_Bandanna_khk";
		_specVest = "rhsusf_spc_squadleader";
		_specRuck = "rhsusf_assault_eagleaiii_coy";
		
		// ================================================
	};
	case "marpatwood" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["rhs_uniform_FROG01_wd"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "rhs_uniform_FROG01_wd";
		_rpilotUniform = "U_B_HeliPilotCoveralls";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["rhsusf_lwh_helmet_marpatwd_headset","rhsusf_lwh_helmet_marpatwd","rhsusf_lwh_helmet_marpatwd_ess","rhsusf_lwh_helmet_marpatwd_headset"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["rhs_Booniehat_marpatwd"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "opshelm" : {
				_plebHelmetArray = ["rhsusf_mich_helmet_marpatwd","rhsusf_mich_helmet_marpatwd_alt","rhsusf_mich_helmet_marpatwd","rhsusf_mich_helmet_marpatwd_norotos","rhsusf_mich_helmet_marpatwd","rhsusf_mich_helmet_marpatwd_norotos_arc","rhsusf_mich_helmet_marpatwd"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["rhsusf_lwh_helmet_marpatwd_headset","rhsusf_lwh_helmet_marpatwd","rhsusf_lwh_helmet_marpatwd_ess","rhsusf_lwh_helmet_marpatwd_headset"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		_crewmanHelmetArray = ["rhsusf_cvc_green_helmet","rhsusf_cvc_green_ess","rhsusf_cvc_green_helmet","rhsusf_cvc_green_ess","rhsusf_cvc_green_helmet"];	
		_crewmanHRandom = (floor(random (count _crewmanHelmetArray))); 
		_crewmanHelmet = _crewmanHelmetArray select _crewmanHRandom; // enter single string value to remove randommess
		
		// ===================== Vests ====================
		
		_plebVest = "rhsusf_spc_rifleman";
		_gunnerVest = "rhsusf_spc_iar";
		_glVest = "rhsusf_spc_teamleader";
		_medVest = "rhsusf_spc_corpsman";
		_pilotVest = "V_TacVest_blk";
		_crewVest = "rhsusf_spc_crewman";
		
		// ===================== Rucks ====================
		
		_smallRuck = "rhsusf_assault_eagleaiii_coy";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_oli";
		
		// ===================== SpecOps ==================
		
		_specUniform = "rhs_uniform_FROG01_wd";
		_specHelmet = "rhsusf_lwh_helmet_marpatwd_headset";
		_specGoggles = "G_Bandanna_khk";
		_specVest = "rhsusf_spc_squadleader";
		_specRuck = "rhsusf_assault_eagleaiii_coy";
		
		// ================================================
	};
	case "marpatdes" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["rhs_uniform_FROG01_d"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "rhs_uniform_FROG01_wd";
		_rpilotUniform = "U_B_HeliPilotCoveralls";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["rhsusf_lwh_helmet_marpatd_headset","rhsusf_lwh_helmet_marpatd","rhsusf_lwh_helmet_marpatd_ess","rhsusf_lwh_helmet_marpatd_headset"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["rhs_Booniehat_marpatd","H_Cap_tan","rhs_Booniehat_marpatd"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "opshelm" : {
				_plebHelmetArray = ["rhsusf_mich_helmet_marpatd","rhsusf_mich_helmet_marpatd_alt","rhsusf_mich_helmet_marpatd","rhsusf_mich_helmet_marpatd_norotos","rhsusf_mich_helmet_marpatd","rhsusf_mich_helmet_marpatd_norotos_arc","rhsusf_mich_helmet_marpatd"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["rhsusf_lwh_helmet_marpatd_headset","rhsusf_lwh_helmet_marpatd","rhsusf_lwh_helmet_marpatd_ess","rhsusf_lwh_helmet_marpatd_headset"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		_crewmanHelmetArray = ["rhsusf_cvc_helmet","rhsusf_cvc_ess","rhsusf_cvc_helmet","rhsusf_cvc_ess","rhsusf_cvc_helmet"];	
		_crewmanHRandom = (floor(random (count _crewmanHelmetArray)));
		_crewmanHelmet = _crewmanHelmetArray select _crewmanHRandom; // enter single string value to remove randommess
		
		// ===================== Vests ====================
		
		_plebVest = "rhsusf_spc_rifleman";
		_gunnerVest = "rhsusf_spc_iar";
		_glVest = "rhsusf_spc_teamleader";
		_medVest = "rhsusf_spc_corpsman";
		_pilotVest = "V_TacVest_blk";
		_crewVest = "rhsusf_spc_crewman";
		
		// ===================== Rucks ====================
		
		_smallRuck = "rhsusf_assault_eagleaiii_coy";
		_medRuck = "B_Kitbag_cbr";
		_largeRuck = "B_Carryall_cbr";
		
		// ===================== SpecOps ==================
		
		_specUniform = "rhs_uniform_FROG01_d";
		_specHelmet = "rhsusf_lwh_helmet_marpatd_headset";
		_specGoggles = "G_Bandanna_khk";
		_specVest = "rhsusf_spc_squadleader";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
	default {
		// ==================== Uniforms ==================

		_plebUniformArray = ["rhs_uniform_cu_ocp"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "rhs_uniform_cu_ocp";
		_rpilotUniform = "rhs_uniform_cu_ocp";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["rhsusf_ach_helmet_headset_ocp","rhsusf_ach_bare_tan_headset","rhsusf_ach_helmet_headset_ocp","rhsusf_ach_bare_tan_headset_ess","rhsusf_ach_helmet_headset_ocp","rhsusf_ach_helmet_headset_ess_ocp"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["rhs_Booniehat_ocp","H_Cap_tan","rhsusf_bowman_cap","H_Cap_oli_hs","H_Watchcap_khk","H_Bandanna_khk_hs","rhsusf_patrolcap_ocp","rhs_Booniehat_ocp"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "opshelm" : {
				_plebHelmetArray = ["rhsusf_opscore_03_ocp","rhsusf_opscore_01_tan","rhsusf_opscore_03_ocp","rhsusf_opscore_01_tan","rhsusf_opscore_03_ocp"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["rhsusf_ach_helmet_headset_ocp","rhsusf_ach_bare_tan_headset","rhsusf_ach_helmet_headset_ocp","rhsusf_ach_bare_tan_headset_ess","rhsusf_ach_helmet_headset_ocp","rhsusf_ach_helmet_headset_ess_ocp"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		_crewmanHelmetArray = ["rhsusf_cvc_green_helmet","rhsusf_cvc_green_ess","rhsusf_cvc_green_helmet","rhsusf_cvc_green_ess","rhsusf_cvc_green_helmet"];	
		_crewmanHRandom = (floor(random (count _crewmanHelmetArray))); 
		_crewmanHelmet = _crewmanHelmetArray select _crewmanHRandom; // enter single string value to remove randommess
		
		// ===================== Vests ====================
		
		_plebVest = "rhsusf_iotv_ocp_Rifleman";
		_gunnerVest = "rhsusf_iotv_ocp_SAW";
		_glVest = "rhsusf_iotv_ocp_Grenadier";
		_medVest = "rhsusf_iotv_ocp_Medic";
		_pilotVest = "V_TacVest_oli";
		_crewVest = "rhsusf_iotv_ocp_Rifleman";
		
		// ===================== Rucks ====================
		
		_smallRuck = "rhsusf_assault_eagleaiii_ocp";
		_medRuck = "B_Kitbag_cbr";
		_largeRuck = "B_Carryall_cbr";
		
		// ==================== SpecOps ===================
		
		_specUniform = "rhs_uniform_cu_ocp";
		_specHelmet = "rhsusf_opscore_03_ocp";
		_specGoggles = "rhs_scarf";
		_specVest = "rhsusf_iotv_ocp_SAW";
		_specRuck = "rhsusf_assault_eagleaiii_ocp";
		
		// ================================================
	};
};

// =======================================================================
// ========================= Non-Camo Specific ===========================
// =======================================================================

// ================== Vehicle Crews ==================

_fpilotUniform = "U_B_PilotCoveralls";
_rotaryPilotHelmet = "rhsusf_hgu56p";
_rotaryCrewHelmet = "rhsusf_hgu56p_mask";
_fixedPilotHelmet = "H_PilotHelmetFighter_B";

// =================== Sniper Team ===================

_sniperUniform = "U_B_GhillieSuit";
_sniperVest = "V_TacVest_oli";
_sniperRuck = "B_AssaultPack_rgr";

// =================== Diver Gear ===================

_diverUniform = "U_B_Wetsuit";
_diverVest = "V_RebreatherB";
_diverRuck = "B_AssaultPack_blk";
_divingGoggles = "G_B_Diving";

// =================== Radio Rucks ===================

switch (_radioSelection) do {
	case "detection" : {
		private "_side";
		_side = toLower format ["%1", side player];
		// BLUFOR UAV
		if (_side == "west") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
		// OPFOR UAV
		if (_side == "east") then { 
			_uavRuck = "O_UAV_01_backpack_F";
			_uavTool = "O_UavTerminal";
		};
		// INDFOR UAV
		if (_side == "guer") then { 
			_uavRuck = "I_UAV_01_backpack_F";
			_uavTool = "I_UavTerminal";
		};
		// Civilian UAV
		if (_side == "civ") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
	};
	default {
		_uavRuck = "B_UAV_01_backpack_F";
		_uavTool = "B_UavTerminal";
	};
};

// =======================================================================