// AssignGear - Uniforms
// by Mr. Agnet
// - Defines all of the wearable items to be used by the assignGear script.
// - Current Side, Faction: INDEPENDENT, SYNDIKAT
// - Primary Mod: VANILLA
// - Variables: _headgear - "helmets", "softcover"

// =======================================================================
// Declares variables

private [
"_plebUniformArray","_plebRandom","_plebUniform",
"_plebHelmetArray","_plebHRandom","_plebHelmet",
"_plebVest","_gunnerVest","_glVest","_medVest",
"_smallRuck","_medRuck","_largeRuck",
"_specUniform","_specHelmet","_specGoggles","_specVest","_specRuck",
"_crewUniform","_rpilotUniform","_fpilotUniform",
"_crewmanHelmetArray","_crewmanHRandom","_crewmanHelmet","_rotaryPilotHelmet","_rotaryCrewHelmet","_fixedPilotHelmet",
"_crewVest","_pilotVest",
"_sniperUniform","_sniperVest","_sniperRuck",
"_diverUniform","_diverVest","_diverRuck",
"_goggles","_insignia","_divingGoggles",
"_airRadioRuck","_radioRuck","_diverRadioRuck","_uavRuck","_uavTool"
];

// =======================================================================
// =========================== Camo Specific =============================
// =======================================================================

_goggles = "";	// leave as "" for player defined
_insignia = ""; 

// ========== Uniforms ===========

_plebUniformArray = ["U_I_C_Soldier_Para_1_F","U_I_C_Soldier_Para_4_F","U_I_C_Soldier_Para_3_F","U_I_C_Soldier_Para_2_F","U_I_C_Soldier_Camo_F","U_BG_Guerilla3_1","U_BG_Guerilla2_1"];	
_plebRandom = (floor(random (count _plebUniformArray)));
_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess

// ========== Headgear ===========

switch (_headgear) do {
	case "helmets" : {
		_plebHelmetArray = ["rhsgref_ssh68_emr","rhsgref_ssh68_emr","rhsgref_ssh68_emr","rhsgref_ssh68_emr"];	
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};		
	case "softcover" : {
		_plebHelmetArray = ["H_Bandanna_sgg","H_Cap_oli","H_Cap_grn","H_Booniehat_oli","H_Bandanna_khk","H_Bandanna_sgg"];	
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};
	default {
		_plebHelmetArray = ["H_Bandanna_sgg","H_Cap_oli","H_Cap_grn","H_Booniehat_oli","H_Bandanna_khk","H_Bandanna_sgg"];	
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};
};

// ============ Vests ============

_plebVest = "rhsgref_otv_digi";
_gunnerVest = "rhsgref_otv_digi";
_glVest = "rhsgref_otv_digi";
_medVest = "rhsgref_otv_digi";

// ============ Rucks ============

_smallRuck = "B_AssaultPack_rgr";
_medRuck = "B_Kitbag_rgr";
_largeRuck = "B_Carryall_oli";

// =======================================================================
// ========================= Non-Camo Specific ===========================
// =======================================================================

// ================== Vehicle Crews ==================

_crewUniform = "U_I_C_Soldier_Para_3_F";

_rpilotUniform = "U_I_C_Soldier_Para_4_F";
_fpilotUniform = "U_I_PilotCoveralls";

_crewmanHelmetArray = ["rhs_tsh4","rhs_tsh4_ess","rhs_tsh4"];	
_crewmanHRandom = (floor(random (count _crewmanHelmetArray))); 
_crewmanHelmet = _crewmanHelmetArray select _crewmanHRandom; // enter single string value to remove randommess

_rotaryPilotHelmet = "rhs_gssh18";
_rotaryCrewHelmet = "rhs_gssh18";
_fixedPilotHelmet = "H_PilotHelmetFighter_I";

_crewVest = "V_TacVest_oli";
_pilotVest = "V_TacVest_oli";

// =================== Sniper Team ===================

_sniperUniform = "U_O_T_Sniper_F";
_sniperVest = "V_TacVest_oli";
_sniperRuck = "B_AssaultPack_rgr";

// =================== Diver Gear ===================

_diverUniform = "U_B_Wetsuit";
_diverVest = "V_RebreatherB";
_diverRuck = "B_AssaultPack_blk";
_divingGoggles = "G_B_Diving";

// =================== Radio Rucks ===================

switch (_radioSelection) do {
	case "detection" : {
		private "_side";
		_side = toLower format ["%1", side player];
		// BLUFOR UAV
		if (_side == "west") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
		// OPFOR UAV
		if (_side == "east") then { 
			_uavRuck = "O_UAV_01_backpack_F";
			_uavTool = "O_UavTerminal";
		};
		// INDFOR UAV
		if (_side == "guer") then { 
			_uavRuck = "I_UAV_01_backpack_F";
			_uavTool = "I_UavTerminal";
		};
		// Civilian UAV
		if (_side == "civ") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
	};
	default {
		_uavRuck = "B_UAV_01_backpack_F";
		_uavTool = "B_UavTerminal";
	};
};

// =======================================================================