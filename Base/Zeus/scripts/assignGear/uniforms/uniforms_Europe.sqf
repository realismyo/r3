// AssignGear - Uniforms
// by Mr. Agnet
// - Defines all of the wearable items to be used by the assignGear script.
// - Current Side, Faction: BLUFOR, EUROPA
// - Primary Mod: KMNP
// - Variables: _camoPattern - "finland", "norway", "norwaydes", "czech", "ireland", "irelanddes"
//				_headgear - "helmets", "softcover"

// =======================================================================
// Declares variables

private [
"_plebUniformArray","_plebRandom","_plebUniform",
"_plebHelmetArray","_plebHRandom","_plebHelmet",
"_plebVest","_gunnerVest","_glVest","_medVest",
"_smallRuck","_medRuck","_largeRuck",
"_specUniform","_specHelmet","_specGoggles","_specVest","_specRuck",
"_crewUniform","_rpilotUniform","_fpilotUniform",
"_crewmanHelmetArray","_crewmanHRandom","_crewmanHelmet","_rotaryPilotHelmet","_rotaryCrewHelmet","_fixedPilotHelmet",
"_crewVest","_pilotVest",
"_sniperUniform","_sniperVest","_sniperRuck",
"_diverUniform","_diverVest","_diverRuck",
"_goggles","_divingGoggles","_insignia",
"_airRadioRuck","_radioRuck","_diverRadioRuck","_uavRuck","_uavTool"
];

// =======================================================================
// =========================== Camo Specific =============================
// =======================================================================

_goggles = "";	// leave as "" for player defined
_insignia = ""; 

switch (_camoPattern) do {
	case "finland" : {
		// ==================== Uniforms ==================

		_plebUniformArray = ["MNP_CombatUniform_Fin_A","MNP_CombatUniform_Fin_B","MNP_CombatUniform_Fin_A"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_Fin_B";
		_rpilotUniform = "MNP_CombatUniform_Fin_B";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_FIN_T","MNP_Helmet_FIN_T","MNP_Helmet_FIN_T"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Booniehat_oli"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_FIN_T","MNP_Helmet_FIN_T","MNP_Helmet_FIN_T"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_FIN_1";
		_gunnerVest = "MNP_Vest_FIN_2";
		_glVest = "MNP_Vest_FIN_2";
		_medVest = "MNP_Vest_FIN_1";
		_crewVest = "V_TacVest_oli";
		_pilotVest = "V_TacVest_oli";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_oli";
		
		// ==================== SpecOps ===================
		
		_specUniform = "MNP_CombatUniform_Fin_B";
		_specHelmet = "MNP_Helmet_FIN_T";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "MNP_Vest_FIN_2";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
	case "norway" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["MNP_CombatUniform_NOR_A","MNP_CombatUniform_NOR_B","MNP_CombatUniform_NOR_A"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_NOR_B";
		_rpilotUniform = "MNP_CombatUniform_NOR_B";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_AMCU","MNP_Helmet_AMCU","MNP_Helmet_AMCU"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["MNP_Boonie_NOR"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_AMCU","MNP_Helmet_AMCU","MNP_Helmet_AMCU"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_NOR_1";
		_gunnerVest = "MNP_Vest_NOR_2";
		_glVest = "MNP_Vest_NOR_2";
		_medVest = "MNP_Vest_NOR_1";
		_crewVest = "V_TacVest_oli";
		_pilotVest = "V_TacVest_oli";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_cbr";
		
		// ===================== SpecOps ==================
		
		_specUniform = "MNP_CombatUniform_NOR_B";
		_specHelmet = "MNP_Helmet_AMCU";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "MNP_Vest_NOR_2";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
	case "norwaydes" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["MNP_CombatUniform_NOR_D_A","MNP_CombatUniform_NOR_D_B","MNP_CombatUniform_NOR_D_A"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_NOR_D_B";
		_rpilotUniform = "MNP_CombatUniform_NOR_D_B";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_3Co","MNP_Helmet_3Co","MNP_Helmet_3Co"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["MNP_Boonie_NOR_D"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_3Co","MNP_Helmet_3Co","MNP_Helmet_3Co"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_NOR_D_1";
		_gunnerVest = "MNP_Vest_NOR_D_2";
		_glVest = "MNP_Vest_NOR_D_2";
		_medVest = "MNP_Vest_NOR_D_1";
		_crewVest = "V_TacVest_oli";
		_pilotVest = "V_TacVest_oli";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_cbr";
		
		// ===================== SpecOps ==================
		
		_specUniform = "MNP_CombatUniform_NOR_D_B";
		_specHelmet = "MNP_Helmet_3Co";
		_specGoggles = "G_Bandanna_tan";
		_specVest = "MNP_Vest_NOR_D_2";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
	case "czech" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["MNP_CombatUniform_CZ_A","MNP_CombatUniform_CZ_B","MNP_CombatUniform_CZ_A"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_CZ_B";
		_rpilotUniform = "MNP_CombatUniform_CZ_B";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_CZ","MNP_Helmet_CZ","MNP_Helmet_CZ"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Booniehat_oli"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_CZ","MNP_Helmet_CZ","MNP_Helmet_CZ"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_CZ_2";
		_gunnerVest = "MNP_Vest_CZ_1";
		_glVest = "MNP_Vest_CZ_1";
		_medVest = "MNP_Vest_CZ_2";
		_crewVest = "V_TacVest_oli";
		_pilotVest = "V_TacVest_oli";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_oli";
		
		// ===================== SpecOps ==================
		
		_specUniform = "MNP_CombatUniform_CZ_B";
		_specHelmet = "MNP_Helmet_CZ";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "MNP_Vest_CZ_1";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
	case "ireland" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["MNP_CombatUniform_Ireland","MNP_CombatUniform_Ireland_S","MNP_CombatUniform_Ireland"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_Ireland_S";
		_rpilotUniform = "MNP_CombatUniform_Ireland_S";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_Ireland","MNP_Helmet_Ireland","MNP_Helmet_Ireland"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["MNP_Boonie_Ireland_T"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_Ireland","MNP_Helmet_Ireland","MNP_Helmet_Ireland"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_Ireland_B";
		_gunnerVest = "MNP_Vest_Ireland";
		_glVest = "MNP_Vest_Ireland";
		_medVest = "MNP_Vest_Ireland_B";
		_crewVest = "V_TacVest_oli";
		_pilotVest = "V_TacVest_oli";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_oli";
		
		// ===================== SpecOps ==================
		
		_specUniform = "MNP_CombatUniform_Ireland_S";
		_specHelmet = "MNP_Helmet_Ireland";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "MNP_Vest_Ireland";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
	case "irelanddes" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["MNP_CombatUniform_Ireland_D","MNP_CombatUniform_Ireland_DS","MNP_CombatUniform_Ireland_D"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_Ireland_DS";
		_rpilotUniform = "MNP_CombatUniform_Ireland_DS";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_Ireland_D","MNP_Helmet_Ireland_D","MNP_Helmet_Ireland_D"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["MNP_Boonie_Ireland_D"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_Ireland_D","MNP_Helmet_Ireland_D","MNP_Helmet_Ireland_D"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_Ireland_B";
		_gunnerVest = "MNP_Vest_Ireland";
		_glVest = "MNP_Vest_Ireland";
		_medVest = "MNP_Vest_Ireland_B";
		_crewVest = "V_TacVest_brn";
		_pilotVest = "V_TacVest_brn";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_cbr";
		_medRuck = "B_Kitbag_cbr";
		_largeRuck = "B_Carryall_cbr";
		
		// ===================== SpecOps ==================
		
		_specUniform = "MNP_CombatUniform_Ireland_DS";
		_specHelmet = "MNP_Helmet_Ireland_D";
		_specGoggles = "G_Bandanna_tan";
		_specVest = "MNP_Vest_Germany_D2";
		_specRuck = "B_AssaultPack_cbr";
		
		// ================================================
	};
	default {
		// ==================== Uniforms ==================

		_plebUniformArray = ["MNP_CombatUniform_Fin_A","MNP_CombatUniform_Fin_B","MNP_CombatUniform_Fin_A"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_Fin_B";
		_rpilotUniform = "MNP_CombatUniform_Fin_B";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_FIN_T","MNP_Helmet_FIN_T","MNP_Helmet_FIN_T"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Booniehat_oli"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_FIN_T","MNP_Helmet_FIN_T","MNP_Helmet_FIN_T"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_FIN_1";
		_gunnerVest = "MNP_Vest_FIN_2";
		_glVest = "MNP_Vest_FIN_2";
		_medVest = "MNP_Vest_FIN_1";
		_crewVest = "V_TacVest_oli";
		_pilotVest = "V_TacVest_oli";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_oli";
		
		// ==================== SpecOps ===================
		
		_specUniform = "MNP_CombatUniform_Fin_B";
		_specHelmet = "MNP_Helmet_FIN_T";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "MNP_Vest_FIN_2";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
};

// =======================================================================
// ========================= Non-Camo Specific ===========================
// =======================================================================

// ================== Vehicle Crews ==================

_fpilotUniform = "U_B_PilotCoveralls";
_crewmanHelmet = "H_HelmetCrew_B";
_rotaryPilotHelmet = "H_PilotHelmetHeli_B";
_rotaryCrewHelmet = "H_CrewHelmetHeli_B";
_fixedPilotHelmet = "H_PilotHelmetFighter_B";

// =================== Sniper Team ===================

_sniperUniform = "U_B_GhillieSuit";
_sniperVest = "V_TacVest_oli";
_sniperRuck = "B_AssaultPack_rgr";

// =================== Diver Gear ===================

_diverUniform = "U_B_Wetsuit";
_diverVest = "V_RebreatherB";
_diverRuck = "B_AssaultPack_blk";
_divingGoggles = "G_B_Diving";

// =================== Radio Rucks ===================

switch (_radioSelection) do {
	case "detection" : {
		private "_side";
		_side = toLower format ["%1", side player];
		// BLUFOR UAV
		if (_side == "west") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
		// OPFOR UAV
		if (_side == "east") then { 
			_uavRuck = "O_UAV_01_backpack_F";
			_uavTool = "O_UavTerminal";
		};
		// INDFOR UAV
		if (_side == "guer") then { 
			_uavRuck = "I_UAV_01_backpack_F";
			_uavTool = "I_UavTerminal";
		};
		// Civilian UAV
		if (_side == "civ") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
	};
	default {
		_uavRuck = "B_UAV_01_backpack_F";
		_uavTool = "B_UavTerminal";
	};
};

// =======================================================================