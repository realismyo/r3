// AssignGear - Uniforms
// by Mr. Agnet
// - Defines all of the wearable items to be used by the assignGear script.
// - Current Side, Faction: BLUFOR, PG SERVICES
// - Primary Mod: PG Services
// - Variables: _camoPattern - "olives", "khaki", "black", "guard" 
//				_headgear - "helmets", "softcover", "opshelm"

// =======================================================================
// Declares variables

private [
"_plebUniformArray","_plebRandom","_plebUniform",
"_plebHelmetArray","_plebHRandom","_plebHelmet",
"_plebVest","_gunnerVest","_glVest","_medVest",
"_smallRuck","_medRuck","_largeRuck",
"_specUniform","_specHelmet","_specGoggles","_specVest","_specRuck",
"_crewUniform","_rpilotUniform","_fpilotUniform",
"_crewmanHelmetArray","_crewmanHRandom","_crewmanHelmet","_rotaryPilotHelmet","_rotaryCrewHelmet","_fixedPilotHelmet",
"_crewVest","_pilotVest",
"_sniperUniform","_sniperVest","_sniperRuck",
"_diverUniform","_diverVest","_diverRuck",
"_goggles","_insignia","_divingGoggles",
"_airRadioRuck","_radioRuck","_diverRadioRuck","_uavRuck","_uavTool"
];

// =======================================================================
// =========================== Camo Specific =============================
// =======================================================================

_goggles = "";	// leave as "" for player defined
_insignia = ""; 

switch (_camoPattern) do {
	case "olives" : {
		// ========== Uniforms ===========
		_plebUniformArray = ["tacs_Uniform_Garment_LS_GS_GP_BB","tacs_Uniform_Garment_LS_ES_GP_BB","tacs_Uniform_Garment_RS_GS_GP_BB","tacs_Uniform_Garment_RS_ES_GP_BB","tacs_Uniform_Garment_LS_GS_EP_TB","tacs_Uniform_Garment_RS_GS_EP_TB","tacs_Uniform_Garment_LS_GS_BP_BB","tacs_Uniform_Garment_RS_GS_BP_BB"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "tacs_Uniform_Garment_LS_BS_BP_BB";
		_rpilotUniform = "U_B_HeliPilotCoveralls";
		// ========== Headgear ===========
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["H_HelmetSpecB","H_HelmetSpecB_paint1","H_HelmetSpecB","H_HelmetSpecB_snakeskin"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};		
			case "softcover" : {
				_plebHelmetArray = ["rhsusf_bowman_cap","H_Cap_headphones","H_Cap_oli","H_Cap_grn","H_Booniehat_oli","H_Watchcap_camo","H_Booniehat_rgr"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "opshelm" : {
				_plebHelmetArray = ["H_HelmetB_light","H_HelmetB_light_snakeskin","H_HelmetB_light","H_HelmetB_light_grass","H_HelmetB_light"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["H_HelmetSpecB","H_HelmetSpecB_paint1","H_HelmetSpecB","H_HelmetSpecB_snakeskin"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		_crewmanHelmetArray = ["rhsusf_cvc_green_helmet","rhsusf_cvc_green_ess","rhsusf_cvc_green_helmet","rhsusf_cvc_green_ess","rhsusf_cvc_green_helmet"];	
		_crewmanHRandom = (floor(random (count _crewmanHelmetArray))); 
		_crewmanHelmet = _crewmanHelmetArray select _crewmanHRandom; // enter single string value to remove randommess
		// ============ Vests ============
		_plebVest = "tacs_Vest_PlateCarrierFull_Green";
		_gunnerVest = "V_PlateCarrierIAGL_oli";
		_glVest = "tacs_Vest_PlateCarrierFull_Green";
		_medVest = "tacs_Vest_PlateCarrierFull_Green";
		_pilotVest = "V_TacVest_blk";
		_crewVest = "V_TacVest_blk";
		// ============ Rucks ============
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_oli";
		// ============ SpecOps ========== 
		_specUniform = "tacs_Uniform_Garment_RS_GS_GP_BB";
		_specHelmet = "H_HelmetB_light";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "tacs_Vest_PlateCarrierFull_Green";
		_specRuck = "B_AssaultPack_rgr";
		// ==============================
	};
	case "khakis" : {
		// ========== Uniforms ===========
		_plebUniformArray = ["tacs_Uniform_Garment_RS_TS_TP_TB","tacs_Uniform_Garment_RS_TS_GP_BB","tacs_Uniform_Garment_RS_TS_BP_BB","tacs_Uniform_Garment_RS_ES_EP_TB","tacs_Uniform_Garment_RS_ES_GP_BB","tacs_Uniform_Garment_RS_ES_BP_BB","tacs_Uniform_Garment_LS_TS_TP_TB","tacs_Uniform_Garment_LS_TS_GP_BB","tacs_Uniform_Garment_LS_TS_BP_BB"];
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "tacs_Uniform_Garment_LS_BS_BP_BB";
		_rpilotUniform = "U_B_HeliPilotCoveralls";
		// ========== Headgear ===========
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["H_HelmetSpecB_sand","H_HelmetSpecB_paint2","H_HelmetSpecB_sand","H_HelmetSpecB_paint2","H_HelmetSpecB_sand"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Cap_oli_hs","H_Watchcap_khk","H_Bandanna_khk_hs","H_Cap_tan","tacs_Cap_Backwards_TanLogo","tacs_Cap_TanLogo"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "opshelm" : {
				_plebHelmetArray = ["H_HelmetB_light_sand","H_HelmetB_light_desert","H_HelmetB_light_sand","H_HelmetB_light_desert","H_HelmetB_light_sand"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["H_HelmetSpecB_sand","H_HelmetSpecB_paint2","H_HelmetSpecB_sand","H_HelmetSpecB_paint2","H_HelmetSpecB_sand"];
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		_crewmanHelmetArray = ["rhsusf_cvc_helmet","rhsusf_cvc_ess","rhsusf_cvc_helmet","rhsusf_cvc_ess","rhsusf_cvc_helmet"];	
		_crewmanHRandom = (floor(random (count _crewmanHelmetArray)));
		_crewmanHelmet = _crewmanHelmetArray select _crewmanHRandom; // enter single string value to remove randommess
		// ============ Vests ============
		_plebVest = "tacs_Vest_PlateCarrier_Coyote";
		_gunnerVest = "tacs_Vest_PlateCarrier_Coyote";
		_glVest = "V_PlateCarrierIAGL_khk";
		_medVest = "tacs_Vest_PlateCarrier_Coyote";
		_pilotVest = "V_TacVest_blk";
		_crewVest = "V_TacVest_blk";
		// ============ Rucks ============
		_smallRuck = "B_AssaultPack_cbr";
		_medRuck = "B_Kitbag_cbr";
		_largeRuck = "B_Carryall_cbr";
		// ============ SpecOps ========== 
		_specUniform = "tacs_Uniform_Garment_RS_TS_TP_TB";
		_specHelmet = "H_HelmetB_light_sand";
		_specGoggles = "rhs_scarf";
		_specVest = "tacs_Vest_PlateCarrier_Coyote";
		_specRuck = "B_AssaultPack_cbr";
		// ==============================
	};
	case "black" : {
		// ========== Uniforms ===========
		_plebUniformArray = ["tacs_Uniform_Garment_RS_BS_BP_BB","tacs_Uniform_Garment_LS_BS_BP_BB","tacs_Uniform_Garment_RS_ES_BP_BB","tacs_Uniform_Garment_LS_ES_BP_BB"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "tacs_Uniform_Garment_LS_BS_BP_BB";
		_rpilotUniform = "U_B_HeliPilotCoveralls";
		// ========== Headgear ===========
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["H_HelmetSpecB_blk","H_HelmetSpecB_blk","H_HelmetSpecB_blk"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["tacs_Cap_Earpiece_BlackLogo","tacs_Cap_Backwards_BlackLogo","tacs_Cap_Headphones_BlackLogo","H_Bandanna_gry","H_Watchcap_blk","rhsusf_Bowman"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "opshelm" : {
				_plebHelmetArray = ["H_HelmetB_light_black","H_HelmetB_light_black","H_HelmetB_light_black"];
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["H_HelmetSpecB_blk","H_HelmetSpecB_blk","H_HelmetSpecB_blk"];
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		_crewmanHelmetArray = ["rhsusf_cvc_green_helmet","rhsusf_cvc_green_ess","rhsusf_cvc_green_helmet","rhsusf_cvc_green_ess","rhsusf_cvc_green_helmet"];	
		_crewmanHRandom = (floor(random (count _crewmanHelmetArray))); 
		_crewmanHelmet = _crewmanHelmetArray select _crewmanHRandom; // enter single string value to remove randommess
		// ============ Vests ============
		_plebVest = "tacs_Vest_PlateCarrier_Black";
		_gunnerVest = "V_PlateCarrierSpec_blk";
		_glVest = "tacs_Vest_PlateCarrier_Black";
		_medVest = "tacs_Vest_PlateCarrier_Black";
		_pilotVest = "V_TacVest_blk";
		_crewVest = "V_TacVest_blk";
		// ============ Rucks ============
		_smallRuck = "B_AssaultPack_blk";
		_medRuck = "tacs_Backpack_Kitbag_DarkBlack";
		_largeRuck = "tacs_Backpack_Carryall_DarkBlack";
		// ============ SpecOps ========== 
		_specUniform = "tacs_Uniform_Garment_LS_BS_BP_BB";
		_specHelmet = "H_HelmetSpecB_blk";
		_specGoggles = "G_Bandanna_blk";
		_specVest = "V_PlateCarrierSpec_blk";
		_specRuck = "B_AssaultPack_blk";
		// ==============================
	};
	case "guard" : {
		// ========== Uniforms ===========
		_plebUniformArray = ["tacs_Uniform_Polo_CP_RS_LP_BB","tacs_Uniform_Polo_CP_LS_TP_OB","tacs_Uniform_Polo_CP_BS_TP_BB","tacs_Uniform_Polo_TP_WS_TP_TB","tacs_Uniform_Polo_TP_LS_GP_BB","tacs_Uniform_Polo_TP_BS_LP_BB"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "tacs_Uniform_Garment_LS_BS_BP_BB";
		_rpilotUniform = "U_B_HeliPilotCoveralls";
		// ========== Headgear ===========
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["H_HelmetB_light","H_HelmetSpecB_paint1","H_HelmetB_light","H_HelmetSpecB_snakeskin","H_HelmetB_light"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["tacs_Cap_Headphones_BlackLogo","H_Watchcap_blk","H_Watchcap_camo","H_Booniehat_khk_hs","H_Watchcap_khk","H_Cap_oli_hs","H_Cap_headphones","rhsusf_bowman_cap"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "opshelm" : {
				_plebHelmetArray = ["H_HelmetB_light","H_HelmetSpecB_paint1","H_HelmetB_light","H_HelmetSpecB_snakeskin","H_HelmetB_light"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["H_HelmetB_light","H_HelmetSpecB_paint1","H_HelmetB_light","H_HelmetSpecB_snakeskin","H_HelmetB_light"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		_crewmanHelmetArray = ["rhsusf_cvc_green_helmet","rhsusf_cvc_green_ess","rhsusf_cvc_green_helmet","rhsusf_cvc_green_ess","rhsusf_cvc_green_helmet"];	
		_crewmanHRandom = (floor(random (count _crewmanHelmetArray))); 
		_crewmanHelmet = _crewmanHelmetArray select _crewmanHRandom; // enter single string value to remove randommess
		// ============ Vests ============
		_plebVest = "tacs_Vest_PlateCarrier_Black";
		_gunnerVest = "V_PlateCarrierSpec_blk";
		_glVest = "tacs_Vest_PlateCarrier_Black";
		_medVest = "tacs_Vest_PlateCarrier_Black";
		_pilotVest = "V_TacVest_blk";
		_crewVest = "V_TacVest_blk";
		// ============ Rucks ============
		_smallRuck = "B_AssaultPack_blk";
		_medRuck = "tacs_Backpack_Kitbag_DarkBlack";
		_largeRuck = "tacs_Backpack_Carryall_DarkBlack";
		// ============ SpecOps ========== 
		_specUniform = "tacs_Uniform_Garment_LS_BS_BP_BB";
		_specHelmet = "H_HelmetSpecB_blk";
		_specGoggles = "G_Bandanna_blk";
		_specVest = "V_PlateCarrierSpec_blk";
		_specRuck = "B_AssaultPack_blk";
		// ==============================
	};
	default {
		// ========== Uniforms ===========
		_plebUniformArray = ["tacs_Uniform_Garment_LS_GS_GP_BB","tacs_Uniform_Garment_LS_ES_GP_BB","tacs_Uniform_Garment_RS_GS_GP_BB","tacs_Uniform_Garment_RS_ES_GP_BB","tacs_Uniform_Garment_LS_GS_EP_TB","tacs_Uniform_Garment_RS_GS_EP_TB","tacs_Uniform_Garment_LS_GS_BP_BB","tacs_Uniform_Garment_RS_GS_BP_BB"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "tacs_Uniform_Garment_LS_BS_BP_BB";
		_rpilotUniform = "U_B_HeliPilotCoveralls";
		// ========== Headgear ===========
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["H_HelmetSpecB","H_HelmetSpecB_paint1","H_HelmetSpecB","H_HelmetSpecB_snakeskin"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};		
			case "softcover" : {
				_plebHelmetArray = ["rhsusf_bowman_cap","H_Cap_headphones","H_Cap_oli","H_Cap_grn","H_Booniehat_oli","H_Watchcap_camo","H_Booniehat_rgr"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "opshelm" : {
				_plebHelmetArray = ["H_HelmetB_light","H_HelmetB_light_snakeskin","H_HelmetB_light","H_HelmetB_light_grass","H_HelmetB_light"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["H_HelmetSpecB","H_HelmetSpecB_paint1","H_HelmetSpecB","H_HelmetSpecB_snakeskin"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		_crewmanHelmetArray = ["rhsusf_cvc_green_helmet","rhsusf_cvc_green_ess","rhsusf_cvc_green_helmet","rhsusf_cvc_green_ess","rhsusf_cvc_green_helmet"];	
		_crewmanHRandom = (floor(random (count _crewmanHelmetArray))); 
		_crewmanHelmet = _crewmanHelmetArray select _crewmanHRandom; // enter single string value to remove randommess
		// ============ Vests ============
		_plebVest = "tacs_Vest_PlateCarrierFull_Green";
		_gunnerVest = "V_PlateCarrierIAGL_oli";
		_glVest = "tacs_Vest_PlateCarrierFull_Green";
		_medVest = "tacs_Vest_PlateCarrierFull_Green";
		_pilotVest = "V_TacVest_blk";
		_crewVest = "V_TacVest_blk";
		// ============ Rucks ============
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_oli";
		// ============ SpecOps ========== 
		_specUniform = "tacs_Uniform_Garment_RS_GS_GP_BB";
		_specHelmet = "H_HelmetB_light";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "tacs_Vest_PlateCarrierFull_Green";
		_specRuck = "B_AssaultPack_rgr";
		// ==============================
	};
};

// =======================================================================
// ========================= Non-Camo Specific ===========================
// =======================================================================

// ================== Vehicle Crews ==================

_rpilotUniform = "U_B_HeliPilotCoveralls";
_fpilotUniform = "U_I_PilotCoveralls";

_crewmanHelmet = "H_HelmetCrew_B";
_rotaryPilotHelmet = "H_PilotHelmetHeli_I";
_rotaryCrewHelmet = "H_CrewHelmetHeli_I";
_fixedPilotHelmet = "H_PilotHelmetFighter_I";

_crewVest = "V_TacVest_oli";
_pilotVest = "V_TacVest_oli";

// =================== Sniper Team ===================

_sniperUniform = "U_I_GhillieSuit";
_sniperVest = "V_TacVest_oli";
_sniperRuck = "B_AssaultPack_rgr";

// =================== Diver Gear ===================

_diverUniform = "U_I_Wetsuit";
_diverVest = "V_RebreatherB";
_diverRuck = "B_AssaultPack_blk";
_divingGoggles = "G_I_Diving";

// =================== Radio Rucks ===================

switch (_radioSelection) do {
	case "detection" : {
		private "_side";
		_side = toLower format ["%1", side player];
		// BLUFOR UAV
		if (_side == "west") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
		// OPFOR UAV
		if (_side == "east") then { 
			_uavRuck = "O_UAV_01_backpack_F";
			_uavTool = "O_UavTerminal";
		};
		// INDFOR UAV
		if (_side == "guer") then { 
			_uavRuck = "I_UAV_01_backpack_F";
			_uavTool = "I_UavTerminal";
		};
		// Civilian UAV
		if (_side == "civ") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
	};
	default {
		_uavRuck = "B_UAV_01_backpack_F";
		_uavTool = "B_UavTerminal";
	};
};

// =======================================================================