// AssignGear - Uniforms
// by Mr. Agnet
// - Defines all of the wearable items to be used by the assignGear script.
// - Current Side, Faction: BLUFOR, COMMONWEALTH
// - Primary Mod: KMNP
// - Variables: _camoPattern - "canada", "canadades", "auscam", "amcu", "nz"
//				_headgear - "helmets", "softcover"

// =======================================================================
// Declares variables

private [
"_plebUniformArray","_plebRandom","_plebUniform",
"_plebHelmetArray","_plebHRandom","_plebHelmet",
"_plebVest","_gunnerVest","_glVest","_medVest",
"_smallRuck","_medRuck","_largeRuck",
"_specUniform","_specHelmet","_specGoggles","_specVest","_specRuck",
"_crewUniform","_rpilotUniform","_fpilotUniform",
"_crewmanHelmetArray","_crewmanHRandom","_crewmanHelmet","_rotaryPilotHelmet","_rotaryCrewHelmet","_fixedPilotHelmet",
"_crewVest","_pilotVest",
"_sniperUniform","_sniperVest","_sniperRuck",
"_diverUniform","_diverVest","_diverRuck",
"_goggles","_divingGoggles","_insignia",
"_airRadioRuck","_radioRuck","_diverRadioRuck","_uavRuck","_uavTool"
];

// =======================================================================
// =========================== Camo Specific =============================
// =======================================================================

_goggles = "";	// leave as "" for player defined
_insignia = ""; 

switch (_camoPattern) do {
	case "canada" : {
		// ==================== Uniforms ==================

		_plebUniformArray = ["MNP_CombatUniform_Canada","MNP_CombatUniform_Canada_S","MNP_CombatUniform_Canada"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_Canada_S";
		_rpilotUniform = "MNP_CombatUniform_Canada_S";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_Canada_T","MNP_Helmet_Canada_T","MNP_Helmet_Canada_T"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["MNP_Boonie_GER_T"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_Canada_T","MNP_Helmet_Canada_T","MNP_Helmet_Canada_T"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_Canada_T";
		_gunnerVest = "MNP_Vest_Canada_T2";
		_glVest = "MNP_Vest_Canada_T2";
		_medVest = "MNP_Vest_Canada_T";
		_crewVest = "V_TacVest_oli";
		_pilotVest = "V_TacVest_oli";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_oli";
		
		// ==================== SpecOps ===================
		
		_specUniform = "MNP_CombatUniform_Canada_S";
		_specHelmet = "MNP_Helmet_Canada_T";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "MNP_Vest_Canada_T2";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
	case "canadades" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["MNP_CombatUniform_Canada_D","MNP_CombatUniform_Canada_DS","MNP_CombatUniform_Canada_D"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_Canada_DS";
		_rpilotUniform = "MNP_CombatUniform_Canada_DS";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_Canada_D","MNP_Helmet_Canada_D","MNP_Helmet_Canada_D"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["MNP_Boonie_CAN_D"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_Canada_D","MNP_Helmet_Canada_D","MNP_Helmet_Canada_D"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_Canada_D";
		_gunnerVest = "MNP_Vest_Canada_D2";
		_glVest = "MNP_Vest_Canada_D2";
		_medVest = "MNP_Vest_Canada_D";
		_crewVest = "V_TacVest_blk";
		_pilotVest = "V_TacVest_blk";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_cbr";
		
		// ===================== SpecOps ==================
		
		_specUniform = "MNP_CombatUniform_Canada_DS";
		_specHelmet = "MNP_Helmet_Canada_D";
		_specGoggles = "G_Bandanna_tan";
		_specVest = "MNP_Vest_Canada_D2";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
	case "auscam" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["MNP_CombatUniform_Australia","MNP_CombatUniform_Australia_S","MNP_CombatUniform_Australia"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_Australia_S";
		_rpilotUniform = "MNP_CombatUniform_Australia_S";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_Australia","MNP_Helmet_Australia","MNP_Helmet_Australia"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["MNP_Boonie_AUS"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_Australia","MNP_Helmet_Australia","MNP_Helmet_Australia"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_Australia_B";
		_gunnerVest = "MNP_Vest_Australia";
		_glVest = "MNP_Vest_Australia";
		_medVest = "MNP_Vest_Australia_B";
		_crewVest = "V_TacVest_oli";
		_pilotVest = "V_TacVest_oli";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_oli";
		
		// ===================== SpecOps ==================
		
		_specUniform = "MNP_CombatUniform_Australia_S";
		_specHelmet = "MNP_Helmet_Australia";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "MNP_Vest_Australia";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
	case "amcu" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["MNP_CombatUniform_AMCU_T","MNP_CombatUniform_AMCU_ST","MNP_CombatUniform_AMCU_T"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_AMCU_ST";
		_rpilotUniform = "MNP_CombatUniform_AMCU_ST";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_AMCU","MNP_Helmet_AMCU","MNP_Helmet_AMCU"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["MNP_Boonie_AMCU"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_AMCU","MNP_Helmet_AMCU","MNP_Helmet_AMCU"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_AMCU";
		_gunnerVest = "MNP_Vest_AMCU_2";
		_glVest = "MNP_Vest_AMCU_2";
		_medVest = "MNP_Vest_AMCU";
		_crewVest = "V_TacVest_oli";
		_pilotVest = "V_TacVest_oli";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_oli";
		
		// ===================== SpecOps ==================
		
		_specUniform = "MNP_CombatUniform_AMCU_ST";
		_specHelmet = "MNP_Helmet_AMCU";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "MNP_Vest_AMCU_2";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
	case "nz" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["MNP_CombatUniform_NZ_A","MNP_CombatUniform_NZ_B","MNP_CombatUniform_NZ_A"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_NZ_B";
		_rpilotUniform = "MNP_CombatUniform_NZ_B";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_NZ","MNP_Helmet_NZ","MNP_Helmet_NZ"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["MNP_Boonie_NZ"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_NZ","MNP_Helmet_NZ","MNP_Helmet_NZ"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_Olive_1";
		_gunnerVest = "MNP_Vest_Olive_2";
		_glVest = "MNP_Vest_Olive_2";
		_medVest = "MNP_Vest_Olive_1";
		_crewVest = "V_TacVest_oli";
		_pilotVest = "V_TacVest_oli";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_oli";
		
		// ===================== SpecOps ==================
		
		_specUniform = "MNP_CombatUniform_NZ_B";
		_specHelmet = "MNP_Helmet_NZ";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "MNP_Vest_Olive_2";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
	default {
		// ==================== Uniforms ==================

		_plebUniformArray = ["MNP_CombatUniform_Canada","MNP_CombatUniform_Canada_S","MNP_CombatUniform_Canada"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_Canada_S";
		_rpilotUniform = "MNP_CombatUniform_Canada_S";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_Canada_T","MNP_Helmet_Canada_T","MNP_Helmet_Canada_T"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["MNP_Boonie_GER_T"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_Canada_T","MNP_Helmet_Canada_T","MNP_Helmet_Canada_T"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_Canada_T";
		_gunnerVest = "MNP_Vest_Canada_T2";
		_glVest = "MNP_Vest_Canada_T2";
		_medVest = "MNP_Vest_Canada_T";
		_crewVest = "V_TacVest_oli";
		_pilotVest = "V_TacVest_oli";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_oli";
		
		// ==================== SpecOps ===================
		
		_specUniform = "MNP_CombatUniform_Canada_S";
		_specHelmet = "MNP_Helmet_Canada_T";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "MNP_Vest_Canada_T2";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
};

// =======================================================================
// ========================= Non-Camo Specific ===========================
// =======================================================================

// ================== Vehicle Crews ==================

_fpilotUniform = "U_B_PilotCoveralls";
_crewmanHelmet = "H_HelmetCrew_B";
_rotaryPilotHelmet = "H_PilotHelmetHeli_B";
_rotaryCrewHelmet = "H_CrewHelmetHeli_B";
_fixedPilotHelmet = "H_PilotHelmetFighter_B";

// =================== Sniper Team ===================

_sniperUniform = "U_B_GhillieSuit";
_sniperVest = "V_TacVest_oli";
_sniperRuck = "B_AssaultPack_rgr";

// =================== Diver Gear ===================

_diverUniform = "U_B_Wetsuit";
_diverVest = "V_RebreatherB";
_diverRuck = "B_AssaultPack_blk";
_divingGoggles = "G_B_Diving";

// =================== Radio Rucks ===================

switch (_radioSelection) do {
	case "detection" : {
		private "_side";
		_side = toLower format ["%1", side player];
		// BLUFOR UAV
		if (_side == "west") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
		// OPFOR UAV
		if (_side == "east") then { 
			_uavRuck = "O_UAV_01_backpack_F";
			_uavTool = "O_UavTerminal";
		};
		// INDFOR UAV
		if (_side == "guer") then { 
			_uavRuck = "I_UAV_01_backpack_F";
			_uavTool = "I_UavTerminal";
		};
		// Civilian UAV
		if (_side == "civ") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
	};
	default {
		_uavRuck = "B_UAV_01_backpack_F";
		_uavTool = "B_UavTerminal";
	};
};

// =======================================================================