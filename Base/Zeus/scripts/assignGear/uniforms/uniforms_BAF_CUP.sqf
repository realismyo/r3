// AssignGear - Uniforms
// by Mr. Agnet
// - Defines all of the wearable items to be used by the assignGear script.
// - Current Side, Faction: BLUFOR, BAF
// - Primary Mod: CUP
// - Variables: _camoPattern - "dpm", "ddpm"
//				_headgear - "helmets", "softcover"

// =======================================================================
// Declares variables

private [
"_plebUniformArray","_plebRandom","_plebUniform",
"_plebHelmetArray","_plebHRandom","_plebHelmet",
"_plebVest","_gunnerVest","_glVest","_medVest",
"_smallRuck","_medRuck","_largeRuck",
"_specUniform","_specHelmet","_specGoggles","_specVest","_specRuck",
"_crewUniform","_rpilotUniform","_fpilotUniform",
"_crewmanHelmetArray","_crewmanHRandom","_crewmanHelmet","_crewmanHelmetArray","_crewmanHRandom","_rotaryPilotHelmet","_rotaryCrewHelmet","_fixedPilotHelmet",
"_crewVest","_pilotVest",
"_sniperUniform","_sniperVest","_sniperRuck",
"_diverUniform","_diverVest","_diverRuck",
"_goggles","_divingGoggles","_insignia",
"_airRadioRuck","_radioRuck","_diverRadioRuck","_uavRuck","_uavTool"
];

// =======================================================================
// =========================== Camo Specific =============================
// =======================================================================

_goggles = "";	// leave as "" for player defined
_insignia = ""; 

switch (_camoPattern) do {
	case "dpm" : {
		// ==================== Uniforms ==================

		_plebUniformArray = ["CUP_U_B_BAF_DPM_S2_UnRolled","CUP_U_B_BAF_DPM_S1_RolledUp","CUP_U_B_BAF_DPM_S2_UnRolled"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "CUP_U_B_BAF_DPM_S1_RolledUp";
		_rpilotUniform = "CUP_U_B_BAF_DPM_S1_RolledUp";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["CUP_H_BAF_Helmet_1_DPM","CUP_H_BAF_Helmet_2_DPM","CUP_H_BAF_Helmet_3_DPM","CUP_H_BAF_Helmet_4_DPM"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Booniehat_oli","H_Cap_oli","H_Bandanna_sgg"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["CUP_H_BAF_Helmet_1_DPM","CUP_H_BAF_Helmet_2_DPM","CUP_H_BAF_Helmet_3_DPM"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		_crewmanHelmet = "CUP_H_BAF_Crew_Helmet_DPM";
		
		// ===================== Vests ====================
		
		_plebVest = "CUP_V_BAF_Osprey_Mk2_DPM_Soldier2";
		_gunnerVest = "CUP_V_BAF_Osprey_Mk2_DPM_Soldier1";
		_glVest = "CUP_V_BAF_Osprey_Mk2_DPM_Grenadier";
		_medVest = "CUP_V_BAF_Osprey_Mk2_DPM_Medic";
		_pilotVest = "CUP_V_BAF_Osprey_Mk2_DPM_Pilot";
		_crewVest = "CUP_V_BAF_Osprey_Mk2_DPM_Crewman";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_oli";
		
		// ==================== SpecOps ===================
		
		_specUniform = "CUP_U_B_BAF_DPM_S1_RolledUp";
		_specHelmet = "CUP_H_BAF_Helmet_4_DPM";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "CUP_V_BAF_Osprey_Mk2_DPM_Soldier1";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
	case "ddpm" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["CUP_U_B_BAF_DDPM_S2_UnRolled","CUP_U_B_BAF_DDPM_S1_RolledUp","CUP_U_B_BAF_DDPM_S2_UnRolled"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "CUP_U_B_BAF_DDPM_S1_RolledUp";
		_rpilotUniform = "CUP_U_B_BAF_DDPM_S1_RolledUp";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["CUP_H_BAF_Helmet_1_DDPM","CUP_H_BAF_Helmet_2_DDPM","CUP_H_BAF_Helmet_3_DDPM","CUP_H_BAF_Helmet_4_DDPM"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Booniehat_oli","H_Cap_oli","H_Bandanna_sgg"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["CUP_H_BAF_Helmet_1_DPM","CUP_H_BAF_Helmet_2_DPM","CUP_H_BAF_Helmet_3_DPM"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		_crewmanHelmet = "CUP_H_BAF_Crew_Helmet_DPM";
		
		// ===================== Vests ====================
		
		_plebVest = "CUP_V_BAF_Osprey_Mk2_DDPM_Soldier2";
		_gunnerVest = "CUP_V_BAF_Osprey_Mk2_DDPM_Soldier1";
		_glVest = "CUP_V_BAF_Osprey_Mk2_DDPM_Grenadier";
		_medVest = "CUP_V_BAF_Osprey_Mk2_DDPM_Medic";
		_pilotVest = "CUP_V_BAF_Osprey_Mk2_DDPM_Pilot";
		_crewVest = "CUP_V_BAF_Osprey_Mk2_DDPM_Crewman";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_cbr";
		_medRuck = "B_Kitbag_cbr";
		_largeRuck = "B_Carryall_cbr";
		
		// ===================== SpecOps ==================
		
		_specUniform = "CUP_U_B_BAF_DDPM_S1_RolledUp";
		_specHelmet = "CUP_H_BAF_Helmet_4_DDPM";
		_specGoggles = "G_Bandanna_khk";
		_specVest = "CUP_V_BAF_Osprey_Mk2_DDPM_Soldier1";
		_specRuck = "B_AssaultPack_cbr";
		
		// ================================================
	};
	default {
		// ==================== Uniforms ==================

		_plebUniformArray = ["CUP_U_B_BAF_DPM_S2_UnRolled","CUP_U_B_BAF_DPM_S1_RolledUp","CUP_U_B_BAF_DPM_S2_UnRolled"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "CUP_U_B_BAF_DPM_S1_RolledUp";
		_rpilotUniform = "CUP_U_B_BAF_DPM_S1_RolledUp";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["CUP_H_BAF_Helmet_1_DPM","CUP_H_BAF_Helmet_2_DPM","CUP_H_BAF_Helmet_3_DPM","CUP_H_BAF_Helmet_4_DPM"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Booniehat_oli","H_Cap_oli","H_Bandanna_sgg"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["CUP_H_BAF_Helmet_1_DPM","CUP_H_BAF_Helmet_2_DPM","CUP_H_BAF_Helmet_3_DPM"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		_crewmanHelmet = "CUP_H_BAF_Crew_Helmet_DPM";
		
		// ===================== Vests ====================
		
		_plebVest = "CUP_V_BAF_Osprey_Mk2_DPM_Soldier2";
		_gunnerVest = "CUP_V_BAF_Osprey_Mk2_DPM_Soldier1";
		_glVest = "CUP_V_BAF_Osprey_Mk2_DPM_Grenadier";
		_medVest = "CUP_V_BAF_Osprey_Mk2_DPM_Medic";
		_pilotVest = "CUP_V_BAF_Osprey_Mk2_DPM_Pilot";
		_crewVest = "CUP_V_BAF_Osprey_Mk2_DPM_Crewman";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_oli";
		
		// ==================== SpecOps ===================
		
		_specUniform = "CUP_U_B_BAF_DPM_S1_RolledUp";
		_specHelmet = "CUP_H_BAF_Helmet_4_DPM";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "CUP_V_BAF_Osprey_Mk2_DPM_Soldier1";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
};

// =======================================================================
// ========================= Non-Camo Specific ===========================
// =======================================================================

// ================== Vehicle Crews ==================

_fpilotUniform = "U_B_PilotCoveralls";
_rotaryPilotHelmet = "CUP_H_BAF_Helmet_Pilot";
_rotaryCrewHelmet = "CUP_H_BAF_Helmet_Pilot";
_fixedPilotHelmet = "H_PilotHelmetFighter_B";

// =================== Sniper Team ===================

_sniperUniform = "U_B_GhillieSuit";
_sniperVest = "V_TacVest_oli";
_sniperRuck = "B_AssaultPack_rgr";

// =================== Diver Gear ===================

_diverUniform = "U_B_Wetsuit";
_diverVest = "V_RebreatherB";
_diverRuck = "B_AssaultPack_blk";
_divingGoggles = "G_B_Diving";

// =================== Radio Rucks ===================

switch (_radioSelection) do {
	case "detection" : {
		private "_side";
		_side = toLower format ["%1", side player];
		// BLUFOR UAV
		if (_side == "west") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
		// OPFOR UAV
		if (_side == "east") then { 
			_uavRuck = "O_UAV_01_backpack_F";
			_uavTool = "O_UavTerminal";
		};
		// INDFOR UAV
		if (_side == "guer") then { 
			_uavRuck = "I_UAV_01_backpack_F";
			_uavTool = "I_UavTerminal";
		};
		// Civilian UAV
		if (_side == "civ") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
	};
	default {
		_uavRuck = "B_UAV_01_backpack_F";
		_uavTool = "B_UavTerminal";
	};
};

// =======================================================================