// AssignGear - Uniforms
// by Mr. Agnet
// - Defines all of the wearable items to be used by the assignGear script.
// - Current Side, Faction: OPFOR, CSAT
// - Primary Mod: VANILLA
// - Variables: _camoPattern - "arid", "urban"
//				_headgear - "helmets", "opshelm", "softcover"

// =======================================================================
// Declares variables

private [
"_plebUniformArray","_plebRandom","_plebUniform",
"_plebHelmetArray","_plebHRandom","_plebHelmet",
"_plebVest","_gunnerVest","_glVest","_medVest",
"_smallRuck","_medRuck","_largeRuck",
"_specUniform","_specHelmet","_specGoggles","_specVest","_specRuck",
"_crewUniform","_rpilotUniform","_fpilotUniform",
"_crewmanHelmetArray","_crewmanHRandom","_crewmanHelmet","_rotaryPilotHelmet","_rotaryCrewHelmet","_fixedPilotHelmet",
"_crewVest","_pilotVest",
"_sniperUniform","_sniperVest","_sniperRuck",
"_diverUniform","_diverVest","_diverRuck",
"_goggles","_insignia","_divingGoggles",
"_airRadioRuck","_radioRuck","_diverRadioRuck","_uavRuck","_uavTool"
];

// =======================================================================
// =========================== Camo Specific =============================
// =======================================================================

_goggles = "";	// leave as "" for player defined
_insignia = ""; 

switch (_camoPattern) do {
	case "arid" : {
		// ==================== Uniforms ==================

		_plebUniformArray = ["U_O_CombatUniform_ocamo","U_O_CombatUniform_ocamo","U_O_CombatUniform_ocamo"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "U_O_CombatUniform_ocamo";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["H_HelmetO_ocamo","H_HelmetLeaderO_ocamo","H_HelmetO_ocamo","H_HelmetLeaderO_ocamo","H_HelmetO_ocamo"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "opshelm" : {
				_plebHelmetArray = ["H_HelmetSpecO_blk","H_HelmetSpecO_blk","H_HelmetSpecO_blk"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_MilCap_ocamo","H_Booniehat_khk_hs","H_Watchcap_khk","H_Bandanna_khk_hs","H_Cap_brn_SPECOPS"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["H_HelmetO_ocamo","H_HelmetLeaderO_ocamo","H_HelmetO_ocamo","H_HelmetLeaderO_ocamo","H_HelmetO_ocamo"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "V_PlateCarrierIA1_khk";
		_gunnerVest = "V_PlateCarrierIA1_khk";
		_glVest = "V_PlateCarrierIA1_khk";
		_medVest = "V_PlateCarrierIA1_khk";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_FieldPack_ocamo";
		_medRuck = "B_TacticalPack_ocamo";
		_largeRuck = "B_Carryall_ocamo";
		
		// ==================== SpecOps ===================
		
		_specUniform = "U_O_CombatUniform_ocamo";
		_specHelmet = "H_HelmetSpecO_blk";
		_specGoggles = "G_Balaclava_blk";
		_specVest = "V_PlateCarrierIA1_khk";
		_specRuck = "B_FieldPack_ocamo";
		
		// ================================================
	};
	case "urban" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["U_O_CombatUniform_oucamo","U_O_CombatUniform_oucamo","U_O_CombatUniform_oucamo"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "U_O_CombatUniform_oucamo";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["H_HelmetO_oucamo","H_HelmetLeaderO_oucamo","H_HelmetO_oucamo","H_HelmetLeaderO_oucamo","H_HelmetO_oucamo"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "opshelm" : {
				_plebHelmetArray = ["H_HelmetSpecO_blk","H_HelmetSpecO_blk","H_HelmetSpecO_blk"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_MilCap_gry","H_Watchcap_blk","H_Bandanna_gry"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["H_HelmetO_oucamo","H_HelmetLeaderO_oucamo","H_HelmetO_oucamo","H_HelmetLeaderO_oucamo","H_HelmetO_oucamo"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "V_PlateCarrierIA1_khk";
		_gunnerVest = "V_PlateCarrierIA1_khk";
		_glVest = "V_PlateCarrierIA1_khk";
		_medVest = "V_PlateCarrierIA1_khk";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_FieldPack_oucamo";
		_medRuck = "B_TacticalPack_blk";
		_largeRuck = "B_Carryall_oucamo";
		
		// ===================== SpecOps ==================
		
		_specUniform = "U_O_CombatUniform_oucamo";
		_specHelmet = "H_HelmetSpecO_blk";
		_specGoggles = "G_Balaclava_blk";
		_specVest = "V_PlateCarrierIA1_khk";
		_specRuck = "B_FieldPack_oucamo";
		
		// ================================================
	};
	default {
		// ==================== Uniforms ==================

		_plebUniformArray = ["U_O_CombatUniform_ocamo","U_O_CombatUniform_ocamo","U_O_CombatUniform_ocamo"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "U_O_CombatUniform_ocamo";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["H_HelmetO_ocamo","H_HelmetLeaderO_ocamo","H_HelmetO_ocamo","H_HelmetLeaderO_ocamo","H_HelmetO_ocamo"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "opshelm" : {
				_plebHelmetArray = ["H_HelmetSpecO_blk","H_HelmetSpecO_blk","H_HelmetSpecO_blk"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_MilCap_ocamo","H_Booniehat_khk_hs","H_Watchcap_khk","H_Bandanna_khk_hs","H_Cap_brn_SPECOPS"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["H_HelmetO_ocamo","H_HelmetLeaderO_ocamo","H_HelmetO_ocamo","H_HelmetLeaderO_ocamo","H_HelmetO_ocamo"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "V_PlateCarrierIA1_khk";
		_gunnerVest = "V_PlateCarrierIA1_khk";
		_glVest = "V_PlateCarrierIA1_khk";
		_medVest = "V_PlateCarrierIA1_khk";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_FieldPack_ocamo";
		_medRuck = "B_TacticalPack_ocamo";
		_largeRuck = "B_Carryall_ocamo";
		
		// ==================== SpecOps ===================
		
		_specUniform = "U_O_CombatUniform_ocamo";
		_specHelmet = "H_HelmetSpecO_blk";
		_specGoggles = "G_Balaclava_blk";
		_specVest = "V_PlateCarrierIA1_khk";
		_specRuck = "B_FieldPack_ocamo";
		
		// ================================================
	};
};

// =======================================================================
// ========================= Non-Camo Specific ===========================
// =======================================================================

// ================== Vehicle Crews ==================

_rpilotUniform = "U_O_PilotCoveralls";
_fpilotUniform = "U_O_PilotCoveralls";

_crewmanHelmet = "H_HelmetCrew_O";
_rotaryPilotHelmet = "H_PilotHelmetHeli_O";
_rotaryCrewHelmet = "H_CrewHelmetHeli_O";
_fixedPilotHelmet = "H_PilotHelmetFighter_O";

_crewVest = "V_TacVest_khk";
_pilotVest = "V_TacVest_khk";

// =================== Sniper Team ===================

_sniperUniform = "U_O_GhillieSuit";
_sniperVest = "V_TacVest_khk";
_sniperRuck = "B_FieldPack_ocamo";

// =================== Diver Gear ===================

_diverUniform = "U_O_Wetsuit";
_diverVest = "V_RebreatherB";
_diverRuck = "B_AssaultPack_blk";
_divingGoggles = "G_O_Diving";

// =================== Radio Rucks ===================

switch (_radioSelection) do {
	case "detection" : {
		private "_side";
		_side = toLower format ["%1", side player];
		// BLUFOR UAV
		if (_side == "west") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
		// OPFOR UAV
		if (_side == "east") then { 
			_uavRuck = "O_UAV_01_backpack_F";
			_uavTool = "O_UavTerminal";
		};
		// INDFOR UAV
		if (_side == "guer") then { 
			_uavRuck = "I_UAV_01_backpack_F";
			_uavTool = "I_UavTerminal";
		};
		// Civilian UAV
		if (_side == "civ") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
	};
	default {
		_uavRuck = "B_UAV_01_backpack_F";
		_uavTool = "B_UavTerminal";
	};
};

// =======================================================================