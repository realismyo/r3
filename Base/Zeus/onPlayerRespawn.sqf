// ====================================================================================
//	File: onPlayerRespawn.sqf
//	Last Modified By: Mr. Agnet
// https://community.bistudio.com/wiki/Arma_3_Respawn
// ====================================================================================
// Force player into TFAR spectator mode
[true] call acre_api_fnc_setSpectator;