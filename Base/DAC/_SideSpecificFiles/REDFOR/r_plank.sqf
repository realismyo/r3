/*
Plank Player Initialisation Script
By Mr. Agnet/Sporkfist
If variable 'r_var_plankForts' is true, assigns players their relevant fortification actions.
Is configured for whichever framework side the script is in. Current: BLUFOR.
==================== Usage ====================
- Script follows the unit naming standard of the R3 framework.
- If the units in your mission aren't named as such, this will not work unless you modify the calls.
- This is one massive script to allow for alterations to any and all roles.

Call: [player, [1, 0, 0, 0, 0, 0, 0, 0]] call plank_deploy_fnc_init;  
- Each number in the array corresponds to a certain fortification, and how many of those the player will get.
- Check plank_fortifications.sqf for respective fortifications.
*/

private ["_blu","_opf","_ind"];

// Let player get into the game first.
waitUntil {time > 1};
if (hasInterface) then { waitUntil { !isNull player && isPlayer player }; };

// Define variable if it doesn't exist - default false.
if (isNil "r_var_plankForts") then { r_var_plankForts = false; };

// Compiles Functions
call compile preprocessFileLineNumbers "scripts\plank\plank_init.sqf";

if (r_var_plankForts) then {																		
	if ((!isNull RED_PLT11_CO) && player == RED_PLT11_CO) then {  		[player, [1, 0, 0, 0, 0, 0, 0, 0]] call plank_deploy_fnc_init; };
	if ((!isNull RED_PLT11_SGT) && player == RED_PLT11_SGT) then {  		[player, [0, 0, 0, 0, 0, 0, 0, 0]] call plank_deploy_fnc_init; };
	if ((!isNull RED_PLT11_MEDIC) && player == RED_PLT11_MEDIC) then {  	[player, [0, 0, 0, 0, 0, 0, 0, 0]] call plank_deploy_fnc_init; };
	if ((!isNull RED_PLT11_FAC) && player == RED_PLT11_FAC) then {  	[player, [0, 0, 0, 0, 0, 0, 0, 0]] call plank_deploy_fnc_init; };
	if ((!isNull RED_PLT11_RM) && player == RED_PLT11_RM) then {  	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init; };
	
	if ((!isNull RED_A11_SECCO) && player == RED_A11_SECCO) then {  [player, [0, 1, 0, 0, 0, 0, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_A11_AR1) && player == RED_A11_AR1) then {  	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_A11_AAR1) && player == RED_A11_AAR1) then {  	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_A11_GREN) && player == RED_A11_GREN) then { 	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_A11_TL2) && player == RED_A11_TL2) then {  	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_A11_AR2) && player == RED_A11_AR2) then { 	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_A11_AAR2) && player == RED_A11_AAR2) then { 	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_A11_RMAT) && player == RED_A11_RMAT) then { 	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	
	if ((!isNull RED_B11_SECCO) && player == RED_B11_SECCO) then {  [player, [0, 1, 0, 0, 0, 0, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_B11_AR1) && player == RED_B11_AR1) then {  	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_B11_AAR1) && player == RED_B11_AAR1) then {  	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_B11_GREN) && player == RED_B11_GREN) then { 	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_B11_TL2) && player == RED_B11_TL2) then {  	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_B11_AR2) && player == RED_B11_AR2) then { 	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_B11_AAR2) && player == RED_B11_AAR2) then { 	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_B11_RMAT) && player == RED_B11_RMAT) then { 	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	
	if ((!isNull RED_C11_SECCO) && player == RED_C11_SECCO) then {  [player, [0, 1, 0, 0, 0, 0, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_C11_AR1) && player == RED_C11_AR1) then {  	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_C11_AAR1) && player == RED_C11_AAR1) then {  	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_C11_GREN) && player == RED_C11_GREN) then { 	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_C11_TL2) && player == RED_C11_TL2) then {  	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_C11_AR2) && player == RED_C11_AR2) then { 	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_C11_AAR2) && player == RED_C11_AAR2) then { 	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_C11_RMAT) && player == RED_C11_RMAT) then { 	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	
	if ((!isNull RED_D11_SECCO) && player == RED_D11_SECCO) then {  [player, [0, 1, 0, 0, 0, 0, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_D11_MMG1) && player == RED_D11_MMG1) then {  	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_D11_MMGASS1) && player == RED_D11_MMGASS1) then {  	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_D11_RMAT) && player == RED_D11_RMAT) then {  	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_D11_FTL2) && player == RED_D11_FTL2) then { 	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_D11_MMG2) && player == RED_D11_MMG2) then { 	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_D11_MMGASS2) && player == RED_D11_MMGASS2) then { 	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_D11_MARKSMAN1) && player == RED_D11_MARKSMAN1) then {  	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_D11_FTL3) && player == RED_D11_FTL3) then {  	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_D11_MMG3) && player == RED_D11_MMG3) then { 	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_D11_MMGASS3) && player == RED_D11_MMGASS3) then { 	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
	if ((!isNull RED_D11_MARKSMAN2) && player == RED_D11_MARKSMAN2) then { 	[player, [0, 0, 3, 1, 0, 2, 0, 0]] call plank_deploy_fnc_init;  };
		
};