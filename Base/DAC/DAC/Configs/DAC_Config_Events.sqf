//////////////////////////////
//    Dynamic-AI-Creator    //
//    Version 3.1b - 2014   //
//--------------------------//
//    DAC_Config_Events     //
//--------------------------//
//    Script by Silola      //
//    silola@freenet.de     //
//////////////////////////////

private [
			"_TypNumber","_TempArray","_Events_Vehicle",
			"_Events_Unit_S","_Events_Unit_V","_Events_Unit_T","_Events_Unit_A","_Events_Unit_C","_Events_Unit_H"
		];

			_TypNumber = _this select 0;_TempArray = [];
 
switch (_TypNumber) do {
//-------------------------------------------------------------------------------------------------------------------------------------------
//------------ Events => Create | ReachWP | NotAliveGroup | NotAliveUnit | (BeforeReduce) | (AfterBuildUp)  | (DetectEnemys) ----------------
//-------------------------------------------------------------------------------------------------------------------------------------------
	case 1:{
		_Events_Unit_S =	[
								["{_x setVariable [""asr_ai_exclude"", false, true];} forEach (units _group); {_x removeWeapon (handgunWeapon _x); _x selectWeapon (primaryWeapon _x);} forEach (units _group);"],
								[], 
								[], 
								[],	
								["_unit setVariable [""asr_ai_exclude"", false, true]; _unit removeWeapon (handgunWeapon _unit); _unit selectWeapon (primaryWeapon _unit);"],
								["_unit setVariable [""asr_ai_exclude"", false, true]; _unit removeWeapon (handgunWeapon _unit); _unit selectWeapon (primaryWeapon _unit);"], 
								[]	
							];
		_Events_Unit_V = 	[
								["{_x setVariable [""asr_ai_exclude"", false, true];} forEach (units _group);"],
								[],
								[],
								[],
								["_unit setVariable [""asr_ai_exclude"", false, true]; _unit removeWeapon (handgunWeapon _unit); _unit selectWeapon (primaryWeapon _unit);"],
								["_unit setVariable [""asr_ai_exclude"", false, true]; _unit removeWeapon (handgunWeapon _unit); _unit selectWeapon (primaryWeapon _unit);"],
								[]
							];
		_Events_Unit_T = 	[
								["{_x setVariable [""asr_ai_exclude"", false, true];} forEach (units _group);"],
								[],
								[],
								[]
							];
		_Events_Unit_A = 	[
								["{_x setVariable [""asr_ai_exclude"", false, true];} forEach (units _group);"],
								[],
								[],
								[]
							];
		_Events_Unit_C = 	[
								["{_x setVariable [""asr_ai_exclude"", false, true];} forEach (units _group); {if (!isNil (handgunWeapon _x) && ((count (weapons _x)) > 1)) then {_x removeWeapon (handgunWeapon _x); _x selectWeapon (primaryWeapon _x);};} forEach (units _group);"],
								[],
								[],
								[],
								["_unit setVariable [""asr_ai_exclude"", false, true]; _unit removeWeapon (handgunWeapon _unit); _unit selectWeapon (primaryWeapon _unit);"],
								["_unit setVariable [""asr_ai_exclude"", false, true]; _unit removeWeapon (handgunWeapon _unit); _unit selectWeapon (primaryWeapon _unit);"]
							];
		_Events_Unit_H = 	[
								["{_x setVariable [""asr_ai_exclude"", false, true];} forEach (units _group);"],
								[],
								[],
								[]
							];
		_Events_Vehicle =	[
								[],
								[],
								[]
							];
	};
//-------------------------------------------------------------------------------------------------
	case 2:{
		_Events_Unit_S =	[
								[],
								[],
								[],
								[],
								[],
								[],
								[]
							];
		_Events_Unit_V = 	[
								[],
								[],
								[],
								[],
								[],
								[],
								[]
							];
		_Events_Unit_T = 	[
								[],
								[],
								[],
								[]
							];
		_Events_Unit_A = 	[
								[],
								[],
								[],
								[]
							];
		_Events_Unit_C = 	[
								[],
								[],
								[],
								[],
								[],
								[]
							];
		_Events_Unit_H = 	[
								[],
								[],
								[],
								[]
							];
		_Events_Vehicle =	[
								[],
								[],
								[]
							];
	};
//-------------------------------------------------------------------------------------------------
	case 3:{
		_Events_Unit_S =	[
								[],
								[],
								[],
								[],
								[],
								[],
								[]
							];
		_Events_Unit_V = 	[
								[],
								[],
								[],
								[],
								[],
								[],
								[]
							];
		_Events_Unit_T = 	[
								[],
								[],
								[],
								[]
							];
		_Events_Unit_A = 	[
								[],
								[],
								[],
								[]
							];
		_Events_Unit_C = 	[
								[],
								[],
								[],
								[],
								[],
								[]
							];
		_Events_Unit_H = 	[
								[],
								[],
								[],
								[]
							];
		_Events_Vehicle =	[
								[],
								[],
								[]
							];
	};
//-------------------------------------------------------------------------------------------------

	Default {
				if(DAC_Basic_Value != 5) then{
					DAC_Basic_Value = 5;
					publicVariable "DAC_Basic_Value";
					hintc "Error: DAC_Config_Events > No valid config number";
					diag_log "Error: DAC_Config_Events > No valid config number";
				};
				if(true) exitwith {};
			};
};

_TempArray = [_Events_Unit_S,_Events_Unit_V,_Events_Unit_T,_Events_Unit_A,_Events_Unit_C,_Events_Unit_H,_Events_Vehicle];
_TempArray