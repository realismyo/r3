// Cratepicker rewrite using Ares/Achilles dialogboxes. Also consolidated to 1 file.
// Only uses the r_fnc_assignGear variable (from init.sqf) to determine which gear
// script to run.

// Define variables
private ["_flag"];

// Grab flag and add action
_flag = _this select 0;

// The whole script will be within the addaction command, since it doesn't need to rely on anything external
_flag addaction ["<t color='#dddd00'>" + "Spawn Crate" + "</t>",{ // Everything within the {} will be executed by the flag action

	private ["_dialog_title","_dialog_options","_dialogCount","_dialogResult","_loadoutArray","_loadoutstrings","_loadout","_location"];

	// Store caller (player) and action ID
	_player = _this select 1;
	_action = _this select 2;

	// Define strings of loadout arrays - same as gear scripts. Format is: [DISPLAY STRING,GEARSCRIPT STRING]

	_loadoutArray = [
	
		// Resupply
		["Resupply - HQ","hqammo"],
		["Resupply - Section","sectionammo"],

		// General Ammo
		["Ammo - Rifle","rifleammo"],
		["Ammo - GL","glammo"],
		["Ammo - AR","arammo"],
		["Ammo - MMG","mmgammo"],
		["Ammo - DMR","dmrammo"],
		["Ammo - LAT","latammo"],
		["Ammo - MAT","matammo"],
		["Ammo - HAT","hatammo"],
		["Ammo - AA","aaammo"],

		// Medical
		["Meds - HQ (All Meds)","hqmeds"],
		["Meds - Section (Bandages, Morphine)","secmeds"],
		["Meds - Bodybags","bodybags"],

		// Explosives
		["Explosives - AT Mines","atmines"],
		["Explosives - AP Mines","apmines"],
		["Explosives - Small Explosives","demosmall"],
		["Explosives - Large Explosives","demobig"],
		["Explosives - Mixed Bag","mix"],

		// Tools
		["Tools - HQ (Basic Items, GPS)","hqtools"],
		["Tools - Section (Basic Items)","sectiontools"],
		["Tools - Radios (343, 148, 117F)","radiogear"],
		["Tools - Engineer (Toolkit, Wirecutters, Clacker, Defusal)","engitools"],
		["Tools - CTab Equipment (Ctab, Android)","ctabgear"],
		["Tools - UAV Equipment (Backpack, Terminal)","uavgear"],
		["Tools - NVG Crate (Strobes, NVGs)","nightvisions"],
		["Tools - Flares/Chemlights","nightgear"],
		["Tools - Attachments","attachments"],
		["Tools - Cable Ties","cableties"],
		["Tools - Flashbangs","flashabangs"],

		// Weapons
		["Weapons - Marskman Crate","marskmancrate"],
		["Weapons - Sniper Team","snipercrate"],
		["Weapons - LAT Crate","latcrate"],
		["Weapons - MAT Team","matcrate"],
		["Weapons - HAT Team","hatteam"],
		["Weapons - Diver Pair","diverpair"],

		// CSW
		["CSW - HMG Weapon and Tripod","hmgweapon"],
		["CSW - GMG Weapon and Tripod","gmgweapon"],

		// Misc
		["Misc - Steerable Parachutes","steerchutes"],
		["Misc - Non-Steerable Parachutes","nonsteerchutes"],
		["Misc - Wetsuits","wetsuits"],
		["Misc - Bananas","bananas"],
		["Misc - Blank","Blank"]

	];

	// Pull out the strings - to be used as option text
	_loadoutstrings = [];
	{ _loadoutstrings pushback (_x select 0); } foreach _loadoutArray;

	_dialog_title = "CRATEPICKER";
	_dialog_options = [["Select Crate",_loadoutstrings]];
	
	_dialogResult = [_dialog_title, _dialog_options] call Ares_fnc_ShowChooseDialog;
	
	// Selection is cancelled
	_dialogCount = count _dialogResult;
	if (_dialogCount == 0) exitWith {};

	// Selection is made
	_ZNum = (_dialogResult select 0);

	// The selection number is now stored in _ZNum. Apply to _loadoutArray to pull out loadout string
	_loadout = (_loadoutArray select _ZNum) select 1;

	// Spawn crate and run the loadout script on it - execute on server
	_location = player modeltoworld [1,0,0];
	[[_location,_loadout],{_crate = "Box_Syndicate_Ammo_F" createVehicle (_this select 0); [_crate,(_this select 1)] call r_fnc_assignGear}] remoteExec ["spawn",2,false];

// Finish of addaction here
},[],6,true,false,"",""];


