// AssignGear - Weapons
// by Mr. Agnet
// - Defines all of the weapon classes to be used by the assignGear script.
// - Current Class: L85
// - Primary Mod: CUP
// - Variables: None.

// =======================================================================
// Declares variables

private [
"_rifle","_rifleGL","_rifleScoped","_autoRifle","_carbine","_dmr","_mmg","_smg","_pistol",
"_rifle_spec","_rifleGL_spec",
"_rifleMag","_rifleMatchMag","_rifleTracerMag","_rifleGLMag","_rifleScopedMag","_autoRifleMag","_autoTracerMag","_carbineMag","_dmrMag","_mmgMag","_mmgTracerMag","_smgMag","_pistolMag",
"_rifleMag_spec","_rifleTracerMag_spec","_rifleGLMag_spec","_rifleGLTracerMag_spec",
"_glExplody","_glSmokeOne","_glSmokeTwo","_glFlareOne","_glFlareTwo",
"_shotty","_shottyBuck","_shottySlug","_glHeavy","_glHeavyExplody","_glHSmokesOne","_glHSmokesTwo",
"_lat","_latMag","_matLaunch","_matMag","_matScope","_hatLaunch","_hatMag","_aaLaunch","_aaMag",
"_boltRifle","_boltRifleMag","_amRifle","_amRifleMag",
"_hmgBarrel","_hmgTripod","_hmgMag","_gmgBarrel","_gmgTripod","_gmgMag",
"_rifleDiver","_rifleDiverMagOne","_rifleDiverMagTwo",
"_generalAttachments","_dmrAttachments","_autoRifleAttachments","_mmgAttachments","_specOpsAttachments",
"_sniperAttachments","_amSniperAttachments","_pistolAttachments",
"_scoped_generalAttachments","_scoped_dmrAttachments","_scoped_autoRifleAttachments","_scoped_mmgAttachments","_scoped_specOpsAttachments",
"_suppressed_generalAttachments","_suppressed_dmrAttachments","_suppressed_autoRifleAttachments","_suppressed_mmgAttachments","_suppressed_specOpsAttachments",
"_suppressed_pistolAttachments","_arMagCount"
];

_arMagCount = 2;

// =======================================================================
// =========================== General Weapons ===========================
// =======================================================================

_rifle = "CUP_arifle_L85A2";  
_rifleGL = "CUP_arifle_L85A2_GL"; 
_rifleScoped = "CUP_arifle_L85A2";  
_autoRifle = "CUP_lmg_L110A1";  
_carbine = "CUP_arifle_L85A2"; 
_dmr = "hlc_rifle_m14sopmod"; 
_mmg = "CUP_lmg_L7A2"; 
_smg = "hlc_smg_mp5a4"; 
_pistol = "CUP_hgun_Colt1911"; 

// ================= SpecOps Weapons ==============

_rifle_spec = "CUP_arifle_L85A2";
_rifleGL_spec = "CUP_arifle_L85A2_GL";

// ================= General Magazines ============

_rifleMag = "30Rnd_556x45_Stanag";
_rifleMatchMag = "30Rnd_556x45_Stanag";
_rifleTracerMag = "30Rnd_556x45_Stanag_Tracer_Red";
_rifleGLMag = "30Rnd_556x45_Stanag";
_rifleScopedMag = "30Rnd_556x45_Stanag";
_autoRifleMag = "CUP_200Rnd_TE4_Red_Tracer_556x45_M249";
_autoTracerMag = "CUP_200Rnd_TE4_Red_Tracer_556x45_M249";
_carbineMag = "30Rnd_556x45_Stanag";
_dmrMag = "hlc_20Rnd_762x51_B_M14";
_mmgMag = "CUP_100Rnd_TE4_LRT4_White_Tracer_762x51_Belt_M";
_mmgTracerMag = "CUP_100Rnd_TE4_LRT4_Red_Tracer_762x51_Belt_M";
_smgMag = "hlc_30Rnd_9x19_B_MP5";
_pistolMag = "CUP_7Rnd_45ACP_1911";

// ================= SpecOps Magazines ============

_rifleMag_spec = "30Rnd_556x45_Stanag";
_rifleTracerMag_spec = "30Rnd_556x45_Stanag_Tracer_Red";
_rifleGLMag_spec = "30Rnd_556x45_Stanag";
_rifleGLTracerMag_spec = "30Rnd_556x45_Stanag_Tracer_Red";

// ===================== GL Rounds ================

_glExplody = "CUP_1Rnd_HEDP_M203";
_glSmokeOne = "CUP_1Rnd_SmokeGreen_M203";
_glSmokeTwo = "CUP_1Rnd_SmokeRed_M203";
_glFlareOne = "CUP_FlareGreen_M203";
_glFlareTwo = "CUP_FlareRed_M203";

// =======================================================================
// =========================== Weapons Teams =============================
// =======================================================================

// ================== Shotgun =====================

_shotty = "rhs_weap_M590_8RD";
_shottyBuck = "rhsusf_8Rnd_00Buck";
_shottySlug = "rhsusf_8Rnd_Slug";

// ============== Heavy Grenadier =================

_glHeavy = "rhs_weap_m32";
_glHeavyExplody = "rhsusf_mag_6Rnd_M433_HEDP";
_glHSmokesOne = "rhsusf_mag_6Rnd_M715_green";
_glHSmokesTwo = "rhsusf_mag_6Rnd_M713_red";

// =================== LAT ========================

_lat = "rhs_weap_M136_hedp";
_latMag = "rhs_m136_hedp_mag";

// =================== MAT ========================

_matLaunch = "CUP_launch_MAAWS";
_matMag = "CUP_MAAWS_HEAT_M";
_matScope = "CUP_optic_MAAWS_Scope";

// =================== HAT ========================

_hatLaunch = "rhs_weap_fgm148";
_hatMag = "rhs_fgm148_magazine_AT";

// =================== Anti-Air ===================

_aaLaunch = "rhs_weap_fim92";
_aaMag = "rhs_fim92_mag";

// ==================== Snipers ===================

_boltRifle = "hlc_rifle_awmagnum_BL";
_boltRifleMag = "hlc_5rnd_300WM_FMJ_AWM";
_amRifle = "CUP_srifle_M107_Base"; 
_amRifleMag = "CUP_10Rnd_127x99_M107";

// ================= HMG Team =====================

_hmgBarrel = "CUP_B_M2_Gun_Bag";
_hmgTripod = "CUP_B_M2_MiniTripod_Bag";
_hmgMag = "";	// no magazines as of yet

// ================= GMG Team =====================

_gmgBarrel = "B_GMG_01_weapon_F";
_gmgTripod = "B_HMG_01_support_F";
_gmgMag = "";	// no magazines as of yet

// ==================== Diver =====================

if (_underwaterWeapons) then { 
	_rifleDiver = "arifle_SDAR_F"; 
} else { 
	_rifleDiver = _rifle; 
};
if (_underwaterWeapons) then { 
	_rifleDiverMagOne = "30Rnd_556x45_Stanag";	// standard mag
	_rifleDiverMagTwo = "20Rnd_556x45_UW_mag";	// underwater mag
} else { 
	_rifleDiverMagOne = _rifleMag;
	_rifleDiverMagTwo = _rifleMag;
};

// ================ Attachments ===================

_generalAttachments = ["CUP_optic_Eotech533Grey","CUP_acc_Flashlight"];
_dmrAttachments = ["CUP_optic_CompM2_Black","CUP_acc_Flashlight"];
_autoRifleAttachments = ["CUP_optic_CompM2_Black","CUP_acc_Flashlight"];
_mmgAttachments = ["CUP_optic_CompM2_Black","CUP_acc_Flashlight"];
_specOpsAttachments = ["CUP_optic_Eotech533Grey","CUP_acc_ANPEQ_2"]; 
_sniperAttachments = ["optic_SOS"];
_amSniperAttachments = ["CUP_optic_SB_3_12x50_PMII"];
_pistolAttachments = [];

_scoped_generalAttachments = ["CUP_optic_ACOG","CUP_acc_Flashlight"];
_scoped_dmrAttachments = ["CUP_optic_ACOG","CUP_acc_Flashlight","bipod_01_F_blk"];
_scoped_autoRifleAttachments = ["CUP_optic_ElcanM145","CUP_acc_Flashlight"];
_scoped_mmgAttachments = ["CUP_optic_ElcanM145","CUP_acc_Flashlight"];
_scoped_specOpsAttachments = ["CUP_optic_ACOG","CUP_acc_ANPEQ_2"];

_suppressed_generalAttachments = ["CUP_muzzle_snds_L85"];
_suppressed_dmrAttachments = ["hlc_muzzle_snds_M14"];
_suppressed_autoRifleAttachments = [];
_suppressed_mmgAttachments = [];
_suppressed_specOpsAttachments = ["CUP_muzzle_snds_L85"];
_suppressed_pistolAttachments = ["muzzle_snds_acp"];

// =======================================================================