// AssignGear - Uniforms
// by Mr. Agnet
// - Defines all of the wearable items to be used by the assignGear script.
// - Current Side, Faction: INDFOR, UN
// - Primary Mod: RHS/Leights
// - Variables: _headgear - "helmets", "beret"

// ============================================================
// Declares variables

private [
"_plebUniformArray","_plebRandom","_plebUniform",
"_plebHelmetArray","_plebHRandom","_plebHelmet",
"_plebVest","_gunnerVest","_glVest","_medVest",
"_smallRuck","_medRuck","_largeRuck",
"_specUniform","_specHelmet","_specGoggles","_specVest","_specRuck",
"_crewUniform","_rpilotUniform","_fpilotUniform",
"_crewmanHelmetArray","_crewmanHRandom","_crewmanHelmet","_crewmanHelmetArray","_crewmanHRandom","_rotaryPilotHelmet","_rotaryCrewHelmet","_fixedPilotHelmet",
"_crewVest","_pilotVest",
"_sniperUniform","_sniperVest","_sniperRuck",
"_diverUniform","_diverVest","_diverRuck",
"_goggles","_insignia","_divingGoggles",
"_airRadioRuck","_radioRuck","_diverRadioRuck","_uavRuck","_uavTool"
];

// ===================== Uniforms ======================

_goggles = "";	// leave as "" for player defined
_insignia = ""; 
_plebUniformArray = ["LOP_U_UN_Fatigue_01","LOP_U_UN_Fatigue_01","LOP_U_UN_Fatigue_01"];	
_plebRandom = (floor(random (count _plebUniformArray)));
_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess

// ===================== Headgear ======================

switch (_headgear) do {
	case "helmets" : {
		_plebHelmetArray = ["LOP_H_6B27M_ess_UN","LOP_H_6B27M_UN","LOP_H_6B27M_ess_UN","LOP_H_6B27M_UN"];	
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};		
	case "beret" : {
		_plebHelmetArray = ["LOP_H_Beret_UN"];	
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};
	default {
		_plebHelmetArray = ["LOP_H_6B27M_ess_UN","LOP_H_6B27M_UN","LOP_H_6B27M_ess_UN","LOP_H_6B27M_UN"];
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};
};
_crewmanHelmetArray = ["rhsusf_cvc_green_helmet","rhsusf_cvc_green_ess","rhsusf_cvc_green_helmet","rhsusf_cvc_green_ess","rhsusf_cvc_green_helmet"];	
_crewmanHRandom = (floor(random (count _crewmanHelmetArray))); 
_crewmanHelmet = _crewmanHelmetArray select _crewmanHRandom; // enter single string value to remove randommess

// ======================= Vests =======================

_plebVest = "LOP_V_6B23_6Sh92_UN";
_gunnerVest = "LOP_V_6B23_CrewOfficer_UN";
_glVest = "LOP_V_6B23_6Sh92_UN";
_medVest = "LOP_V_6B23_6Sh92_UN";

// ======================= Rucks =======================

_smallRuck = "B_AssaultPack_rgr";
_medRuck = "B_Kitbag_rgr";
_largeRuck = "B_Carryall_oli";

// ===================== Spec Ops ======================

_specUniform = "LOP_U_UN_Fatigue_01";
_specHelmet = "LOP_H_6B27M_ess_UN";
_specGoggles = "rhs_scarf";
_specVest = "LOP_V_6B23_CrewOfficer_UN";
_specRuck = "B_AssaultPack_rgr";

// =================== Vehicle Crews ===================

_crewUniform = "LOP_U_UN_Fatigue_01";
_rpilotUniform = "LOP_U_UN_Fatigue_01";
_fpilotUniform = "U_I_PilotCoveralls";

_rotaryPilotHelmet = "rhsusf_hgu56p";
_rotaryCrewHelmet = "rhsusf_hgu56p_mask";
_fixedPilotHelmet = "RHS_jetpilot_usaf";

_pilotVest = "LOP_V_6B23_CrewOfficer_UN";
_crewVest = "LOP_V_6B23_CrewOfficer_UN";

// ==================== Sniper Team ====================

_sniperUniform = "U_I_GhillieSuit";
_sniperVest = "V_TacVest_oli";
_sniperRuck = "B_AssaultPack_rgr";

// ==================== Diver Gear ====================

_diverUniform = "U_I_Wetsuit";
_diverVest = "V_RebreatherB";
_diverRuck = "B_AssaultPack_blk";
_divingGoggles = "G_I_Diving";

// ==================== Radio Rucks ====================

switch (_radioSelection) do {
	case "detection" : {
		private "_side";
		_side = toLower format ["%1", side player];
		// BLUFOR UAV
		if (_side == "west") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
		// OPFOR UAV
		if (_side == "east") then { 
			_uavRuck = "O_UAV_01_backpack_F";
			_uavTool = "O_UavTerminal";
		};
		// INDFOR UAV
		if (_side == "guer") then { 
			_uavRuck = "I_UAV_01_backpack_F";
			_uavTool = "I_UavTerminal";
		};
		// Civilian UAV
		if (_side == "civ") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
	};
	default {
		_uavRuck = "B_UAV_01_backpack_F";
		_uavTool = "B_UavTerminal";
	};
};

// =====================================================
