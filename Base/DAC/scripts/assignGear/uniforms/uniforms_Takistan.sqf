// AssignGear - Uniforms
// by Mr. Agnet
// - Defines all of the wearable items to be used by the assignGear script.
// - Current Side, Faction: OPFOR, TAKISTAN
// - Primary Mod: KMNP
// - Variables: _headgear - "helmets", "softcover"

// ============================================================
// Declares variables

private [
"_plebUniformArray","_plebRandom","_plebUniform",
"_plebHelmetArray","_plebHRandom","_plebHelmet",
"_plebVest","_gunnerVest","_glVest","_medVest",
"_smallRuck","_medRuck","_largeRuck",
"_specUniform","_specHelmet","_specGoggles","_specVest","_specRuck",
"_crewUniform","_rpilotUniform","_fpilotUniform",
"_crewmanHelmetArray","_crewmanHRandom","_crewmanHelmet","_rotaryPilotHelmet","_rotaryCrewHelmet","_fixedPilotHelmet",
"_crewVest","_pilotVest",
"_sniperUniform","_sniperVest","_sniperRuck",
"_diverUniform","_diverVest","_diverRuck",
"_goggles","_insignia","_divingGoggles",
"_airRadioRuck","_radioRuck","_diverRadioRuck","_uavRuck","_uavTool"
];

// ===================== Uniforms ======================

_goggles = "";	// leave as "" for player defined
_insignia = ""; 
_plebUniformArray = ["CUP_U_O_TK_Green","CUP_U_O_TK_MixedCamo"];	
_plebRandom = (floor(random (count _plebUniformArray)));
_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default (what was spawned with), or enter single string value to remove randommess

// ===================== Headgear ======================

switch (_headgear) do {
	case "helmets" : {
		_plebHelmetArray = ["CUP_H_SLA_Helmet","CUP_H_SLA_Helmet"];	
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};
	case "softcover" : {
		_plebHelmetArray = ["CUP_H_TKI_Lungee_Open_03","CUP_H_TKI_Lungee_Open_02","CUP_H_TKI_Lungee_Open_01","CUP_H_TKI_Lungee_Open_06","CUP_H_TKI_Pakol_2_01","CUP_H_TKI_Pakol_1_06","CUP_H_TKI_Pakol_1_01","CUP_H_TKI_Pakol_2_06","CUP_H_TKI_Pakol_2_03"];	
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};
	default {
		_plebHelmetArray = ["CUP_H_SLA_Helmet","CUP_H_SLA_Helmet"];	
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};
};

// ======================= Vests =======================

_plebVest = "CUP_V_RUS_Smersh_1";
_gunnerVest = "CUP_V_RUS_Smersh_2";
_glVest = "CUP_V_RUS_Smersh_2";
_medVest = "CUP_V_RUS_Smersh_1";

// ======================= Rucks =======================

_smallRuck = "CUP_B_AlicePack_Khaki";
_medRuck = "CUP_B_HikingPack_Civ";
_largeRuck = "B_Carryall_oli";

// ===================== Spec Ops ======================

_specUniform = "CUP_U_O_TK_MixedCamo";
_specHelmet = "CUP_U_O_TK_Green";
_specGoggles = "G_Bandanna_oli";
_specVest = "CUP_V_RUS_Smersh_2";
_specRuck = "CUP_B_AlicePack_Khaki";

// =================== Vehicle Crews ===================

_crewUniform = "CUP_U_O_TK_MixedCamo";
_rpilotUniform = "CUP_U_O_TK_Green";
_fpilotUniform = "U_I_pilotCoveralls";

_crewmanHelmetArray = ["rhs_tsh4","rhs_tsh4_ess","rhs_tsh4","rhs_tsh4_bala","rhs_tsh4","rhs_tsh4_ess_bala","rhs_tsh4"];	
_crewmanHRandom = (floor(random (count _crewmanHelmetArray))); 
_crewmanHelmet = _crewmanHelmetArray select _crewmanHRandom; // enter single string value to remove randommess
_rotaryPilotHelmet = "CUP_H_TK_PilotHelmet";
_rotaryCrewHelmet = "CUP_H_TK_PilotHelmet";
_fixedPilotHelmet = "H_PilotHelmetFighter_I";

_crewVest = "CUP_V_O_TK_CrewBelt";
_pilotVest = "CUP_V_O_TK_CrewBelt";

// ==================== Sniper Team ====================

_sniperUniform = "CUP_U_O_TK_Ghillie_Top";
_sniperVest = "V_TacVest_oli";
_sniperRuck = "B_FieldPack_oli";

// ==================== Diver Gear ====================

_diverUniform = "U_I_Wetsuit";
_diverVest = "V_RebreatherB";
_diverRuck = "B_AssaultPack_blk";
_divingGoggles = "G_I_Diving";

// ==================== Radio Rucks ====================

switch (_radioSelection) do {
	case "detection" : {
		private "_side";
		_side = toLower format ["%1", side player];
		// BLUFOR UAV
		if (_side == "west") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
		// OPFOR UAV
		if (_side == "east") then { 
			_uavRuck = "O_UAV_01_backpack_F";
			_uavTool = "O_UavTerminal";
		};
		// INDFOR UAV
		if (_side == "guer") then { 
			_uavRuck = "I_UAV_01_backpack_F";
			_uavTool = "I_UavTerminal";
		};
		// Civilian UAV
		if (_side == "civ") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
	};
	default {
		_uavRuck = "B_UAV_01_backpack_F";
		_uavTool = "B_UavTerminal";
	};
};

// =====================================================
