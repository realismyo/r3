// AssignGear - Uniforms
// by Mr. Agnet
// - Defines all of the wearable items to be used by the assignGear script.
// - Current Side, Faction: BLUFOR, FIA
// - Primary Mod: VANILLA
// - Variables: _headgear - "helmets", "softcover"

// =======================================================================
// Declares variables

private [
"_plebUniformArray","_plebRandom","_plebUniform",
"_plebHelmetArray","_plebHRandom","_plebHelmet",
"_plebVest","_gunnerVest","_glVest","_medVest",
"_smallRuck","_medRuck","_largeRuck",
"_specUniform","_specHelmet","_specGoggles","_specVest","_specRuck",
"_crewUniform","_rpilotUniform","_fpilotUniform",
"_crewmanHelmetArray","_crewmanHRandom","_crewmanHelmet","_rotaryPilotHelmet","_rotaryCrewHelmet","_fixedPilotHelmet",
"_crewVest","_pilotVest",
"_sniperUniform","_sniperVest","_sniperRuck",
"_diverUniform","_diverVest","_diverRuck",
"_goggles","_insignia","_divingGoggles",
"_airRadioRuck","_radioRuck","_diverRadioRuck","_uavRuck","_uavTool"
];

// =======================================================================
// =========================== Camo Specific =============================
// =======================================================================

_goggles = "";	// leave as "" for player defined
_insignia = ""; 

// ========== Uniforms ===========

_plebUniformArray = ["U_BG_Guerilla1_1","U_BG_Guerilla2_2","U_BG_Guerilla2_3","U_BG_leader","U_BG_Guerrilla_6_1","U_BG_Guerilla3_1","U_BG_Guerilla2_1"];	
_plebRandom = (floor(random (count _plebUniformArray)));
_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess

// ========== Headgear ===========

switch (_headgear) do {
	case "helmets" : {
		_plebHelmetArray = ["rhs_6b27m_green","rhs_6b27m_green_bala","rhs_6b26_green","rhs_6b27m_green_ess","rhs_6b27m_green","rhs_6b27m_green_ess_bala","rhs_6b26_green","rhs_6b26_bala_green","rhs_6b27m_green","rhs_6b26_ess_green","rhs_6b26_green","rhs_6b26_ess_bala_green","rhs_6b27m_green"];	
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};		
	case "softcover" : {
		_plebHelmetArray = ["H_Booniehat_khk_hs","H_Bandanna_khk_hs","H_Cap_oli_hs","H_Shemag_olive_hs","H_Cap_headphones","H_Watchcap_blk"];	
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};
	default {
		_plebHelmetArray = ["H_Booniehat_khk_hs","H_Bandanna_khk_hs","H_Cap_oli_hs","H_Shemag_olive_hs","H_Cap_headphones","H_Watchcap_blk"];	
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};
};

// ============ Vests ============

_plebVest = "V_TacVest_oli";
_gunnerVest = "V_PlateCarrierIAGL_oli";
_glVest = "V_TacVest_oli";
_medVest = "V_TacVest_oli";

// ============ Rucks ============

_smallRuck = "B_AssaultPack_rgr";
_medRuck = "B_Kitbag_rgr";
_largeRuck = "B_Carryall_oli";

// =======================================================================
// ========================= Non-Camo Specific ===========================
// =======================================================================

// ================== Vehicle Crews ==================

_crewUniform = "U_I_G_Story_Protagonist_F";

_rpilotUniform = "U_B_HeliPilotCoveralls";
_fpilotUniform = "U_B_PilotCoveralls";

_crewmanHelmetArray = ["rhsusf_cvc_green_helmet","rhsusf_cvc_green_ess","rhsusf_cvc_green_helmet","rhsusf_cvc_green_ess","rhsusf_cvc_green_helmet"];	
_crewmanHRandom = (floor(random (count _crewmanHelmetArray))); 
_crewmanHelmet = _crewmanHelmetArray select _crewmanHRandom; // enter single string value to remove randommess

_rotaryPilotHelmet = "rhs_gssh18";
_rotaryCrewHelmet = "rhs_gssh18";
_fixedPilotHelmet = "H_PilotHelmetFighter_B";

_crewVest = "V_TacVest_oli";
_pilotVest = "V_TacVest_oli";

// =================== Sniper Team ===================

_sniperUniform = "U_B_GhillieSuit";
_sniperVest = "V_TacVest_oli";
_sniperRuck = "B_AssaultPack_rgr";

// =================== Diver Gear ===================

_diverUniform = "U_B_Wetsuit";
_diverVest = "V_RebreatherB";
_diverRuck = "B_AssaultPack_blk";
_divingGoggles = "G_B_Diving";

// =================== Radio Rucks ===================

switch (_radioSelection) do {
	case "detection" : {
		private "_side";
		_side = toLower format ["%1", side player];
		// BLUFOR UAV
		if (_side == "west") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
		// OPFOR UAV
		if (_side == "east") then { 
			_uavRuck = "O_UAV_01_backpack_F";
			_uavTool = "O_UavTerminal";
		};
		// INDFOR UAV
		if (_side == "guer") then { 
			_uavRuck = "I_UAV_01_backpack_F";
			_uavTool = "I_UavTerminal";
		};
		// Civilian UAV
		if (_side == "civ") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
	};
	default {
		_uavRuck = "B_UAV_01_backpack_F";
		_uavTool = "B_UavTerminal";
	};
};

// =======================================================================