// AssignGear - Uniforms
// by Mr. Agnet
// - Defines all of the wearable items to be used by the assignGear script.
// - Current Side, Faction: BLUFOR, USMC
// - Primary Mod: CUP
// - Variables: _camoPattern - "wdlmarpat", "wdl", "fr"
//				_headgear - "helmets", "softcover"

// =======================================================================
// Declares variables

private [
"_plebUniformArray","_plebRandom","_plebUniform",
"_plebHelmetArray","_plebHRandom","_plebHelmet",
"_plebVest","_gunnerVest","_glVest","_medVest",
"_smallRuck","_medRuck","_largeRuck",
"_specUniform","_specHelmet","_specGoggles","_specVest","_specRuck",
"_crewUniform","_rpilotUniform","_fpilotUniform",
"_crewmanHelmetArray","_crewmanHRandom","_crewmanHelmet","_rotaryPilotHelmet","_rotaryCrewHelmet","_fixedPilotHelmet",
"_crewVest","_pilotVest",
"_sniperUniform","_sniperVest","_sniperRuck",
"_diverUniform","_diverVest","_diverRuck",
"_goggles","_divingGoggles","_insignia",
"_airRadioRuck","_radioRuck","_diverRadioRuck","_uavRuck","_uavTool"
];

// =======================================================================
// =========================== Camo Specific =============================
// =======================================================================

_goggles = "";	// leave as "" for player defined
_insignia = ""; 

switch (_camoPattern) do {
	case "wdlmarpat" : {
		// ==================== Uniforms ==================

		_plebUniformArray = ["CUP_U_B_FR_Officer","CUP_U_B_FR_DirAction","CUP_U_B_FR_DirAction2","CUP_U_B_FR_Corpsman"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "CUP_U_B_FR_Officer";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["CUP_H_USMC_HelmetWDL","CUP_H_USMC_Goggles_HelmetWDL","CUP_H_USMC_Headset_HelmetWDL","CUP_H_USMC_Headset_GoggleW_HelmetWDL"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Watchcap_camo","CUP_H_FR_Bandana_Headset","CUP_H_FR_BoonieMARPAT","CUP_H_FR_BoonieWDL","CUP_H_FR_PRR_BoonieWDL","CUP_H_FR_Cap_Headset_Green","CUP_H_FR_Headband_Headset","CUP_H_FR_Headset"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["CUP_H_USMC_HelmetWDL","CUP_H_USMC_Goggles_HelmetWDL","CUP_H_USMC_Headset_HelmetWDL","CUP_H_USMC_Headset_GoggleW_HelmetWDL"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "CUP_V_B_MTV_PistolBlack";
		_gunnerVest = "CUP_V_B_MTV_MG";
		_glVest = "CUP_V_B_MTV_LegPouch";
		_medVest = "CUP_V_B_MTV_Pouches";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_cbr";
		_medRuck = "CUP_B_USMC_MOLLE";
		_largeRuck = "B_Carryall_cbr";
		
		// ==================== SpecOps ===================
		
		_specUniform = "CUP_U_B_FR_DirAction2";
		_specHelmet = "CUP_H_USMC_Goggles_HelmetWDL";
		_specGoggles = "G_Bandanna_khk";
		_specVest = "CUP_V_B_MTV_TL";
		_specRuck = "B_AssaultPack_cbr";
		
		// ================================================
	};
	case "wdl" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["CUP_U_B_FR_Light","CUP_U_B_FR_Scout2","CUP_U_B_FR_Scout3","CUP_U_B_FR_Scout"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "CUP_U_B_FR_Officer";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["CUP_H_USMC_HelmetWDL","CUP_H_USMC_Goggles_HelmetWDL","CUP_H_USMC_Headset_HelmetWDL","CUP_H_USMC_Headset_GoggleW_HelmetWDL"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Watchcap_camo","CUP_H_FR_Bandana_Headset","CUP_H_FR_BoonieMARPAT","CUP_H_FR_BoonieWDL","CUP_H_FR_PRR_BoonieWDL","CUP_H_FR_Cap_Headset_Green","CUP_H_FR_Headband_Headset","CUP_H_FR_Headset"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["CUP_H_USMC_HelmetWDL","CUP_H_USMC_Goggles_HelmetWDL","CUP_H_USMC_Headset_HelmetWDL","CUP_H_USMC_Headset_GoggleW_HelmetWDL"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "CUP_V_B_MTV_PistolBlack";
		_gunnerVest = "CUP_V_B_MTV_MG";
		_glVest = "CUP_V_B_MTV_LegPouch";
		_medVest = "CUP_V_B_MTV_Pouches";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_cbr";
		_medRuck = "CUP_B_USMC_MOLLE";
		_largeRuck = "B_Carryall_cbr";
		
		// ==================== SpecOps ===================
		
		_specUniform = "CUP_U_B_FR_DirAction2";
		_specHelmet = "CUP_H_USMC_Goggles_HelmetWDL";
		_specGoggles = "G_Bandanna_khk";
		_specVest = "CUP_V_B_MTV_TL";
		_specRuck = "B_AssaultPack_cbr";
		
		// ================================================
	};
	case "fr" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["CUP_U_B_FR_DirAction2","CUP_U_B_FR_DirAction","CUP_U_B_FR_DirAction","CUP_U_B_FR_DirAction2"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "CUP_U_B_FR_DirAction";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["CUP_H_FR_ECH","CUP_H_FR_ECH","CUP_H_FR_ECH"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Watchcap_camo","CUP_H_FR_Bandana_Headset","CUP_H_FR_BoonieMARPAT","CUP_H_FR_BoonieWDL","CUP_H_FR_PRR_BoonieWDL","CUP_H_FR_Cap_Headset_Green","CUP_H_FR_Headband_Headset","CUP_H_FR_Headset"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["CUP_H_FR_ECH","CUP_H_FR_ECH","CUP_H_FR_ECH"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "CUP_V_I_RACS_Carrier_Vest";
		_gunnerVest = "CUP_V_I_RACS_Carrier_Vest_3";
		_glVest = "CUP_V_I_RACS_Carrier_Vest_3";
		_medVest = "CUP_V_I_RACS_Carrier_Vest";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_cbr";
		_medRuck = "CUP_B_USMC_MOLLE";
		_largeRuck = "B_Carryall_cbr";
		
		// ==================== SpecOps ===================
		
		_specUniform = "CUP_U_B_FR_DirAction2";
		_specHelmet = "CUP_H_FR_ECH";
		_specGoggles = "G_Bandanna_khk";
		_specVest = "CUP_V_B_MTV_TL";
		_specRuck = "B_AssaultPack_cbr";
		
		// ================================================
	};
	default {
		// ==================== Uniforms ==================

		_plebUniformArray = ["CUP_U_B_FR_Officer","CUP_U_B_FR_DirAction","CUP_U_B_FR_DirAction2","CUP_U_B_FR_Corpsman"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "CUP_U_B_FR_Officer";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["CUP_H_USMC_HelmetWDL","CUP_H_USMC_Goggles_HelmetWDL","CUP_H_USMC_Headset_HelmetWDL","CUP_H_USMC_Headset_GoggleW_HelmetWDL"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Watchcap_camo","CUP_H_FR_Bandana_Headset","CUP_H_FR_BoonieMARPAT","CUP_H_FR_BoonieWDL","CUP_H_FR_PRR_BoonieWDL","CUP_H_FR_Cap_Headset_Green","CUP_H_FR_Headband_Headset","CUP_H_FR_Headset"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["CUP_H_USMC_HelmetWDL","CUP_H_USMC_Goggles_HelmetWDL","CUP_H_USMC_Headset_HelmetWDL","CUP_H_USMC_Headset_GoggleW_HelmetWDL"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "CUP_V_B_MTV_PistolBlack";
		_gunnerVest = "CUP_V_B_MTV_MG";
		_glVest = "CUP_V_B_MTV_LegPouch";
		_medVest = "CUP_V_B_MTV_Pouches";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_cbr";
		_medRuck = "CUP_B_USMC_MOLLE";
		_largeRuck = "B_Carryall_cbr";
		
		// ==================== SpecOps ===================
		
		_specUniform = "CUP_U_B_FR_DirAction2";
		_specHelmet = "CUP_H_USMC_Goggles_HelmetWDL";
		_specGoggles = "G_Bandanna_khk";
		_specVest = "CUP_V_B_MTV_TL";
		_specRuck = "B_AssaultPack_cbr";
		
		// ================================================
	};
};

// =======================================================================
// ========================= Non-Camo Specific ===========================
// =======================================================================

// ================== Vehicle Crews ==================

_rpilotUniform = "CUP_U_B_USMC_PilotOverall";
_fpilotUniform = "U_B_PilotCoveralls";

_crewmanHelmet = "CUP_H_USMC_Crew_Helmet";
_rotaryPilotHelmet = "CUP_H_USMC_Helmet_Pilot";
_rotaryCrewHelmet = "CUP_H_USMC_Helmet_Pilot";
_fixedPilotHelmet = "H_PilotHelmetFighter_B";

_crewVest = "CUP_V_B_MTV";
_pilotVest = "CUP_V_B_PilotVest";

// =================== Sniper Team ===================

_sniperUniform = "CUP_U_B_USMC_Ghillie_WDL";
_sniperVest = "V_TacVest_oli";
_sniperRuck = "CUP_B_USMC_AssaultPack";

// =================== Diver Gear ===================

_diverUniform = "U_B_Wetsuit";
_diverVest = "V_RebreatherB";
_diverRuck = "B_AssaultPack_blk";
_divingGoggles = "G_B_Diving";

// =================== Radio Rucks ===================

switch (_radioSelection) do {
	case "detection" : {
		private "_side";
		_side = toLower format ["%1", side player];
		// BLUFOR UAV
		if (_side == "west") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
		// OPFOR UAV
		if (_side == "east") then { 
			_uavRuck = "O_UAV_01_backpack_F";
			_uavTool = "O_UavTerminal";
		};
		// INDFOR UAV
		if (_side == "guer") then { 
			_uavRuck = "I_UAV_01_backpack_F";
			_uavTool = "I_UavTerminal";
		};
		// Civilian UAV
		if (_side == "civ") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
	};
	default {
		_uavRuck = "B_UAV_01_backpack_F";
		_uavTool = "B_UavTerminal";
	};
};

// =======================================================================