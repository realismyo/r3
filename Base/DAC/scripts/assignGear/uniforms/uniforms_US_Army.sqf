// AssignGear - Uniforms
// by Mr. Agnet
// - Defines all of the wearable items to be used by the assignGear script.
// - Current Side, Faction: BLUFOR, NATO
// - Primary Mod: RHS
// - Variables: _camoPattern - "acu", "scorpion", "m81", "3colour"
//				_headgear - "helmets", "softcover"

// =======================================================================
// Declares variables

private [
"_plebUniformArray","_plebRandom","_plebUniform",
"_plebHelmetArray","_plebHRandom","_plebHelmet",
"_plebVest","_gunnerVest","_glVest","_medVest",
"_smallRuck","_medRuck","_largeRuck",
"_specUniform","_specHelmet","_specGoggles","_specVest","_specRuck",
"_crewUniform","_rpilotUniform","_fpilotUniform",
"_crewmanHelmetArray","_crewmanHRandom","_crewmanHelmet","_rotaryPilotHelmet","_rotaryCrewHelmet","_fixedPilotHelmet",
"_crewVest","_pilotVest",
"_sniperUniform","_sniperVest","_sniperRuck",
"_diverUniform","_diverVest","_diverRuck",
"_goggles","_divingGoggles","_insignia",
"_airRadioRuck","_radioRuck","_diverRadioRuck","_uavRuck","_uavTool"
];

// =======================================================================
// =========================== Camo Specific =============================
// =======================================================================

_goggles = "";	// leave as "" for player defined
_insignia = ""; 

switch (_camoPattern) do {
	case "acu" : {
		// ==================== Uniforms ==================

		_plebUniformArray = ["MNP_CombatUniform_Ranger_C","MNP_CombatUniform_Ranger_E","MNP_CombatUniform_Ranger_C"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_Ranger_E";
		_rpilotUniform = "MNP_CombatUniform_Ranger_E";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_ACU","MNP_Helmet_ACU","MNP_Helmet_ACU"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["MNP_Boonie_ACU"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_ACU","MNP_Helmet_ACU","MNP_Helmet_ACU"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_ACU_1";
		_gunnerVest = "MNP_Vest_ACU_2";
		_glVest = "MNP_Vest_ACU_2";
		_medVest = "MNP_Vest_ACU_1";
		_crewVest = "V_TacVest_blk";
		_pilotVest = "V_TacVest_blk";
		
		// ===================== Rucks ====================
		
		_smallRuck = "MNP_B_ACU_AP";
		_medRuck = "MNP_B_ACU_KB";
		_largeRuck = "B_Carryall_oucamo";
		
		// ==================== SpecOps ===================
		
		_specUniform = "MNP_CombatUniform_Ranger_E";
		_specHelmet = "MNP_Helmet_ACU";
		_specGoggles = "G_Bandanna_blk";
		_specVest = "MNP_Vest_ACU_2";
		_specRuck = "MNP_B_ACU_AP";
		
		// ================================================
	};
	case "scorpion" : {
		// ==================== Uniforms ==================

		_plebUniformArray = ["MNP_CombatUniform_Scorpion_B","MNP_CombatUniform_Scorpion_A","MNP_CombatUniform_Ranger_B","MNP_CombatUniform_Ranger_A"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_Scorpion_B";
		_rpilotUniform = "MNP_CombatUniform_Scorpion_B";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_Scorpion","MNP_Helmet_Scorpion","MNP_Helmet_Scorpion"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["MNP_Boonie_Scorpion"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_Scorpion","MNP_Helmet_Scorpion","MNP_Helmet_Scorpion"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_Scorpion_2";
		_gunnerVest = "MNP_Vest_Scorpion_1";
		_glVest = "MNP_Vest_Scorpion_1";
		_medVest = "MNP_Vest_Scorpion_2";
		_crewVest = "V_TacVest_oli";
		_pilotVest = "V_TacVest_oli";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_oli";
		
		// ==================== SpecOps ===================
		
		_specUniform = "MNP_CombatUniform_Ranger_B";
		_specHelmet = "MNP_Helmet_Scorpion";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "MNP_Vest_Scorpion_1";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
	case "m81" : {
		// ==================== Uniforms ==================

		_plebUniformArray = ["MNP_CombatUniform_Wood_A","MNP_CombatUniform_Wood_B","MNP_CombatUniform_M81_Sh","MNP_CombatUniform_M81_Rg"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_Wood_B";
		_rpilotUniform = "MNP_CombatUniform_Wood_B";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_USW","MNP_Helmet_PAGST_M81","MNP_Helmet_USW"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["MNP_Boonie_USW"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_USW","MNP_Helmet_PAGST_M81","MNP_Helmet_USW"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_M81b";
		_gunnerVest = "MNP_Vest_M81";
		_glVest = "MNP_Vest_M81";
		_medVest = "MNP_Vest_M81b";
		_crewVest = "V_TacVest_oli";
		_pilotVest = "V_TacVest_oli";
		
		// ===================== Rucks ====================
		
		_smallRuck = "MNP_B_WD_FP";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "MNP_B_WD_CA";
		
		// ==================== SpecOps ===================
		
		_specUniform = "MNP_CombatUniform_Wood_B";
		_specHelmet = "MNP_Helmet_USW";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "MNP_Vest_M81";
		_specRuck = "MNP_B_WD_FP";
		
		// ================================================
	};
	case "3colour" : {
		// ==================== Uniforms ==================

		_plebUniformArray = ["MNP_CombatUniform_US3Co_A","MNP_CombatUniform_US3Co_B","MNP_CombatUniform_US3Co_Sh","MNP_CombatUniform_US3Co_Rg"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_US3Co_B";
		_rpilotUniform = "MNP_CombatUniform_US3Co_B";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_3Co","MNP_Helmet_PAGST_US3Co","MNP_Helmet_3Co"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["MNP_Boonie_3CO"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_3Co","MNP_Helmet_PAGST_US3Co","MNP_Helmet_3Co"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_NZ_1";
		_gunnerVest = "MNP_Vest_NZ_2";
		_glVest = "MNP_Vest_NZ_2";
		_medVest = "MNP_Vest_NZ_1";
		_crewVest = "V_TacVest_khk";
		_pilotVest = "V_TacVest_khk";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_cbr";
		_largeRuck = "B_Carryall_cbr";
		
		// ==================== SpecOps ===================
		
		_specUniform = "MNP_CombatUniform_US3Co_B";
		_specHelmet = "MNP_Helmet_3Co";
		_specGoggles = "G_Bandanna_khk";
		_specVest = "MNP_Vest_NZ_2";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
	default {
		// ==================== Uniforms ==================

		_plebUniformArray = ["MNP_CombatUniform_Ranger_C","MNP_CombatUniform_Ranger_E","MNP_CombatUniform_Ranger_C"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_Ranger_E";
		_rpilotUniform = "MNP_CombatUniform_Ranger_E";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_ACU","MNP_Helmet_ACU","MNP_Helmet_ACU"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["MNP_Boonie_ACU"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_ACU","MNP_Helmet_ACU","MNP_Helmet_ACU"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_ACU_1";
		_gunnerVest = "MNP_Vest_ACU_2";
		_glVest = "MNP_Vest_ACU_2";
		_medVest = "MNP_Vest_ACU_1";
		_crewVest = "V_TacVest_blk";
		_pilotVest = "V_TacVest_blk";
		
		// ===================== Rucks ====================
		
		_smallRuck = "MNP_B_ACU_AP";
		_medRuck = "MNP_B_ACU_KB";
		_largeRuck = "B_Carryall_oucamo";
		
		// ==================== SpecOps ===================
		
		_specUniform = "MNP_CombatUniform_Ranger_E";
		_specHelmet = "MNP_Helmet_ACU";
		_specGoggles = "G_Bandanna_blk";
		_specVest = "MNP_Vest_ACU_2";
		_specRuck = "MNP_B_ACU_AP";
		
		// ================================================
	};
};

// =======================================================================
// ========================= Non-Camo Specific ===========================
// =======================================================================

// ================== Vehicle Crews ==================

_fpilotUniform = "U_B_PilotCoveralls";

_crewmanHelmet = "H_HelmetCrew_B";
_rotaryPilotHelmet = "H_PilotHelmetHeli_B";
_rotaryCrewHelmet = "H_CrewHelmetHeli_B";
_fixedPilotHelmet = "H_PilotHelmetFighter_B";


// =================== Sniper Team ===================

_sniperUniform = "U_B_GhillieSuit";
_sniperVest = "V_TacVest_oli";
_sniperRuck = "B_AssaultPack_rgr";

// =================== Diver Gear ===================

_diverUniform = "U_B_Wetsuit";
_diverVest = "V_RebreatherB";
_diverRuck = "B_AssaultPack_blk";
_divingGoggles = "G_B_Diving";

// =================== Radio Rucks ===================

switch (_radioSelection) do {
	case "detection" : {
		private "_side";
		_side = toLower format ["%1", side player];
		// BLUFOR UAV
		if (_side == "west") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
		// OPFOR UAV
		if (_side == "east") then { 
			_uavRuck = "O_UAV_01_backpack_F";
			_uavTool = "O_UavTerminal";
		};
		// INDFOR UAV
		if (_side == "guer") then { 
			_uavRuck = "I_UAV_01_backpack_F";
			_uavTool = "I_UavTerminal";
		};
		// Civilian UAV
		if (_side == "civ") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
	};
	default {
		_uavRuck = "B_UAV_01_backpack_F";
		_uavTool = "B_UavTerminal";
	};
};

// =======================================================================