// AssignGear - Uniforms
// by Mr. Agnet
// - Defines all of the wearable items to be used by the assignGear script.
// - Current Side, Faction: INDFOR, IDF
// - Primary Mod: RHS
// - Variables: _headgear - "helmets", "softcover"

// ============================================================
// Declares variables

private [
"_plebUniformArray","_plebRandom","_plebUniform",
"_plebHelmetArray","_plebHRandom","_plebHelmet",
"_plebVest","_gunnerVest","_glVest","_medVest",
"_smallRuck","_medRuck","_largeRuck",
"_specUniform","_specHelmet","_specGoggles","_specVest","_specRuck",
"_crewUniform","_rpilotUniform","_fpilotUniform",
"_crewmanHelmetArray","_crewmanHRandom","_crewmanHelmet","_rotaryPilotHelmet","_rotaryCrewHelmet","_fixedPilotHelmet",
"_crewVest","_pilotVest",
"_sniperUniform","_sniperVest","_sniperRuck",
"_diverUniform","_diverVest","_diverRuck",
"_goggles","_insignia","_divingGoggles",
"_airRadioRuck","_radioRuck","_diverRadioRuck","_uavRuck","_uavTool"
];

// ===================== Uniforms ======================

_goggles = "";	// leave as "" for player defined
_insignia = ""; 
_plebUniformArray = ["tacs_Uniform_Garment_LS_GS_GP_BB","tacs_Uniform_Garment_RS_GS_GP_BB","tacs_Uniform_Garment_LS_GS_GP_BB"];	
_plebRandom = (floor(random (count _plebUniformArray)));
_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess

// ===================== Headgear ======================

switch (_headgear) do {
	case "helmets" : {
		_plebHelmetArray = ["rhs_6b28_green","rhs_6b28_green_ess"];	
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};
	case "softcover" : {
		_plebHelmetArray = ["H_Booniehat_oli","H_Watchcap_camo","H_Cap_headphones"];
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};
	default {
		_plebHelmetArray = ["rhs_6b28_green","rhs_6b28_green_ess"];	
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};
};

// ======================= Vests =======================

_plebVest = "V_PlateCarrierIA1_dgtl";
_gunnerVest = "V_PlateCarrierIA1_dgtl";
_glVest = "V_TacVest_oli";
_medVest = "V_TacVest_oli";

// ======================= Rucks =======================

_smallRuck = "B_AssaultPack_rgr";
_medRuck = "B_Kitbag_rgr";
_largeRuck = "B_Carryall_oli";

// ===================== Spec Ops ======================

_specUniform = "tacs_Uniform_Garment_RS_GS_GP_BB";
_specHelmet = "rhs_6b28_green_ess";
_specGoggles = "rhs_balaclava";
_specVest = "V_PlateCarrierIA1_dgtl";
_specRuck = "B_AssaultPack_rgr";

// =================== Vehicle Crews ===================

_crewUniform = "tacs_Uniform_Garment_RS_GS_GP_BB";
_rpilotUniform = "U_B_HeliPilotCoveralls";
_fpilotUniform = "U_B_pilotCoveralls";

_rotaryPilotHelmet = "H_PilotHelmetHeli_B";
_rotaryCrewHelmet = "H_CrewHelmetHeli_B";
_fixedPilotHelmet = "H_PilotHelmetFighter_B";
_crewmanHelmet = "H_HelmetCrew_I";

_pilotVest = "V_TacVest_oli";
_crewVest = "V_TacVest_oli";

// ==================== Sniper Team ====================

_sniperUniform = "U_I_GhillieSuit";
_sniperVest = "V_TacVest_oli";
_sniperRuck = "B_AssaultPack_dgtl";

// ==================== Diver Gear ====================

_diverUniform = "U_I_Wetsuit";
_diverVest = "V_RebreatherB";
_diverRuck = "B_AssaultPack_blk";
_divingGoggles = "G_I_Diving";

// ==================== Radio Rucks ====================

switch (_radioSelection) do {
	case "detection" : {
		private "_side";
		_side = toLower format ["%1", side player];
		// BLUFOR UAV
		if (_side == "west") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
		// OPFOR UAV
		if (_side == "east") then { 
			_uavRuck = "O_UAV_01_backpack_F";
			_uavTool = "O_UavTerminal";
		};
		// INDFOR UAV
		if (_side == "guer") then { 
			_uavRuck = "I_UAV_01_backpack_F";
			_uavTool = "I_UavTerminal";
		};
		// Civilian UAV
		if (_side == "civ") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
	};
	default {
		_uavRuck = "B_UAV_01_backpack_F";
		_uavTool = "B_UavTerminal";
	};
};

// =====================================================
