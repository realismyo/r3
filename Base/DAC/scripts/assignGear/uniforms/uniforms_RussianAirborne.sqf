// AssignGear - Uniforms
// by Mr. Agnet
// - Defines all of the wearable items to be used by the assignGear script.
// - Current Side, Faction: OPFOR, Russian Airborne
// - Primary Mod: KMNP
// - Variables: _camoPattern - "digiflora", "digides", "ttsko"
//				_headgear - "helmets", "softcover"

// =======================================================================
// Declares variables

private [
"_plebUniformArray","_plebRandom","_plebUniform",
"_plebHelmetArray","_plebHRandom","_plebHelmet",
"_plebVest","_gunnerVest","_glVest","_medVest",
"_smallRuck","_medRuck","_largeRuck",
"_specUniform","_specHelmet","_specGoggles","_specVest","_specRuck",
"_crewUniform","_rpilotUniform","_fpilotUniform",
"_crewmanHelmetArray","_crewmanHRandom","_crewmanHelmet","_rotaryPilotHelmet","_rotaryCrewHelmet","_fixedPilotHelmet",
"_crewVest","_pilotVest",
"_sniperUniform","_sniperVest","_sniperRuck",
"_diverUniform","_diverVest","_diverRuck",
"_goggles","_insignia","_divingGoggles",
"_airRadioRuck","_radioRuck","_diverRadioRuck","_uavRuck","_uavTool"
];

// =======================================================================
// =========================== Camo Specific =============================
// =======================================================================

_goggles = "";	// leave as "" for player defined
_insignia = ""; 

switch (_camoPattern) do {
	case "digiflora" : {
		// ==================== Uniforms ==================

		_plebUniformArray = ["MNP_CombatUniform_RU_Med"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_RU_Med";
		_rpilotUniform = "MNP_CombatUniform_RU_Med";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_PAGST_RT","MNP_Helmet_RU_LT","MNP_Helmet_PAGST_RT","MNP_Helmet_RU_LT","MNP_Helmet_PAGST_RT"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["MNP_Boonie_RU_T"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_PAGST_RT","MNP_Helmet_RU_LT","MNP_Helmet_PAGST_RT","MNP_Helmet_RU_LT","MNP_Helmet_PAGST_RT"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_RU_T";
		_gunnerVest = "MNP_Vest_RU_T2";
		_glVest = "MNP_Vest_RU_T2";
		_medVest = "MNP_Vest_RU_T";
		_crewVest = "V_TacVest_oli";
		_pilotVest = "V_TacVest_oli";
		
		// ===================== Rucks ====================
		
		_smallRuck = "MNP_B_RU2_FP";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "MNP_B_RU2_CA";
		
		// ==================== SpecOps ===================
		
		_specUniform = "MNP_CombatUniform_RU_Med";
		_specHelmet = "MNP_Helmet_PAGST_RT";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "MNP_Vest_RU_T2";
		_specRuck = "MNP_B_RU2_FP";
		
		// ================================================
	};
	case "digides" : {
		// ==================== Uniforms ==================

		_plebUniformArray = ["MNP_CombatUniform_RU_Med_D","MNP_CombatUniform_RU_Med_DB","MNP_CombatUniform_RU_Med_D"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_RU_Med_DB";
		_rpilotUniform = "MNP_CombatUniform_RU_Med_DB";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_PAGST_RD","MNP_Helmet_PAGST_RD","MNP_Helmet_PAGST_RD"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["MNP_Boonie_RU_D","H_Booniehat_khk_hs","H_Watchcap_khk","H_Bandanna_khk_hs","H_Cap_brn_SPECOPS"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_PAGST_RD","MNP_Helmet_PAGST_RD","MNP_Helmet_PAGST_RD"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_RU_D";
		_gunnerVest = "MNP_Vest_RU_D2";
		_glVest = "MNP_Vest_RU_D2";
		_medVest = "MNP_Vest_RU_D";
		_crewVest = "V_TacVest_khk";
		_pilotVest = "V_TacVest_khk";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_FieldPack_cbr";
		_medRuck = "B_Kitbag_cbr";
		_largeRuck = "B_Carryall_cbr";
		
		// ==================== SpecOps ===================
		
		_specUniform = "MNP_CombatUniform_RU_Med_DB";
		_specHelmet = "MNP_Helmet_PAGST_RD";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "MNP_Vest_RU_D2";
		_specRuck = "B_FieldPack_cbr";
		
		// ================================================
	};
	case "ttsko" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["MNP_CombatUniform_RO2_Rg","MNP_CombatUniform_RO2_Sh","MNP_CombatUniform_RO2_Rg"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_RO2_Sh";
		_rpilotUniform = "MNP_CombatUniform_RO2_Sh";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_PAGST_UKR","MNP_Helmet_PAGST_UKR","MNP_Helmet_PAGST_UKR"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Booniehat_khk"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_PAGST_UKR","MNP_Helmet_PAGST_UKR","MNP_Helmet_PAGST_UKR"];
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_UKR_A";
		_gunnerVest = "MNP_Vest_UKR_B";
		_glVest = "MNP_Vest_UKR_B";
		_medVest = "MNP_Vest_UKR_A";
		_crewVest = "V_TacVest_oli";
		_pilotVest = "V_TacVest_oli";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_FieldPack_khk";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_khk";
		
		// ===================== SpecOps ==================
		
		_specUniform = "MNP_CombatUniform_RO2_Sh";
		_specHelmet = "MNP_Helmet_PAGST_UKR";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "MNP_Vest_UKR_B";
		_specRuck = "B_FieldPack_khk";
		
		// ================================================
	};
	default {
		// ==================== Uniforms ==================

		_plebUniformArray = ["MNP_CombatUniform_RU_Med"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_RU_Med";
		_rpilotUniform = "MNP_CombatUniform_RU_Med";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_PAGST_RT","MNP_Helmet_RU_LT","MNP_Helmet_PAGST_RT","MNP_Helmet_RU_LT","MNP_Helmet_PAGST_RT"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["MNP_Boonie_RU_T"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_PAGST_RT","MNP_Helmet_RU_LT","MNP_Helmet_PAGST_RT","MNP_Helmet_RU_LT","MNP_Helmet_PAGST_RT"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_RU_T";
		_gunnerVest = "MNP_Vest_RU_T2";
		_glVest = "MNP_Vest_RU_T2";
		_medVest = "MNP_Vest_RU_T";
		_crewVest = "V_TacVest_oli";
		_pilotVest = "V_TacVest_oli";
		
		// ===================== Rucks ====================
		
		_smallRuck = "MNP_B_RU2_FP";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "MNP_B_RU2_CA";
		
		// ==================== SpecOps ===================
		
		_specUniform = "MNP_CombatUniform_RU_Med";
		_specHelmet = "MNP_Helmet_PAGST_RT";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "MNP_Vest_RU_T2";
		_specRuck = "MNP_B_RU2_FP";
		
		// ================================================
	};
};

// =======================================================================
// ========================= Non-Camo Specific ===========================
// =======================================================================

// ================== Vehicle Crews ==================

_fpilotUniform = "U_O_PilotCoveralls";
_crewmanHelmetArray = ["rhs_tsh4","rhs_tsh4_ess","rhs_tsh4","rhs_tsh4_bala","rhs_tsh4","rhs_tsh4_ess_bala","rhs_tsh4"];	
_crewmanHRandom = (floor(random (count _crewmanHelmetArray))); 
_crewmanHelmet = _crewmanHelmetArray select _crewmanHRandom; // enter single string value to remove randommess
_rotaryPilotHelmet = "H_PilotHelmetHeli_O";
_rotaryCrewHelmet = "H_CrewHelmetHeli_O";
_fixedPilotHelmet = "H_PilotHelmetFighter_O";

// =================== Sniper Team ===================

_sniperUniform = "U_O_GhillieSuit";
_sniperVest = "V_TacVest_khk";
_sniperRuck = "B_FieldPack_ocamo";

// =================== Diver Gear ===================

_diverUniform = "U_O_Wetsuit";
_diverVest = "V_RebreatherB";
_diverRuck = "B_AssaultPack_blk";
_divingGoggles = "G_O_Diving";

// =================== Radio Rucks ===================

switch (_radioSelection) do {
	case "detection" : {
		private "_side";
		_side = toLower format ["%1", side player];
		// BLUFOR UAV
		if (_side == "west") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
		// OPFOR UAV
		if (_side == "east") then { 
			_uavRuck = "O_UAV_01_backpack_F";
			_uavTool = "O_UavTerminal";
		};
		// INDFOR UAV
		if (_side == "guer") then { 
			_uavRuck = "I_UAV_01_backpack_F";
			_uavTool = "I_UavTerminal";
		};
		// Civilian UAV
		if (_side == "civ") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
	};
	default {
		_uavRuck = "B_UAV_01_backpack_F";
		_uavTool = "B_UavTerminal";
	};
};

// =======================================================================