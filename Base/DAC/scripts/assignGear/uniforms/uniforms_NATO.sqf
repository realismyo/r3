// AssignGear - Uniforms
// by Mr. Agnet
// - Defines all of the wearable items to be used by the assignGear script.
// - Current Side, Faction: BLUFOR, NATO
// - Primary Mod: Vanilla
// - Variables: _camoPattern - "us", "ctrg" 
//				_headgear - "helmets", "softcover", "opshelm"

// =======================================================================
// Declares variables

private [
"_plebUniformArray","_plebRandom","_plebUniform",
"_plebHelmetArray","_plebHRandom","_plebHelmet",
"_plebVest","_gunnerVest","_glVest","_medVest",
"_smallRuck","_medRuck","_largeRuck",
"_specUniform","_specHelmet","_specGoggles","_specVest","_specRuck",
"_crewUniform","_rpilotUniform","_fpilotUniform",
"_crewmanHelmetArray","_crewmanHRandom","_crewmanHelmet","_rotaryPilotHelmet","_rotaryCrewHelmet","_fixedPilotHelmet",
"_crewVest","_pilotVest",
"_sniperUniform","_sniperVest","_sniperRuck",
"_diverUniform","_diverVest","_diverRuck",
"_goggles","_divingGoggles","_insignia",
"_airRadioRuck","_radioRuck","_diverRadioRuck","_uavRuck","_uavTool"
];

// =======================================================================
// =========================== Camo Specific =============================
// =======================================================================

_goggles = "";	// leave as "" for player defined
_insignia = ""; 

switch (_camoPattern) do {
	case "us" : {
		// ==================== Uniforms ==================

		_plebUniformArray = ["U_B_CombatUniform_mcam","U_B_CombatUniform_mcam_vest","U_B_CombatUniform_mcam"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "U_B_CombatUniform_mcam_vest";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["H_HelmetSpecB","H_HelmetSpecB_snakeskin","H_HelmetSpecB","H_HelmetSpecB_paint1","H_HelmetSpecB"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_MilCap_mcamo","H_Cap_headphones","H_Booniehat_khk_hs","H_Watchcap_camo","H_Booniehat_khk_hs","H_Watchcap_blk","H_Booniehat_khk_hs","H_Watchcap_khk","H_Booniehat_khk_hs","H_Bandanna_khk_hs"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "opshelm" : {
				_plebHelmetArray = ["H_HelmetB_light","H_HelmetB_light_grass","H_HelmetB_light","H_HelmetB_light_snakeskin","H_HelmetB_light"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["H_HelmetSpecB","H_HelmetSpecB_snakeskin","H_HelmetSpecB","H_HelmetSpecB_paint1","H_HelmetSpecB"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "V_PlateCarrier2_rgr";
		_gunnerVest = "V_PlateCarrierSpec_rgr";
		_glVest = "V_PlateCarrierGL_rgr";
		_medVest = "V_PlateCarrier2_rgr";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_mcamo";
		_largeRuck = "B_Carryall_mcamo";
		
		// ==================== SpecOps ===================
		
		_specUniform = "U_B_CombatUniform_mcam_vest";
		_specHelmet = "H_HelmetB_light";
		_specGoggles = "G_Bandanna_khk";
		_specVest = "V_PlateCarrierGL_rgr";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
	case "ctrg" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["U_B_CTRG_1","U_B_CTRG_3","U_B_CTRG_1"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "U_B_CTRG_3";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["H_HelmetSpecB","H_HelmetSpecB_snakeskin","H_HelmetSpecB","H_HelmetSpecB_paint1","H_HelmetSpecB"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_MilCap_gry","H_Cap_headphones","H_Booniehat_khk_hs","H_Watchcap_camo","H_Booniehat_khk_hs","H_Watchcap_blk","H_Booniehat_khk_hs","H_Watchcap_khk","H_Booniehat_khk_hs","H_Bandanna_khk_hs"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "opshelm" : {
				_plebHelmetArray = ["H_HelmetB_light","H_HelmetB_light_grass","H_HelmetB_light","H_HelmetB_light_snakeskin","H_HelmetB_light"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["H_HelmetSpecB","H_HelmetSpecB_snakeskin","H_HelmetSpecB","H_HelmetSpecB_paint1","H_HelmetSpecB"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "V_PlateCarrierL_CTRG";
		_gunnerVest = "V_PlateCarrierL_CTRG";
		_glVest = "V_PlateCarrierH_CTRG";
		_medVest = "V_PlateCarrierH_CTRG";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_cbr";
		
		// ===================== SpecOps ==================
		
		_specUniform = "U_B_CTRG_3";
		_specHelmet = "H_HelmetB_light";
		_specGoggles = "G_Bandanna_khk";
		_specVest = "V_PlateCarrierH_CTRG";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
	default {
		// ==================== Uniforms ==================

		_plebUniformArray = ["U_B_CombatUniform_mcam","U_B_CombatUniform_mcam_vest","U_B_CombatUniform_mcam"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "U_B_CombatUniform_mcam_vest";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["H_HelmetSpecB","H_HelmetSpecB_snakeskin","H_HelmetSpecB","H_HelmetSpecB_paint1","H_HelmetSpecB"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_MilCap_mcamo","H_Cap_headphones","H_Booniehat_khk_hs","H_Watchcap_camo","H_Booniehat_khk_hs","H_Watchcap_blk","H_Booniehat_khk_hs","H_Watchcap_khk","H_Booniehat_khk_hs","H_Bandanna_khk_hs"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "opshelm" : {
				_plebHelmetArray = ["H_HelmetB_light","H_HelmetB_light_grass","H_HelmetB_light","H_HelmetB_light_snakeskin","H_HelmetB_light"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["H_HelmetSpecB","H_HelmetSpecB_snakeskin","H_HelmetSpecB","H_HelmetSpecB_paint1","H_HelmetSpecB"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "V_PlateCarrier2_rgr";
		_gunnerVest = "V_PlateCarrierSpec_rgr";
		_glVest = "V_PlateCarrierGL_rgr";
		_medVest = "V_PlateCarrier2_rgr";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_mcamo";
		_largeRuck = "B_Carryall_mcamo";
		
		// =================== SpecOps ====================
		
		_specUniform = "U_B_CombatUniform_mcam_vest";
		_specHelmet = "H_HelmetB_light";
		_specGoggles = "G_Bandanna_khk";
		_specVest = "V_PlateCarrierGL_rgr";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
};

// =======================================================================
// ========================= Non-Camo Specific ===========================
// =======================================================================

// ================== Vehicle Crews ==================

_rpilotUniform = "U_B_HeliPilotCoveralls";
_fpilotUniform = "U_B_PilotCoveralls";

_crewmanHelmet = "H_HelmetCrew_B";
_rotaryPilotHelmet = "H_PilotHelmetHeli_B";
_rotaryCrewHelmet = "H_CrewHelmetHeli_B";
_fixedPilotHelmet = "H_PilotHelmetFighter_B";

_crewVest = "V_TacVest_oli";
_pilotVest = "V_TacVest_oli";

// =================== Sniper Team ===================

_sniperUniform = "U_B_GhillieSuit";
_sniperVest = "V_TacVest_oli";
_sniperRuck = "B_AssaultPack_rgr";

// =================== Diver Gear ===================

_diverUniform = "U_B_Wetsuit";
_diverVest = "V_RebreatherB";
_diverRuck = "B_AssaultPack_blk";
_divingGoggles = "G_B_Diving";

// =================== Radio Rucks ===================

switch (_radioSelection) do {
	case "detection" : {
		private "_side";
		_side = toLower format ["%1", side player];
		// BLUFOR UAV
		if (_side == "west") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
		// OPFOR UAV
		if (_side == "east") then { 
			_uavRuck = "O_UAV_01_backpack_F";
			_uavTool = "O_UavTerminal";
		};
		// INDFOR UAV
		if (_side == "guer") then { 
			_uavRuck = "I_UAV_01_backpack_F";
			_uavTool = "I_UavTerminal";
		};
		// Civilian UAV
		if (_side == "civ") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
	};
	default {
		_uavRuck = "B_UAV_01_backpack_F";
		_uavTool = "B_UavTerminal";
	};
};

// =======================================================================