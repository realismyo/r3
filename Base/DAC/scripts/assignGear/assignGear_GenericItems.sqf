// AssignGear - Generic Items
// by Mr. Agnet
// - Defines all of the generic items to be used by the assignGear script.

// =======================================================================
// Declares variables

private [
"_binos","_radio","_toolbox","_wireCutters","_cableTieItem","_bananas","_backupSight","_strobe","_mapLight","_uavBat","_ladder",
"_tabletDevice","_androidDevice","_microDevice","_cameraDevice",
"_steerableChute","_nonsteerableChute",
"_facItems","_facSmokes",
"_rangeFinder","_rangeFinderBat",
"_designator","_designatorBat","_dagr",
"_grenade","_smoke","_throwG","_chemlightOne","_chemlightTwo","_handFlareOne","_handFlareTwo","_flashbang",
"_atMine","_apersMine","_apersBound","_apersTrip","_slam","_claymore","_demoCharge","_satchelCharge",
"_clackOne","_clackTwo","_defuseKit","_deadManSwitch","_cellularDevice",
"_nightVision","_basicTools","_basicItems","_spareBarrel","_ftlItems","_secItems","_pltTools","_pltItems","_sniperItems",
"_bandageOne","_bandageTwo","_bandageThree","_bandageFour",
"_bloodOne","_bloodTwo","_bloodThree",
"_injectorOne","_injectorTwo","_injectorThree",
"_medkitOne","_medkitTwo",
"_plasmaOne","_plasmaTwo","_plasmaThree",
"_salineOne","_salineTwo","_salineThree",
"_tourniquet","_bodybag"
];

// =======================================================================
// =========================== Equipment/Items ===========================
// =======================================================================

// ===================== General ==================

_binos = "Binocular";
_radio = "ItemRadio";
_toolbox = "ToolKit";
_wireCutters = "ACE_wirecutter"; 
_cableTieItem = "ACE_CableTie";
_bananas = "ACE_Banana";
_strobe = "ACE_IR_Strobe_Item";
_mapLight = "ACE_Flashlight_MX991";
_uavBat = "ACE_UAVBattery";
_ladder = "ACE_TacticalLadder_Pack";

// ====================== CTab ====================

_tabletDevice = "ItemcTab";  		// Commander Tablet
_androidDevice = "ItemAndroid"; 	// Android based Blue Force Tracker
_microDevice = "ItemMicroDAGR"; 	// MicroDAGR GPS
_cameraDevice = "ItemcTabHCam";  	// Helmet Cam

// ====================== Para ====================

_steerableChute = "B_Parachute";
_nonsteerableChute = "ACE_NonSteerableParachute";

// ====================== FAC =====================

_facItems = ["ACE_MapTools"];
_facSmokes = ["SmokeShellBlue","SmokeShellOrange"];

// ================= Rangefinder ==================

if !(_nightGear) then { _rangeFinder = "ACE_VectorDay"; } else { _rangeFinder = "ACE_Vector"; };	// Rangefinder
_rangeFinderBat = "";			// no batteries as of yet

// ================== Laser Des ===================

_designator = "LaserDesignator";
_designatorBat = "LaserBatteries";
_dagr = "ACE_microDAGR";

// ============= General Throwables ===============

_grenade = "HandGrenade";
_smoke = "SmokeShell";
_throwG = [_smoke,_smoke,_grenade,_grenade];
_chemlightOne = "chemlight_green";
_chemlightTwo = "chemlight_red";
_handFlareOne = "ACE_HandFlare_Green";
_handFlareTwo = "ACE_HandFlare_Red";
_flashbang = "ACE_M84";

// ================= Explosives ===================

_atMine = "ATMine_Range_Mag";
_apersMine = "APERSMine_Range_Mag";
_apersBound = "APERSBoundingMine_Range_Mag";
_apersTrip = "APERSTripMine_Wire_Mag";
_slam = "SLAMDirectionalMine_Wire_Mag";
_claymore = "ClaymoreDirectionalMine_Remote_Mag";
_demoCharge = "DemoCharge_Remote_Mag";
_satchelCharge = "SatchelCharge_Remote_Mag";

// =============== Explosive Tools ================

_clackOne = "ACE_Clacker"; 
_clackTwo = "ACE_M26_Clacker";
_defuseKit = "ACE_DefusalKit";
_deadManSwitch = "ACE_DeadManSwitch";
_cellularDevice = "ACE_Cellphone";

// ==================== Tools =====================

_nightVision = "ACE_NVG_Wide";
_basicTools = [_radio,"ItemCompass","ItemMap","ACE_Altimeter"];
_basicItems = ["ACE_EarPlugs","ACE_MapTools",_mapLight]; 
_spareBarrel = "ACE_SpareBarrel";
_ftlItems = ["itemGPS"];
_secItems = ["itemGPS"];
_pltTools = [];
_pltItems = ["itemGPS"];
_sniperItems = ["ACE_Kestrel4500"];
_backupSight = ""; 

// ==================== ACRE Radios =====================

_radioOne = "ACRE_PRC343";
_radioTwo = "ACRE_PRC148";
_radioThree = "ACRE_PRC152";
_radioFour = "ACRE_PRC77";
_radioFive = "ACRE_PRC117F";

// =======================================================================
// =========================== Medical Items =============================
// =======================================================================

// ================= Basic Gear ===================

_bandageOne = "ACE_fieldDressing";
_bloodOne = "ACE_bloodIV_250";
_bloodTwo = "ACE_bloodIV_500";
_bloodThree = "ACE_bloodIV";
_injectorOne = "ACE_morphine";
_injectorTwo = "ACE_epinephrine";
_bodybag = "ACE_bodyBag";

// ================ Advanced Gear =================

_bandageTwo = "ACE_elasticBandage";
_bandageThree = "ACE_quikclot";
_bandageFour = "ACE_packingBandage";
_injectorThree = "ACE_atropine";
_medkitOne = "ACE_personalAidKit";
_medkitTwo = "ACE_surgicalKit";
_plasmaOne = "ACE_plasmaIV_250";
_plasmaTwo = "ACE_plasmaIV_500";
_plasmaThree = "ACE_plasmaIV";
_salineOne = "ACE_salineIV_250";
_salineTwo = "ACE_salineIV_500";
_salineThree = "ACE_salineIV";
_tourniquet = "ACE_tourniquet";

// =======================================================================