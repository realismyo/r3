/* 
Custom Time Script
by AGeNT

- Allows for custom time options via mission parameters.
*/ 

if !(isServer) exitWith {};

private ["_timeChange","_year","_newMonth","_newDay","_newHour","_newMinute"];

// ====================================================================================


_timeChange = ["Environment_TimeController",0] call BIS_fnc_GetParamValue;
if (_timeChange == 0) then {_timeChange = false} else {_timeChange = true}; 

_year = 2015;
_newMonth = ["Environment_MonthToSet",1] call BIS_fnc_GetParamValue;
_newDay = ["Environment_DayToSet",1] call BIS_fnc_GetParamValue;
_newHour = ["Environment_HourToSet",10] call BIS_fnc_GetParamValue;
_newMinute = ["Environment_MinuteToSet",0] call BIS_fnc_GetParamValue;

// ====================================================================================
// If we're changing the date in-mission, set the BIS function to display a smooth transition
// We check the various dates to make sure we stick with a smart value

if (_timeChange) then {
	if (time > 0) then {
		_day = date select 2;
		_month = date select 1;
		_year = date select 0;

		if (date select 3 > _newHour) then {_newDay = _newDay + 1};
		if (_newDay > 31) then {_newDay = 1; _newMonth = _newMonth + 1};
		if (_newMonth > 12) then {_month = 1; _year = _year + 1};
	};

	// Using the single variables, we construct a new variable _date
	_date = [_year,_newMonth,_newDay,_newHour,_newMinute];

	// Using a BIS function we share the new date across the network
	[_date,true,false] call BIS_fnc_setDate;

};
// ====================================================================================

