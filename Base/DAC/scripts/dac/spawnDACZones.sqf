// Headless Client DAC Initialisation Script
// By Mac/Mr. Agnet
// Spawns all the mission's DAC zones. This should be run from the init.sqf file
// Note that the headless client check is done in the init.sqf
// Usage: [] execVM "scripts\dac\spawnDACZones.sqf"
// ========================================================
// DAC Readme v3.1 - https://www.dropbox.com/s/zrvw0cw3u7vpk3s/DAC%20V3.1%20Readme.pdf?dl=0
// DAC Readme Short v3.1 - https://www.dropbox.com/s/59cm06f13cl7nuj/DAC%20V3.1%20Short%20instruction.pdf?dl=0
// ========================================================
// - This is where you put all of the DAC mission zones calls. 
// - Follows the standard DAC zone call, just isn't placed in the trigger's On Act field. 
// - Make sure you set them to disabled, i.e. the second array parameter, of the second array parameter to 1.
// - This allows you to activate zones as they become necessary, via trigger. Reference the example trigger in the framework.
// - EVENT config must now be 1. This is the third value in the first array. 
// For example:
// _nil = ["zone_exampleZone",[1,1,1],[4,1,10,10],[2,1,10,10],[],[],[1,4,6,0,4]] spawn DAC_Zone; 