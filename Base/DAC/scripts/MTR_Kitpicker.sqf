// Kitpicker rewrite using Ares/Achilles dialogboxes. Also consolidated to 1 file.
// Only uses the r_fnc_assignGear variable (from init.sqf) to determine which gear
// script to run.

// Main KitPicker Function.

MTR_Kitpicker = {
	private ["_dialog_title","_dialog_options","_dialogCount","_dialogResult","_loadoutArray","_loadoutstrings","_loadout","_player","_action"];
	
	// Store caller (player) and action ID
	_player = _this select 1;
	_action = _this select 2;
	
	// Define strings of loadout arrays - same as gear scripts. Format is: [DISPLAY STRING,GEARSCRIPT STRING]
	_loadoutArray = [
	
		// Company Roles
		["Company Commander","coyld"],
		["Platoon Commander","pltld"],
		["Platoon Sergeant","pltld"],
		["Platoon FAC","pltfac"],
		["Platoon UAV Operator","pltuavop"],
		["Platoon Medic","pltmed"],

		// Section Roles
		["Section Commander","secco"],
		["Team Leader (2iC)","sectl"],
		["Automatic Rifleman","ar"],
		["Asst. Automatic Rifleman","aar"],
		["Rifleman","rm"],
		["Rifleman (AT)","rmat"],
		["Grenadier","gren"],
		["Machinegunner","mmg"],
		["Asst. Machinegunner","mmgass"],
			
		// Vehicles
		["Crew Commander","crewmander"],
		["Crewman","crewman"],
		["Rotary Wing Pilot","rotarypilot"],
		["Rotary Wing Aircrew","rotarycrew"],
		["Fixed Wing Pilot","fixedpilot"],
			
		// CSW and Weapons
		["HAT Gunner","hatgun"],
		["HAT Assistant","hatammo"],
		["MAT Gunner","matgun"],
		["MAT Assistant","matammo"],
		["AA Gunner","aagun"],
		["AA Assistant","aaammo"],
		["HMG Gunner","hmggun"],
		["HMG Assistant","hmgass"],
		["HMG Ammobearer","hmgammo"],
		["GMG Gunner","gmggun"],
		["GMG Assistant","gmgass"],
		["GMG Ammobearer","gmgammo"],
			
		// Specialist Roles
		["Scoped Rifleman","rmsc"],
		["Designated Marksman","dmr"],
		["Heavy Grenadier","heavygren"],
		["SpecOps Team Leader","spectl"],
		["SpecOps Rifleman","specrm"],
		["Diver Team Leader","divertl"],
		["Diver","diver"],
		["Sniper","sniper"],
		["AM Sniper","amsniper"],
		["Spotter","spotter"],
		["Engineer","engi"],
		["Carbineer","carbineer"],
		["SMG Gunner","smgunner"],
		["Shotgunner","shotgunner"]
	];

	// Pull out the strings - to be used as option text
	_loadoutstrings = [];
	{_loadoutstrings pushback (_x select 0);} foreach _loadoutArray;


	_dialog_title = "JIP KITPICKER";
	_dialog_options = [["Select Kit",_loadoutstrings]];

	_dialogResult = [_dialog_title, _dialog_options] call Ares_fnc_ShowChooseDialog;
	
	// Selection is cancelled
	_dialogCount = count _dialogResult;
	if (_dialogCount == 0) exitWith {};

	// Selection is made
	_ZNum = (_dialogResult select 0);

	// The selection number is now stored in _ZNum. Apply to _loadoutArray to pull out loadout string
	_loadout = (_loadoutArray select _ZNum) select 1;

	// Fire gear script
	[_player,_loadout] call r_fnc_assignGear;

	// Remove action
	_player removeaction _action;

};

// PART 2 - WHO GETS THE ACTION - using isJip function now native to engine.
// Add action, otherwise do nothing
if (r_isJIP) then {
	
	// Use spawn to store in scheduled and move on
	[] spawn {
		
		// Wait to spawn/init properly
		waitUntil {time > 1};																			
		if (player != vehicle player) then { waitUntil { !isNull player; }; };
		if (!isDedicated && isMultiplayer) then { waitUntil { !isNull player && isPlayer player; }; };

		// Add action
		player addaction ["<t color='#dddd00'>" + "Select Loadout" + "</t>",MTR_kitpicker,[],6,true,false,"","_target == player"];

	};
};




