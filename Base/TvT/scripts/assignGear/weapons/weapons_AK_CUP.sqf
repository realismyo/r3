// AssignGear - Weapons
// by Mr. Agnet
// - Defines all of the weapon classes to be used by the assignGear script.
// - Current Class: AKs
// - Primary Mod: CUP
// - Variables: _variant - "ak107", "aks74", "ak74m"

// =======================================================================
// Declares variables

private [
"_rifle","_rifleGL","_rifleScoped","_autoRifle","_carbine","_dmr","_mmg","_smg","_pistol",
"_rifle_spec","_rifleGL_spec",
"_rifleMag","_rifleMatchMag","_rifleTracerMag","_rifleGLMag","_rifleScopedMag","_autoRifleMag","_autoTracerMag","_carbineMag","_dmrMag","_mmgMag","_mmgTracerMag","_smgMag","_pistolMag",
"_rifleMag_spec","_rifleTracerMag_spec","_rifleGLMag_spec","_rifleGLTracerMag_spec",
"_glExplody","_glSmokeOne","_glSmokeTwo","_glFlareOne","_glFlareTwo",
"_shotty","_shottyBuck","_shottySlug","_glHeavy","_glHeavyExplody","_glHSmokesOne","_glHSmokesTwo",
"_lat","_latMag","_matLaunch","_matMag","_matScope","_hatLaunch","_hatMag","_aaLaunch","_aaMag",
"_boltRifle","_boltRifleMag","_amRifle","_amRifleMag",
"_hmgBarrel","_hmgTripod","_hmgMag","_gmgBarrel","_gmgTripod","_gmgMag",
"_rifleDiver","_rifleDiverMagOne","_rifleDiverMagTwo",
"_generalAttachments","_dmrAttachments","_autoRifleAttachments","_mmgAttachments","_specOpsAttachments",
"_sniperAttachments","_amSniperAttachments","_pistolAttachments",
"_scoped_generalAttachments","_scoped_dmrAttachments","_scoped_autoRifleAttachments","_scoped_mmgAttachments","_scoped_specOpsAttachments",
"_suppressed_generalAttachments","_suppressed_dmrAttachments","_suppressed_autoRifleAttachments","_suppressed_mmgAttachments","_suppressed_specOpsAttachments",
"_suppressed_pistolAttachments","_arMagCount"
];

_arMagCount = 6;

// =======================================================================
// =========================== General Weapons ===========================
// =======================================================================

switch (_variant) do {
	case "ak107" :  {
		_rifle = "CUP_arifle_AK107";
		_rifleGL = "CUP_arifle_AK107_GL";
		_rifleScoped = "CUP_arifle_AK107";
		_carbine = "CUP_arifle_AKS74U";
		_rifle_spec = "CUP_arifle_AK107";
		_rifleGL_spec = "CUP_arifle_AK107_GL";
	};
	case "aks74" :  {
		_rifle = "CUP_arifle_AK74";
		_rifleGL = "CUP_arifle_AK74_GL";
		_rifleScoped = "CUP_arifle_AK74";
		_carbine = "hlc_rifle_aks74u";
		_rifle_spec = "CUP_arifle_AK74";
		_rifleGL_spec = "CUP_arifle_AK74_GL";
	};
	case "ak74m" :  {
		_rifle = "CUP_arifle_AK74M";
		_rifleGL = "CUP_arifle_AK74M_GL";
		_rifleScoped = "CUP_arifle_AK74M";
		_carbine = "hlc_rifle_aks74u";
		_rifle_spec = "CUP_arifle_AK74M";
		_rifleGL_spec = "CUP_arifle_AK74M_GL";
	};
	default {
		_rifle = "CUP_arifle_AK107";
		_rifleGL = "CUP_arifle_AK107_GL";
		_rifleScoped = "CUP_arifle_AK107";
		_carbine = "CUP_arifle_AKS74U";
		_rifle_spec = "CUP_arifle_AK107";
		_rifleGL_spec = "CUP_arifle_AK107_GL";
	};
};
_autoRifle = "CUP_arifle_RPK74_45";
_dmr = "CUP_srifle_SVD";
_mmg = "CUP_lmg_Pecheneg";
_smg = "hlc_rifle_aku12";
_pistol = "CUP_hgun_PB6P9";

// ================= General Magazines ============

_rifleMag = "CUP_30Rnd_545x39_AK_M";
_rifleMatchMag = "CUP_30Rnd_545x39_AK_M";
_rifleTracerMag = "CUP_30Rnd_TE1_Red_Tracer_545x39_AK_M";
_rifleGLMag = "CUP_30Rnd_545x39_AK_M";
_rifleScopedMag = "CUP_30Rnd_545x39_AK_M";
_autoRifleMag = "CUP_45Rnd_TE4_LRT4_Green_Tracer_545x39_RPK_M";
_autoTracerMag = "CUP_45Rnd_TE4_LRT4_Green_Tracer_545x39_RPK_M";
_carbineMag = "CUP_30Rnd_545x39_AK_M";
_dmrMag = "CUP_10Rnd_762x54_SVD_M";
_mmgMag = "CUP_100Rnd_TE4_LRT4_762x54_PK_Tracer_Green_M";
_mmgTracerMag = "CUP_100Rnd_TE4_LRT4_762x54_PK_Tracer_Green_M";
_smgMag = "CUP_30Rnd_545x39_AK_M";
_pistolMag = "CUP_8Rnd_9x18_Makarov_M";

// ================= SpecOps Magazines ============

_rifleMag_spec = "CUP_30Rnd_545x39_AK_M";
_rifleTracerMag_spec = "CUP_30Rnd_TE1_Red_Tracer_545x39_AK_M";
_rifleGLMag_spec = "CUP_30Rnd_545x39_AK_M";
_rifleGLTracerMag_spec = "CUP_30Rnd_TE1_Red_Tracer_545x39_AK_M";

// ===================== GL Rounds ================

_glExplody = "CUP_1Rnd_HE_GP25_M";
_glSmokeOne = "CUP_1Rnd_SmokeGreen_GP25_M";
_glSmokeTwo = "CUP_1Rnd_SmokeRed_GP25_M";
_glFlareOne = "CUP_FlareGreen_GP25_M";
_glFlareTwo = "CUP_FlareRed_GP25_M";


// =======================================================================
// =========================== Weapons Teams =============================
// =======================================================================

// ================== Shotgun =====================

_shotty = "rhs_weap_M590_8RD";
_shottyBuck = "rhsusf_8Rnd_00Buck";
_shottySlug = "rhsusf_8Rnd_Slug";

// ============== Heavy Grenadier =================

_glHeavy = "rhs_weap_m32";
_glHeavyExplody = "rhsusf_mag_6Rnd_M433_HEDP";
_glHSmokesOne = "rhsusf_mag_6Rnd_M715_green";
_glHSmokesTwo = "rhsusf_mag_6Rnd_M713_red";

// =================== LAT ========================

_lat = "rhs_weap_rpg26";
_latMag = "rhs_rpg26_mag";

// =================== MAT ========================

_matLaunch = "launch_RPG32_F";
_matMag = "RPG32_F";
_matScope = "CUP_optic_MAAWS_Scope";

// =================== HAT ========================

_hatLaunch = "launch_O_Titan_short_F";
_hatMag = "Titan_AT";

// =================== Anti-Air ===================

_aaLaunch = "rhs_weap_igla";
_aaMag = "rhs_mag_9k38_rocket";

// ==================== Snipers ===================

_boltRifle = "hlc_rifle_awmagnum_BL";
_boltRifleMag = "hlc_5rnd_300WM_FMJ_AWM";
_amRifle = "srifle_LRR_F"; 
_amRifleMag = "7Rnd_408_Mag";

// ================= HMG Team =====================

_hmgBarrel = "O_HMG_01_weapon_F";
_hmgTripod = "O_HMG_01_support_F";
_hmgMag = "";	// no magazines as of yet

// ================= GMG Team =====================

_gmgBarrel = "O_GMG_01_weapon_F";
_gmgTripod = "O_HMG_01_support_F";
_gmgMag = "";	// no magazines as of yet

// ==================== Diver =====================

if (_underwaterWeapons) then { 
	_rifleDiver = "arifle_SDAR_F"; 
} else { 
	_rifleDiver = _rifle; 
};
if (_underwaterWeapons) then { 
	_rifleDiverMagOne = "30Rnd_556x45_Stanag";	// standard mag
	_rifleDiverMagTwo = "20Rnd_556x45_UW_mag";	// underwater mag
} else { 
	_rifleDiverMagOne = _rifleMag;
	_rifleDiverMagTwo = _rifleMag;
};

// ================ Attachments ===================

_generalAttachments = ["CUP_optic_Kobra","acc_flashlight"];
_dmrAttachments = ["optic_ACO_grn"];
_autoRifleAttachments = ["CUP_optic_Kobra"];
_mmgAttachments = [];
_specOpsAttachments = ["CUP_optic_Kobra","acc_pointer_IR"]; 
_sniperAttachments = ["optic_SOS"];
_amSniperAttachments = ["optic_SOS"];
_pistolAttachments = [];

_scoped_generalAttachments = ["CUP_optic_PSO_1","acc_flashlight"]; 
_scoped_dmrAttachments = ["CUP_optic_PSO_3"];
_scoped_autoRifleAttachments = ["CUP_optic_PSO_1"];
_scoped_mmgAttachments = ["rhs_acc_1p78","acc_flashlight"];
_scoped_specOpsAttachments = ["CUP_optic_PSO_1","acc_pointer_IR"];

_suppressed_generalAttachments = ["CUP_muzzle_PBS4"];
_suppressed_dmrAttachments = [];
_suppressed_autoRifleAttachments = ["CUP_muzzle_PBS4"];
_suppressed_mmgAttachments = [];
_suppressed_specOpsAttachments = ["CUP_muzzle_PBS4"];
_suppressed_pistolAttachments = [];

// =======================================================================