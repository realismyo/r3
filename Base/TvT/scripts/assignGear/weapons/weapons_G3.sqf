// AssignGear - Weapons
// by Mr. Agnet
// - Defines all of the weapon classes to be used by the assignGear script.
// - Current Class: G3
// - Primary Mod: HLC
// - Variables: None.

// =======================================================================
// Declares variables

private [
"_rifle","_rifleGL","_rifleScoped","_autoRifle","_carbine","_dmr","_mmg","_smg","_pistol",
"_rifle_spec","_rifleGL_spec",
"_rifleMag","_rifleMatchMag","_rifleTracerMag","_rifleGLMag","_rifleScopedMag","_autoRifleMag","_autoTracerMag","_carbineMag","_dmrMag","_mmgMag","_mmgTracerMag","_smgMag","_pistolMag",
"_rifleMag_spec","_rifleTracerMag_spec","_rifleGLMag_spec","_rifleGLTracerMag_spec",
"_glExplody","_glSmokeOne","_glSmokeTwo","_glFlareOne","_glFlareTwo",
"_shotty","_shottyBuck","_shottySlug","_glHeavy","_glHeavyExplody","_glHSmokesOne","_glHSmokesTwo",
"_lat","_latMag","_matLaunch","_matMag","_matScope","_hatLaunch","_hatMag","_aaLaunch","_aaMag",
"_boltRifle","_boltRifleMag","_amRifle","_amRifleMag",
"_hmgBarrel","_hmgTripod","_hmgMag","_gmgBarrel","_gmgTripod","_gmgMag",
"_rifleDiver","_rifleDiverMagOne","_rifleDiverMagTwo",
"_generalAttachments","_dmrAttachments","_autoRifleAttachments","_mmgAttachments","_specOpsAttachments",
"_sniperAttachments","_amSniperAttachments","_pistolAttachments",
"_scoped_generalAttachments","_scoped_dmrAttachments","_scoped_autoRifleAttachments","_scoped_mmgAttachments","_scoped_specOpsAttachments",
"_suppressed_generalAttachments","_suppressed_dmrAttachments","_suppressed_autoRifleAttachments","_suppressed_mmgAttachments","_suppressed_specOpsAttachments",
"_suppressed_pistolAttachments","_arMagCount"
];

_arMagCount = 2;

// =======================================================================
// =========================== General Weapons ===========================
// =======================================================================

_rifle = "hlc_rifle_g3ka4";
_rifleGL = "HLC_Rifle_g3ka4_GL";
_rifleScoped = "hlc_rifle_g3ka4";
_autoRifle = "LMG_Mk200_F";
_carbine = "hlc_rifle_hk53";
_dmr = "hlc_rifle_g3ka4";
_mmg = "LMG_Zafir_F";
_smg = "hlc_rifle_hk53";
_pistol = "hgun_Rook40_F";

// ================= SpecOps Weapons ==============

_rifle_spec = "hlc_rifle_g3ka4";
_rifleGL_spec = "HLC_Rifle_g3ka4_GL";

// ================= General Magazines ============

_rifleMag = "hlc_20rnd_762x51_b_G3";
_rifleMatchMag = "hlc_20rnd_762x51_Mk316_G3";
_rifleTracerMag = "hlc_20rnd_762x51_T_G3";
_rifleGLMag = "hlc_20rnd_762x51_b_G3";
_rifleScopedMag = "hlc_20rnd_762x51_b_G3";
_autoRifleMag = "200Rnd_65x39_cased_Box";
_autoTracerMag = "200Rnd_65x39_cased_Box_Tracer";
_carbineMag = "hlc_30rnd_556x45_b_HK33";
_dmrMag = "hlc_20rnd_762x51_b_G3";
_mmgMag = "150Rnd_762x54_Box";
_mmgTracerMag = "150Rnd_762x54_Box";
_smgMag = "hlc_30rnd_556x45_b_HK33";
_pistolMag = "16Rnd_9x21_Mag";
// ================= SpecOps Magazines ============

_rifleMag_spec = "hlc_20rnd_762x51_Mk316_G3";
_rifleTracerMag_spec = "hlc_20rnd_762x51_T_G3";
_rifleGLMag_spec = "hlc_20rnd_762x51_Mk316_G3";
_rifleGLTracerMag_spec = "hlc_20rnd_762x51_T_G3";

// ===================== GL Rounds ================

_glExplody = "1Rnd_HE_Grenade_shell";
_glSmokeOne = "1Rnd_SmokeGreen_Grenade_shell";
_glSmokeTwo = "1Rnd_SmokeRed_Grenade_shell";
_glFlareOne = "UGL_FlareGreen_F";
_glFlareTwo = "UGL_FlareRed_F";

// =======================================================================
// =========================== Weapons Teams =============================
// =======================================================================

// ================== Shotgun =====================

_shotty = "rhs_weap_M590_8RD";
_shottyBuck = "rhsusf_8Rnd_00Buck";
_shottySlug = "rhsusf_8Rnd_Slug";

// ============== Heavy Grenadier =================

_glHeavy = "rhs_weap_m32";
_glHeavyExplody = "rhsusf_mag_6Rnd_M433_HEDP";
_glHSmokesOne = "rhsusf_mag_6Rnd_M715_green";
_glHSmokesTwo = "rhsusf_mag_6Rnd_M713_red";

// =================== LAT ========================

_lat = "rhs_weap_rpg26";
_latMag = "rhs_rpg26_mag";

// =================== MAT ========================

_matLaunch = "launch_RPG32_F";
_matMag = "RPG32_F";
_matScope = "CUP_optic_MAAWS_Scope";

// =================== HAT ========================

_hatLaunch = "launch_O_Titan_short_F";
_hatMag = "Titan_AT";

// =================== Anti-Air ===================

_aaLaunch = "rhs_weap_igla";
_aaMag = "rhs_mag_9k38_rocket";

// ==================== Snipers ===================

_boltRifle = "hlc_rifle_awmagnum_BL";
_boltRifleMag = "hlc_5rnd_300WM_FMJ_AWM";
_amRifle = "srifle_LRR_F"; 
_amRifleMag = "7Rnd_408_Mag";

// ================= HMG Team =====================

_hmgBarrel = "O_HMG_01_weapon_F";
_hmgTripod = "O_HMG_01_support_F";
_hmgMag = "";	// no magazines as of yet

// ================= GMG Team =====================

_gmgBarrel = "O_GMG_01_weapon_F";
_gmgTripod = "O_HMG_01_support_F";
_gmgMag = "";	// no magazines as of yet

// ==================== Diver =====================

if (_underwaterWeapons) then { 
	_rifleDiver = "arifle_SDAR_F"; 
} else { 
	_rifleDiver = _rifle; 
};
if (_underwaterWeapons) then { 
	_rifleDiverMagOne = "30Rnd_556x45_Stanag";	// standard mag
	_rifleDiverMagTwo = "20Rnd_556x45_UW_mag";	// underwater mag
} else { 
	_rifleDiverMagOne = _rifleMag;
	_rifleDiverMagTwo = _rifleMag;
};

// ================ Attachments ===================

_generalAttachments = ["optic_ACO_grn","acc_flashlight"];
_dmrAttachments = ["optic_ACO_grn","acc_flashlight","bipod_02_F_blk"];
_autoRifleAttachments = ["optic_ACO_grn","acc_flashlight","bipod_02_F_blk"];
_mmgAttachments = ["optic_ACO_grn","acc_flashlight"];
_specOpsAttachments = ["optic_ACO_grn","acc_pointer_IR"]; 
_sniperAttachments = ["optic_SOS"];
_amSniperAttachments = ["optic_SOS"];
_pistolAttachments = [];


_scoped_generalAttachments = ["optic_Arco","acc_flashlight"]; 
_scoped_dmrAttachments = ["optic_MRCO","acc_flashlight","bipod_02_F_blk"];
_scoped_autoRifleAttachments = ["optic_Arco","acc_flashlight","bipod_02_F_blk"];
_scoped_mmgAttachments = ["optic_Arco","acc_flashlight"];
_scoped_specOpsAttachments = ["optic_Arco","acc_pointer_IR"];

_suppressed_generalAttachments = ["muzzle_snds_B"];
_suppressed_dmrAttachments = ["muzzle_snds_B"];
_suppressed_autoRifleAttachments = ["muzzle_snds_H_MG"];
_suppressed_mmgAttachments = [];
_suppressed_specOpsAttachments = ["muzzle_snds_H"];
_suppressed_pistolAttachments = ["muzzle_snds_L"];

// =======================================================================