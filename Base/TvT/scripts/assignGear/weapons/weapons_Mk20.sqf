// AssignGear - Weapons
// by Mr. Agnet
// - Defines all of the weapon classes to be used by the assignGear script.
// - Current Class: Mk20
// - Primary Mod: Vanilla
// - Variables: None.
// =======================================================================
// Declares variables

private [
"_rifle","_rifleGL","_rifleScoped","_autoRifle","_carbine","_dmr","_mmg","_smg","_pistol",
"_rifle_spec","_rifleGL_spec",
"_rifleMag","_rifleMatchMag","_rifleTracerMag","_rifleGLMag","_rifleScopedMag","_autoRifleMag","_autoTracerMag","_carbineMag","_dmrMag","_mmgMag","_mmgTracerMag","_smgMag","_pistolMag",
"_rifleMag_spec","_rifleTracerMag_spec","_rifleGLMag_spec","_rifleGLTracerMag_spec",
"_glExplody","_glSmokeOne","_glSmokeTwo","_glFlareOne","_glFlareTwo",
"_shotty","_shottyBuck","_shottySlug","_glHeavy","_glHeavyExplody","_glHSmokesOne","_glHSmokesTwo",
"_lat","_latMag","_matLaunch","_matMag","_matScope","_hatLaunch","_hatMag","_aaLaunch","_aaMag",
"_boltRifle","_boltRifleMag","_amRifle","_amRifleMag",
"_hmgBarrel","_hmgTripod","_hmgMag","_gmgBarrel","_gmgTripod","_gmgMag",
"_rifleDiver","_rifleDiverMagOne","_rifleDiverMagTwo",
"_generalAttachments","_dmrAttachments","_autoRifleAttachments","_mmgAttachments","_specOpsAttachments",
"_sniperAttachments","_amSniperAttachments","_pistolAttachments",
"_scoped_generalAttachments","_scoped_dmrAttachments","_scoped_autoRifleAttachments","_scoped_mmgAttachments","_scoped_specOpsAttachments",
"_suppressed_generalAttachments","_suppressed_dmrAttachments","_suppressed_autoRifleAttachments","_suppressed_mmgAttachments","_suppressed_specOpsAttachments",
"_suppressed_pistolAttachments","_arMagCount"
];

_arMagCount = 2;

// =======================================================================
// =========================== General Weapons ===========================
// =======================================================================

_rifle = "arifle_Mk20_F";  
_rifleGL = "arifle_Mk20_GL_plain_F"; 
_rifleScoped = "arifle_Mk20_F"; 
_autoRifle = "LMG_Mk200_F"; 
_carbine = "arifle_Mk20C_F"; 
_dmr = "rhs_weap_m14ebrri";  
_mmg = "LMG_Zafir_F"; 
_smg = "SMG_01_F"; 
_pistol = "hgun_Pistol_heavy_01_F";

// ================= SpecOps Weapons ==============

_rifle_spec = "arifle_Mk20C_F";
_rifleGL_spec = "arifle_Mk20_GL_plain_F";

// ================= General Magazines ============

_rifleMag = "30Rnd_556x45_Stanag";
_rifleMatchMag = "30Rnd_556x45_Stanag";
_rifleTracerMag = "30Rnd_556x45_Stanag_Tracer_Red";
_rifleGLMag = "30Rnd_556x45_Stanag";
_rifleScopedMag = "30Rnd_556x45_Stanag";
_autoRifleMag = "200Rnd_65x39_cased_Box";
_autoTracerMag = "200Rnd_65x39_cased_Box";
_carbineMag = "30Rnd_556x45_Stanag";
_dmrMag = "rhsusf_20Rnd_762x51_m118_special_Mag";
_mmgMag = "150Rnd_762x54_Box";
_mmgTracerMag = "150Rnd_762x54_Box";
_smgMag = "30Rnd_45ACP_Mag_SMG_01";
_pistolMag = "11Rnd_45ACP_Mag";

// ================= SpecOps Magazines ============

_rifleMag_spec = "30Rnd_556x45_Stanag";
_rifleTracerMag_spec = "30Rnd_556x45_Stanag_Tracer_Red";
_rifleGLMag_spec = "30Rnd_556x45_Stanag";
_rifleGLTracerMag_spec = "30Rnd_556x45_Stanag_Tracer_Red";

// ===================== GL Rounds ================

_glExplody = "1Rnd_HE_Grenade_shell";
_glSmokeOne = "1Rnd_SmokeGreen_Grenade_shell";
_glSmokeTwo = "1Rnd_SmokeRed_Grenade_shell";
_glFlareOne = "UGL_FlareGreen_F";
_glFlareTwo = "UGL_FlareRed_F";

// =======================================================================
// =========================== Weapons Teams =============================
// =======================================================================

// ================== Shotgun =====================

_shotty = "rhs_weap_M590_8RD";
_shottyBuck = "rhsusf_8Rnd_00Buck";
_shottySlug = "rhsusf_8Rnd_Slug";

// ============== Heavy Grenadier =================

_glHeavy = "rhs_weap_m32";
_glHeavyExplody = "rhsusf_mag_6Rnd_M433_HEDP";
_glHSmokesOne = "rhsusf_mag_6Rnd_M715_green";
_glHSmokesTwo = "rhsusf_mag_6Rnd_M713_red";

// =================== LAT ========================

_lat = "launch_NLAW_F";
_latMag = "NLAW_F";

// =================== MAT ========================

_matLaunch = "launch_RPG32_F";
_matMag = "RPG32_F";
_matScope = "CUP_optic_MAAWS_Scope";

// =================== HAT ========================

_hatLaunch = "launch_B_Titan_short_F";
_hatMag = "Titan_AT";

// =================== Anti-Air ===================

_aaLaunch = "launch_B_Titan_F";
_aaMag = "Titan_AA";

// ==================== Snipers ===================

_boltRifle = "hlc_rifle_awmagnum_BL";
_boltRifleMag = "hlc_5rnd_300WM_FMJ_AWM";
_amRifle = "srifle_LRR_F"; 
_amRifleMag = "7Rnd_408_Mag";

// ================= HMG Team =====================

_hmgBarrel = "B_HMG_01_weapon_F";
_hmgTripod = "B_HMG_01_support_F";
_hmgMag = "";	// no magazines as of yet

// ================= GMG Team =====================

_gmgBarrel = "B_GMG_01_weapon_F";
_gmgTripod = "B_HMG_01_support_F";
_gmgMag = "";	// no magazines as of yet

// ==================== Diver =====================

if (_underwaterWeapons) then { 
	_rifleDiver = "arifle_SDAR_F"; 
} else { 
	_rifleDiver = _rifle; 
};
if (_underwaterWeapons) then { 
	_rifleDiverMagOne = "30Rnd_556x45_Stanag";	// standard mag
	_rifleDiverMagTwo = "20Rnd_556x45_UW_mag";	// underwater mag
} else { 
	_rifleDiverMagOne = _rifleMag;
	_rifleDiverMagTwo = _rifleMag;
};

// ================ Attachments ===================

_generalAttachments = ["optic_ACO_grn","acc_flashlight"];
_dmrAttachments = ["optic_ACO_grn","acc_flashlight"];
_autoRifleAttachments = ["optic_ACO_grn","acc_flashlight"];
_mmgAttachments = ["optic_ACO_grn","acc_flashlight"];
_specOpsAttachments = ["optic_ACO_grn","acc_pointer_IR"]; 
_sniperAttachments = ["optic_SOS"];
_amSniperAttachments = ["optic_SOS"];
_pistolAttachments = ["optic_MRD"];

_scoped_generalAttachments = ["optic_Hamr","acc_flashlight"];
_scoped_dmrAttachments = ["optic_MRCO","acc_flashlight","bipod_01_F_blk"];
_scoped_autoRifleAttachments = ["optic_Hamr","acc_flashlight"];
_scoped_mmgAttachments = ["optic_Hamr","acc_flashlight"];
_scoped_specOpsAttachments = ["optic_Hamr","acc_pointer_IR"];

_suppressed_generalAttachments = ["muzzle_snds_M"];
_suppressed_dmrAttachments = ["muzzle_snds_B"];
_suppressed_autoRifleAttachments = ["muzzle_snds_H_MG"];
_suppressed_mmgAttachments = [];
_suppressed_specOpsAttachments = ["muzzle_snds_M"];
_suppressed_pistolAttachments = ["muzzle_snds_acp"];

// =======================================================================