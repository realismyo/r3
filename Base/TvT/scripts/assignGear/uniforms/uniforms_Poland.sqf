// AssignGear - Uniforms
// by Mr. Agnet
// - Defines all of the wearable items to be used by the assignGear script.
// - Current Side, Faction: BLUFOR, Poland
// - Primary Mod: PSZ
// - Variables: _camoPattern - "woodland", "des"
//				_headgear - "helmets", "softcover"

// =======================================================================
// Declares variables

private [
"_plebUniformArray","_plebRandom","_plebUniform",
"_plebHelmetArray","_plebHRandom","_plebHelmet",
"_plebVest","_gunnerVest","_glVest","_medVest",
"_smallRuck","_medRuck","_largeRuck",
"_specUniform","_specHelmet","_specGoggles","_specVest","_specRuck",
"_crewUniform","_rpilotUniform","_fpilotUniform",
"_crewmanHelmetArray","_crewmanHRandom","_crewmanHelmet","_crewmanHelmetArray","_crewmanHRandom","_rotaryPilotHelmet","_rotaryCrewHelmet","_fixedPilotHelmet",
"_crewVest","_pilotVest",
"_sniperUniform","_sniperVest","_sniperRuck",
"_diverUniform","_diverVest","_diverRuck",
"_goggles","_divingGoggles","_insignia",
"_airRadioRuck","_radioRuck","_diverRadioRuck","_uavRuck","_uavTool"
];

// =======================================================================
// =========================== Camo Specific =============================
// =======================================================================

_goggles = "";	// leave as "" for player defined
_insignia = ""; 

switch (_camoPattern) do {
	case "woodland" : {
		// ==================== Uniforms ==================

		_plebUniformArray = ["PSZ_U_PL_WDL_wz2010_Crye","PSZ_U_PL_WDL_wz2010_Crye_Folded","PSZ_U_PL_WDL_wz2010_Crye"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "PSZ_U_PL_WDL_wz2010_Crye_Folded";
		_rpilotUniform = "PSZ_U_PL_WDL_wz2010_Crye_Folded";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["PSZ_H_wz2005_WDL","PSZ_H_wz2005_WDL_ESS","PSZ_H_wz2005_OLIVE","PSZ_H_wz2005_OLIVE_ESS"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["PSZ_H_Booniehat_WDL","rhs_beanie_green","H_Booniehat_oli","H_Cap_oli","H_Bandanna_khk"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["PSZ_H_wz2005_WDL","PSZ_H_wz2005_WDL_ESS","PSZ_H_wz2005_OLIVE","PSZ_H_wz2005_OLIVE_ESS"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		_crewmanHelmetArray = ["rhsusf_cvc_green_helmet","rhsusf_cvc_green_ess","rhsusf_cvc_green_helmet","rhsusf_cvc_green_ess","rhsusf_cvc_green_helmet"];	
		_crewmanHRandom = (floor(random (count _crewmanHelmetArray))); 
		_crewmanHelmet = _crewmanHelmetArray select _crewmanHRandom; // enter single string value to remove randommess
		
		// ===================== Vests ====================
		
		_plebVest = "PSZ_V_UKO_H_WDL_R_Headset";
		_gunnerVest = "PSZ_V_UKO_H_WDL_MG_Headset";
		_glVest = "PSZ_V_UKO_H_WDL_CO_Headset";
		_medVest = "PSZ_V_UKO_H_WDL_M_Headset";
		_pilotVest = "PSZ_V_UKO_L_WDL";
		_crewVest = "PSZ_V_UKO_L_WDL";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "PSZ_B_wz93_WDL";
		_largeRuck = "PSZ_B_wz97_WDL";
		
		// ==================== SpecOps ===================
		
		_specUniform = "PSZ_U_PL_WDL_wz2010_Crye_Folded";
		_specHelmet = "PSZ_H_wz2005_WDL_ESS";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "PSZ_V_UKO_H_WDL_MG_Headset";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
	case "des" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["PSZ_U_PL_DES_wz2010_Crye","PSZ_U_PL_DES_wz2010_Crye_Folded","PSZ_U_PL_DES_wz2010_Crye"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "PSZ_U_PL_DES_wz2010_Crye_Folded";
		_rpilotUniform = "PSZ_U_PL_DES_wz2010_Crye_Folded";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["PSZ_H_wz2005_DES","PSZ_H_wz2005_DES_ESS","PSZ_H_wz2005_CAMO","PSZ_H_wz2005_CAMO_ESS"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["PSZ_H_Booniehat_DES","H_Cap_tan","H_Booniehat_tan","H_Bandanna_khk"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["PSZ_H_wz2005_DES","PSZ_H_wz2005_DES_ESS","PSZ_H_wz2005_CAMO","PSZ_H_wz2005_CAMO_ESS"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		_crewmanHelmetArray = ["rhsusf_cvc_helmet","rhsusf_cvc_ess","rhsusf_cvc_helmet","rhsusf_cvc_ess","rhsusf_cvc_helmet"];	
		_crewmanHRandom = (floor(random (count _crewmanHelmetArray)));
		_crewmanHelmet = _crewmanHelmetArray select _crewmanHRandom; // enter single string value to remove randommess
		
		// ===================== Vests ====================
		
		_plebVest = "PSZ_V_UKO_H_DES_R_Headset";
		_gunnerVest = "PSZ_V_UKO_H_DES_MG_Headset";
		_glVest = "PSZ_V_UKO_H_DES_CO_Headset";
		_medVest = "PSZ_V_UKO_H_DES_M_Headset";
		_pilotVest = "PSZ_V_UKO_L_DES";
		_crewVest = "PSZ_V_UKO_L_DES";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_khk";
		_medRuck = "PSZ_B_wz93_DES";
		_largeRuck = "PSZ_B_wz97_DES";
		
		// ===================== SpecOps ==================
		
		_specUniform = "PSZ_U_PL_DES_wz2010_Crye_Folded";
		_specHelmet = "PSZ_H_wz2005_DES_ESS";
		_specGoggles = "G_Bandanna_khk";
		_specVest = "PSZ_V_UKO_H_DES_CO_Headset";
		_specRuck = "B_AssaultPack_khk";
		
		// ================================================
	};
	default {
		// ==================== Uniforms ==================

		_plebUniformArray = ["PSZ_U_PL_WDL_wz2010_Crye","PSZ_U_PL_WDL_wz2010_Crye_Folded","PSZ_U_PL_WDL_wz2010_Crye"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "PSZ_U_PL_WDL_wz2010_Crye_Folded";
		_rpilotUniform = "PSZ_U_PL_WDL_wz2010_Crye_Folded";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["PSZ_H_wz2005_WDL","PSZ_H_wz2005_WDL_ESS","PSZ_H_wz2005_OLIVE","PSZ_H_wz2005_OLIVE_ESS"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["PSZ_H_Booniehat_WDL","rhs_beanie_green","H_Booniehat_oli","H_Cap_oli","H_Bandanna_khk"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["PSZ_H_wz2005_WDL","PSZ_H_wz2005_WDL_ESS","PSZ_H_wz2005_OLIVE","PSZ_H_wz2005_OLIVE_ESS"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		_crewmanHelmetArray = ["rhsusf_cvc_green_helmet","rhsusf_cvc_green_ess","rhsusf_cvc_green_helmet","rhsusf_cvc_green_ess","rhsusf_cvc_green_helmet"];	
		_crewmanHRandom = (floor(random (count _crewmanHelmetArray))); 
		_crewmanHelmet = _crewmanHelmetArray select _crewmanHRandom; // enter single string value to remove randommess
		
		// ===================== Vests ====================
		
		_plebVest = "PSZ_V_UKO_H_WDL_R_Headset";
		_gunnerVest = "PSZ_V_UKO_H_WDL_MG_Headset";
		_glVest = "PSZ_V_UKO_H_WDL_CO_Headset";
		_medVest = "PSZ_V_UKO_H_WDL_M_Headset";
		_pilotVest = "PSZ_V_UKO_L_WDL";
		_crewVest = "PSZ_V_UKO_L_WDL";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "PSZ_B_wz93_WDL";
		_largeRuck = "PSZ_B_wz97_WDL";
		
		// ==================== SpecOps ===================
		
		_specUniform = "PSZ_U_PL_WDL_wz2010_Crye_Folded";
		_specHelmet = "PSZ_H_wz2005_WDL_ESS";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "PSZ_V_UKO_H_WDL_MG_Headset";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
};

// =======================================================================
// ========================= Non-Camo Specific ===========================
// =======================================================================

// ================== Vehicle Crews ==================

_fpilotUniform = "U_B_PilotCoveralls";
_rotaryPilotHelmet = "rhsusf_hgu56p";
_rotaryCrewHelmet = "rhsusf_hgu56p_mask";
_fixedPilotHelmet = "H_PilotHelmetFighter_B";

// =================== Sniper Team ===================

_sniperUniform = "U_B_GhillieSuit";
_sniperVest = "V_TacVest_oli";
_sniperRuck = "B_AssaultPack_rgr";

// =================== Diver Gear ===================

_diverUniform = "U_B_Wetsuit";
_diverVest = "V_RebreatherB";
_diverRuck = "B_AssaultPack_blk";
_divingGoggles = "G_B_Diving";

// =================== Radio Rucks ===================

switch (_radioSelection) do {
	case "detection" : {
		private "_side";
		_side = toLower format ["%1", side player];
		// BLUFOR UAV
		if (_side == "west") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
		// OPFOR UAV
		if (_side == "east") then { 
			_uavRuck = "O_UAV_01_backpack_F";
			_uavTool = "O_UavTerminal";
		};
		// INDFOR UAV
		if (_side == "guer") then { 
			_uavRuck = "I_UAV_01_backpack_F";
			_uavTool = "I_UavTerminal";
		};
		// Civilian UAV
		if (_side == "civ") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
	};
	default {
		_uavRuck = "B_UAV_01_backpack_F";
		_uavTool = "B_UavTerminal";
	};
};

// =======================================================================