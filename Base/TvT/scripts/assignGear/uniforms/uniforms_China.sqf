// AssignGear - Uniforms
// by Mr. Agnet
// - Defines all of the wearable items to be used by the assignGear script.
// - Current Side, Faction: OPFOR, CHINA
// - Primary Mod: KMNP
// - Variables: _camoPattern - "universal", "arid", "wdl"
//				_headgear - "helmets", "softcover"

// =======================================================================
// Declares variables

private [
"_plebUniformArray","_plebRandom","_plebUniform",
"_plebHelmetArray","_plebHRandom","_plebHelmet",
"_plebVest","_gunnerVest","_glVest","_medVest",
"_smallRuck","_medRuck","_largeRuck",
"_specUniform","_specHelmet","_specGoggles","_specVest","_specRuck",
"_crewUniform","_rpilotUniform","_fpilotUniform",
"_crewmanHelmetArray","_crewmanHRandom","_crewmanHelmet","_rotaryPilotHelmet","_rotaryCrewHelmet","_fixedPilotHelmet",
"_crewVest","_pilotVest",
"_sniperUniform","_sniperVest","_sniperRuck",
"_diverUniform","_diverVest","_diverRuck",
"_goggles","_insignia","_divingGoggles",
"_airRadioRuck","_radioRuck","_diverRadioRuck","_uavRuck","_uavTool"
];

// =======================================================================
// =========================== Camo Specific =============================
// =======================================================================

_goggles = "";	// leave as "" for player defined
_insignia = ""; 

switch (_camoPattern) do {
	case "universal" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["MNP_CombatUniform_China","MNP_CombatUniform_China","MNP_CombatUniform_China"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_China";
		_rpilotUniform = "MNP_CombatUniform_China";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_PAGST_CN","MNP_Helmet_PAGST_CN","MNP_Helmet_PAGST_CN"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["MNP_Boonie_CN_T"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_PAGST_CN","MNP_Helmet_PAGST_CN","MNP_Helmet_PAGST_CN"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_ChinaH_T";
		_gunnerVest = "MNP_Vest_ChinaH_T2";
		_glVest = "MNP_Vest_ChinaH_T2";
		_medVest = "MNP_Vest_ChinaH_T";
		
		// ===================== Rucks ====================
		
		_smallRuck = "MNP_B_FieldPack_PLA_Basic";
		_medRuck = "MNP_B_ACU_KB";
		_largeRuck = "MNP_B_Carryall_PLA_Basic";
		
		// ===================== SpecOps ==================
		
		_specUniform = "MNP_CombatUniform_China";
		_specHelmet = "H_HelmetSpecO_blk";
		_specGoggles = "G_Balaclava_oli";
		_specVest = "MNP_Vest_ChinaH_T2";
		_specRuck = "MNP_B_FieldPack_PLA_Basic";
		
		// ================================================
	};
	case "arid" : {
		// ==================== Uniforms ==================

		_plebUniformArray = ["MNP_CombatUniform_China_D","MNP_CombatUniform_China_D","MNP_CombatUniform_China_D"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_China_D";
		_rpilotUniform = "MNP_CombatUniform_China_D";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Boonie_CN_D","MNP_Boonie_CN_D","MNP_Boonie_CN_D"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["MNP_Boonie_CN_D"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Boonie_CN_D","MNP_Boonie_CN_D","MNP_Boonie_CN_D"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_ChinaH_D";
		_gunnerVest = "MNP_Vest_ChinaH_D2";
		_glVest = "MNP_Vest_ChinaH_D2";
		_medVest = "MNP_Vest_ChinaH_D";
		
		// ===================== Rucks ====================
		
		_smallRuck = "MNP_B_FieldPack_PLA_Basic_D";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "MNP_B_Carryall_PLA_Basic_D";
		
		// ==================== SpecOps ===================
		
		_specUniform = "MNP_CombatUniform_China_D";
		_specHelmet = "MNP_Boonie_CN_D";
		_specGoggles = "G_Balaclava_blk";
		_specVest = "MNP_Vest_ChinaH_D2";
		_specRuck = "B_FieldPack_ocamo";
		
		// ================================================
	};
	case "wdl" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["MNP_CombatUniform_China_J","MNP_CombatUniform_China_J","MNP_CombatUniform_China_J"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_China_J";
		_rpilotUniform = "MNP_CombatUniform_China_J";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_PAGST_CNJ","MNP_Helmet_PAGST_CNJ","MNP_Helmet_PAGST_CNJ"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["MNP_Boonie_CN_T"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_PAGST_CNJ","MNP_Helmet_PAGST_CNJ","MNP_Helmet_PAGST_CNJ"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_ChinaH_J";
		_gunnerVest = "MNP_Vest_ChinaH_J2";
		_glVest = "MNP_Vest_ChinaH_J2";
		_medVest = "MNP_Vest_ChinaH_J";
		_crewVest = "V_TacVest_oli";
		_pilotVest = "V_TacVest_oli";
		
		// ===================== Rucks ====================
		
		_smallRuck = "MNP_B_FieldPack_PLA_Basic_T";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "MNP_B_Carryall_PLA_Basic_T";
		
		// ===================== SpecOps ==================
		
		_specUniform = "MNP_CombatUniform_China_J";
		_specHelmet = "MNP_Helmet_PAGST_CNJ";
		_specGoggles = "G_Balaclava_oli";
		_specVest = "MNP_Vest_ChinaH_J2";
		_specRuck = "MNP_B_FieldPack_PLA_Basic_T";
		
		// ================================================
	};
	default {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["MNP_CombatUniform_China","MNP_CombatUniform_China","MNP_CombatUniform_China"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_China";
		_rpilotUniform = "MNP_CombatUniform_China";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_PAGST_CN","MNP_Helmet_PAGST_CN","MNP_Helmet_PAGST_CN"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["MNP_Boonie_CN_T"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_PAGST_CN","MNP_Helmet_PAGST_CN","MNP_Helmet_PAGST_CN"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_ChinaH_T";
		_gunnerVest = "MNP_Vest_ChinaH_T2";
		_glVest = "MNP_Vest_ChinaH_T2";
		_medVest = "MNP_Vest_ChinaH_T";
		
		// ===================== Rucks ====================
		
		_smallRuck = "MNP_B_FieldPack_PLA_Basic";
		_medRuck = "MNP_B_ACU_KB";
		_largeRuck = "MNP_B_Carryall_PLA_Basic";
		
		// ===================== SpecOps ==================
		
		_specUniform = "MNP_CombatUniform_China";
		_specHelmet = "H_HelmetSpecO_blk";
		_specGoggles = "G_Balaclava_oli";
		_specVest = "MNP_Vest_ChinaH_T2";
		_specRuck = "MNP_B_FieldPack_PLA_Basic";
		
		// ================================================
	};
};

// =======================================================================
// ========================= Non-Camo Specific ===========================
// =======================================================================

// ================== Vehicle Crews ==================

_fpilotUniform = "U_O_PilotCoveralls";
_crewmanHelmetArray = ["rhs_tsh4","rhs_tsh4_ess","rhs_tsh4","rhs_tsh4_bala","rhs_tsh4","rhs_tsh4_ess_bala","rhs_tsh4"];	
_crewmanHRandom = (floor(random (count _crewmanHelmetArray))); 
_crewmanHelmet = _crewmanHelmetArray select _crewmanHRandom; // enter single string value to remove randommess
_rotaryPilotHelmet = "H_PilotHelmetHeli_O";
_rotaryCrewHelmet = "H_CrewHelmetHeli_O";
_fixedPilotHelmet = "H_PilotHelmetFighter_O";

// =================== Sniper Team ===================

_sniperUniform = "U_O_GhillieSuit";
_sniperVest = "V_TacVest_khk";
_sniperRuck = "B_FieldPack_ocamo";

// =================== Diver Gear ===================

_diverUniform = "U_O_Wetsuit";
_diverVest = "V_RebreatherB";
_diverRuck = "B_AssaultPack_blk";
_divingGoggles = "G_O_Diving";

// =================== Radio Rucks ===================

switch (_radioSelection) do {
	case "detection" : {
		private "_side";
		_side = toLower format ["%1", side player];
		// BLUFOR UAV
		if (_side == "west") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
		// OPFOR UAV
		if (_side == "east") then { 
			_uavRuck = "O_UAV_01_backpack_F";
			_uavTool = "O_UavTerminal";
		};
		// INDFOR UAV
		if (_side == "guer") then { 
			_uavRuck = "I_UAV_01_backpack_F";
			_uavTool = "I_UavTerminal";
		};
		// Civilian UAV
		if (_side == "civ") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
	};
	default {
		_uavRuck = "B_UAV_01_backpack_F";
		_uavTool = "B_UavTerminal";
	};
};

// =======================================================================