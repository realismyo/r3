// AssignGear - Uniforms
// by Mr. Agnet
// - Defines all of the wearable items to be used by the assignGear script.
// - Current Side, Faction: INDFOR, AAF

// ============================================================
// Declares variables

private [
"_plebUniformArray","_plebRandom","_plebUniform",
"_plebHelmetArray","_plebHRandom","_plebHelmet",
"_plebVest","_gunnerVest","_glVest","_medVest",
"_smallRuck","_medRuck","_largeRuck",
"_specUniform","_specHelmet","_specGoggles","_specVest","_specRuck",
"_crewUniform","_rpilotUniform","_fpilotUniform",
"_crewmanHelmetArray","_crewmanHRandom","_crewmanHelmet","_rotaryPilotHelmet","_rotaryCrewHelmet","_fixedPilotHelmet",
"_crewVest","_pilotVest",
"_sniperUniform","_sniperVest","_sniperRuck",
"_diverUniform","_diverVest","_diverRuck",
"_goggles","_insignia","_divingGoggles",
"_airRadioRuck","_radioRuck","_diverRadioRuck","_uavRuck","_uavTool"
];

// ===================== Uniforms ======================

_goggles = "";	// leave as "" for player defined
_insignia = ""; 
_plebUniformArray = ["U_I_CombatUniform","U_I_CombatUniform_shortsleeve","U_I_CombatUniform"];	
_plebRandom = (floor(random (count _plebUniformArray)));
_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default (what was spawned with), or enter single string value to remove randommess

// ===================== Headgear ======================

switch (_headgear) do {
	case "helmets" : {
		_plebHelmetArray = ["H_HelmetIA_net","H_HelmetIA"];	
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};
	case "softcover" : {
		_plebHelmetArray = ["H_Booniehat_dgtl","H_Booniehat_oli","H_Watchcap_camo","H_Cap_blk_Raven","H_MilCap_dgtl","H_Cap_headphones"];	
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};
	default {
		_plebHelmetArray = ["H_HelmetIA_net","H_HelmetIA"];	
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};
};

// ======================= Vests =======================

_plebVest = "V_PlateCarrierIA1_dgtl";
_gunnerVest = "V_PlateCarrierIAGL_dgtl";
_glVest = "V_PlateCarrierIAGL_dgtl";
_medVest = "V_PlateCarrierIA1_dgtl";

// ======================= Rucks =======================

_smallRuck = "B_AssaultPack_dgtl";
_medRuck = "B_Kitbag_rgr";
_largeRuck = "B_Carryall_oli";

// ===================== Spec Ops ======================

_specUniform = "U_I_CombatUniform_shortsleeve";
_specHelmet = "H_HelmetIA";
_specGoggles = "G_Bandanna_oli";
_specVest = "V_PlateCarrierIAGL_dgtl";
_specRuck = "B_AssaultPack_dgtl";

// =================== Vehicle Crews ===================

_crewUniform = "U_I_CombatUniform_shortsleeve";
_rpilotUniform = "U_I_HeliPilotCoveralls";
_fpilotUniform = "U_I_pilotCoveralls";

_crewmanHelmet = "H_HelmetCrew_I";
_rotaryPilotHelmet = "H_PilotHelmetHeli_I";
_rotaryCrewHelmet = "H_CrewHelmetHeli_I";
_fixedPilotHelmet = "H_PilotHelmetFighter_I";

_crewVest = "V_TacVest_oli";
_pilotVest = "V_TacVest_khk";

// ==================== Sniper Team ====================

_sniperUniform = "U_I_GhillieSuit";
_sniperVest = "V_TacVest_oli";
_sniperRuck = "B_AssaultPack_dgtl";

// ==================== Diver Gear ====================

_diverUniform = "U_I_Wetsuit";
_diverVest = "V_RebreatherB";
_diverRuck = "B_AssaultPack_blk";
_divingGoggles = "G_I_Diving";

// ==================== Radio Rucks ====================

switch (_radioSelection) do {
	case "detection" : {
		private "_side";
		_side = toLower format ["%1", side player];
		// BLUFOR UAV
		if (_side == "west") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
		// OPFOR UAV
		if (_side == "east") then { 
			_uavRuck = "O_UAV_01_backpack_F";
			_uavTool = "O_UavTerminal";
		};
		// INDFOR UAV
		if (_side == "guer") then { 
			_uavRuck = "I_UAV_01_backpack_F";
			_uavTool = "I_UavTerminal";
		};
		// Civilian UAV
		if (_side == "civ") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
	};
	default {
		_uavRuck = "B_UAV_01_backpack_F";
		_uavTool = "B_UavTerminal";
	};
};

// =====================================================
