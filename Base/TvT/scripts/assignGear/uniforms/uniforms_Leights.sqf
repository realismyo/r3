// AssignGear - Uniforms
// by Mr. Agnet
// - Defines all of the wearable items to be used by the assignGear script.
// - Current Side, Faction: INDFOR, LEIGHTS
// - Primary Mod: RHS/Leights
// - Variables: _camoPattern - "afghanarmy", "cdf", "racs", "iraqiarmy", "sla"
//				_headgear - "helmets", "softcover"

// =======================================================================
// Declares variables

private [
"_plebUniformArray","_plebRandom","_plebUniform",
"_plebHelmetArray","_plebHRandom","_plebHelmet",
"_plebVest","_gunnerVest","_glVest","_medVest",
"_smallRuck","_medRuck","_largeRuck",
"_specUniform","_specHelmet","_specGoggles","_specVest","_specRuck",
"_crewUniform","_rpilotUniform","_fpilotUniform",
"_crewmanHelmetArray","_crewmanHRandom","_crewmanHelmet","_crewmanHelmetArray","_crewmanHRandom","_rotaryPilotHelmet","_rotaryCrewHelmet","_fixedPilotHelmet",
"_crewVest","_pilotVest",
"_sniperUniform","_sniperVest","_sniperRuck",
"_diverUniform","_diverVest","_diverRuck",
"_goggles","_insignia","_divingGoggles",
"_airRadioRuck","_radioRuck","_diverRadioRuck","_uavRuck","_uavTool"
];

// =======================================================================
// =========================== Camo Specific =============================
// =======================================================================

_goggles = "";	// leave as "" for player defined
_insignia = ""; 

switch (_camoPattern) do {
	case "afghanarmy" : {
		// ==================== Uniforms ==================

		_plebUniformArray = ["LOP_U_AA_Fatigue_01_slv","LOP_U_AA_Fatigue_01","LOP_U_AA_Fatigue_02_slv","LOP_U_AA_Fatigue_02"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "LOP_U_AA_Fatigue_02_slv";
		_rpilotUniform = "LOP_U_AA_Fatigue_02_slv";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmet" : {
				_plebHelmetArray = ["LOP_H_6B27M_ANA","rhs_6b27m_green_ess","LOP_H_6B27M_ANA","LOP_H_6B27M_ess_ANA","rhs_6b27m_green","LOP_H_6B27M_ess_ANA"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Booniehat_oli","H_Bandanna_gry","rhs_beanie_green","H_Bandanna_sgg"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["LOP_H_6B27M_ANA","rhs_6b27m_green_ess","LOP_H_6B27M_ANA","LOP_H_6B27M_ess_ANA","rhs_6b27m_green","LOP_H_6B27M_ess_ANA"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};	
		
		// ===================== Vests ====================
		
		_plebVest = "LOP_V_6B23_6Sh92_OLV";
		_gunnerVest = "LOP_V_6B23_6Sh92_OLV";
		_glVest = "LOP_V_6B23_6Sh92_OLV";
		_medVest = "LOP_V_6B23_Medic_OLV";
		_pilotVest = "LOP_V_6B23_CrewOfficer_OLV";
		_crewVest = "LOP_V_6B23_CrewOfficer_OLV";
		
		// ===================== Rucks ====================
		
		_smallRuck = "rhs_assault_umbts";
		_medRuck = "B_TacticalPack_oli";
		_largeRuck = "B_Carryall_oli";
		
		// ==================== SpecOps ===================
		
		_specUniform = "LOP_U_AA_Fatigue_02_slv";
		_specHelmet = "LOP_H_6B27M_ess_ANA";
		_specGoggles = "rhs_scarf";
		_specVest = "LOP_V_6B23_CrewOfficer_OLV";
		_specRuck = "rhs_assault_umbts";
		
		// ================================================
	};
	case "cdf" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["LOP_U_CDF_Fatigue_01"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "LOP_U_CDF_Fatigue_01";
		_rpilotUniform = "LOP_U_CDF_Fatigue_01";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmet" : {
				_plebHelmetArray = ["LOP_H_6B27M_ess_CDF","LOP_H_6B27M_CDF","LOP_H_6B27M_ess_CDF"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Booniehat_oli","H_Bandanna_gry","rhs_beanie_green","H_Bandanna_sgg"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["LOP_H_6B27M_ess_CDF","LOP_H_6B27M_CDF","LOP_H_6B27M_ess_CDF"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};	
		
		// ===================== Vests ====================
		
		_plebVest = "LOP_V_6B23_6Sh92_CDF";
		_gunnerVest = "LOP_V_6B23_6Sh92_CDF";
		_glVest = "LOP_V_6B23_6Sh92_CDF";
		_medVest = "LOP_V_6B23_Medic_CDF";
		_pilotVest = "LOP_V_6B23_CrewOfficer_OLV";
		_crewVest = "LOP_V_6B23_CrewOfficer_OLV";
		
		// ===================== Rucks ====================
		
		_smallRuck = "rhs_assault_umbts";
		_medRuck = "B_TacticalPack_oli";
		_largeRuck = "B_Carryall_oli";
		
		// ===================== SpecOps ==================
		
		_specUniform = "LOP_U_CDF_Fatigue_01";
		_specHelmet = "LOP_H_6B27M_ess_CDF";
		_specGoggles = "rhs_scarf";
		_specVest = "LOP_V_6B23_CrewOfficer_CDF";
		_specRuck = "rhs_assault_umbts";
		
		// ================================================
	};
	case "racs" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["LOP_U_RACS_Fatigue_01","LOP_U_RACS_Fatigue_01_slv"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "LOP_U_RACS_Fatigue_01_slv";
		_rpilotUniform = "LOP_U_RACS_Fatigue_01_slv";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmet" : {
				_plebHelmetArray = ["LOP_H_6B27M_ess_RACS","LOP_H_6B27M_RACS","LOP_H_6B27M_ess_RACS"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Booniehat_khk","H_Cap_tan","H_Bandanna_cbr","LOP_H_Booniehat_RACS"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["LOP_H_6B27M_ess_RACS","LOP_H_6B27M_RACS","LOP_H_6B27M_ess_RACS"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};	
		
		// ===================== Vests ====================
		
		_plebVest = "LOP_V_CarrierLite_TAN";
		_gunnerVest = "LOP_V_CarrierRig_TAN";
		_glVest = "LOP_V_CarrierRig_TAN";
		_medVest = "LOP_V_CarrierRig_TAN";
		_pilotVest = "LOP_V_6B23_CrewOfficer_OLV";
		_crewVest = "LOP_V_6B23_CrewOfficer_OLV";
		
		// ===================== Rucks ====================
		
		_smallRuck = "rhs_sidor";
		_medRuck = "B_Kitbag_cbr";
		_largeRuck = "B_Carryall_cbr";
		
		// ===================== SpecOps ==================
		
		_specUniform = "LOP_U_RACS_Fatigue_01_slv";
		_specHelmet = "LOP_H_6B27M_ess_RACS";
		_specGoggles = "rhs_scarf";
		_specVest = "LOP_V_CarrierRig_TAN";
		_specRuck = "rhs_sidor";
		
		// ================================================
	};
	case "iraqiarmy" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["LOP_U_IA_Fatigue_01","LOP_U_IA_Fatigue_01_slv"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "LOP_U_IA_Fatigue_01_slv";
		_rpilotUniform = "LOP_U_IA_Fatigue_01_slv";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmet" : {
				_plebHelmetArray = ["LOP_H_6B27M_ess_TRI","LOP_H_6B27M_TRI","LOP_H_6B27M_ess_TRI"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Booniehat_khk","H_Cap_tan","H_Bandanna_cbr"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["LOP_H_6B27M_ess_TRI","LOP_H_6B27M_TRI","LOP_H_6B27M_ess_TRI"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};	
		
		// ===================== Vests ====================
		
		_plebVest = "LOP_V_CarrierLite_TRI";
		_gunnerVest = "LOP_V_CarrierRig_TRI";
		_glVest = "LOP_V_CarrierRig_TRI";
		_medVest = "LOP_V_CarrierRig_TRI";
		_pilotVest = "LOP_V_6B23_CrewOfficer_OLV";
		_crewVest = "LOP_V_6B23_CrewOfficer_OLV";
		
		// ===================== Rucks ====================
		
		_smallRuck = "rhs_sidor";
		_medRuck = "B_Kitbag_cbr";
		_largeRuck = "B_Carryall_cbr";
		
		// ===================== SpecOps ==================
		
		_specUniform = "LOP_U_IA_Fatigue_01_slv";
		_specHelmet = "LOP_H_6B27M_ess_TRI";
		_specGoggles = "rhs_scarf";
		_specVest = "LOP_V_CarrierRig_TRI";
		_specRuck = "rhs_sidor";
		
		// ================================================
	};
	case "sla" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["LOP_U_SLA_Fatigue_01","LOP_U_SLA_Fatigue_01"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "LOP_U_SLA_Fatigue_01";
		_rpilotUniform = "LOP_U_SLA_Fatigue_01";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmet" : {
				_plebHelmetArray = ["rhs_6b27m_green_ess","rhs_6b27m_green","rhs_6b27m_green_ess"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {  
				_plebHelmetArray = ["H_Booniehat_oli","H_Bandanna_gry","rhs_beanie_green","H_Bandanna_sgg"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["rhs_6b27m_green_ess","rhs_6b27m_green","rhs_6b27m_green_ess"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};	
		
		// ===================== Vests ====================
		
		_plebVest = "LOP_V_6B23_6Sh92_OLV";
		_gunnerVest = "LOP_V_6B23_6Sh92_OLV";
		_glVest = "LOP_V_6B23_6Sh92_OLV";
		_medVest = "LOP_V_6B23_Medic_OLV";
		_pilotVest = "LOP_V_6B23_CrewOfficer_OLV";
		_crewVest = "LOP_V_6B23_CrewOfficer_OLV";
		
		// ===================== Rucks ====================
		
		_smallRuck = "rhs_sidor";
		_medRuck = "B_Kitbag_cbr";
		_largeRuck = "B_Carryall_cbr";
		
		// ===================== SpecOps ==================
		
		_specUniform = "LOP_U_SLA_Fatigue_01";
		_specHelmet = "rhs_6b27m_green_ess";
		_specGoggles = "rhs_scarf";
		_specVest = "LOP_V_6B23_CrewOfficer_OLV";
		_specRuck = "rhs_sidor";
		
		// ================================================
	};
	default {
		// ==================== Uniforms ==================

		_plebUniformArray = ["LOP_U_AA_Fatigue_01_slv","LOP_U_AA_Fatigue_01","LOP_U_AA_Fatigue_02_slv","LOP_U_AA_Fatigue_02"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "LOP_U_AA_Fatigue_02_slv";
		_rpilotUniform = "LOP_U_AA_Fatigue_02_slv";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmet" : {
				_plebHelmetArray = ["LOP_H_6B27M_ANA","rhs_6b27m_green_ess","LOP_H_6B27M_ANA","LOP_H_6B27M_ess_ANA","rhs_6b27m_green","LOP_H_6B27M_ess_ANA"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Booniehat_oli","H_Bandanna_gry","rhs_beanie_green","H_Bandanna_sgg"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["LOP_H_6B27M_ANA","rhs_6b27m_green_ess","LOP_H_6B27M_ANA","LOP_H_6B27M_ess_ANA","rhs_6b27m_green","LOP_H_6B27M_ess_ANA"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};	
		
		// ===================== Vests ====================
		
		_plebVest = "LOP_V_6B23_6Sh92_OLV";
		_gunnerVest = "LOP_V_6B23_6Sh92_OLV";
		_glVest = "LOP_V_6B23_6Sh92_OLV";
		_medVest = "LOP_V_6B23_Medic_OLV";
		_pilotVest = "LOP_V_6B23_CrewOfficer_OLV";
		_crewVest = "LOP_V_6B23_CrewOfficer_OLV";
		
		// ===================== Rucks ====================
		
		_smallRuck = "rhs_assault_umbts";
		_medRuck = "B_TacticalPack_oli";
		_largeRuck = "B_Carryall_oli";
		
		// ==================== SpecOps ===================
		
		_specUniform = "LOP_U_AA_Fatigue_02_slv";
		_specHelmet = "LOP_H_6B27M_ess_ANA";
		_specGoggles = "rhs_scarf";
		_specVest = "LOP_V_6B23_CrewOfficer_OLV";
		_specRuck = "rhs_assault_umbts";
		
		// ================================================
	};
};

// =======================================================================
// ========================= Non-Camo Specific ===========================
// =======================================================================

// ================== Vehicle Crews ==================

_rpilotUniform = "rhs_uniform_df15";
_fpilotUniform = "rhs_uniform_df15";
_rotaryPilotHelmet = "rhs_zsh7a_mike";
_rotaryCrewHelmet = "rhs_zsh7a_mike";
_fixedPilotHelmet = "rhs_zsh7a";
_crewmanHelmetArray = ["rhs_tsh4","rhs_tsh4_ess","rhs_tsh4","rhs_tsh4_bala","rhs_tsh4","rhs_tsh4_ess_bala","rhs_tsh4"];	
_crewmanHRandom = (floor(random (count _crewmanHelmetArray))); 
_crewmanHelmet = _crewmanHelmetArray select _crewmanHRandom; // enter single string value to remove randommess

// =================== Sniper Team ===================

_sniperUniform = "U_O_GhillieSuit";
_sniperVest = "V_TacVest_oli";
_sniperRuck = "rhs_assault_umbts";

// =================== Diver Gear ===================

_diverUniform = "U_O_Wetsuit";
_diverVest = "V_RebreatherB";
_diverRuck = "B_AssaultPack_blk";
_divingGoggles = "G_O_Diving";

// =================== Radio Rucks ===================

switch (_radioSelection) do {
	case "detection" : {
		private "_side";
		_side = toLower format ["%1", side player];
		// BLUFOR UAV
		if (_side == "west") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
		// OPFOR UAV
		if (_side == "east") then { 
			_uavRuck = "O_UAV_01_backpack_F";
			_uavTool = "O_UavTerminal";
		};
		// INDFOR UAV
		if (_side == "guer") then { 
			_uavRuck = "I_UAV_01_backpack_F";
			_uavTool = "I_UavTerminal";
		};
		// Civilian UAV
		if (_side == "civ") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
	};
	default {
		_uavRuck = "B_UAV_01_backpack_F";
		_uavTool = "B_UavTerminal";
	};
};

// =======================================================================