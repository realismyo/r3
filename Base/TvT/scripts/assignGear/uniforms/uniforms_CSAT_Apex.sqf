// AssignGear - Uniforms
// by Mr. Agnet
// - Defines all of the wearable items to be used by the assignGear script.
// - Current Side, Faction: OPFOR, CSAT
// - Primary Mod: VANILLA
// - Variables: _camoPattern - "arid", "urban"
//				_headgear - "helmets", "opshelm", "softcover"

// =======================================================================
// Declares variables

private [
"_plebUniformArray","_plebRandom","_plebUniform",
"_plebHelmetArray","_plebHRandom","_plebHelmet",
"_plebVest","_gunnerVest","_glVest","_medVest",
"_smallRuck","_medRuck","_largeRuck",
"_specUniform","_specHelmet","_specGoggles","_specVest","_specRuck",
"_crewUniform","_rpilotUniform","_fpilotUniform",
"_crewmanHelmetArray","_crewmanHRandom","_crewmanHelmet","_rotaryPilotHelmet","_rotaryCrewHelmet","_fixedPilotHelmet",
"_crewVest","_pilotVest",
"_sniperUniform","_sniperVest","_sniperRuck",
"_diverUniform","_diverVest","_diverRuck",
"_goggles","_insignia","_divingGoggles",
"_airRadioRuck","_radioRuck","_diverRadioRuck","_uavRuck","_uavTool"
];

// =======================================================================
// =========================== Camo Specific =============================
// =======================================================================

_goggles = "";	// leave as "" for player defined
_insignia = ""; 

// ==================== Uniforms ==================

_plebUniformArray = ["U_O_T_Soldier_F","U_O_T_Soldier_F","U_O_T_Soldier_F"];	
_plebRandom = (floor(random (count _plebUniformArray)));
_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
_crewUniform = "U_O_T_Soldier_F";

// ==================== Headgear ==================

switch (_headgear) do {
	case "helmets" : {
		_plebHelmetArray = ["H_HelmetO_ghex_F","H_HelmetLeaderO_ghex_F","H_HelmetO_ghex_F","H_HelmetLeaderO_ghex_F","H_HelmetO_ghex_F"];	
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};
	case "opshelm" : {
		_plebHelmetArray = ["H_HelmetSpecO_ghex_F","H_HelmetSpecO_ghex_F","H_HelmetSpecO_ghex_F"];	
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};
	case "softcover" : {
		_plebHelmetArray = ["H_MilCap_ocamo","H_Booniehat_oli","H_Watchcap_camo","H_Bandanna_sgg","H_Cap_oli"];	
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};
	default {
		_plebHelmetArray = ["H_HelmetO_ghex_F","H_HelmetLeaderO_ghex_F","H_HelmetO_ghex_F","H_HelmetLeaderO_ghex_F","H_HelmetO_ghex_F"];	
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};
};

// ===================== Vests ====================

_plebVest = "rhsgref_otv_khaki";
_gunnerVest = "rhsgref_otv_khaki";
_glVest = "rhsgref_otv_khaki";
_medVest = "rhsgref_otv_khaki";

// ===================== Rucks ====================

_smallRuck = "B_FieldPack_ghex_F";
_medRuck = "B_TacticalPack_oli";
_largeRuck = "B_Carryall_ghex_F";

// ==================== SpecOps ===================

_specUniform = "U_O_CombatUniform_ocamo";
_specHelmet = "H_HelmetSpecO_ghex_F";
_specGoggles = "G_Bandanna_oli";
_specVest = "rhsgref_otv_khaki";
_specRuck = "B_FieldPack_ghex_F";

// =======================================================================
// ========================= Non-Camo Specific ===========================
// =======================================================================

// ================== Vehicle Crews ==================

_rpilotUniform = "U_O_T_Soldier_F";
_fpilotUniform = "U_O_PilotCoveralls";

_crewmanHelmet = "H_HelmetCrew_O";
_rotaryPilotHelmet = "H_PilotHelmetHeli_O";
_rotaryCrewHelmet = "H_CrewHelmetHeli_O";
_fixedPilotHelmet = "H_PilotHelmetFighter_O";

_crewVest = "V_TacVest_oli";
_pilotVest = "V_TacVest_oli";

// =================== Sniper Team ===================

_sniperUniform = "U_O_GhillieSuit";
_sniperVest = "V_TacVest_oli";
_sniperRuck = "B_FieldPack_ghex_F";

// =================== Diver Gear ===================

_diverUniform = "U_O_Wetsuit";
_diverVest = "V_RebreatherB";
_diverRuck = "B_AssaultPack_blk";
_divingGoggles = "G_O_Diving";

// =================== Radio Rucks ===================

switch (_radioSelection) do {
	case "detection" : {
		private "_side";
		_side = toLower format ["%1", side player];
		// BLUFOR UAV
		if (_side == "west") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
		// OPFOR UAV
		if (_side == "east") then { 
			_uavRuck = "O_UAV_01_backpack_F";
			_uavTool = "O_UavTerminal";
		};
		// INDFOR UAV
		if (_side == "guer") then { 
			_uavRuck = "I_UAV_01_backpack_F";
			_uavTool = "I_UavTerminal";
		};
		// Civilian UAV
		if (_side == "civ") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
	};
	default {
		_uavRuck = "B_UAV_01_backpack_F";
		_uavTool = "B_UavTerminal";
	};
};

// =======================================================================