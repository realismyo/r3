// AssignGear - Uniforms
// by Mr. Agnet
// - Defines all of the wearable items to be used by the assignGear script.
// - Current Side, Faction: INDFOR, MID EAST
// - Primary Mod: KMNP
// - Variables: _camoPattern - "irgc", "irgcbasij", "israelidrab" 
//				_headgear - "helmets", "softcover"

// =======================================================================
// Declares variables

private [
"_plebUniformArray","_plebRandom","_plebUniform",
"_plebHelmetArray","_plebHRandom","_plebHelmet",
"_plebVest","_gunnerVest","_glVest","_medVest",
"_smallRuck","_medRuck","_largeRuck",
"_specUniform","_specHelmet","_specGoggles","_specVest","_specRuck",
"_crewUniform","_rpilotUniform","_fpilotUniform",
"_crewmanHelmetArray","_crewmanHRandom","_crewmanHelmet","_rotaryPilotHelmet","_rotaryCrewHelmet","_fixedPilotHelmet",
"_crewVest","_pilotVest",
"_sniperUniform","_sniperVest","_sniperRuck",
"_diverUniform","_diverVest","_diverRuck",
"_goggles","_insignia","_divingGoggles",
"_airRadioRuck","_radioRuck","_diverRadioRuck","_uavRuck","_uavTool"
];

// =======================================================================
// =========================== Camo Specific =============================
// =======================================================================

_goggles = "";	// leave as "" for player defined
_insignia = ""; 

switch (_camoPattern) do {
	case "irgc" : {
		// ==================== Uniforms ==================

		_plebUniformArray = ["MNP_CombatUniform_IR_IRGC_A"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_IR_IRGC_A";
		_rpilotUniform = "MNP_CombatUniform_IR_IRGC_A";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_PAGST_IRGC","MNP_Helmet_PAGST_IRGC","MNP_Helmet_PAGST_IRGC"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Booniehat_khk"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_PAGST_IRGC","MNP_Helmet_PAGST_IRGC","MNP_Helmet_PAGST_IRGC"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_NZ_1";
		_gunnerVest = "MNP_Vest_NZ_2";
		_glVest = "MNP_Vest_NZ_2";
		_medVest = "MNP_Vest_NZ_1";
		_crewVest = "V_TacVest_khk";
		_pilotVest = "V_TacVest_khk";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_cbr";
		_largeRuck = "B_Carryall_cbr";
		
		// ==================== SpecOps ===================
		
		_specUniform = "MNP_CombatUniform_IR_IRGC_A";
		_specHelmet = "MNP_Helmet_PAGST_IRGC";
		_specGoggles = "G_Bandanna_khk";
		_specVest = "MNP_Vest_NZ_2";
		_specRuck = "MNP_B_RU2_FP";
		
		// ================================================
	};
	case "irgcbasij" : {
		// ==================== Uniforms ==================

		_plebUniformArray = ["MNP_CombatUniform_IR_BSJ_A"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_IR_BSJ_A";
		_rpilotUniform = "MNP_CombatUniform_IR_BSJ_A";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_PAGST_IRGC","MNP_Helmet_PAGST_IRGC","MNP_Helmet_PAGST_IRGC"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Booniehat_oli"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_PAGST_IRGC","MNP_Helmet_PAGST_IRGC","MNP_Helmet_PAGST_IRGC"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_Olive_1";
		_gunnerVest = "MNP_Vest_Olive_2";
		_glVest = "MNP_Vest_Olive_2";
		_medVest = "MNP_Vest_Olive_1";
		_crewVest = "V_TacVest_oli";
		_pilotVest = "V_TacVest_oli";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_oli";
		
		// ==================== SpecOps ===================
		
		_specUniform = "MNP_CombatUniform_IR_BSJ_A";
		_specHelmet = "MNP_Helmet_PAGST_IRGC";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "MNP_Vest_Olive_2";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
	case "iraelidrab" : {
		// ==================== Uniforms ==================

		_plebUniformArray = ["MNP_CombatUniform_ISR"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_ISR";
		_rpilotUniform = "MNP_CombatUniform_ISR";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_OD","MNP_Helmet_OD","MNP_Helmet_OD"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Booniehat_oli"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_OD","MNP_Helmet_OD","MNP_Helmet_OD"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_ISRKahki_2";
		_gunnerVest = "MNP_Vest_ISRKahki_1";
		_glVest = "MNP_Vest_ISRKahki_1";
		_medVest = "MNP_Vest_ISRKahki_2";
		_crewVest = "V_TacVest_oli";
		_pilotVest = "V_TacVest_oli";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_oli";
		
		// ==================== SpecOps ===================
		
		_specUniform = "MNP_CombatUniform_ISR";
		_specHelmet = "MNP_Helmet_OD";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "MNP_Vest_ISRKahki_1";
		_specRuck = "MNP_B_RU2_FP";
		
		// ================================================
	};
	default {
		// ==================== Uniforms ==================

		_plebUniformArray = ["MNP_CombatUniform_IR_IRGC_A"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_IR_IRGC_A";
		_rpilotUniform = "MNP_CombatUniform_IR_IRGC_A";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_PAGST_IRGC","MNP_Helmet_PAGST_IRGC","MNP_Helmet_PAGST_IRGC"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Booniehat_khk"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_PAGST_IRGC","MNP_Helmet_PAGST_IRGC","MNP_Helmet_PAGST_IRGC"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_NZ_1";
		_gunnerVest = "MNP_Vest_NZ_2";
		_glVest = "MNP_Vest_NZ_2";
		_medVest = "MNP_Vest_NZ_1";
		_crewVest = "V_TacVest_khk";
		_pilotVest = "V_TacVest_khk";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_cbr";
		_largeRuck = "B_Carryall_cbr";
		
		// ==================== SpecOps ===================
		
		_specUniform = "MNP_CombatUniform_IR_IRGC_A";
		_specHelmet = "MNP_Helmet_PAGST_IRGC";
		_specGoggles = "G_Bandanna_khk";
		_specVest = "MNP_Vest_NZ_2";
		_specRuck = "MNP_B_RU2_FP";
		
		// ================================================
	};
};

// =======================================================================
// ========================= Non-Camo Specific ===========================
// =======================================================================

// ================== Vehicle Crews ==================

_fpilotUniform = "U_I_PilotCoveralls";
_crewmanHelmetArray = ["rhs_tsh4","rhs_tsh4_ess","rhs_tsh4","rhs_tsh4_bala","rhs_tsh4","rhs_tsh4_ess_bala","rhs_tsh4"];	
_crewmanHRandom = (floor(random (count _crewmanHelmetArray))); 
_crewmanHelmet = _crewmanHelmetArray select _crewmanHRandom; // enter single string value to remove randommess
_rotaryPilotHelmet = "H_PilotHelmetHeli_B";
_rotaryCrewHelmet = "H_CrewHelmetHeli_B";
_fixedPilotHelmet = "H_PilotHelmetFighter_I";

// =================== Sniper Team ===================

_sniperUniform = "U_I_GhillieSuit";
_sniperVest = "V_TacVest_oli";
_sniperRuck = "B_AssaultPack_rgr";

// =================== Diver Gear ===================

_diverUniform = "U_I_Wetsuit";
_diverVest = "V_RebreatherB";
_diverRuck = "B_AssaultPack_blk";
_divingGoggles = "G_I_Diving";

// =================== Radio Rucks ===================

switch (_radioSelection) do {
	case "detection" : {
		private "_side";
		_side = toLower format ["%1", side player];
		// BLUFOR UAV
		if (_side == "west") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
		// OPFOR UAV
		if (_side == "east") then { 
			_uavRuck = "O_UAV_01_backpack_F";
			_uavTool = "O_UavTerminal";
		};
		// INDFOR UAV
		if (_side == "guer") then { 
			_uavRuck = "I_UAV_01_backpack_F";
			_uavTool = "I_UavTerminal";
		};
		// Civilian UAV
		if (_side == "civ") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
	};
	default {
		_uavRuck = "B_UAV_01_backpack_F";
		_uavTool = "B_UavTerminal";
	};
};

// =======================================================================