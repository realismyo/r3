// AssignGear - Uniforms
// by Mr. Agnet
// - Defines all of the wearable items to be used by the assignGear script.
// - Current Side, Faction: OPFOR, AFRF
// - Primary Mod: RHS
// - Variables: _camoPattern - "universal", "arid", "wdl"
//				_headgear - "helmets", "softcover"

// =======================================================================
// Declares variables

private [
"_plebUniformArray","_plebRandom","_plebUniform",
"_plebHelmetArray","_plebHRandom","_plebHelmet",
"_plebVest","_gunnerVest","_glVest","_medVest",
"_smallRuck","_medRuck","_largeRuck",
"_specUniform","_specHelmet","_specGoggles","_specVest","_specRuck",
"_crewUniform","_rpilotUniform","_fpilotUniform",
"_crewmanHelmetArray","_crewmanHRandom","_crewmanHelmet","_crewmanHelmetArray","_crewmanHRandom","_rotaryPilotHelmet","_rotaryCrewHelmet","_fixedPilotHelmet",
"_crewVest","_pilotVest",
"_sniperUniform","_sniperVest","_sniperRuck",
"_diverUniform","_diverVest","_diverRuck",
"_goggles","_insignia","_divingGoggles",
"_airRadioRuck","_radioRuck","_diverRadioRuck","_uavRuck","_uavTool"
];

// =======================================================================
// =========================== Camo Specific =============================
// =======================================================================

_goggles = "";	// leave as "" for player defined
_insignia = ""; 

switch (_camoPattern) do {
	case "emr" : {
		// ==================== Uniforms ==================

		_plebUniformArray = ["rhs_uniform_emr_patchless"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "rhs_uniform_emr_patchless";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "6b26" : {
				_plebHelmetArray = ["rhs_6b26_green","rhs_6b26_green_bala","rhs_6b26_green_ess","rhs_6b26_green_ess_bala","rhs_6b26_green_ess","rhs_6b26_green"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "6b27" : {
				_plebHelmetArray = ["rhs_6b27m_digi","rhs_6b27m_digi_bala","rhs_6b27m_digi_ess","rhs_6b27m_digi_ess_bala","rhs_6b27m_digi_ess","rhs_6b27m_digi"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "6b28" : {
				_plebHelmetArray = ["rhs_6b28","rhs_6b28_bala","rhs_6b28_ess","rhs_6b28_ess_bala","rhs_6b28_ess","rhs_6b28"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Bandanna_sgg","rhs_beanie_green","rhs_fieldcap_digi","rhs_Booniehat_digi","rhs_beanie_green","rhs_Booniehat_digi"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["rhs_6b27m_digi","rhs_6b27m_digi_bala","rhs_6b27m_digi_ess","rhs_6b27m_digi_ess_bala","rhs_6b27m_digi_ess","rhs_6b27m_digi"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};	
		
		// ===================== Vests ====================
		
		_plebVest = "rhs_6b23_digi_6sh92_headset";
		_gunnerVest = "rhs_6b23_digi_6sh92_headset";
		_glVest = "rhs_6b23_digi_6sh92_vog_headset";
		_medVest = "rhs_6b23_digi_6sh92_headset";
		_pilotVest = "rhs_vest_commander";
		_crewVest = "rhs_6b23_digi_6sh92_radio";
		
		// ===================== Rucks ====================
		
		_smallRuck = "rhs_assault_umbts";
		_medRuck = "B_TacticalPack_oli";
		_largeRuck = "B_Carryall_oli";
		
		// ==================== SpecOps ===================
		
		_specUniform = "rhs_uniform_emr_patchless";
		_specHelmet = "rhs_6b28_ess";
		_specGoggles = "rhs_scarf";
		_specVest = "rhs_6b23_digi_6sh92_headset";
		_specRuck = "rhs_assault_umbts";
		
		// ================================================
	};
	case "flora" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["rhs_uniform_flora_patchless"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "rhs_uniform_flora_patchless";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "6b26" : {
				_plebHelmetArray = ["rhs_6b26","rhs_6b26_bala","rhs_6b26_ess","rhs_6b26_ess_bala","rhs_6b26_ess","rhs_6b26"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "6b27" : {
				_plebHelmetArray = ["rhs_6b27m","rhs_6b27m_bala","rhs_6b27m_ess","rhs_6b27m_ess_bala","rhs_6b27m_ess","rhs_6b27m"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "6b28" : {
				_plebHelmetArray = ["rhs_6b28_flora","rhs_6b28_flora_bala","rhs_6b28_flora_ess","rhs_6b28_flora_ess_bala","rhs_6b28_flora_ess","rhs_6b28_flora"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Bandanna_khk","rhs_beanie","rhs_fieldcap","rhs_Booniehat_flora","rhs_beanie_green","rhs_Booniehat_flora"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["rhs_6b27m","rhs_6b27m_bala","rhs_6b27m_ess","rhs_6b27m_ess_bala","rhs_6b27m_ess","rhs_6b27m"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};	
		
		// ===================== Vests ====================
		
		_plebVest = "rhs_6b23_6sh92_headset";
		_gunnerVest = "rhs_6b23_6sh92_headset";
		_glVest = "rhs_6b23_6sh92_vog_headset";
		_medVest = "rhs_6b23_6sh92_headset";
		_pilotVest = "rhs_vest_commander";
		_crewVest = "rhs_6b23_6sh92_radio";
		
		// ===================== Rucks ====================
		
		_smallRuck = "rhs_assault_umbts";
		_medRuck = "B_TacticalPack_oli";
		_largeRuck = "B_Carryall_oli";
		
		// ===================== SpecOps ==================
		
		_specUniform = "rhs_uniform_flora_patchless";
		_specHelmet = "rhs_6b28_ess";
		_specGoggles = "rhs_scarf";
		_specVest = "rhs_6b23_6sh92_headset";
		_specRuck = "rhs_assault_umbts";
		
		// ================================================
	};
	case "mountainflora" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["rhs_uniform_mflora_patchless"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "rhs_uniform_mflora_patchless";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "6b26" : {
				_plebHelmetArray = ["rhs_6b27m_ml","rhs_6b27m_ml_bala","rhs_6b27m_ml_ess","rhs_6b27m_ML_ess_bala","rhs_6b27m_ml_ess","rhs_6b27m_ml"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "6b27" : {
				_plebHelmetArray = ["rhs_6b27m_ml","rhs_6b27m_ml_bala","rhs_6b27m_ml_ess","rhs_6b27m_ML_ess_bala","rhs_6b27m_ml_ess","rhs_6b27m_ml"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "6b28" : {
				_plebHelmetArray = ["rhs_6b27m_ml","rhs_6b27m_ml_bala","rhs_6b27m_ml_ess","rhs_6b27m_ML_ess_bala","rhs_6b27m_ml_ess","rhs_6b27m_ml"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Bandanna_khk","rhs_beanie","rhs_fieldcap_ml","rhs_beanie","rhs_beanie","H_Bandanna_khk"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["rhs_6b27m_ml","rhs_6b27m_ml_bala","rhs_6b27m_ml_ess","rhs_6b27m_ML_ess_bala","rhs_6b27m_ml_ess","rhs_6b27m_ml"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};	
		
		// ===================== Vests ====================
		
		_plebVest = "rhs_6b23_ML_6sh92_headset";
		_gunnerVest = "rhs_6b23_ML_6sh92_headset";
		_glVest = "rhs_6b23_ML_6sh92_vog_headset";
		_medVest = "rhs_6b23_ML_6sh92_headset";
		_pilotVest = "rhs_vest_commander";
		_crewVest = "rhs_6b23_ML_6sh92_radio";
		
		// ===================== Rucks ====================
		
		_smallRuck = "rhs_assault_umbts";
		_medRuck = "B_TacticalPack_oli";
		_largeRuck = "B_Carryall_oli";
		
		// ===================== SpecOps ==================
		
		_specUniform = "rhs_uniform_mflora_patchless";
		_specHelmet = "rhs_6b27m_ml_ess";
		_specGoggles = "rhs_scarf";
		_specVest = "rhs_6b23_ML_6sh92_headset";
		_specRuck = "rhs_assault_umbts";
		
		// ================================================
	};
	default {
		// ==================== Uniforms ==================

		_plebUniformArray = ["rhs_uniform_emr_patchless"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "rhs_uniform_emr_patchless";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "6b26" : {
				_plebHelmetArray = ["rhs_6b26_green","rhs_6b26_green_bala","rhs_6b26_green_ess","rhs_6b26_green_ess_bala","rhs_6b26_green_ess","rhs_6b26_green"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "6b27" : {
				_plebHelmetArray = ["rhs_6b27m_digi","rhs_6b27m_digi_bala","rhs_6b27m_digi_ess","rhs_6b27m_digi_ess_bala","rhs_6b27m_digi_ess","rhs_6b27m_digi"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "6b28" : {
				_plebHelmetArray = ["rhs_6b28","rhs_6b28_bala","rhs_6b28_ess","rhs_6b28_ess_bala","rhs_6b28_ess","rhs_6b28"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Bandanna_sgg","rhs_beanie_green","rhs_fieldcap_digi","rhs_Booniehat_digi","rhs_beanie_green","rhs_Booniehat_digi"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["rhs_6b27m_digi","rhs_6b27m_digi_bala","rhs_6b27m_digi_ess","rhs_6b27m_digi_ess_bala","rhs_6b27m_digi_ess","rhs_6b27m_digi"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};	
		
		// ===================== Vests ====================
		
		_plebVest = "rhs_6b23_digi_6sh92_headset";
		_gunnerVest = "rhs_6b23_digi_6sh92_headset";
		_glVest = "rhs_6b23_digi_6sh92_vog_headset";
		_medVest = "rhs_6b23_digi_6sh92_headset";
		_pilotVest = "rhs_vest_commander";
		_crewVest = "rhs_6b23_digi_6sh92_radio";
		
		// ===================== Rucks ====================
		
		_smallRuck = "rhs_assault_umbts";
		_medRuck = "B_TacticalPack_oli";
		_largeRuck = "B_Carryall_oli";
		
		// ==================== SpecOps ===================
		
		_specUniform = "rhs_uniform_emr_patchless";
		_specHelmet = "rhs_6b28_ess";
		_specGoggles = "rhs_scarf";
		_specVest = "rhs_6b23_digi_6sh92_headset";
		_specRuck = "rhs_assault_umbts";
		
		// ================================================
	};
};

// =======================================================================
// ========================= Non-Camo Specific ===========================
// =======================================================================

// ================== Vehicle Crews ==================

_rpilotUniform = "rhs_uniform_df15";
_fpilotUniform = "rhs_uniform_df15";
_rotaryPilotHelmet = "rhs_zsh7a_mike";
_rotaryCrewHelmet = "rhs_zsh7a_mike";
_fixedPilotHelmet = "rhs_zsh7a";
_crewmanHelmetArray = ["rhs_tsh4","rhs_tsh4_ess","rhs_tsh4","rhs_tsh4_bala","rhs_tsh4","rhs_tsh4_ess_bala","rhs_tsh4"];	
_crewmanHRandom = (floor(random (count _crewmanHelmetArray))); 
_crewmanHelmet = _crewmanHelmetArray select _crewmanHRandom; // enter single string value to remove randommess

// =================== Sniper Team ===================

_sniperUniform = "U_O_GhillieSuit";
_sniperVest = "V_TacVest_oli";
_sniperRuck = "rhs_assault_umbts";

// =================== Diver Gear ===================

_diverUniform = "U_O_Wetsuit";
_diverVest = "V_RebreatherB";
_diverRuck = "B_AssaultPack_blk";
_divingGoggles = "G_O_Diving";

// =================== Radio Rucks ===================

switch (_radioSelection) do {
	case "detection" : {
		private "_side";
		_side = toLower format ["%1", side player];
		// BLUFOR UAV
		if (_side == "west") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
		// OPFOR UAV
		if (_side == "east") then { 
			_uavRuck = "O_UAV_01_backpack_F";
			_uavTool = "O_UavTerminal";
		};
		// INDFOR UAV
		if (_side == "guer") then { 
			_uavRuck = "I_UAV_01_backpack_F";
			_uavTool = "I_UavTerminal";
		};
		// Civilian UAV
		if (_side == "civ") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
	};
	default {
		_uavRuck = "B_UAV_01_backpack_F";
		_uavTool = "B_UavTerminal";
	};
};

// =======================================================================