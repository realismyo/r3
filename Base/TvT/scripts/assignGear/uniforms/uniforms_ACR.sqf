// AssignGear - Uniforms
// by Mr. Agnet
// - Defines all of the wearable items to be used by the assignGear script.
// - Current Side, Faction: BLUFOR, ACR
// - Primary Mod: ACR
// - Variables: _camoPattern - "woodland", "woodlandmout", "desert", "desertmout" 
//				_headgear - "helmets", "softcover", "ech"

// =======================================================================
// Declares variables

private [
"_plebUniformArray","_plebRandom","_plebUniform",
"_plebHelmetArray","_plebHRandom","_plebHelmet",
"_plebVest","_gunnerVest","_glVest","_medVest",
"_smallRuck","_medRuck","_largeRuck",
"_specUniform","_specHelmet","_specGoggles","_specVest","_specRuck",
"_crewUniform","_rpilotUniform","_fpilotUniform",
"_crewmanHelmetArray","_crewmanHRandom","_crewmanHelmet","_rotaryPilotHelmet","_rotaryCrewHelmet","_fixedPilotHelmet",
"_crewVest","_pilotVest",
"_sniperUniform","_sniperVest","_sniperRuck",
"_diverUniform","_diverVest","_diverRuck",
"_goggles","_divingGoggles","_insignia",
"_airRadioRuck","_radioRuck","_diverRadioRuck","_uavRuck","_uavTool"
];

// =======================================================================
// =========================== Camo Specific =============================
// =======================================================================

_goggles = "";	// leave as "" for player defined
_insignia = ""; 

switch (_camoPattern) do {
	case "woodland" : {
		// ==================== Uniforms ==================

		_plebUniformArray = ["U_ACR_A3_CombatUniform","U_ACR_A3_CombatUniform_vest","U_ACR_A3_CombatUniform","U_ACR_A3_SweaterUniform_vz95","U_ACR_A3_CombatUniform"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "U_ACR_A3_CombatUniform_vest";
		_rpilotUniform = "U_ACR_A3_HeliPilotCoveralls_rgr";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["H_ACR_A3_HelmetIA_vz95","H_ACR_A3_HelmetIA_Goggles_vz95","H_ACR_A3_HelmetIA_vz95","H_ACR_A3_HelmetIA_Goggles_vz95","H_ACR_A3_HelmetIA_vz95"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_ACR_A3_Booniehat_fold_vz95","H_Watchcap_camo","H_ACR_A3_Booniehat_vz95","H_Cap_oli_hs","H_ACR_A3_Cap_rgr","H_Cap_headphones"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "ech" : {
				_plebHelmetArray = ["H_ACR_A3_HelmetB_rgr","H_ACR_A3_HelmetB_rgr","H_ACR_A3_HelmetB_rgr"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["H_ACR_A3_HelmetIA_vz95","H_ACR_A3_HelmetIA_Goggles_vz95","H_ACR_A3_HelmetIA_vz95","H_ACR_A3_HelmetIA_Goggles_vz95","H_ACR_A3_HelmetIA_vz95"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "V_ACR_A3_PlateCarrier1_vz95";
		_gunnerVest = "V_ACR_A3_PlateCarrier2_vz95";
		_glVest = "V_ACR_A3_PlateCarrier2_vz95";
		_medVest = "V_ACR_A3_PlateCarrier1_vz95";
		_pilotVest = "V_TacVest_oli";
		_crewVest = "V_TacVest_oli";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "ACR_A3_Backpack_Carryall_vz95";
		
		// ==================== SpecOps ===================
		
		_specUniform = "U_ACR_A3_CombatUniform_vest";
		_specHelmet = "H_ACR_A3_HelmetB_rgr";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "V_ACR_A3_PlateCarrier2_vz95";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
	case "woodlandmout" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["U_ACR_A3_CombatUniform_MOUT","U_ACR_A3_CombatUniform_MOUT_vest","U_ACR_A3_CombatUniform_MOUT","U_ACR_A3_CombatUniform_MOUT_vest","U_ACR_A3_CombatUniform_MOUT"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "U_ACR_A3_CombatUniform_MOUT_vest";
		_rpilotUniform = "U_ACR_A3_HeliPilotCoveralls_rgr";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["H_ACR_A3_HelmetIA_vz95","H_ACR_A3_HelmetIA_Goggles_vz95","H_ACR_A3_HelmetIA_vz95","H_ACR_A3_HelmetIA_Goggles_vz95","H_ACR_A3_HelmetIA_vz95"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_ACR_A3_Booniehat_fold_vz95","H_Watchcap_camo","H_ACR_A3_Booniehat_vz95","H_Cap_oli_hs","H_ACR_A3_Cap_rgr","H_Cap_headphones"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "ech" : {
				_plebHelmetArray = ["H_ACR_A3_HelmetB_rgr","H_HelmetSpecB_snakeskin","H_ACR_A3_HelmetB_rgr","H_HelmetSpecB_paint1","H_ACR_A3_HelmetB_rgr"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["H_ACR_A3_HelmetIA_vz95","H_ACR_A3_HelmetIA_Goggles_vz95","H_ACR_A3_HelmetIA_vz95","H_ACR_A3_HelmetIA_Goggles_vz95","H_ACR_A3_HelmetIA_vz95"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};		
		
		// ===================== Vests ====================
		
		_plebVest = "V_ACR_A3_PlateCarrier1_rgr";
		_gunnerVest = "V_ACR_A3_PlateCarrier2_vz95";
		_glVest = "V_ACR_A3_PlateCarrier2_rgr";
		_medVest = "V_ACR_A3_PlateCarrier1_rgr";
		_pilotVest = "V_TacVest_oli";
		_crewVest = "V_TacVest_oli";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_oli";
		
		// ===================== SpecOps ==================
		
		_specUniform = "U_ACR_A3_CombatUniform_MOUT_vest";
		_specHelmet = "H_ACR_A3_HelmetB_rgr";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "V_ACR_A3_PlateCarrier2_rgr";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
	case "desert" : {
		// ==================== Uniforms ==================

		_plebUniformArray = ["U_ACR_A3_CombatUniform_Des","U_ACR_A3_CombatUniform_Des_vest","U_ACR_A3_CombatUniform_Des","U_ACR_A3_CombatUniform_Des_vest","U_ACR_A3_CombatUniform_Des"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "U_ACR_A3_CombatUniform_Des_vest";
		_rpilotUniform = "U_ACR_A3_HeliPilotCoveralls_khk";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["H_ACR_A3_HelmetIA_vz95_des","H_ACR_A3_HelmetIA_Goggles_vz95_des","H_ACR_A3_HelmetIA_vz95_des","H_ACR_A3_HelmetIA_Goggles_vz95_des","H_ACR_A3_HelmetIA_vz95_des"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_ACR_A3_Cap_khk","H_Cap_tan","H_Watchcap_khk","H_ACR_A3_Booniehat_vz95_des","H_ACR_A3_Pakol_grey"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "ech" : {
				_plebHelmetArray = ["H_HelmetSpecB_paint2","H_HelmetSpecB_sand","H_HelmetSpecB_paint2"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["H_ACR_A3_HelmetIA_vz95_des","H_ACR_A3_HelmetIA_Goggles_vz95_des","H_ACR_A3_HelmetIA_vz95_des","H_ACR_A3_HelmetIA_Goggles_vz95_des","H_ACR_A3_HelmetIA_vz95_des"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "V_ACR_A3_PlateCarrier1_khk";
		_gunnerVest = "V_ACR_A3_PlateCarrier2_khk";
		_glVest = "V_ACR_A3_PlateCarrier2_khk";
		_medVest = "V_ACR_A3_PlateCarrier1_khk";
		_pilotVest = "V_TacVest_khk";
		_crewVest = "V_TacVest_khk";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_cbr";
		_largeRuck = "B_Carryall_cbr";
		
		// ==================== SpecOps ===================
		
		_specUniform = "U_ACR_A3_CombatUniform_Des_vest";
		_specHelmet = "H_HelmetSpecB_paint2";
		_specGoggles = "G_Bandanna_tan";
		_specVest = "V_ACR_A3_PlateCarrier2_khk";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
	case "desertmout" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["U_ACR_A3_CombatUniform_MOUT_Des","U_ACR_A3_CombatUniform_MOUT_Des_vest","U_ACR_A3_CombatUniform_MOUT_Des","U_ACR_A3_CombatUniform_MOUT_Des_vest","U_ACR_A3_CombatUniform_MOUT_Des"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "U_ACR_A3_CombatUniform_MOUT_Des_vest";
		_rpilotUniform = "U_ACR_A3_HeliPilotCoveralls_khk";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["H_ACR_A3_HelmetIA_vz95_des","H_ACR_A3_HelmetIA_Goggles_vz95_des","H_ACR_A3_HelmetIA_vz95_des","H_ACR_A3_HelmetIA_Goggles_vz95_des","H_ACR_A3_HelmetIA_vz95_des"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_ACR_A3_Cap_khk","H_Cap_tan","H_Watchcap_khk","H_ACR_A3_Booniehat_vz95_des","H_ACR_A3_Pakol_grey"];
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "ech" : {
				_plebHelmetArray = ["H_HelmetSpecB_paint2","H_HelmetSpecB_sand","H_HelmetSpecB_paint2"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["H_ACR_A3_HelmetIA_vz95_des","H_ACR_A3_HelmetIA_Goggles_vz95_des","H_ACR_A3_HelmetIA_vz95_des","H_ACR_A3_HelmetIA_Goggles_vz95_des","H_ACR_A3_HelmetIA_vz95_des"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};	
		
		// ===================== Vests ====================
		
		_plebVest = "V_ACR_A3_PlateCarrier1_khk";
		_gunnerVest = "V_ACR_A3_PlateCarrier2_khk";
		_glVest = "V_ACR_A3_PlateCarrier2_khk";
		_medVest = "V_ACR_A3_PlateCarrier1_khk";
		_pilotVest = "V_TacVest_khk";
		_crewVest = "V_TacVest_khk";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_cbr";
		_largeRuck = "B_Carryall_cbr";
		
		// ===================== SpecOps ==================
		
		_specUniform = "U_ACR_A3_CombatUniform_MOUT_Des_vest";
		_specHelmet = "H_HelmetSpecB_paint2";
		_specGoggles = "G_Bandanna_tan";
		_specVest = "V_ACR_A3_PlateCarrier2_khk";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
	default {
		// ==================== Uniforms ==================

		_plebUniformArray = ["U_ACR_A3_CombatUniform","U_ACR_A3_CombatUniform_vest","U_ACR_A3_CombatUniform","U_ACR_A3_SweaterUniform_vz95","U_ACR_A3_CombatUniform"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "U_ACR_A3_CombatUniform_vest";
		_rpilotUniform = "U_ACR_A3_HeliPilotCoveralls_rgr";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["H_ACR_A3_HelmetIA_vz95","H_ACR_A3_HelmetIA_Goggles_vz95","H_ACR_A3_HelmetIA_vz95","H_ACR_A3_HelmetIA_Goggles_vz95","H_ACR_A3_HelmetIA_vz95"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_ACR_A3_Booniehat_fold_vz95","H_Watchcap_camo","H_ACR_A3_Booniehat_vz95","H_Cap_oli_hs","H_ACR_A3_Cap_rgr","H_Cap_headphones"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "ech" : {
				_plebHelmetArray = ["H_ACR_A3_HelmetB_rgr","H_ACR_A3_HelmetB_rgr","H_ACR_A3_HelmetB_rgr"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["H_ACR_A3_HelmetIA_vz95","H_ACR_A3_HelmetIA_Goggles_vz95","H_ACR_A3_HelmetIA_vz95","H_ACR_A3_HelmetIA_Goggles_vz95","H_ACR_A3_HelmetIA_vz95"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "V_ACR_A3_PlateCarrier1_vz95";
		_gunnerVest = "V_ACR_A3_PlateCarrier2_vz95";
		_glVest = "V_ACR_A3_PlateCarrier2_vz95";
		_medVest = "V_ACR_A3_PlateCarrier1_vz95";
		_pilotVest = "V_TacVest_oli";
		_crewVest = "V_TacVest_oli";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "ACR_A3_Backpack_Carryall_vz95";
		
		// ==================== SpecOps ===================
		
		_specUniform = "U_ACR_A3_CombatUniform_vest";
		_specHelmet = "H_ACR_A3_HelmetB_rgr";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "V_ACR_A3_PlateCarrier2_vz95";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
};

// =======================================================================
// ========================= Non-Camo Specific ===========================
// =======================================================================

// ================== Vehicle Crews ==================
_fpilotUniform = "U_B_PilotCoveralls";
_rotaryPilotHelmet = "H_PilotHelmetHeli_B";
_rotaryCrewHelmet = "H_CrewHelmetHeli_B";
_fixedPilotHelmet = "H_PilotHelmetFighter_B";
_crewmanHelmet = "H_HelmetCrew_B";

// =================== Sniper Team ===================

_sniperUniform = "U_B_GhillieSuit";
_sniperVest = "V_TacVest_oli";
_sniperRuck = "B_AssaultPack_rgr";

// =================== Diver Gear ===================

_diverUniform = "U_B_Wetsuit";
_diverVest = "V_RebreatherB";
_diverRuck = "B_AssaultPack_blk";
_divingGoggles = "G_B_Diving";

// =================== Radio Rucks ===================

switch (_radioSelection) do {
	case "detection" : {
		private "_side";
		_side = toLower format ["%1", side player];
		// BLUFOR UAV
		if (_side == "west") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
		// OPFOR UAV
		if (_side == "east") then { 
			_uavRuck = "O_UAV_01_backpack_F";
			_uavTool = "O_UavTerminal";
		};
		// INDFOR UAV
		if (_side == "guer") then { 
			_uavRuck = "I_UAV_01_backpack_F";
			_uavTool = "I_UavTerminal";
		};
		// Civilian UAV
		if (_side == "civ") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
	};
	default {
		_uavRuck = "B_UAV_01_backpack_F";
		_uavTool = "B_UavTerminal";
	};
};

// =======================================================================