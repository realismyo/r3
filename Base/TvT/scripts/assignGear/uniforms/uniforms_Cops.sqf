// AssignGear - Uniforms
// by Mr. Agnet
// - Defines all of the wearable items to be used by the assignGear script.
// - Current Side, Faction: BLUFOR, NATO
// - Primary Mod: PG SERVICES/VANILLA
// - Variables: _camoPattern - "blue", "black"
//				_headgear - "helmets", "softcover"

// =======================================================================
// Declares variables

private [
"_plebUniformArray","_plebRandom","_plebUniform",
"_plebHelmetArray","_plebHRandom","_plebHelmet",
"_plebVest","_gunnerVest","_glVest","_medVest",
"_smallRuck","_medRuck","_largeRuck",
"_specUniform","_specHelmet","_specGoggles","_specVest","_specRuck",
"_crewUniform","_rpilotUniform","_fpilotUniform",
"_crewmanHelmetArray","_crewmanHRandom","_crewmanHelmet","_crewmanHelmetArray","_crewmanHRandom","_rotaryPilotHelmet","_rotaryCrewHelmet","_fixedPilotHelmet",
"_crewVest","_pilotVest",
"_sniperUniform","_sniperVest","_sniperRuck",
"_diverUniform","_diverVest","_diverRuck",
"_goggles","_divingGoggles","_insignia",
"_airRadioRuck","_radioRuck","_diverRadioRuck","_uavRuck","_uavTool"
];

// =======================================================================
// =========================== Camo Specific =============================
// =======================================================================

_goggles = "";	// leave as "" for player defined
_insignia = ""; 

switch (_camoPattern) do {
	case "blue" : {
		// ==================== Uniforms ==================

		_plebUniformArray = ["tacs_Uniform_Polo_TP_BS_LP_BB","tacs_Uniform_Polo_TP_BS_LP_BB","tacs_Uniform_Polo_TP_BS_LP_BB"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "tacs_Uniform_Polo_TP_BS_LP_BB";
		_rpilotUniform = "tacs_Uniform_Polo_TP_BS_LP_BB";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "softcover" : {
				_plebHelmetArray = ["H_Cap_police","H_Cap_blk","H_Cap_police","H_Cap_blk","H_Cap_police","H_Cap_blk","H_Cap_police"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "helmets" : {
				_plebHelmetArray = ["H_HelmetB_black","H_HelmetB_black","H_HelmetB_black","H_HelmetB_black"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};	
			default {
				_plebHelmetArray = ["H_Cap_police","H_Cap_blk","H_Cap_police","H_Cap_blk","H_Cap_police","H_Cap_blk","H_Cap_police"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		_crewmanHelmetArray = ["rhsusf_Bowman","rhsusf_Bowman","rhsusf_Bowman"];	
		_crewmanHRandom = (floor(random (count _crewmanHelmetArray))); 
		_crewmanHelmet = _crewmanHelmetArray select _crewmanHRandom; // enter single string value to remove randommess
		
		// ===================== Vests ====================
		
		_plebVest = "V_TacVest_blk_POLICE";
		_gunnerVest = "V_TacVest_blk_POLICE";
		_glVest = "V_TacVest_blk_POLICE";
		_medVest = "V_TacVest_blk_POLICE";
		_pilotVest = "V_TacVest_blk_POLICE";
		_crewVest = "V_TacVest_blk_POLICE";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_blk";
		_medRuck = "tacs_Backpack_Kitbag_DarkBlack";
		_largeRuck = "tacs_Backpack_Carryall_DarkBlack";
		
		// ==================== SpecOps ===================
		
		_specUniform = "tacs_Uniform_Garment_RS_BS_BP_BB";
		_specHelmet = "H_Cap_blk";
		_specGoggles = "G_Balaclava_blk";
		_specVest = "tacs_Vest_PlateCarrierFull_Black";
		_specRuck = "B_AssaultPack_blk";
		
		// ================================================
	};
	case "black" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["tacs_Uniform_Garment_LS_BS_BP_BB","tacs_Uniform_Garment_RS_BS_BP_BB","tacs_Uniform_Garment_LS_BS_BP_BB"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "tacs_Uniform_Garment_RS_BS_BP_BB";
		_rpilotUniform = "tacs_Uniform_Garment_RS_BS_BP_BB";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "softcover" : {
				_plebHelmetArray = ["H_Cap_blk","H_Cap_blk","H_Cap_blk","H_Cap_blk","H_Cap_blk","H_Cap_blk","H_Cap_blk"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "helmets" : {
				_plebHelmetArray = ["H_HelmetB_black","H_HelmetB_black","H_HelmetB_black","H_HelmetB_black"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};	
			default {
				_plebHelmetArray = ["H_Cap_blk","H_Cap_blk","H_Cap_blk","H_Cap_blk","H_Cap_blk","H_Cap_blk","H_Cap_blk"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		_crewmanHelmetArray = ["rhsusf_Bowman","rhsusf_Bowman","rhsusf_Bowman"];	
		_crewmanHRandom = (floor(random (count _crewmanHelmetArray))); 
		_crewmanHelmet = _crewmanHelmetArray select _crewmanHRandom; // enter single string value to remove randommess
		
		// ===================== Vests ====================
		
		_plebVest = "V_TacVest_blk";
		_gunnerVest = "V_TacVest_blk";
		_glVest = "V_TacVest_blk";
		_medVest = "V_TacVest_blk";
		_pilotVest = "V_TacVest_blk_POLICE";
		_crewVest = "V_TacVest_blk_POLICE";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_blk";
		_medRuck = "tacs_Backpack_Kitbag_DarkBlack";
		_largeRuck = "tacs_Backpack_Carryall_DarkBlack";
		
		// ===================== SpecOps ==================
		
		_specUniform = "tacs_Uniform_Garment_RS_BS_BP_BB";
		_specHelmet = "H_Cap_blk";
		_specGoggles = "G_Balaclava_blk";
		_specVest = "tacs_Vest_PlateCarrierFull_Black";
		_specRuck = "B_AssaultPack_blk";
		
		// ================================================
	};
	default {
		// ==================== Uniforms ==================

		_plebUniformArray = ["tacs_Uniform_Polo_TP_BS_LP_BB","tacs_Uniform_Polo_TP_BS_LP_BB","tacs_Uniform_Polo_TP_BS_LP_BB"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "tacs_Uniform_Polo_TP_BS_LP_BB";
		_rpilotUniform = "tacs_Uniform_Polo_TP_BS_LP_BB";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "softcover" : {
				_plebHelmetArray = ["H_Cap_police","H_Cap_blk","H_Cap_police","H_Cap_blk","H_Cap_police","H_Cap_blk","H_Cap_police"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "helmets" : {
				_plebHelmetArray = ["H_HelmetB_black","H_HelmetB_black","H_HelmetB_black","H_HelmetB_black"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};	
			default {
				_plebHelmetArray = ["H_Cap_police","H_Cap_blk","H_Cap_police","H_Cap_blk","H_Cap_police","H_Cap_blk","H_Cap_police"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		_crewmanHelmetArray = ["rhsusf_Bowman","rhsusf_Bowman","rhsusf_Bowman"];	
		_crewmanHRandom = (floor(random (count _crewmanHelmetArray))); 
		_crewmanHelmet = _crewmanHelmetArray select _crewmanHRandom; // enter single string value to remove randommess
		
		// ===================== Vests ====================
		
		_plebVest = "V_TacVest_blk_POLICE";
		_gunnerVest = "V_TacVest_blk_POLICE";
		_glVest = "V_TacVest_blk_POLICE";
		_medVest = "V_TacVest_blk_POLICE";
		_pilotVest = "V_TacVest_blk_POLICE";
		_crewVest = "V_TacVest_blk_POLICE";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_blk";
		_medRuck = "tacs_Backpack_Kitbag_DarkBlack";
		_largeRuck = "tacs_Backpack_Carryall_DarkBlack";
		
		// ==================== SpecOps ===================
		
		_specUniform = "tacs_Uniform_Garment_RS_BS_BP_BB";
		_specHelmet = "H_Cap_blk";
		_specGoggles = "G_Balaclava_blk";
		_specVest = "tacs_Vest_PlateCarrierFull_Black";
		_specRuck = "B_AssaultPack_blk";
		
		// ================================================
	};
};

// =======================================================================
// ========================= Non-Camo Specific ===========================
// =======================================================================

// ================== Vehicle Crews ==================

_fpilotUniform = "U_B_PilotCoveralls";
_rotaryPilotHelmet = "rhsusf_Bowman";
_rotaryCrewHelmet = "rhsusf_Bowman";
_fixedPilotHelmet = "H_PilotHelmetFighter_B";

// =================== Sniper Team ===================

_sniperUniform = "tacs_Uniform_Garment_RS_BS_BP_BB";
_sniperVest = "V_TacVest_blk_POLICE";
_sniperRuck = "B_AssaultPack_blk";

// =================== Diver Gear ===================

_diverUniform = "U_B_Wetsuit";
_diverVest = "V_RebreatherB";
_diverRuck = "B_AssaultPack_blk";
_divingGoggles = "G_B_Diving";

// =================== Radio Rucks ===================

switch (_radioSelection) do {
	case "detection" : {
		private "_side";
		_side = toLower format ["%1", side player];
		// BLUFOR UAV
		if (_side == "west") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
		// OPFOR UAV
		if (_side == "east") then { 
			_uavRuck = "O_UAV_01_backpack_F";
			_uavTool = "O_UavTerminal";
		};
		// INDFOR UAV
		if (_side == "guer") then { 
			_uavRuck = "I_UAV_01_backpack_F";
			_uavTool = "I_UavTerminal";
		};
		// Civilian UAV
		if (_side == "civ") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
	};
	default {
		_uavRuck = "B_UAV_01_backpack_F";
		_uavTool = "B_UavTerminal";
	};
};

// =======================================================================