// AssignGear - Uniforms
// by Mr. Agnet
// - Defines all of the wearable items to be used by the assignGear script.
// - Current Side, Faction: BLUFOR, US ARMY
// - Primary Mod: CUP
// - Variables: _headgear - "echblack", "echgreen", "echsand", "echmix", "opshelm"

// ============================================================
// Declares variables

private [
"_plebUniformArray","_plebRandom","_plebUniform",
"_plebHelmetArray","_plebHRandom","_plebHelmet",
"_plebVest","_plebVRandom","_plebVestArray","_gunnerVest","_glVestArray","_glVRandom","_glVest","_medVest",
"_smallRuck","_medRuck","_largeRuck",
"_specUniform","_specHelmet","_specGoggles","_specVest","_specRuck",
"_crewUniform","_rpilotUniform","_fpilotUniform",
"_crewmanHelmetArray","_crewmanHRandom","_crewmanHelmet","_rotaryPilotHelmet","_rotaryCrewHelmet","_fixedPilotHelmet",
"_crewVest","_pilotVest",
"_sniperUniform","_sniperVest","_sniperRuck",
"_diverUniform","_diverVest","_diverRuck",
"_goggles","_insignia","_divingGoggles",
"_airRadioRuck","_radioRuck","_diverRadioRuck","_uavRuck","_uavTool"
];

// ===================== Uniforms ======================

_goggles = "";	// leave as "" for player defined
_insignia = ""; 
_plebUniformArray = ["CUP_U_B_USArmy_TwoKnee","CUP_U_B_USArmy_Base","CUP_U_B_USArmy_TwoKnee"];	
_plebRandom = (floor(random (count _plebUniformArray)));
_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess

// ===================== Headgear ======================

switch (_headgear) do {
	case "echblack" : {
		_plebHelmetArray = ["CUP_H_USArmy_Helmet_ECH1_Black","CUP_H_USArmy_Helmet_ECH2_Black","CUP_H_USArmy_Helmet_ECH1_Black"];	
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};
	case "echgreen" : {
		_plebHelmetArray = ["CUP_H_USArmy_Helmet_ECH1_Green","CUP_H_USArmy_Helmet_ECH2_GREEN","CUP_H_USArmy_Helmet_ECH1_Green"];	
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};	
	case "echsand" : {
		_plebHelmetArray = ["CUP_H_USArmy_Helmet_ECH1_Sand","CUP_H_USArmy_Helmet_ECH2_Sand","CUP_H_USArmy_Helmet_ECH1_Sand"];	
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};
	case "echmix" : {
		_plebHelmetArray = ["CUP_H_USArmy_Helmet_ECH2_Sand","CUP_H_USArmy_Helmet_ECH1_Sand","CUP_H_USArmy_Helmet_ECH2_GREEN","CUP_H_USArmy_Helmet_ECH1_Green","CUP_H_USArmy_Helmet_ECH2_Black","CUP_H_USArmy_Helmet_ECH1_Black"];	
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};
	case "opshelm" : {
		_plebHelmetArray = ["CUP_H_USArmy_Helmet_Pro","CUP_H_USArmy_Helmet_Pro_gog","CUP_H_USArmy_Helmet_Pro"];	
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};		
	default {
		_plebHelmetArray = ["CUP_H_USArmy_Helmet_ECH1_Black","CUP_H_USArmy_Helmet_ECH2_Black","CUP_H_USArmy_Helmet_ECH1_Black"];	
		_plebHRandom = (floor(random (count _plebHelmetArray)));
		_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
	};
};

// ======================= Vests =======================

_plebVest = "CUP_V_B_IOTV_Rifleman";
_glVest = "CUP_V_B_IOTV_gl";	
_gunnerVest = "CUP_V_B_IOTV_MG";
_medVest = "CUP_V_B_IOTV_Medic";

// ======================= Rucks =======================

_smallRuck = "CUP_B_AssaultPack_ACU";
_medRuck = "CUP_B_USPack_Coyote";
_largeRuck = "B_Carryall_cbr";

// ===================== Spec Ops ======================

_specUniform = "CUP_U_B_USArmy_Soft";
_specHelmet = "CUP_H_USArmy_Helmet_Pro_gog";
_specGoggles = "G_Bandanna_khk";
_specVest = "CUP_V_B_IOTV_MG";
_specRuck = "CUP_B_AssaultPack_ACU";

// =================== Vehicle Crews ===================

_crewUniform = "CUP_U_B_USArmy_Soft";
_rpilotUniform = "CUP_U_B_USArmy_PilotOverall";
_fpilotUniform = "CUP_U_B_USArmy_PilotOverall";

_rotaryPilotHelmet = "H_PilotHelmetHeli_B";
_rotaryCrewHelmet = "H_CrewHelmetHeli_B";
_fixedPilotHelmet = "H_PilotHelmetFighter_B";
_crewmanHelmet = "CUP_H_USMC_Crew_Helmet";

_pilotVest = "CUP_V_B_USArmy_PilotVest";
_crewVest = "CUP_V_B_IOTV_SL";

// ==================== Sniper Team ====================

_sniperUniform = "U_B_GhillieSuit";
_sniperVest = "V_TacVest_khk";
_sniperRuck = "CUP_B_AssaultPack_ACU";

// ==================== Diver Gear ====================

_diverUniform = "U_B_Wetsuit";
_diverVest = "V_RebreatherB";
_diverRuck = "B_AssaultPack_blk";
_divingGoggles = "G_I_Diving";

// ==================== Radio Rucks ====================

switch (_radioSelection) do {
	case "detection" : {
		private "_side";
		_side = toLower format ["%1", side player];
		// BLUFOR UAV
		if (_side == "west") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
		// OPFOR UAV
		if (_side == "east") then { 
			_uavRuck = "O_UAV_01_backpack_F";
			_uavTool = "O_UavTerminal";
		};
		// INDFOR UAV
		if (_side == "guer") then { 
			_uavRuck = "I_UAV_01_backpack_F";
			_uavTool = "I_UavTerminal";
		};
		// Civilian UAV
		if (_side == "civ") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
	};
	default {
		_uavRuck = "B_UAV_01_backpack_F";
		_uavTool = "B_UavTerminal";
	};
};

// =====================================================
