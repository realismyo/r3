// AssignGear - Uniforms
// by Mr. Agnet
// - Defines all of the wearable items to be used by the assignGear script.
// - Current Side, Faction: BLUFOR, USMC
// - Primary Mod: KMNP
// - Variables: _camoPattern - "marpat", "marpatdes", "marpatsnow", "6colour"
//				_headgear - "helmets", "softcover"

// =======================================================================
// Declares variables

private [
"_plebUniformArray","_plebRandom","_plebUniform",
"_plebHelmetArray","_plebHRandom","_plebHelmet",
"_plebVest","_gunnerVest","_glVest","_medVest",
"_smallRuck","_medRuck","_largeRuck",
"_specUniform","_specHelmet","_specGoggles","_specVest","_specRuck",
"_crewUniform","_rpilotUniform","_fpilotUniform",
"_crewmanHelmetArray","_crewmanHRandom","_crewmanHelmet","_rotaryPilotHelmet","_rotaryCrewHelmet","_fixedPilotHelmet",
"_crewVest","_pilotVest",
"_sniperUniform","_sniperVest","_sniperRuck",
"_diverUniform","_diverVest","_diverRuck",
"_goggles","_divingGoggles","_insignia",
"_airRadioRuck","_radioRuck","_diverRadioRuck","_uavRuck","_uavTool"
];

// =======================================================================
// =========================== Camo Specific =============================
// =======================================================================

_goggles = "";	// leave as "" for player defined
_insignia = ""; 

switch (_camoPattern) do {
	case "marpat" : {
		// ==================== Uniforms ==================

		_plebUniformArray = ["MNP_CombatUniform_USMC_T","MNP_CombatUniform_USMC_ST","MNP_CombatUniform_USMC_T"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_USMC_ST";
		_rpilotUniform = "MNP_CombatUniform_USMC_ST";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_USMC","MNP_Helmet_USMC","MNP_Helmet_USMC"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Booniehat_oli"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_USMC","MNP_Helmet_USMC","MNP_Helmet_USMC"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_USMC";
		_gunnerVest = "MNP_Vest_USMC_2";
		_glVest = "MNP_Vest_USMC_2";
		_medVest = "MNP_Vest_USMC";
		_crewVest = "V_TacVest_oli";
		_pilotVest = "V_TacVest_oli";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_oli";
		
		// ==================== SpecOps ===================
		
		_specUniform = "MNP_CombatUniform_USMC_ST";
		_specHelmet = "MNP_Helmet_USMC";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "MNP_Vest_USMC_2";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
	case "marpatdes" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["MNP_CombatUniform_USMC_D","MNP_CombatUniform_USMC_SD","MNP_CombatUniform_USMC_D"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_USMC_SD";
		_rpilotUniform = "MNP_CombatUniform_USMC_SD";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_USMC_D","MNP_Helmet_USMC_D","MNP_Helmet_USMC_D"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Booniehat_khk"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_USMC_D","MNP_Helmet_USMC_D","MNP_Helmet_USMC_D"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_USMC";
		_gunnerVest = "MNP_Vest_USMC_2";
		_glVest = "MNP_Vest_USMC_2";
		_medVest = "MNP_Vest_USMC";
		_crewVest = "V_TacVest_blk";
		_pilotVest = "V_TacVest_blk";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_cbr";
		_largeRuck = "B_Carryall_khk";
		
		// ===================== SpecOps ==================
		
		_specUniform = "MNP_CombatUniform_USMC_SD";
		_specHelmet = "MNP_Helmet_USMC_D";
		_specGoggles = "G_Bandanna_tan";
		_specVest = "MNP_Vest_USMC_2";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
	case "marpatsnow" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["MNP_CombatUniform_USMC_arctic","MNP_CombatUniform_USMC_arctic_B","MNP_CombatUniform_USMC_arctic"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_USMC_arctic";
		_rpilotUniform = "MNP_CombatUniform_USMC_arctic";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_USMC_arctic","MNP_Helmet_USMC_arctic","MNP_Helmet_USMC_arctic"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["MNP_Boonie_WW2"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_USMC_arctic","MNP_Helmet_USMC_arctic","MNP_Helmet_USMC_arctic"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_ACU_1";
		_gunnerVest = "MNP_Vest_ACU_2";
		_glVest = "MNP_Vest_ACU_2";
		_medVest = "MNP_Vest_ACU_1";
		_crewVest = "V_TacVest_blk";
		_pilotVest = "V_TacVest_blk";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_oli";
		
		// ===================== SpecOps ==================
		
		_specUniform = "MNP_CombatUniform_USMC_arctic";
		_specHelmet = "MNP_Helmet_USMC_arctic";
		_specGoggles = "G_Bandanna_blk";
		_specVest = "MNP_Vest_ACU_2";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
	case "6colour" : {
		// ==================== Uniforms ==================
		
		_plebUniformArray = ["MNP_CombatUniform_US6co_A","MNP_CombatUniform_US6co_B","MNP_CombatUniform_US6co_A"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_US6co_B";
		_rpilotUniform = "MNP_CombatUniform_US6co_B";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_6Co","MNP_Helmet_6Co","MNP_Helmet_6Co"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Booniehat_khk"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_6Co","MNP_Helmet_6Co","MNP_Helmet_6Co"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_6co_B";
		_gunnerVest = "MNP_Vest_6co_A";
		_glVest = "MNP_Vest_6co_A";
		_medVest = "MNP_Vest_6co_B";
		_crewVest = "V_TacVest_blk";
		_pilotVest = "V_TacVest_blk";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_cbr";
		_largeRuck = "B_Carryall_cbr";
		
		// ===================== SpecOps ==================
		
		_specUniform = "MNP_CombatUniform_US6co_B";
		_specHelmet = "MNP_Helmet_USMC_D";
		_specGoggles = "G_Bandanna_tan";
		_specVest = "MNP_Vest_6co_A";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
	default {
		// ==================== Uniforms ==================

		_plebUniformArray = ["MNP_CombatUniform_USMC_T","MNP_CombatUniform_USMC_ST","MNP_CombatUniform_USMC_T"];	
		_plebRandom = (floor(random (count _plebUniformArray)));
		_plebUniform = _plebUniformArray select _plebRandom;	// leave as "" for default, or enter single string value to remove randommess
		_crewUniform = "MNP_CombatUniform_USMC_ST";
		_rpilotUniform = "MNP_CombatUniform_USMC_ST";
		
		// ==================== Headgear ==================
		
		switch (_headgear) do {
			case "helmets" : {
				_plebHelmetArray = ["MNP_Helmet_USMC","MNP_Helmet_USMC","MNP_Helmet_USMC"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			case "softcover" : {
				_plebHelmetArray = ["H_Booniehat_oli"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
			default {
				_plebHelmetArray = ["MNP_Helmet_USMC","MNP_Helmet_USMC","MNP_Helmet_USMC"];	
				_plebHRandom = (floor(random (count _plebHelmetArray)));
				_plebHelmet = _plebHelmetArray select _plebHRandom;	// enter single string value to remove randommess
			};
		};
		
		// ===================== Vests ====================
		
		_plebVest = "MNP_Vest_USMC";
		_gunnerVest = "MNP_Vest_USMC_2";
		_glVest = "MNP_Vest_USMC_2";
		_medVest = "MNP_Vest_USMC";
		_crewVest = "V_TacVest_oli";
		_pilotVest = "V_TacVest_oli";
		
		// ===================== Rucks ====================
		
		_smallRuck = "B_AssaultPack_rgr";
		_medRuck = "B_Kitbag_rgr";
		_largeRuck = "B_Carryall_oli";
		
		// ==================== SpecOps ===================
		
		_specUniform = "MNP_CombatUniform_USMC_ST";
		_specHelmet = "MNP_Helmet_USMC";
		_specGoggles = "G_Bandanna_oli";
		_specVest = "MNP_Vest_USMC_2";
		_specRuck = "B_AssaultPack_rgr";
		
		// ================================================
	};
};

// =======================================================================
// ========================= Non-Camo Specific ===========================
// =======================================================================

// ================== Vehicle Crews ==================

_fpilotUniform = "U_B_PilotCoveralls";
_crewmanHelmet = "H_HelmetCrew_B";
_rotaryPilotHelmet = "H_PilotHelmetHeli_B";
_rotaryCrewHelmet = "H_CrewHelmetHeli_B";
_fixedPilotHelmet = "H_PilotHelmetFighter_B";

// =================== Sniper Team ===================

_sniperUniform = "U_B_GhillieSuit";
_sniperVest = "V_TacVest_oli";
_sniperRuck = "B_AssaultPack_rgr";

// =================== Diver Gear ===================

_diverUniform = "U_B_Wetsuit";
_diverVest = "V_RebreatherB";
_diverRuck = "B_AssaultPack_blk";
_divingGoggles = "G_B_Diving";

// =================== Radio Rucks ===================

switch (_radioSelection) do {
	case "detection" : {
		private "_side";
		_side = toLower format ["%1", side player];
		// BLUFOR UAV
		if (_side == "west") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
		// OPFOR UAV
		if (_side == "east") then { 
			_uavRuck = "O_UAV_01_backpack_F";
			_uavTool = "O_UavTerminal";
		};
		// INDFOR UAV
		if (_side == "guer") then { 
			_uavRuck = "I_UAV_01_backpack_F";
			_uavTool = "I_UavTerminal";
		};
		// Civilian UAV
		if (_side == "civ") then { 
			_uavRuck = "B_UAV_01_backpack_F";
			_uavTool = "B_UavTerminal";
		};
	};
	default {
		_uavRuck = "B_UAV_01_backpack_F";
		_uavTool = "B_UavTerminal";
	};
};

// =======================================================================