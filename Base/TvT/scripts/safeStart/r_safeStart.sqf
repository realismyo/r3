/*
SafeStart Script
by Mr. Agnet
- Allows script to be enabled/disabled by adminerinos. 
- Disables player damage and projectiles during a 'safe' starting/briefing period. 
*/
//=====================================================================================
// MAKE SURE THE PLAYER INITIALIZES PROPERLY

if (!isDedicated && (isNull player)) then { waitUntil {sleep 0.1; !isNull player}; };
if (!local player) exitWith {};

// ====================================================================================
// Make sure the component only starts after the mission has initialized

sleep 0.1;
if (isNil "r_var_safeStartRunning") then { r_var_safeStartRunning = false; };
if (isNil "r_var_safeStartExit") then { r_var_safeStartExit = false; };

// ====================================================================================
// Define SafeStart Function

r_fnc_safeStart = {
	if ((!isNil "r_var_safeStartRunning") && r_var_safeStartRunning) exitWith {};
	if ((!isNil "r_var_safeStartExit") && r_var_safeStartExit) exitWith {};
	if (!r_var_safeStartRunning) then { 
		player allowDamage false; 																	// disable damage on player
		r_safetyHandler = player addEventHandler ["Fired", {deletevehicle (_this select 6);}];		// Delete bullets from fired weapons
		r_var_safeStartRunning = true;
	};

	while {r_var_safeStartRunning} do {
		_z = 30;
		scopeName "main";
		systemChat "Weapons are cold. Waiting for an admin to begin the mission.";
		for "_i" from 1 to _z do {
			sleep 1;
			if (r_var_safeStartExit) then { r_var_safeStartRunning = false; _z = 0; breakTo "main"; };
		};
	};
	
	player removeEventHandler ["Fired", r_safetyHandler];	// Allow player to fire weapons
	player allowDamage true; 								// Make playable units vulnerable, clientside	
	
	systemChat "Weapons are hot, the mission has begun!";
	hint parseText format ["<t color='#FFFFFF' size='2.0'align='center'>Weapons are hot, the mission has begun!</t>"];
	
	r_var_safeStartRunning = false;		// reset
	r_var_safeStartExit = false;		// reset
}; 

// ====================================================================================
// Spawn the SafeStart Function

[] spawn r_fnc_safeStart;

// ====================================================================================
// Non-Admin Folk Workaround

player addAction ["<t color='#ecd524'>SafeStart Off</t>", {r_var_safeStartExit = true; publicVariable "r_var_safeStartExit";}, true, 7, false, true, "", "(!(isClass (configFile >> 'CfgPatches' >> 'rifm_adminMenu'))) && ((serverCommandAvailable '#logout') || (!isMultiplayer)) && r_var_safeStartRunning"] 