# RIFM Mission Framework #

The Rifling Matters mission making framework is the standard template for all of RIFM's missions. The resources contained within have been tried and tested to ensure missions run as seamlessly as possible, and to allow the mission maker to create their mission with greater ease. In order to create and have your mission played by RIFM, you are required to use the framework for your missions.

Read our wiki page for more information: http://riflingmatters.com/wiki/Mission_Making